import glob
import Common.functions as app
import Common.Gcode as Gcode
import sys, os, time
store_folder = "/CNCandUNF"


def get_images_in_folder(this_path, pattern):
    print("this_path:", this_path)
    imageList = sorted(glob.glob1(this_path, pattern))
    print("imageList:", imageList)
    return imageList

# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')


# Restore
def enablePrint():
    sys.stdout = sys.__stdout__


def imageToUnf(imgName, it, this_path):
    product = "DWS/flyer"
    app.setCurrentProduct(product)
    filename = this_path + '/' + imgName
    cnc_filename = this_path + store_folder + '/' + imgName + str(it) + '.cnc'
    pinMode = 1
    print("\t\tfilename:", filename)
    print("\t\tcnc_filename:", cnc_filename)
    Gcode.imageToCncFile(filename, cnc_filename, pinMode)
    #Gcode.createUnfFile(this_path + store_folder, "default")


def test_cnc_performance(nIt):
    this_path = os.path.dirname(os.path.abspath(__file__))
    imageList = get_images_in_folder(this_path, '*.png')
    times = []
    i = 0
    while i < nIt:
        enablePrint()
        print("-----------------------------------------", i+1, "/", nIt, "-----------------------------------------")
        blockPrint()
        for img in imageList:
            start_time = time.time()
            imageToUnf(img, i, this_path)
            end_time = time.time()
            times.append(end_time - start_time)
        i = i+1

    enablePrint()
    print("\n\n FINAL TIMES:", times)
    print("FINAL AVERAGE TIME:", sum(times)/len(times))
    return times

