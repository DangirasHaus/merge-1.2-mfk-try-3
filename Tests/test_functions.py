import unittest
from Common import functions as f
import os,time
from datetime import date, datetime
from PIL import Image

import Common.LogsDWS as LOG


class TestFunctions(unittest.TestCase):
    dir_path = os.path.dirname(os.path.realpath(__file__))

######TESTING getDataFromFile ##############################################
    def test_getDataFromFile_withEqualINinfoFile(self):
        f.resetProductData()
        fichierInfoAvec = self.dir_path+"\\TestData\\fichierInfoAvec=.txt"
        self.assertEqual(f.getDataFromFile(fichierInfoAvec,"nom du premier produit"),"Power bank")
        self.assertEqual(f.getDataFromFile(fichierInfoAvec, "nom du deuxieme produit"), "Air spray")
        self.assertEqual(f.getDataFromFile(fichierInfoAvec, "Volume1"), "500mL")
        self.assertEqual(f.getDataFromFile(fichierInfoAvec, "largeur1"), "50")


    def test_getDataFromFile_withoutEqualINinfoFile(self):
        fichierInfoSans=self.dir_path+"\\TestData\\fichierInfoSans=.txt"
        f.resetProductData()
        self.assertEqual(f.getDataFromFile(fichierInfoSans, ""), "")



######TESTING storeEngravingFiles ##########################################
    def test_storeEngravingFilesOK(self):
        source=self.dir_path+"\\TestData\\LoadInfo"
        destination=self.dir_path+"\\TestData\\Historique"
        self.assertEqual(f.storeEngravingFiles(destination,source,"TEST2"),{"result": "OK", "msg": ""})

    def test_storeEngravingFilesNot_NOT_OK(self):
        currentYear = str(date.today().year)
        currentMonth = str(date.today().month)
        currentDay = str(date.today().day)
        source=self.dir_path+"\\TestData\\LoadInfogfjj"
        destination=self.dir_path+"\\TestData\\Historique"
        DayDirPath = destination + "/" + currentYear + "/" + currentMonth + "/" + currentDay + "/"
        self.assertEqual(f.storeEngravingFiles(destination,source,"TEST2"),{"result": "KO", "msg": "ERROR copying files in " + str(DayDirPath)})



#######TESTING addPNGtoImage ###################################################################
   # def test_addPNGtoImage(self):
  #      dataFolderPath=self.dir_path+"\\TestData\\savedimage"
 #       ouT=Image.open(self.dir_path+"\\TestData\\display.png")
#        self.assertEqual(f.addPNGtoImage(dataFolderPath,ouT),ouT)









if __name__ == '__main__':
    unittest.main()
