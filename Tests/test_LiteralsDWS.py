import unittest
import os
import Common.LiteralsDWS as Lt

path = os.path.dirname(os.path.realpath(__file__))
filePath=path+"\TestData\Literals.txt"

class TestScreen1(unittest.TestCase):

    def test_getLitNotOk(self):
        test = "text that doesnt exist"
        Lt.init(filePath)
        #for i in range(1,5000):
        self.assertEqual(Lt.getLit(test),"")

    def test_getLitOK(self):
        test = "real_text"
        Lt.init(filePath)
        #for i in range(1,5000):
        self.assertEqual(Lt.getLit(test),"real_text_result")

    def test_initOK(self):
        pathtest=filePath
        self.assertEqual(Lt.init(pathtest),"ok")

    def test_initNotOK(self):
        pathtest=filePath+"//test"
        self.assertEqual(Lt.init(pathtest),"not ok")





if __name__ == '__main__':
    unittest.main()