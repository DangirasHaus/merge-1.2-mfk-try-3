function recenterText() {
  if (product_name === "dior_rouge/rouge_dior") {
    return 0.09;
  } else {
    return 0;
  }
}

function getScale(canvasID) {
  if (canvasID == "textCanvas") {
    var scale = 1;
  } else {
    var scale = 1;
  }
  return scale;
}

function getLineSeparationValue() {
  return 0.9;
}

$(document).ready(function () {
  if (/screen2/i.test(location.href)) {
    if (product_name != undefined && product_name === "la_minaudiere") {
      //document.getElementById("clavier").style.display = "none";
      $("button#clavier").addClass("hidden");
    }

    $(".ui-keyboard-button.ui-keyboard-a.ui-state-default.ui-corner-all").click(
      function () {
        $(this).val($(this).val().toUpperCase());
        $(".ui-keyboard-text").css("color", "red");
      }
    );

    if (
      product_name === "jadore_edp/100ml_noel_2022" ||
      product_name === "dior_sauvage_edt/100ml_noel_2022" ||
      product_name === "dior_sauvage_edp/100ml_noel_2022" ||
      product_name === "dior_sauvage_parfum/100ml_noel_2022" ||
      product_name === "miss_dior_edp/100ml_noel_2022"
    ) {
      $("#mcd-images").show();
      $("#textToEngrave").css("display", "none");
      $("#lang-selector").css("display", "none");
      $("#font-selector").css("display", "none");
      if (product_name === "jadore_edp/100ml_noel_2022")
        $("#arbre_noel").hide();
      if (product_name === "dior_sauvage_edt/100ml_noel_2022")
        $("#arbre_noel").hide();
      if (product_name === "dior_sauvage_edp/100ml_noel_2022")
        $("#arbre_noel").hide();
      if (product_name === "dior_sauvage_parfum/100ml_noel_2022")
        $("#arbre_noel").hide();
      if (product_name === "miss_dior_edp/100ml_noel_2022")
        $("#etoile_noel").hide();
    }
    if (product_name === "miss_dior_edp/100ml_noel_2022") {
      $("#textCanvas").css("margin-left", "-390px");
    }
  }
  if (/screen2/i.test(location.href) || /screen3/i.test(location.href)) {
    if (product_name === "forever" || product_name === "forever/couture") {
      $(".container-fluid").css("margin-top", "400px");
    }
  }
});

function createLogo(canvasID, xCenter, yCenter, logoUrl, logoText, myfontSize) {
  var canvas = document.getElementById(canvasID);
  var c = canvas.getContext("2d");
  if (typeof xCenter == "undefined" || Number.isNaN(xCenter))
    xCenter = canvas.width / 2;
  if (typeof yCenter == "undefined" || Number.isNaN(yCenter))
    yCenter = canvas.height / 2;
  base_image = new Image();
  base_image.src = logoUrl;
  base_image.onload = function () {
    lastLogo = logoUrl;

    var textFits = true;
    var canvas = document.getElementById(canvasID);
    var c = canvas.getContext("2d");

    c.clearRect(0, 0, canvas.width, canvas.height);
    c.font = myfontSize + "px " + engravingFontName + ", EmojisFont";
    c.textAlign = "center";
    if (typeof xCenter == "undefined" || Number.isNaN(xCenter))
      xCenter = canvas.width / 2 + canvas.width * recenterText();
    if (typeof yCenter == "undefined" || Number.isNaN(yCenter))
      yCenter = canvas.height / 2;
    var resText = logoText.split("\n");
    // Rotation angle of the text
    var angle = getRotationAngle(canvasID, logoText);

    // Image parameters
    var proportion = base_image.naturalWidth / base_image.naturalHeight;
    // *********** MCD *************
    if (product_name === "PERFUMES/450ml" || product_name === "BOXES/450ml") {
      var sizeOfImageMM = 8;
      var interligneImageMM = 7.5;
    } else if (
      product_name === "PERFUMES/250ml" ||
      product_name === "BOXES/250ml"
    ) {
      var sizeOfImageMM = 8;
      var interligneImageMM = 6.7;
    } else if (
      product_name === "PERFUMES/125ml" ||
      product_name === "BOXES/125ml"
    ) {
      var sizeOfImageMM = 7;
      var interligneImageMM = 5.9;
    } else if (product_name === "PERFUMES/40ml") {
      var sizeOfImageMM = 5.5;
      var interligneImageMM = 5.4;
    }
    // *********** Miss Dior ***********
    else if (
      product_name === "miss_dior_blooming_bouquet/100ml" ||
      product_name === "miss_dior_edt/100ml"
    ) {
      var sizeOfImageMM = 8;
      var interligneImageMM = 5.8;
    } else if (product_name === "miss_dior_rose_nroses/100ml") {
      var sizeOfImageMM = 8;
      var interligneImageMM = 5.85;
    } else if (product_name === "miss_dior_edp/100ml_2021") {
      var sizeOfImageMM = 5;
      var interligneImageMM = 6.1;
    } else if (
      product_name === "miss_dior_edp/100ml" ||
      product_name === "miss_dior_absolutely_blooming/100mll"
    ) {
      var sizeOfImageMM = 6;
      var interligneImageMM = 4.85;
    }
    // *********** J'adore ***********
    else if (product_name === "jadore_infinissime/100ml") {
      var sizeOfImageMM = 10;
      var interligneImageMM = 5.85;
    } else if (
      product_name === "jadore_infinissime/150ml" ||
      product_name === "jadore_edp/150ml"
    ) {
      var sizeOfImageMM = 10;
      var interligneImageMM = 6.4;
    } else if (product_name === "jadore_edp/100ml") {
      var sizeOfImageMM = 10;
      var interligneImageMM = 5.85;
    } else if (product_name === "jadore_edt/100ml") {
      var sizeOfImageMM = 9.5;
      var interligneImageMM = 5.9;
    } else if (product_name === "jadore_lor/40ml") {
      var sizeOfImageMM = 7.5;
      var interligneImageMM = 5.5;
    }
    //********* sauvage ********
    else if (
      product_name === "dior_sauvage_edp/100ml" ||
      product_name === "dior_sauvage_edt/100ml" ||
      product_name === "dior_sauvage_parfum/100ml"
    ) {
      var sizeOfImageMM = 7.5;
      var interligneImageMM = 5;
    } else if (
      product_name === "dior_sauvage_edp/200ml" ||
      product_name === "dior_sauvage_edt/200ml" ||
      product_name === "dior_sauvage_parfum/200ml"
    ) {
      var sizeOfImageMM = 9.5;
      var interligneImageMM = 5.85;
    } else if (product_name === "dior_sauvage_elixir/60ml") {
      var sizeOfImageMM = 7.5;
      var interligneImageMM = 5.4;
    }
    //********* dior homme ********
    else if (
      product_name === "dior_homme_edt/100ml" ||
      product_name === "dior_homme_edt/100ml_2021" ||
      product_name === "dior_homme_parfum/100ml" ||
      product_name === "dior_homme_edp_intense/100ml"
    ) {
      var sizeOfImageMM = 8.5;
      var interligneImageMM = 5.9;
    } else if (
      product_name === "dior_homme_sport_cologne/125ml" ||
      product_name == "dior_homme_sport_edt/125ml"
    ) {
      var sizeOfImageMM = 8.5;
      var interligneImageMM = 5.4;
    }
    //********* rouge dior   ********
    else if (product_name === "dior_rouge/rouge_dior") {
      var sizeOfImageMM = 8;
      var interligneImageMM = 4.5;
    }
    //********* Les produits avec motifs de noel 2022   ********
    else if (product_name === "jadore_edp/100ml_noel_2022") {
      var sizeOfImageMM = 45;
      var interligneImageMM = 5;
    } else if (product_name === "dior_sauvage_edt/100ml_noel_2022") {
      var sizeOfImageMM = 35;
      var interligneImageMM = 5.85;
    } else if (product_name === "dior_sauvage_edp/100ml_noel_2022") {
      var sizeOfImageMM = 35;
      var interligneImageMM = 5;
    } else if (product_name === "dior_sauvage_parfum/100ml_noel_2022") {
      var sizeOfImageMM = 35;
      var interligneImageMM = 5;
    } else if (product_name === "miss_dior_edp/100ml_noel_2022") {
      var sizeOfImageMM = 35;
      var interligneImageMM = 6.1;
    }
    var sizeOfImage = (canvas.height * sizeOfImageMM) / 60;
    var interligneImage = (canvas.height * interligneImageMM) / 60;
    var desiredHeight = sizeOfImage;
    var desiredWidth = desiredHeight * proportion;

    // Text parameters
    var xDeviation =
      (resText.length - 1) *
      myfontSize *
      getLineSeparationValue() *
      Math.sin(angle);
    var yDeviation =
      ((resText.length - 1) * myfontSize * getLineSeparationValue() +
        desiredHeight) *
      Math.cos(angle);
    var newXCenter = xCenter - xDeviation / 2;
    var newYCenter = getNewYCenterValue(yCenter, yDeviation, canvasID);
    c.fillStyle = text_color;
    // textBaseline explained --> https://www.w3schools.com/tags/canvas_textbaseline.asp
    var aaaa = getTextBaseline();
    c.textBaseline = aaaa;
    c.translate(newXCenter, newYCenter);
    // Rotation of the canvas according to the rotation of the text
    c.rotate(-angle);
    var lastYpos = 0;
    for (var i = 0; i < resText.length; i++) {
      c.fillText(resText[i], 0, 0 + i * myfontSize * getLineSeparationValue());
    }
    lastYpos = (resText.length - 1) * myfontSize * getLineSeparationValue();

    // Image position
    var imgX0 = -(desiredWidth / 2);
    var imgY0 = lastYpos + interligneImage; // + (desiredHeight/2)
    // Drawing image
    c.drawImage(base_image, imgX0, imgY0, desiredWidth, desiredHeight);

    c.rotate(angle);
    c.translate(-newXCenter, -newYCenter);

    var bbox = getBoundingBox(c, 0, 0, canvas.width, canvas.height);
    // This function draws the square of the object
    //drawBoundingBox(c, bbox);

    objectCreated = new objectInCanvas(
      bbox.left,
      bbox.right,
      bbox.top,
      bbox.bottom,
      logoText,
      0,
      canvasID,
      logoUrl
    );
    objInCanvas.push(objectCreated);

    // Draws the restriction rectangle on the canvas the one that will allow the text to move
    if (
      canvasID === "textCanvas" &&
      typeof canvas_restriction !== "undefined" &&
      $("#goForwardA").attr("hideRestrictionSquare") !== "true"
    ) {
      var c = document.getElementById("textCanvas");
      var ctx = c.getContext("2d");
      ctx.strokeStyle = "red";
      ctx.rect(
        canvas_restriction[0],
        canvas_restriction[1],
        canvas_restriction[2],
        canvas_restriction[3]
      );
      ctx.stroke();
    }

    var img = canvas.toDataURL("image/png");
  };
}

function printLogo(logoUrl, logoText = $("#textToEngrave").val()) {
  logoText = logoText.trim();
  if (logoText.length == 0) {
    logoText = " ";
  }
  if (logoText.split("\n").length <= 1) {
    textToEngravePreview(logoText, logoUrl);
    $("#textToEngrave").val(logoText);
    if (logoText == " ") $("#textToEngrave").change();
  }
}

// get image cale for only product inside array
var miss_dior_list = [
  "miss_dior_absolutely_blooming/30ml",
  "miss_dior_absolutely_blooming/50ml",
  "miss_dior_edp/50ml",
  "miss_dior_edp/30ml",
];
$(document).ready(function () {
  if (/screen3/i.test(location.href)) {
    if (miss_dior_list.indexOf(product_name) > -1) {
      $("#warning-msg-remove-cale").show();
      $("#warning-msg-remove-cale").css("color", "red");
      $(".platform-img.tour_3_5").addClass("hidden");

      /*function blinText(){
			$('#warning-msg-remove-cale').fadeOut(1200).delay(0.01).fadeIn(1200);
		}
		setInterval(blinText,2200);*/
    } else {
      $("#cale_remove").addClass("hidden");
      $("#warning-msg-remove-cale").hide();
    }
  }
});
//make upercase first letter
$(document).ready(function () {
  $("textarea#textToEngrave").click(function () {
    var textToEngrave1 = $("textarea#textToEngrave").text();
    var textToEngrave =
      textToEngrave1.substring(0, 1).toUpperCase() +
      textToEngrave1.substring(1);
    $("textarea#textToEngrave").text(textToEngrave);
  });
});
// make fist letter capitalize
function makeFirstLetterCapital(text) {
  return text.charAt(0).toUpperCase() + text.slice(1);
}
// make text uppercase for DIN only
function changeEngravingFont(
  fontName,
  pinMode,
  engravingFont,
  new_fontsize,
  new_fontsize_max,
  rec_color
) {
  console.log("#@ changing 1");
  var color = rec_color ? rec_color : $("input[name=textColor]").val();
  var prevFont = $("input[name='font']").val();
  var isDifferentFont = !(prevFont == fontName);
  $("input[name=font]").val(fontName);

  if (isDifferentFont) {
    console.log("#@ changing font to:", fontName);
    setEngravingFont(fontName);
    engravingFontName = engravingFont;
    recalculateOptimalFontSize(new_fontsize, new_fontsize_max);
    // Get the textarea element
    var $textToEngrave = $("textarea#textToEngrave");

    if (engravingFontName === "engravingFont_DINOT-CondBold") {
      // Convert to uppercase
      $textToEngrave.val($textToEngrave.val().toUpperCase());
    } else if (
      engravingFontName === "engravingFont_CochinLTPro" ||
      engravingFontName === "engravingFont_Kunstler_Script"
    ) {
      // Convert to lowercase
      var lowercaseText = $textToEngrave.val().toLowerCase();
      // Make first letter uppercase
      $textToEngrave.val(makeFirstLetterCapital(lowercaseText));
    }

    if (isTextInBoundaries($textToEngrave.val())) {
      $("input[name=textColor]").val(color);
      $("input[name=pinMode]").val(pinMode);
      $("input[name=font]").val(fontName);
      $textToEngrave.attr("previous-val", "");
      $textToEngrave.change();
      $("#goForwardA").prop("disabled", false);
    } else {
      $("#goForwardA").prop("disabled", true);
      console.warn("object didn't fit after changing font");
    }

    // Changing slider values for the new font if there is a slider
    if ($("#myRange").length) {
      changeSliderValuesForFont();
    }
  }
}

function createImage(canvasID, text, myfontSize, xCenter, yCenter, logoUrl) {
  font = $("input[name=font]").val();

  console.log("font:", font);
  // Check if font is DIN make text to uppercase
  if (font === "DINOT-CondBold.ttf") {
    text = text.toUpperCase();
  }

  myfontSize = myfontSize * getScale(canvasID);
  lastLogo = undefined;
  var canvas = document.getElementById(canvasID);
  if (typeof canvas != "undefined" && canvas != null) {
    if (typeof logoUrl != "undefined") {
      createLogo(canvasID, xCenter, yCenter, logoUrl, text, myfontSize);
    } else {
      var c = canvas.getContext("2d");
      c.clearRect(0, 0, canvas.width, canvas.height);
      c.font = myfontSize + "px " + engravingFontName + ", EmojisFont";

      // Rotation angle of the text
      var angle = getRotationAngle(canvasID, text);
      c.textAlign = getTextAlignCanvas(angle);
      if (typeof xCenter == "undefined" || Number.isNaN(xCenter))
        xCenter = canvas.width / 2 + canvas.width * recenterText();
      if (typeof yCenter == "undefined" || Number.isNaN(yCenter))
        yCenter = canvas.height / 2;
      var resText = text.split("\n");
      var xDeviation =
        (resText.length - 1) *
        myfontSize *
        getLineSeparationValue() *
        Math.sin(angle);
      var yDeviation =
        (resText.length - 1) *
        myfontSize *
        getLineSeparationValue() *
        Math.cos(angle);
      var newXCenter = xCenter - xDeviation / 2;
      var newYCenter = getNewYCenterValue(yCenter, yDeviation, canvasID);
      var text_color_fill = $("input[name=textColor]").val();
      if (text_color_fill.indexOf(",") != -1) {
        c.fillStyle = "rgba(" + text_color_fill + ")";
      } else {
        c.fillStyle = text_color_fill;
      }
      // textBaseline explained --> https://www.w3schools.com/tags/canvas_textbaseline.asp
      var aaaa = getTextBaseline();
      c.textBaseline = aaaa;
      c.translate(newXCenter, newYCenter);
      // Rotation of the canvas according to the rotation of the text
      c.rotate(-angle);
      for (var i = 0; i < resText.length; i++) {
        c.fillText(
          resText[i],
          0,
          0 + i * myfontSize * getLineSeparationValue()
        );
      }
      c.rotate(angle);
      c.translate(-newXCenter, -newYCenter);
      var bbox = getBoundingBox(c, 0, 0, canvas.width, canvas.height);
      // This function draws the square of the object
      //drawBoundingBox(c, bbox);
      if (bbox) {
        objectCreated = new objectInCanvas(
          bbox.left,
          bbox.right,
          bbox.top,
          bbox.bottom,
          text,
          myfontSize,
          canvasID
        );
      } else {
        objectCreated = new objectInCanvas(
          0,
          0,
          0,
          0,
          text,
          myfontSize,
          canvasID
        );
      }
      objInCanvas.push(objectCreated);

      // Draws the restriction rectangle on the canvas the one that will allow the text to move
      if (
        canvasID === "textCanvas" &&
        typeof canvas_restriction !== "undefined" &&
        $("#goForwardA").attr("hideRestrictionSquare") !== "true"
      ) {
        var c = document.getElementById("textCanvas");
        var ctx = c.getContext("2d");
        ctx.strokeStyle = "#FF0000";
        ctx.rect(
          canvas_restriction[0],
          canvas_restriction[1],
          canvas_restriction[2],
          canvas_restriction[3]
        );
        ctx.stroke();
      }

      var img = canvas.toDataURL("image/png");
    }
  }
}
