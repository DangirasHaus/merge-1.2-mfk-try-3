var accessoriesDict = [
	{
		"matchingId": "paradise_accessories/col_clipse_paradise_imperial_",
		"dotList": [
			{
				"prod_id": "paradise_accessories/col_clipse_paradise_imperial_cognac",
				"class": "cognac",
				"display_name": "Cognac"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_imperial_creme",
				"class": "creme hidden",
				"display_name": "Creme"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_imperial_noir",
				"class": "noir",
				"display_name": "Noir"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_imperial_rouge",
				"class": "rouge",
				"display_name": "Rouge"
			}
		]
	},{
		"matchingId": "paradise_accessories/col_clipse_outfit_",
		"dotList": [
			{
				"prod_id": "paradise_accessories/col_clipse_outfit__prestige-red",
				"class": "prestige_red",
				"display_name": "prestige red"
			}
		]
	},{
		"matchingId": "paradise_accessories/col_clipse_paradise_",
		"dotList": [
			{
				"prod_id": "paradise_accessories/col_clipse_paradise_celestial_blue",
				"class": "celestial_blue",
				"display_name": "celestial blue"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_prestige_red",
				"class": "prestige_red",
				"display_name": "prestige red"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_creme",
				"class": "creme hidden",
				"display_name": "creme"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_dandy_green",
				"class": "dandy_green",
				"display_name": "dandy green"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_elixir_brown",
				"class": "elixir_brown",
				"display_name": "elixir brown"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_joy_red",
				"class": "joy_red",
				"display_name": "joy red"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_stone_beige",
				"class": "stone_beige",
				"display_name": "stone beige"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_sunlight_yellow",
				"class": "sunlight_yellow",
				"display_name": "sunlight yellow"
			},{
				"prod_id": "paradise_accessories/col_clipse_paradise_magnetic_grey",
				"class": "magnetic_grey",
				"display_name": "magnetic grey"
			}
		]
	},{
		"matchingId": "paradise_accessories/cravate_paradise_",
		"dotList": [
			{
				"prod_id": "paradise_accessories/cravate_paradise_celestial_blue",
				"class": "celestial_blue",
				"display_name": "celestial blue"
			},{
				"prod_id": "paradise_accessories/cravate_paradise_prestige_red",
				"class": "prestige_red",
				"display_name": "prestige red"
			},{
				"prod_id": "paradise_accessories/cravate_paradise_creme",
				"class": "creme hidden",
				"display_name": "creme"
			},{
				"prod_id": "paradise_accessories/cravate_paradise_dandy_green",
				"class": "dandy_green",
				"display_name": "dandy green"
			},{
				"prod_id": "paradise_accessories/cravate_paradise_elixir_brown",
				"class": "elixir_brown",
				"display_name": "elixir brown"
			},{
				"prod_id": "paradise_accessories/cravate_paradise_joy_red",
				"class": "joy_red",
				"display_name": "joy red"
			},{
				"prod_id": "paradise_accessories/cravate_paradise_stone_beige",
				"class": "stone_beige",
				"display_name": "stone beige"
			},{
				"prod_id": "paradise_accessories/cravate_paradise_sunlight_yellow",
				"class": "sunlight_yellow",
				"display_name": "sunlight yellow"
			},{
				"prod_id": "paradise_accessories/cravate_paradise_magnetic_grey",
				"class": "magnetic_grey",
				"display_name": "magnetic grey"
			}
		]
	},{
		"matchingId": "paradise_accessories/link_",
		"dotList": [
			{
				"prod_id": "paradise_accessories/link_celestial_blue",
				"class": "celestial_blue",
				"display_name": "celestial blue"
			},{
				"prod_id": "paradise_accessories/link_prestige_red",
				"class": "prestige_red",
				"display_name": "prestige red"
			},{
				"prod_id": "paradise_accessories/link_creme",
				"class": "creme hidden",
				"display_name": "creme"
			},{
				"prod_id": "paradise_accessories/link_dandy_green",
				"class": "dandy_green",
				"display_name": "dandy green"
			},{
				"prod_id": "paradise_accessories/link_elixir_brown",
				"class": "elixir_brown",
				"display_name": "elixir brown"
			},{
				"prod_id": "paradise_accessories/link_joy_red",
				"class": "joy_red",
				"display_name": "joy red"
			},{
				"prod_id": "paradise_accessories/link_stone_beige",
				"class": "stone_beige",
				"display_name": "stone beige"
			},{
				"prod_id": "paradise_accessories/link_sunlight_yellow",
				"class": "sunlight_yellow",
				"display_name": "sunlight yellow"
			},{
				"prod_id": "paradise_accessories/link_magnetic_grey",
				"class": "magnetic_grey",
				"display_name": "magnetic grey"
			}
		]
	}
];


/* This function gets and displays the correct dots for the current hennessy accessory */
function showRightDots(){
	console.log("Current product is:", product_name);
	for(var option of accessoriesDict){
		if(product_name.includes(option["matchingId"])){
			console.log("matching product:", option);
			// Adding dots to dropdown
			for(var dot of option["dotList"]){
				var htmlElem = `<div id="${dot["prod_id"]}" class="dot-color ${dot["class"]}">
						<div class="dot accessory ${dot["class"]}"></div>
					<p>${dot["display_name"]}</p>
				</div>`;
				$(htmlElem).insertAfter('.insert-after');
			}
			break;
		}
	}
	// Assigning the dots the click action
	$(".dot-color").click(function(){
		$(".dot-color div").removeClass("active");
		$(this).find("div").addClass("active");
		console.log($(this))
		console.log($(this).attr("id"))
		changeAccessory($(this).attr("id"))
	});
}



function changeAccessory(newProdId){
	var url = 'screen2';
	var form = $('<form action="' + url + '" method="post">' +
	  '<input type="text" name="product" value="' + newProdId + '" />' +
	  '</form>');
	$('body').append(form);
	form.submit();
}

$( document ).ready(function() {
	if(/screen2/i.test(location.href)){
		if(/accessories/i.test(product_name)){
			showRightDots();
			// Adding circle to show current color
			$('[id="' + product_name + '"]').find("div").addClass("active");
			$(".Hennessy-color-selector").show();
		}
		var accessories = ["paradise_accessories/col_clipse_paradise_celestial_blue"]
		if(product_name.indexOf("cravate_paradise") === -1 && product_name.indexOf("link_") === -1){
			if(product_name === 'hennessy_richard/100cl') $("#tiger-hennessy-picto").hide();
			$("#hennessy-images").show();
		}

		if(product_name.includes('paradise_accessories')){
			$("#hennessy-images").hide();
		}

        setTimeout(function(){
            console.log("\t\t changing to chinese");
			if(app_lang == "_chi") changeKeyboardFromList("chi_man", "chinese")
        }, 1000);
	};

	if(!/screen1/i.test(location.href)){
		if(/accessories/i.test(product_name)){
			// Accessory
			$('.dot.bottle').parent().hide();
			$('.dot.bottle').hide();
			$('.slidecontainer').hide();
			$("#img-spacer").height("100px");
		} else {
			// Bottle
			$('.dot.accessory').parent().hide();
			$('.dot.accessory').hide();
			$('.accessory').hide();
		}
	}


    chineseAutocomplete = false
    if(/screen1/i.test(location.href)){
        $('#selectLangBox').show();
        $('button').each(function(){
            var text = $(this).html().trim();
        });
        $('#logo').attr("src","/customStatic/images/LOGO_full_transparent.png");
        $('#logo').attr("src","/customStatic/images/HENNESSY-LogoCorporateNobrasarme-blanc-png_thumbnail.319.319.png");

    }
    if(/screen3/i.test(location.href) && !/BRAVE/i.test(product_name.toUpperCase())){
        $('#warning-msg-platform').text($('#warning-msg-platform').text().replace("top","second top"))
        $('.finishText.stars-container').addClass("hidden");

    }
})


function createLogo(canvasID, xCenter, yCenter, logoUrl, logoText, myfontSize){
    var canvas = document.getElementById(canvasID);
    var c = canvas.getContext("2d");
    if(typeof xCenter == "undefined" || Number.isNaN(xCenter)) xCenter = canvas.width/2;
    if(typeof yCenter == "undefined" || Number.isNaN(yCenter)) yCenter = canvas.height/2;
    base_image = new Image();
    base_image.src = logoUrl;
    base_image.onload = function(){
			lastLogo = logoUrl;

			var textFits = true;
		    var canvas = document.getElementById(canvasID);
		    var c = canvas.getContext("2d");


	        c.clearRect(0, 0, canvas.width, canvas.height);
	        c.font = myfontSize + 'px CustomEmojisFont, ' + engravingFontName + ', EmojisFont';

	        // Rotation angle of the text
	        var angle = getRotationAngle(canvasID, logoText);
            c.textAlign = getTextAlignCanvas(angle);
	        if(typeof xCenter == "undefined" || Number.isNaN(xCenter)) xCenter = canvas.width/2 + canvas.width*recenterText();
	        if(typeof yCenter == "undefined" || Number.isNaN(yCenter)) yCenter = canvas.height/2;
	        var resText = logoText.split("\n");

			// Image parameters
	        var proportion = base_image.naturalWidth / base_image.naturalHeight;
	        if (product_name === "hennessy_richard/100cl"){
	            var sizeOfImageMM = 10;
	            var interligneImageMM = 4;
	        } else if (product_name === "hennessy_XXO/100cl") {
	            var sizeOfImageMM = 10;
	            var interligneImageMM = 5.5;
	        } else if (product_name === "paradise/Paradis_150cl") {
	            var sizeOfImageMM = 10;
	            var interligneImageMM = 6;
		} else if (product_name === "hennessy_XO/100cl") {
	            var sizeOfImageMM = 10;
	            var interligneImageMM = 6;
		} else if (product_name === "paradise/Paradis_5cl") {
	            var sizeOfImageMM = 9.8;
	            var interligneImageMM = 3.5;
	        } else {
	            var sizeOfImageMM = 10;
	            var interligneImageMM = 5.5;
				
            }
	        var sizeOfImage = canvas.height * sizeOfImageMM/60;
	        var interligneImage = canvas.height * interligneImageMM/60;
	        var desiredHeight = sizeOfImage;
	        var desiredWidth = desiredHeight * proportion;

		if (product_name === "paradise/Paradis_5cl"){
	            var wantedHeight = 0;
		} else {
			var wantedHeight = 1;
		}
			// Text parameters
	        var xDeviation = ((resText.length - 1) * myfontSize * getLineSeparationValue()) * Math.sin(angle);
	        var yDeviation = ((resText.length - wantedHeight) * myfontSize * getLineSeparationValue() + desiredHeight) * Math.cos(angle);
	        var newXCenter = xCenter - xDeviation/2;
	        var newYCenter = getNewYCenterValue(yCenter, yDeviation, canvasID);
	        c.fillStyle = text_color;
	        // textBaseline explained --> https://www.w3schools.com/tags/canvas_textbaseline.asp
	        var aaaa = getTextBaseline();
	        c.textBaseline = aaaa;
	        c.translate( newXCenter, newYCenter );
	        // Rotation of the canvas according to the rotation of the text
	        c.rotate( -angle);
	        var lastYpos = 0;
	        for (var i=0;i<resText.length;i++){
	            c.fillText(resText[i], 0, 0 + i * myfontSize * getLineSeparationValue());
	        }
            lastYpos = (resText.length - 1) * myfontSize * getLineSeparationValue();

			if(angle != 0){
			// Image position for vertical text
		        var imgX0 = - (desiredWidth/2) + myfontSize;
	            var imgY0 = - (desiredWidth/2);// + (desiredHeight/2)
			} else {
			// Image position
		        var imgX0 = - (desiredWidth/2);
	            var imgY0 = lastYpos + interligneImage;// + (desiredHeight/2)
			}
			// Drawing image
	        c.drawImage(base_image, imgX0, imgY0, desiredWidth, desiredHeight);

	        c.rotate( angle);
	        c.translate( -newXCenter, -newYCenter );

	        var bbox = getBoundingBox(c, 0, 0, canvas.width, canvas.height);
	        // This function draws the square of the object
	        //drawBoundingBox(c, bbox);

	        objectCreated = new objectInCanvas(bbox.left,bbox.right,bbox.top,bbox.bottom,logoText , 0, canvasID, logoUrl)
	        objInCanvas.push(objectCreated);

	        // Draws the restriction rectangle on the canvas the one that will allow the text to move
	        if(canvasID === "textCanvas" && typeof(canvas_restriction) !== "undefined" && $("#goForwardA").attr("hideRestrictionSquare") !== "true"){
	            var c = document.getElementById("textCanvas");
	            var ctx = c.getContext("2d");
	            ctx.strokeStyle="red";
	            ctx.rect(canvas_restriction[0], canvas_restriction[1], canvas_restriction[2], canvas_restriction[3]);
	            ctx.stroke();
	        }

	        var img = canvas.toDataURL("image/png");
    }
}


// Special function to execute when tiger has been added and remove it for the CNC generation
function customFunctionBeforeGoToScreen3(){
	console.log("overwritten by hennessy");
	var picto = $("input[name='pictogram']").val();
	console.log("picto:", picto);


	if(picto.indexOf("tigre.png") !== -1 || picto.indexOf("tigre_white.png") !== -1) {
		// Making a backup of the textCanvas before removing the Tiger
		$("input[name='base64img_display']").after("<input type=\"hidden\" name=\"base64img_display_temp\" value=\"\">")
		generateBase64img("textCanvas", "base64img_display_temp")
		console.log("HIDING TIGER FOR CNC!!");
		$('#textToEngrave').change();
	}

}

function customFunctionAfterGoToScreen3(){
	var picto = $("input[name='pictogram']").val();
	if(picto.indexOf("tigre.png") !== -1 || picto.indexOf("tigre_white.png") !== -1) {
		console.log("SHOWING TIGER FOR canvas display!!");
		// Adding again the tiger to textCanvas input
		$("input[name='base64img_display']").val($("input[name='base64img_display_temp']").val())
	}
}




function printLogo(logoUrl, logoText=$("#textToEngrave").val()){
	$("input[name='pictogram']").val(logoUrl)
	logoText = logoText.trim();
	if(logoText.length == 0){
		logoText = " ";
	}
	// Allowing picto only with certain lines
	if(textLinesAllowPicto(logoText)){
	    textToEngravePreview(logoText, logoUrl);
	    $("#textToEngrave").val(logoText);
	    if(logoText == " ") $('#textToEngrave').change();
	}
}

function customAcceptKeyboardLogic(val){
	
	if(/screen2/i.test(location.href)){
        if (product_name === "paradise/Paradis_5cl" && val.length>0){
        console.log("yes")
           $("#hennessy-images .buttonDWS").prop("disabled", true);
          }else{
            if(!textLinesAllowPicto(val)){
				$("#hennessy-images .buttonDWS").prop("disabled", true)
			} else {
				$("#hennessy-images .buttonDWS").prop("disabled", false)
			}
          }
    }
}

function textLinesAllowPicto(text){
	var res = false;
	if(/screen2/i.test(location.href)){
		text = text.trim()
		var allowed_lines = 1;
		// Number of lines allowed depend on the product
		var lines = {
			"hennessy_XO/70cl": 2,
			"hennessy_XO/100cl": 2,
			"hennessy_XO/150cl": 1,
			"hennessy_XO/300cl": 1,
			"hennessy_XXO/100cl": 2,
			"paradise/Paradis": 2,
			"paradise/Paradis_150cl": 1,
			"paradise/Paradis_35cl": 2,
			"paradise_accessories/col_clipse_paradise_celestial_blue": 1,
			"paradise_accessories/col_clipse_paradise_dandy_green": 1,
			"paradise_accessories/col_clipse_paradise_stone_beige": 1,
			"paradise_accessories/col_clipse_paradise_creme": 1,
			"paradise_accessories/col_clipse_paradise_elixir_brown": 1,
			"paradise_accessories/col_clipse_paradise_joy_red": 1,
			"paradise_accessories/col_clipse_paradise_magnetic_grey": 1,
			"paradise_accessories/col_clipse_paradise_sunlight_yellow": 1,
			"paradise_accessories/col_clipse_paradise_prestige_red": 1,
			"paradise_accessories/col_clipse_outfit__paradise_green_harrods": 1,
			"paradise_accessories/col_clipse_outfit__prestige-red": 1,
			"hennessy_richard/100cl": 1

		}
		if(product_name in lines) {
			allowed_lines = lines[product_name];
		}
		res = text.split("\n").length <= allowed_lines;
	} else {
		res = true;
	}
	return res;
}




/*$( document ).ready(function() {
	$( ".primaryNav_link" ).click(function(){
	  $(".hideMeWhenActive").addClass("showMeWhenActive").removeClass("hideMeWhenActive");
	  $(this).parent().addClass("hideMeWhenActive").removeClass("showMeWhenActive");
	});
  });*/