function changeLangSelector(){
    var newHtml = '<option value="" disabled="" selected="" style="display:none;">Change language</option>';
    newHtml += '<option value="">English</option>';
    newHtml += '<option value="_fr_fr">French</option>';
    newHtml += '<option value="_chi">Chinese</option>';
    $('#selectLangBox').html(newHtml);
}

function getProductImageProportion(){
    return 0.75;
}

// Used for button "positioning", determines the % that the text will ascend or descend
function getIncrementChangeHeightProp(){
    if(product_name === "aqua_universalis/35ml"){
        return 120;
    } else if(product_name === "aqua_universalis/70ml") {
        return 95;
    } else {
        return 137;

    }
}

function getImageContainerHeight(){
    return $('#imgcontainer').height()*1.2;
}

function customTextToEngraveChangeFunction(){
    if((product_name === "aqua_universalis/35ml") && $('#textToEngrave').val().length > 4){
        default_height = maindefault_height;
        origPos = 1;
        $("#positioning-button button").prop("disabled", true);
    } else {
        $("#positioning-button button").prop("disabled", false);
    }
}

function getRotationAngle(canvasID, text){
    console.log(product_name + " this is name");
    if(isNaN(vertical_engraving)) vertical_engraving = 0;
    if(product_name === "aqua_universalis/35ml"){
        if(text.length < 5){
            vertical_engraving = 0;
            maxHeightMM = 20;
            maxWidthMM = 30;
        } else {
            vertical_engraving = 90;
            maxHeightMM = defMaxHeightMM;
            maxWidthMM = defMaxWidthMM;
        }
    }
    return degreeToRad(vertical_engraving)
}


$(document).ready(function () {
    if (/screen2/i.test(location.href)) {
        if (
            product_name === "leather_case/leather_case_brown" ||
            product_name === "leather_case/leather_case_blue" ||
            product_name === "leather_case/leather_case_petal_pink" ||
            product_name === "leather_case/leather_case_pink" ||
            product_name === "leather_case/leather_case_red"
        ) {
            $(".font-selector-buttons-container").css("visibility", "hidden");
        } else if (
            product_name === "aqua_universalis/35ml" ||
            product_name === "aqua_universalis/70ml" ||
            product_name === "aqua_universalis/200ml"
        ) {
            $(".font-selector-buttons-container").css("visibility", "visible");
        }
    }
    if (/screen3/i.test(location.href)) {
        if (
            product_name === "leather_case/leather_case_brown" ||
            product_name === "leather_case/leather_case_blue" ||
            product_name === "leather_case/leather_case_petal_pink" ||
            product_name === "leather_case/leather_case_pink" ||
            product_name === "leather_case/leather_case_red"
        ) {
            $(".platform-img.tour_3_5").css("display", "none");
        }
    }
});

$(document).ready(function () {
    if (/screen3/i.test(location.href)) {
        if (
            product_name === "aqua_universalis/35ml" ||
            product_name === "aqua_universalis/70ml" ||
            product_name === "aqua_universalis/200ml"
        ) {
            $("#cale_remove").css("display", "none");
        }
    }
});

$(document).ready(function () {
    if (/screen3/i.test(location.href)) {
        console.log("this is the language of the app" + app_lang);

        var cale_remove_img = "/customStatic/images/DWS_App_step_3_MFK_EN.png";  // Default image path

        if (app_lang === "fr_fr") {
            cale_remove_img = "/customStatic/images/DWS_App-step-3_MFK_FR.png";
        } else if (app_lang === "_chi") {
            cale_remove_img = "/customStatic/images/DWS_App-step-3_MFK_SC.png";
        }

        // Use jQuery to update the src attribute of the element with id "cale_remove"
        $("#cale_remove").attr("src", cale_remove_img);
    }
});


$( document ).ready(function() {
    if(/screen1/i.test(location.href)){
        changeLangSelector();
        setupTimersForInactivity(6000000000);
    }
    if(/screen2/i.test(location.href)){
        // leather_buttons();
        // Making Zapfino display correctly on dropdown
        $('.dropdown-menu a').each(function(){
            if(/Zapfino/i.test($(this).html()) || /Classic/i.test($(this).html())){
                $(this).css("padding-top","50px");
                $(this).css("padding-bottom","0px");
            }
        })
    }


});


/********* INIT E_COMMERCE MODE LOGIC *********/
function customLogicLoadScreen3(){
	customLogicLoadScreen2();
}
function customLogicLoadScreen2(){
	console.log("#@# e-commerce screen 2");
	var position = $("input[name=position]").val();
	// Changing position
	if(position == "0") {
		console.log("#@# move text to bottom!!!!!!!!!!!!!!!!!!!!")
		changeTextHeight();
	}
	// Changing color
	var color = $("input[name=color]").val();
	if(color != "None"){
		var selector = ".dot.accessory." + color;
		console.log("#@# -parent:", $(selector).parent());
		$(selector).parent().addClass("active");
		$(selector).addClass("active");
		$('.dot-color-container.MFK').show();
		console.log("#@# ------ position:", position);
		console.log("#@# ------ color:", color);
	} else {
		console.warn("Dot color not found, not showing. Are we in e-commerce mode?")
	}
}
/********* END E_COMMERCE MODE LOGIC *********/