import io, os


#newFilePath = "literals test\Fr.txt"


def prepareData(FilePath):
    File = io.open(FilePath, mode="r", encoding="utf-8")
    Data = File.read()
    List = Data.splitlines()

    listeN =[]
    listeV = []
    for x in List:
        if "=" in x:
            listeN.append(x.split("=")[0])
            listeV.append(x.split("=")[1])
        else:
            print("Char = not found! Please check if the file ",x," is well written")

    print (listeN)
    print(listeV)
    File.close()
    return (listeN,listeV)




def getMissingLiterals(originalList,newList):
    listOfMissingLiterals = []
    for x in originalList:
        if x in newList:
            print("this literal" ,x, " is already in the original file")
        else:
            listOfMissingLiterals.append(x)
    print("the missing literals are:",listOfMissingLiterals)
    return (listOfMissingLiterals)


def addMissingLiteralsToFile(list,filepath):
    file = io.open(filepath, mode="a", encoding="utf-8")
    for x in list:
        a = input("Please enter your traduction for this message: "+x+" = ")
        file.write("\n"+x+"="+a)
    file.close()

#This function give as a list of name of files from a path folder
def filesLister(path):
    list = []
    for x in os.listdir(path):
        print(os.listdir(path))
        print(x)
        if "." in x:
            if x.split(".")[1] == "txt":
                list.append(x)
    print(list)
    return(list)



#this function compare all the literal files with the original one
def compareAllLiteralfiles(originalFilePath,path):
#first we get the list of literal files except the original one "Literals.txt"
    list = filesLister(path)
    del list[list.index("original.txt")]
#then we compare each one with the original one
    ListOfMlist = []
    ListOfNNList = []
    ListOfNVList = []

    List_of_original_name, List_of_original_value = prepareData(originalFilePath)
    for y in list:
        print("#########Begin  now comparing ",y,"################### ")
        List_of_new_name, List_of_new_value = prepareData(path+"\\"+y)
        ListOfNNList.append(List_of_new_name)
        ListOfNVList.append(List_of_new_value)
        Mlist = getMissingLiterals(List_of_original_name, List_of_new_name)
        print("### Finiching comparing ", y, "##Finished Mlist is ",Mlist)
        if len(Mlist) != 0:
            ListOfMlist.append(Mlist)
    print("################## CONCLUSION ################### \n The list of Missing lists are ",ListOfMlist )
    print("The length of the list of lists is", len(ListOfMlist))
    return (ListOfNNList,ListOfNVList,ListOfMlist)







if __name__ == '__main__':
    originalFilePath = r"literals test\Nouveau dossier\original.txt"
    path = r"C:\Users\ahmed\Documents\decorwordservices\AAlYCE\App_configuration\Literals\literals test\Nouveau dossier"
    #o,n = prepareTwoFiles(originalFilePath,newFilePath)
    #L = getMissingLiterals(o,n)
    #print(L)
    #prepareData(newFilePath)
    #fielsLister(path)
    LNNL,LNVL,LML = compareAllLiteralfiles(originalFilePath,path)
    print(LNNL)
    print(LNVL)
    print(LML)
    #addMissingLitLMLeralsToFile(Mlist, newFilePath)