import io, os

originalFilePath = "Literals.txt"
newFilePath = "Literals_ar_tu_test.txt"
path = r"C:\Users\ahmed\Documents\decorwordservices\AAlYCE\App_configuration\Literals"
def prepareTwoFiles(originalFilePath,newFilePath):
    o = prepareData(originalFilePath)
    n = prepareData(newFilePath)
    return (o,n)


def prepareData(FilePath):
    File = io.open(FilePath, mode="r", encoding="utf-8")
    Data = File.read()
    List = Data.splitlines()

    liste =[]
    for x in List:
        liste.append(x.split("=")[0])
    print (liste)
    File.close()
    return (liste)


def getMissingLiterals(originalList,newList):
    listOfMissingLiterals = []
    for x in originalList:
        if x in newList:
            print("this literal", x, " is already in the original file")
        else:
            listOfMissingLiterals.append(x)
    print("the missing literals are:",listOfMissingLiterals)
    return (listOfMissingLiterals)


def addMissingLiteralsToFile(list,filepath):
    file = io.open(filepath, mode="a", encoding="utf-8")
    for x in list:
        a = input("Please enter your traduction for this message: "+x+" = ")
        file.write("\n"+x+"="+a)
    file.close()


#this function compare all the literal files with the original one
def compareAllLiterlfiles(path):
#first we get the list of literal files except the original one "Literals.txt"
    list = []
    for x in os.listdir(path):
        if x.split(".")[1] == "txt":
            list.append(x)
    del list[list.index("Literals.txt")]
#then we compare each one with the original one
    ListOfMlist = []
    for y in list:
        o,n = prepareTwoFiles(originalFilePath, y)
        Mlist = getMissingLiterals(o, n)
        if len(Mlist) != 0:
            ListOfMlist.append(Mlist)
    return (ListOfMlist)







if __name__ == '__main__':
    compareAllLiterlfiles(path)
    #addMissingLiteralsToFile(Mlist, newFilePath)