import logging
import logging.handlers

#https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/

logger = logging.getLogger(__name__)
listOfActiveLogs = []

def init(logName, debug_mode):
    if debug_mode == "True":
        log_level = logging.INFO
    else:
        log_level = logging.INFO

    # Storing the name of the logs already created, otherwise the lines are printed many times
    if len(logName.split("Common")) > 0:
        logFileName = logName.split("Common")[1]
    else:
        logFileName = logName

    if logFileName not in listOfActiveLogs:
        listOfActiveLogs.append(logFileName)
        logger.setLevel(log_level)

        # create a file handler
        #handler = logging.FileHandler(logName)
        handler = logging.handlers.RotatingFileHandler(
                  logName, maxBytes=10000000, backupCount=5, encoding='utf-8')
        handler.setLevel(log_level)

        # create a logging format
        formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        handler.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(handler)

def log(msg, *args, **kwargs):
    # Importance not set
    logger.log(msg, *args, **kwargs)

def debug(msg, *args, **kwargs):
    # Importance 10
    logger.debug(msg, *args, **kwargs)

def info(msg, *args, **kwargs):
    # Importance 20
    logger.info(msg, *args, **kwargs)

def warning(msg, *args, **kwargs):
    # Importance 30
    logger.warning(msg, *args, **kwargs)

def error(msg, *args, **kwargs):
    # Importance 40
    logger.error(msg, *args, **kwargs)

def critical(msg, *args, **kwargs):
    # Importance 50
    logger.critical(msg, *args, **kwargs)

def exception(msg, *args, **kwargs):
    # Importance 50
    logger.exception(msg, *args, **kwargs)
