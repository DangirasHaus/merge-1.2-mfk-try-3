from PIL import Image, ImageDraw, ImageFont
import numpy as np
import cv2 as cv
import math
from sys import platform
from flask import session
import os, traceback, time
import Common.LogsDWS as LOG
import Common.functions as app
import Common.LiteralsDWS as L

"""
Ce programme genere un fichier Gcode a partir d un fichier texte contenant les informations suivantes
nom du fichier = nom du Client = nom du fichierGcode (qui aura en plus un numero correspondant au nombre de fichier dans les commandes)
Collection
Volume
Texte a graver
mode (O=> initales, 1=>prenom court, 2=>prenom long)

Il va lire les information de gravure et de creation du texte dans le fichier "fichierInfo" that is inside every collection folder
Il va generer l image du texte et creer le Gcode qui lui correspond
Enfin il va utiliser le convertisseur unf pour genrer un fichier unf tout ca dans le meme repertoire LoadInfo
"""

# TODO dont rely so much in global variables

'''
-------------------------------------- GCODE FUNCTIONS -----------------------------------------------
'''

# TODO remove this global variables
## Global variables
fichier = None
data2 = ""
collection = ""
volume = ""
texte = ""
mode = 0



# Rounds a number
def arr(nombre, arrondi=2):
    return round(nombre, arrondi)

# Creates the UNF file given a CNC file
def createUnfFile(filepath, filename):
    # Generating the .unf file
    newfilepath = filepath.split("AAlYCE")[1]
    newfilepath = ".." + newfilepath.split("Common")[1]
    gCodeFile = newfilepath + "/" + filename + ".cnc"
    unfFile = newfilepath + "/" + filename + ".unf"
    LOG.info("Generating .unf for file: " + str(gCodeFile))
    try:
        print("./gconv -co2 " + gCodeFile)
        LOG.info("./gconv -co2 " + gCodeFile)
        os.chdir(app.getConfPath("gcode_folder"))  # positionnement dans le bon repertoire sinon ca ne fonctionne pas
        if platform == "linux" or platform == "linux2":
            LOG.info("Executin GCONV for linux")
            os.system("chmod +x gconv")  # Give gconv execution permissions
            os.system("./gconv -co2 " + gCodeFile)  # Conversion
        elif platform == "win32":
            LOG.info("Executin GCONV for Windows")
            os.system("gconv_static.exe -co2 " + gCodeFile + " >nul 2>&1")  # Conversion
        LOG.info("Generating .unf file zith ./gconv: ")

        # If gconv doesnt execute, no error is raised, so we check if the .unf file has been created
        if not os.path.isfile(unfFile):
            raise ValueError('.unf file not created.')
    except Exception:
        LOG.error("ERROR executing ./gconv:")
        LOG.error(str(traceback.format_exc()))
        return L.returnResponse("004")

    unfFileCheck = filepath + "/" + filename + ".unfCHECK"
    LOG.info("Creating " + str(unfFileCheck))
    fCheck = open(unfFileCheck, "w")
    fCheck.write("finished")
    fCheck.close()
    return L.returnResponse(None)

'''
-------------------------------------- CNC genreation -----------------------------------------------
'''


def getL2DfromImage(outT):
    pic = np.asarray(outT)
    data = np.transpose(pic, (1, 0, 2))
    thirdvalues = data[:,:,3]
    L2D = thirdvalues.tolist()
    return L2D


# Return a matrix with booleans for pixels to engrave
def getL2DfromImage_color_filter(outT, color):
    # Getting color in numbers
    color_int = [int(num) for num in color.split(",")]
    data = np.asarray(outT)
    # Temporarily unpack the bands for readability
    red, green, blue, alpha = data.T
    # TODO TODO IMPORTANT fix this issue. Doing some ugly think for Ambre tomorrow
    #wanted_area = (red == color_int[0]) & (green == color_int[1]) & (blue == color_int[2]) & (alpha != 0)
    wanted_area = (alpha != 0)
    return wanted_area.tolist()

# Adds margin to an image filling it with a color
def add_margin(pil_img, top = 1, right = 1, bottom = 1, left = 1, color = (0, 0, 0, 0)):
    width, height = pil_img.size
    new_width = width + right + left
    new_height = height + top + bottom
    result = Image.new(pil_img.mode, (new_width, new_height), color)
    result.paste(pil_img, (left, top))
    return result


# TODO TODO - LAST mark these functions as deprecated
# Creates a grid of the same image for tests
def imageToCncFile_GRID(imagePath,filePath, pinNumber, x_spacing=1, y_spacing=1, n_cols=3, n_lines=2, interlude=30):
    print("imageToCncFile_GRID INIT")
    # Product Name
    # TODO REMOVE, THIS IS ONLY FOR GEP
    pn = app.getOrderTempVariables("product")
    app.storeOrderTempVariables("engravingTime", "")
    app.storeOrderTempVariables("engravingTimeFormat", "")

    laser_data = app.getLaserParams(pinNumber)

    # This is the response object for time performances
    cnc_sub_times = {
        "L2D_time": 0,
        "PD_time": 0,
        "CNC_write_time": 0,
        "PD2_time": 0,
    }

    new_prod_data = app.getProductData()

    # We prepare the warm up for the laser
    warmup_pin = app.getConf("warm_up_pin_number")
    warmup_time_in_seconds = app.get_warmup_time()
    laser_data_warmup = app.get_warm_up_params()

    LOG.info("then image path is :" + imagePath)
    LOG.info("we will store cnc file in: " + filePath)
    outT = Image.open(imagePath)
    # We add a 1 pixel border to the image to avoid error in CNC generation
    outT = add_margin(outT)
    outT = outT.transpose(Image.FLIP_TOP_BOTTOM)
    if app.getConf("calibration_red_filename") == "GEP_MHLab_26-03-2020":
        if "Hennessy" not in pn and "Krug" not in pn and "MoetChandon" not in pn:
            angle = 180
            outT = outT.rotate(angle)
    LOG.info("the image opend")
    cncFile = filePath

    LOG.warning("ENGRAVING PARAMETERS = " + str(new_prod_data))
    hatch = float(new_prod_data.get("hatch"))
    vitesse = int(laser_data.get("vitesse"))
    nombrePassage = int(laser_data.get("nbpassage"))
    modeDoubleHatch = laser_data.get("doublehatch") == "1"
    X_Offset = str(app.getX_Offset())

    print("calculating L2D")

    start_L2D_time = time.time()
    L2D = getL2DfromImage(outT)
    end_L2D_time = time.time()
    L2D_time = end_L2D_time - start_L2D_time
    cnc_sub_times["L2D_time"] = L2D_time
    LOG.info("TIME - L2D time:" + str(L2D_time))

    print("end L2D")

    # position initale de trace
    pos_init = []
    pos_init.append(-outT.size[0] / 2 * hatch)
    pos_init.append(-outT.size[1] / 2 * hatch)

    # PD calculation
    start_PD_time = time.time()
    # Function to get the red rectangle if needed
    x0, x1, y0, y1, L2D, pos_init = app.get_square_pixel(L2D, pos_init, hatch)
    app.createRectangleCNC(x0, x1, y0, y1)
    PD = app.obtainHorizontalPD(L2D, pos_init, hatch, laser_data.get("use_new_cnc_method"))
    end_PD_time = time.time()
    PD_time = end_PD_time - start_PD_time
    cnc_sub_times["PD_time"] = PD_time
    LOG.info("TIME - PD time:" + str(PD_time))

    # ------------- GRID configuration ------------- #

    maxWidthMM = 50
    maxHeightMM = 59
    # Verify that all the engravings fit in the laser zone
    engraving_area = int(maxWidthMM), int(maxHeightMM)

    xl, yl = abs(x1 - x0), abs(y1 - y0)
    tot_width = (xl + x_spacing) * n_cols
    while tot_width > engraving_area[0]:
        n_cols -= 1
        tot_width = (xl + x_spacing) * n_cols
    print("Max horizontal engravings is", n_cols)
    tot_height = (yl + y_spacing) * n_lines
    while tot_height > engraving_area[1]:
        n_lines -= 1
        tot_height = (yl + y_spacing) * n_lines
    print("Max vertical engravings is", n_lines)

    ###### offset = - engraving_area[0]/2 - tot_width/2 , -engraving_area[1] - (y0+y1)/2 + yl


    # ------------- END GRID configuration ------------- #



    # Pin protection will be the protection parameters for small engravings
    protection_pin_number = app.getConf("protection_pin_number")
    laser_data_protection = app.getProtectionLaserParams(pinNumber, protection_pin_number)

    # ----------------------------- Creation du fichier CNC -----------------------------
    fichier = open(cncFile, 'w')
    # Commandes de base et creation variables START of CNC FILE
    fichier.write("G17")
    fichier.write("\n" + "G71")
    fichier.write("\n" + "G90")
    # Define du GCode
    fichier.write("\n" + "$SC_DEFINES = 1")
    fichier.write("\n" + "SC_SELECT_JOB[6]")
    # We changed this to 35 so when rotating the images for diagonal engraving, if there was a part of the engraving
    # with more than 30, it doesnt break the CNC
    # This causes a slight resizing of the engraving, which doesnt seem important from the preliminary tests
    # The resizing was a problem in the end, we come back to 30 since the problem we had that made us change to 35 is
    # now solved. The problem was with pos_init, that would move coordinates to the right and down, causing
    # some values to go over 30
    fichier.write("\n" + "$SC_FIELDXMIN = -30")
    fichier.write("\n" + "$SC_FIELDXMAX =  30")
    fichier.write("\n" + "$SC_FIELDYMIN = -30")
    fichier.write("\n" + "$SC_FIELDYMAX =  30")
    fichier.write("\n" + "$SC_FIELDXGAIN =  1")
    fichier.write("\n" + "$SC_FIELDYGAIN =  1")
    fichier.write("\n" + "$SC_FIELDXOFFSET ="+"  "+X_Offset)
    fichier.write("\n" + "$SC_FIELDYOFFSET =  0")
    fichier.write("\n" + "$SC_FIELDAXISSTATE = 3")

    # Definition des pinceaux
    for params in new_prod_data.get("laser"):
        # Protection set of parameters
        if params.get("pinLoad") == str(protection_pin_number):
            params = laser_data_protection
        fichier.write("\n" + "SC_TOOL_LOAD[" + str(params.get("pinLoad")) + "]")
        fichier.write("\n" + "$SC_LASERPOWER = " + str(params.get("puissance")))
        fichier.write("\n" + "$SC_FREQUENCY = " + str(params.get("frequence")))
        fichier.write("\n" + "$SC_STDBYPERIOD = 0.0")
        fichier.write("\n" + "$SC_JUMPSPEED = 9000.0")
        fichier.write("\n" + "$SC_MARKSPEED = " + str(params.get("vitesse")))
        fichier.write("\n" + "$SC_JUMPDELAY = 100.0")
        fichier.write("\n" + "$SC_MARKDELAY = " + str(params.get("mark_delay")))
        fichier.write("\n" + "$SC_POLYDELAY = 0")
        fichier.write("\n" + "$SC_LASERONDELAY = 30.0")
        fichier.write("\n" + "$SC_LASEROFFDELAY = 130.0")
        fichier.write("\n" + "SC_TOOL_SAVE[" + str(params.get("pinLoad")) + "]")

    fichier.write("\n" + "$SC_ArcStepMode = 1")  # Permet de mettre en place le fractionnement des arc inutile ici

    fichier.write("\n" + "$SC_DEFINES = 0")

    if app.laser_needs_warm_up():
        LOG.info("Laser needs warmup")
        print("Laser needs warmup")
        # We only load the waiting time in the first iteration
        fichier.write(get_double_engraving_wait_cnc(warmup_time_in_seconds, laser_data_warmup, "WARM UP GRID"))

    lastPoint = [0,0,0]
    mark_delay_time = float(laser_data["mark_delay"]) / 1000000

    # We load the pin we will use
    xl = math.fabs(x0 - x1)
    yl = math.fabs(y0 - y1)
    engraving_needs_protection = xl * yl < float(laser_data["protection_mode_area"])
    engraving_needs_protection = engraving_needs_protection and laser_data.get("use_new_cnc_method")
    engraving_needs_protection = engraving_needs_protection and app.getLaserParams(protection_pin_number).get("pinLoad")

    # IF the engraving is small we change the loaded pin for protection
    if engraving_needs_protection:
        LOG.info("Engraving is too small, we need the protection parameters")
        vitesse = int(laser_data_protection.get("vitesse"))
        nombrePassage = int(laser_data_protection.get("nbpassage"))
        modeDoubleHatch = laser_data_protection.get("doublehatch") == "1"
        pinNumber = str(protection_pin_number)

    # ------------------------------------------------------------------------
    # SPECIAL LOGIC TO "DOUBLE ENGRAVING" WITH SOME WAITING TIME IN THE MIDDLE
    # ------------------------------------------------------------------------
    double_engraving_seconds = new_prod_data.get("double_engraving_wait_time")
    double_engraving = "double_engraving_wait_time" in new_prod_data and double_engraving_seconds is not ""
    double_engraving_n = 1
    LOG.info("Is double engraving active? " + str(double_engraving))
    if double_engraving:
        double_engraving_n = 2

    pos_init_int = pos_init[0] - tot_width/2 + xl/2, pos_init[1] - tot_height/2 + yl/2
    #pos_init_int = - engraving_area[0]/2 - tot_width/2 , -engraving_area[1] - (y0+y1)/2 + yl
    for i in range(n_cols):
        for j in range(n_lines):
            engravingTime = 0
            pos_init = pos_init_int[0] + i*(xl + x_spacing), pos_init_int[1] + j*(yl + y_spacing)
            # Function to get the red rectangle if needed
            x0, x1, y0, y1, L2D, pos_init = app.get_square_pixel(L2D, pos_init, hatch)
            PD = app.obtainHorizontalPD(L2D, pos_init, hatch, laser_data.get("use_new_cnc_method"))

            print('\n\ncoord :', i,j )
            print('pos_init :', pos_init)
            print('element size', xl, yl)
            print('pos_init_int', pos_init_int)

            # Usually double_engraving_n will be just one, but if we need double_engraving, it will be two
            for double_engraving_i in range(double_engraving_n):
                ## ici commence la gravure
                fichier.write("\n" + "$SC_LoopCount = " + str(nombrePassage))
                fichier.write("\n" + "SC_Start_Entity")

                # We load the pen we will use
                fichier.write("\n" + "SC_TOOL_LOAD[" + str(pinNumber) + "]")
                fichier.write("\n" + "SC_TOOL[0]")

                start_CNC_write_time = time.time()
                i_lines = 0
                j_lines = 0
                init_j = 0
                n_jump_lines = int(laser_data.get("n_jump_lines"))
                while i_lines < len(PD):
                    for item in PD[j_lines]:
                        # we can change G1 by SC_TOOL if we want the laser to move at jump speed when there is no engraving
                        fichier.write("\n" + "SC_TOOL[" + str(item[0]) + "]")
                        fichier.write("\n" + "G" + str(item[0]) + " X" + str(item[1]) + " Y" + str(item[2]))
                        speed = 9000 * (1 - item[0]) + vitesse * item[0]
                        distance = ((lastPoint[1] - item[1])**2 + (lastPoint[2] - item[2])**2)**0.5
                        engravingTime = engravingTime + distance/speed
                        lastPoint = item
                    # Adding mark delay time for each point
                    engravingTime = engravingTime + len(PD[j_lines]) * mark_delay_time
                    i_lines = i_lines + 1
                    j_lines = j_lines + n_jump_lines
                    if j_lines >= len(PD):
                        init_j = init_j + 1
                        j_lines = init_j

                end_CNC_write_time = time.time()
                CNC_write_time = end_CNC_write_time - start_CNC_write_time
                cnc_sub_times["CNC_write_time"] = CNC_write_time
                LOG.info("TIME - CNC write time:" + str(CNC_write_time))

                if modeDoubleHatch:
                    start_PD2_time = time.time()
                    PDvertical = app.obtainVerticalPD(L2D, pos_init, hatch, laser_data.get("use_new_cnc_method"))
                    end_PD2_time = time.time()
                    PD2_time = end_PD2_time - start_PD2_time
                    cnc_sub_times["PD2_time"] = PD2_time
                    LOG.info("TIME - PD2 time:" + str(PD2_time))
                    i_lines = 0
                    j_lines = 0
                    init_j = 0
                    while i_lines < len(PDvertical):
                        for item in PDvertical[j_lines]:
                            # we can change G1 by SC_TOOL if we want the laser to move at jump speed when there is no engraving
                            fichier.write("\n" + "SC_TOOL[" + str(item[0]) + "]")
                            fichier.write("\n" + "G" + str(item[0]) + " X" + str(item[1]) + " Y" + str(item[2]))
                            speed = 9000 * (1 - item[0]) + vitesse * item[0]
                            distance = ((lastPoint[1] - item[1]) ** 2 + (lastPoint[2] - item[2]) ** 2) ** 0.5
                            engravingTime = engravingTime + distance / speed
                            lastPoint = item
                        # Adding mark delay time for each point
                        engravingTime = engravingTime + len(PDvertical[j_lines]) * mark_delay_time
                        #fichier.write("\n" + "(---------------- New engraving line --------------)")
                        i_lines = i_lines + 1
                        j_lines = j_lines + n_jump_lines
                        if j_lines >= len(PDvertical):
                            init_j = init_j + 1
                            j_lines = init_j
                fichier.write("\n" + "SC_TOOL[0]")
                fichier.write("\n" + "SC_End_Entity")
                fichier.write(get_double_engraving_wait_cnc(interlude, comment="WAIT TIME FOR GRID"))

                # Adding logic for the waiting time in double_engraving
                if double_engraving and double_engraving_i < double_engraving_n-1:
                    # We only load the waiting time in the first iteration
                    fichier.write(get_double_engraving_wait_cnc(double_engraving_seconds, comment="WAIT TIME FOR DOUBLE ENGRAVING GRID"))

    # Engraving time is per loop at this point
    engravingTime = engravingTime * nombrePassage * double_engraving_n
    if double_engraving:
        engravingTime = engravingTime + float(double_engraving_seconds) * (double_engraving_n-1)
    # This is only for grid
    engravingTime = (engravingTime + interlude) * (n_lines + n_cols)
    app.storeOrderTempVariables("engravingTime", engravingTime)
    engravingTimeSeconds, engravingTimeMinutes = math.modf(engravingTime/60)
    engravingTimeFormat = str(int(engravingTimeMinutes)) + " minutes and " + str(int(engravingTimeSeconds*60)) + " seconds."
    app.storeOrderTempVariables("engravingTimeFormat", engravingTimeFormat)
    LOG.info("engravingTime = " + str(engravingTime))
    LOG.info("engravingTimeFormat = " + str(engravingTimeFormat))
    print("engravingTime = " + str(engravingTime))
    print("engravingTimeFormat = " + str(engravingTimeFormat))


    ##Fermeture du fichier
    fichier.write("\n" + "G0 X0 Y0")
    fichier.write("\n" + "M2")
    fichier.close()
    return cnc_sub_times


# TODO TODO - LAST mark these functions as deprecated
def imageToCncFile(imagePath,filePath, pinNumber):
    print("imageToCncFile INIT")
    # Product Name
    # TODO REMOVE, THIS IS ONLY FOR GEP
    pn = app.getOrderTempVariables("product")
    app.storeOrderTempVariables("engravingTime", "")
    app.storeOrderTempVariables("engravingTimeFormat", "")

    laser_data = app.getLaserParams(pinNumber)

    # This is the response object for time performances
    cnc_sub_times = {
        "L2D_time": 0,
        "PD_time": 0,
        "CNC_write_time": 0,
        "PD2_time": 0,
    }

    new_prod_data = app.getProductData()

    # We prepare the warm up for the laser
    warmup_pin = app.getConf("warm_up_pin_number")
    warmup_time_in_seconds = app.get_warmup_time()
    laser_data_warmup = app.get_warm_up_params()

    LOG.info("then image path is :" + imagePath)
    LOG.info("we will store cnc file in: " + filePath)
    outT = Image.open(imagePath)
    # We add a 1 pixel border to the image to avoid error in CNC generation
    outT = add_margin(outT)
    outT = outT.transpose(Image.FLIP_TOP_BOTTOM)
    if app.getConf("calibration_red_filename") == "GEP_MHLab_26-03-2020":
        if "Hennessy" not in pn and "Krug" not in pn and "MoetChandon" not in pn:
            angle = 180
            outT = outT.rotate(angle)
    LOG.info("the image opend")

    # If hatch_angle exists and it is a number != 0, we rotate the image
    print("##@# hatch_angle:", laser_data.get("hatch_angle"))
    hatch_angle_comment = "0"
    if "hatch_angle" in laser_data and laser_data["hatch_angle"] and laser_data["hatch_angle"] is not "0":
        angle = int(laser_data["hatch_angle"])
        hatch_angle_comment = str(angle)
        outT = outT.rotate(angle, resample=0, expand=1, center=None, translate=None, fillcolor=None)
        # Storing variable to rotate laser
        app.storeOrderTempVariables("hatch_angle", angle)
    cncFile = filePath

    LOG.warning("ENGRAVING PARAMETERS = " + str(new_prod_data))
    hatch = float(new_prod_data.get("hatch"))
    vitesse = int(laser_data.get("vitesse"))
    nombrePassage = int(laser_data.get("nbpassage"))
    modeDoubleHatch = laser_data.get("doublehatch") == "1"
    X_Offset = str(app.getX_Offset())

    print("calculating L2D")

    start_L2D_time = time.time()
    L2D = getL2DfromImage(outT)
    end_L2D_time = time.time()
    L2D_time = end_L2D_time - start_L2D_time
    cnc_sub_times["L2D_time"] = L2D_time
    LOG.info("TIME - L2D time:" + str(L2D_time))

    print("end L2D")

    # position initale de trace
    pos_init = []
    pos_init.append(-outT.size[0] / 2 * hatch)
    pos_init.append(-outT.size[1] / 2 * hatch)

    # PD calculation
    start_PD_time = time.time()
    # Function to get the red rectangle if needed
    x0, x1, y0, y1, L2D, pos_init = app.get_square_pixel(L2D, pos_init, hatch)
    app.createRectangleCNC(x0, x1, y0, y1)
    PD = app.obtainHorizontalPD(L2D, pos_init, hatch, laser_data.get("use_new_cnc_method"))
    end_PD_time = time.time()
    PD_time = end_PD_time - start_PD_time
    cnc_sub_times["PD_time"] = PD_time
    LOG.info("TIME - PD time:" + str(PD_time))

    # Pin protection will be the protection parameters for small engravings
    protection_pin_number = app.getConf("protection_pin_number")
    laser_data_protection = app.getProtectionLaserParams(pinNumber, protection_pin_number)

    # ----------------------------- Creation du fichier CNC -----------------------------
    fichier = open(cncFile, 'w')
    # Commandes de base et creation variables START of CNC FILE
    fichier.write("G17")
    fichier.write("\n" + "G71")
    fichier.write("\n" + "G90")
    # Define du GCode
    fichier.write("\n" + "$SC_DEFINES = 1")
    fichier.write("\n" + "SC_SELECT_JOB[6]")
    # We changed this to 35 so when rotating the images for diagonal engraving, if there was a part of the engraving
    # with more than 30, it doesnt break the CNC
    # This causes a slight resizing of the engraving, which doesnt seem important from the preliminary tests
    # The resizing was a problem in the end, we come back to 30 since the problem we had that made us change to 35 is
    # now solved. The problem was with pos_init, that would move coordinates to the right and down, causing
    # some values to go over 30
    fichier.write("\n" + "$SC_FIELDXMIN = -30")
    fichier.write("\n" + "$SC_FIELDXMAX =  30")
    fichier.write("\n" + "$SC_FIELDYMIN = -30")
    fichier.write("\n" + "$SC_FIELDYMAX =  30")
    fichier.write("\n" + "$SC_FIELDXGAIN =  1")
    fichier.write("\n" + "$SC_FIELDYGAIN =  1")
    fichier.write("\n" + "$SC_FIELDXOFFSET ="+"  "+X_Offset)
    fichier.write("\n" + "$SC_FIELDYOFFSET =  0")
    fichier.write("\n" + "$SC_FIELDAXISSTATE = 3")

    # Definition des pinceaux
    for params in new_prod_data.get("laser"):
        # Protection set of parameters
        if params.get("pinLoad") == str(protection_pin_number):
            params = laser_data_protection
        fichier.write("\n" + "SC_TOOL_LOAD[" + str(params.get("pinLoad")) + "]")
        fichier.write("\n" + "$SC_LASERPOWER = " + str(params.get("puissance")))
        fichier.write("\n" + "$SC_FREQUENCY = " + str(params.get("frequence")))
        fichier.write("\n" + "$SC_STDBYPERIOD = 0.0")
        fichier.write("\n" + "$SC_JUMPSPEED = 9000.0")
        fichier.write("\n" + "$SC_MARKSPEED = " + str(params.get("vitesse")))
        fichier.write("\n" + "$SC_JUMPDELAY = 100.0")
        fichier.write("\n" + "$SC_MARKDELAY = " + str(params.get("mark_delay")))
        fichier.write("\n" + "$SC_POLYDELAY = 0")
        fichier.write("\n" + "$SC_LASERONDELAY = 30.0")
        fichier.write("\n" + "$SC_LASEROFFDELAY = 130.0")
        fichier.write("\n" + "SC_TOOL_SAVE[" + str(params.get("pinLoad")) + "]")

    # Adding comment for the hatch_angle

    fichier.write("\n" + "(---------------- hatch_angle_comment = " + str(hatch_angle_comment) + "---)")
    fichier.write("\n" + "$SC_ArcStepMode = 1")  # Permet de mettre en place le fractionnement des arc inutile ici

    fichier.write("\n" + "$SC_DEFINES = 0")

    if app.laser_needs_warm_up():
        LOG.info("Laser needs warmup")
        print("Laser needs warmup")
        # We only load the waiting time in the first iteration
        fichier.write(get_double_engraving_wait_cnc(warmup_time_in_seconds, laser_data_warmup, "WARM UP"))


    lastPoint = [0,0,0]
    mark_delay_time = float(laser_data["mark_delay"]) / 1000000

    # We load the pin we will use
    xl = math.fabs(x0 - x1)
    yl = math.fabs(y0 - y1)
    engraving_needs_protection = xl * yl < float(laser_data["protection_mode_area"])
    engraving_needs_protection = engraving_needs_protection and laser_data.get("use_new_cnc_method")
    engraving_needs_protection = engraving_needs_protection and app.getLaserParams(protection_pin_number).get("pinLoad")

    # IF the engraving is small we change the loaded pin for protection
    if engraving_needs_protection:
        LOG.info("Engraving is too small, we need the protection parameters")
        vitesse = int(laser_data_protection.get("vitesse"))
        nombrePassage = int(laser_data_protection.get("nbpassage"))
        modeDoubleHatch = laser_data_protection.get("doublehatch") == "1"
        pinNumber = str(protection_pin_number)

    # ------------------------------------------------------------------------
    # SPECIAL LOGIC TO "DOUBLE ENGRAVING" WITH SOME WAITING TIME IN THE MIDDLE
    # ------------------------------------------------------------------------
    double_engraving_seconds = new_prod_data.get("double_engraving_wait_time")
    double_engraving = "double_engraving_wait_time" in new_prod_data and double_engraving_seconds is not ""
    double_engraving_n = 1
    LOG.info("Is double engraving active? " + str(double_engraving))
    if double_engraving:
        double_engraving_n = 2

    engravingTime = 0

    # Usually double_engraving_n will be just one, but if we need double_engraving, it will be two
    for double_engraving_i in range(double_engraving_n):
        ## ici commence la gravure
        fichier.write("\n" + "$SC_LoopCount = " + str(nombrePassage))
        # We load the pen we will use
        fichier.write("\n" + "SC_Start_Entity")
        fichier.write("\n" + "SC_TOOL_LOAD[" + str(pinNumber) + "]")
        # Calling figure to engrave
        fichier.write("\n" + "CALL Figure")
        fichier.write("\n" + "SC_End_Entity")

        # Adding logic for the waiting time in double_engraving
        if double_engraving and double_engraving_i < double_engraving_n-1:
            # We only load the waiting time in the first iteration
            fichier.write(get_double_engraving_wait_cnc(double_engraving_seconds))

    if "SECOND_SET_PARAMETERS" in laser_data:
        # adding pin for second set of engraving
        fichier.write("\n" + "SC_TOOL_LOAD[" + str(laser_data.get("SECOND_SET_PARAMETERS").get("pinLoad")) + "]")
        fichier.write("\n" + "$SC_LASERPOWER = " + str(laser_data.get("SECOND_SET_PARAMETERS").get("puissance")))
        fichier.write("\n" + "$SC_FREQUENCY = " + str(laser_data.get("SECOND_SET_PARAMETERS").get("frequence")))
        fichier.write("\n" + "$SC_STDBYPERIOD = 0.0")
        fichier.write("\n" + "$SC_JUMPSPEED = 9000.0")
        fichier.write("\n" + "$SC_MARKSPEED = " + str(laser_data.get("SECOND_SET_PARAMETERS").get("vitesse")))
        fichier.write("\n" + "$SC_JUMPDELAY = 100.0")
        fichier.write("\n" + "$SC_MARKDELAY = " + str(laser_data.get("SECOND_SET_PARAMETERS").get("mark_delay")))
        fichier.write("\n" + "$SC_POLYDELAY = 0")
        fichier.write("\n" + "$SC_LASERONDELAY = 30.0")
        fichier.write("\n" + "$SC_LASEROFFDELAY = 130.0")
        fichier.write("\n" + "SC_TOOL_SAVE[" + str(laser_data.get("SECOND_SET_PARAMETERS").get("pinLoad")) + "]")

        # Calling figure to engrave
        fichier.write("\n" + "$SC_LoopCount = " + str(laser_data.get("SECOND_SET_PARAMETERS").get("nbpassage")))
        fichier.write("\n" + "SC_Start_Entity")
        # We load the pen we will use
        fichier.write("\n" + "SC_TOOL_LOAD[" + str(laser_data.get("SECOND_SET_PARAMETERS").get("pinLoad")) + "]")
        fichier.write("\n" + "CALL Figure")
        fichier.write("\n" + "SC_End_Entity")
        print("#@@# SECOND_SET_PARAMETERS:", laser_data["SECOND_SET_PARAMETERS"])
    else:
        print("#@@# SECOND_SET_PARAMETERS not in laser data:", laser_data)

    # Engraving time is per loop at this point
    print("##### 1 engravingTime = " + str(engravingTime))
    print("##### 1 nombrePassage = " + str(nombrePassage))
    print("##### 1 double_engraving_n = " + str(double_engraving_n))
    engravingTime = engravingTime * nombrePassage * double_engraving_n
    print("##### 2 engravingTime = " + str(engravingTime))
    if double_engraving:
        engravingTime = engravingTime + float(double_engraving_seconds) * (double_engraving_n-1)
    print("##### 3 engravingTime = " + str(engravingTime))
    app.storeOrderTempVariables("engravingTime", engravingTime)
    engravingTimeSeconds, engravingTimeMinutes = math.modf(engravingTime/60)
    engravingTimeFormat = str(int(engravingTimeMinutes)) + " minutes and " + str(int(engravingTimeSeconds*60)) + " seconds."
    app.storeOrderTempVariables("engravingTimeFormat", engravingTimeFormat)
    LOG.info("engravingTime = " + str(engravingTime))
    LOG.info("engravingTimeFormat = " + str(engravingTimeFormat))
    print("##### 4 engravingTime = " + str(engravingTime))
    print("engravingTimeFormat = " + str(engravingTimeFormat))

    # start figure
    fichier.write("\n" + "DFS Figure")
    fichier.write("\n" + "SC_TOOL[0]")
    start_CNC_write_time = time.time()
    i_lines = 0
    j_lines = 0
    init_j = 0
    n_jump_lines = int(laser_data.get("n_jump_lines"))
    while i_lines < len(PD):
        for item in PD[j_lines]:
            # we can change G1 by SC_TOOL if we want the laser to move at jump speed when there is no engraving
            fichier.write("\n" + "SC_TOOL[" + str(item[0]) + "]")
            fichier.write("\n" + "G" + str(item[0]) + " X" + str(item[1]) + " Y" + str(item[2]))
            speed = 9000 * (1 - item[0]) + vitesse * item[0]
            distance = round(((lastPoint[1] - item[1])**2 + (lastPoint[2] - item[2])**2)**0.5, 4)
            engravingTime += round(distance / speed, 4)
            lastPoint = item
        # Adding mark delay time for each point
        engravingTime += len(PD[j_lines]) * mark_delay_time/2
        i_lines = i_lines + 1
        j_lines = j_lines + n_jump_lines
        if j_lines >= len(PD):
            init_j = init_j + 1
            j_lines = init_j


    end_CNC_write_time = time.time()
    CNC_write_time = end_CNC_write_time - start_CNC_write_time
    cnc_sub_times["CNC_write_time"] = CNC_write_time
    LOG.info("TIME - CNC write time:" + str(CNC_write_time))

    print("##### 0 engravingTime = " + str(engravingTime))
    if modeDoubleHatch:
        start_PD2_time = time.time()
        PDvertical = app.obtainVerticalPD(L2D, pos_init, hatch, laser_data.get("use_new_cnc_method"))
        end_PD2_time = time.time()
        PD2_time = end_PD2_time - start_PD2_time
        cnc_sub_times["PD2_time"] = PD2_time
        LOG.info("TIME - PD2 time:" + str(PD2_time))
        i_lines = 0
        j_lines = 0
        init_j = 0
        while i_lines < len(PDvertical):
            for item in PDvertical[j_lines]:
                # we can change G1 by SC_TOOL if we want the laser to move at jump speed when there is no engraving
                fichier.write("\n" + "SC_TOOL[" + str(item[0]) + "]")
                fichier.write("\n" + "G" + str(item[0]) + " X" + str(item[1]) + " Y" + str(item[2]))
                speed = 9000 * (1 - item[0]) + vitesse * item[0]
                distance = round(((lastPoint[1] - item[1])**2 + (lastPoint[2] - item[2])**2)**0.5, 4)
                engravingTime += round(distance / speed, 4)
                lastPoint = item
            # Adding mark delay time for each point
            engravingTime += len(PDvertical[j_lines]) * mark_delay_time/2
            #fichier.write("\n" + "(---------------- New engraving line --------------)")
            i_lines = i_lines + 1
            j_lines = j_lines + n_jump_lines
            if j_lines >= len(PDvertical):
                init_j = init_j + 1
                j_lines = init_j
    fichier.write("\n" + "SC_TOOL[0]")
    fichier.write("\n" + "ENDDFS")
    # end figure


    ##Fermeture du fichier
    fichier.write("\n" + "SC_TOOL[0]")
    fichier.write("\n" + "G0 X0 Y0")
    fichier.write("\n" + "M2")
    fichier.close()
    return cnc_sub_times



# Gets the contours of an engraving
def get_image_contours(jpg_path, jpg_filename, thresh=230, maxval=255):
    print("getImageContours INIT")
    print("# Step 1: Reading image #")
    full_jpd_path = jpg_path + "/" + jpg_filename
    print("## reading imageee ", full_jpd_path)
    print("## img exists")
    im = cv.imread(full_jpd_path, cv.IMREAD_GRAYSCALE)
    # Flipping image for laser
    im = cv.flip(im, 0)
    print("# Step 2: Treating image to get proper contours #")
    # Getting threshold image
    ret, thresh_res = cv.threshold(im, thresh, maxval, cv.THRESH_BINARY_INV)
    # Getting contours of threshold image
    contours, hierarchy = cv.findContours(thresh_res, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
    # Removing main contour --> just a big rectangle containing all the image
    contours.pop(0)

    # We store the SVG generation only if wanted by a property
    if app.getConf("storeSVG") == "True":
        final_filename = "SVG2"
        width, height = im.shape
        svg_file_name = jpg_path + "/" + final_filename + ".svg"
        with open(svg_file_name, "w+") as f:
            f.write(f'<svg width="{width}" height="{height}" xmlns="http://www.w3.org/2000/svg">')
            for c in contours:
                f.write('<path d="M ')
                for i in range(len(c)):
                    x, y = c[i][0]
                    f.write(f"{x} {y} ")
                f.write('" style="stroke:black" fill="transparent"/>')
            f.write("</svg>")

    return contours


# Returns a CNC that will move the laser with the lowest power for X amount of seconds
def get_double_engraving_wait_cnc(seconds=10, parameters=None, comment="test"):
    # If we dont receive any parameters, we load the double engraving wait time parameters
    if parameters is None:
        parameters = app.get_wait_time_params()
    pin_number = parameters.get("pinLoad")
    # We are drawing a 50mm square --> 200mm permiter
    speed = float(parameters.get("vitesse"))
    loop_time = 200 / speed
    np = float(seconds) / loop_time

    cnc = ""
    cnc += "\n" + "(---------------- get_double_engraving_wait_cnc " + str(comment) + " --------------)"
    cnc += "\n" + "SC_TOOL_LOAD[" + str(pin_number) + "]"
    cnc += "\n" + "$SC_LASERPOWER = " + str(parameters.get("puissance"))
    cnc += "\n" + "$SC_FREQUENCY = " + str(parameters.get("frequence"))
    cnc += "\n" + "$SC_STDBYPERIOD = 0.0"
    cnc += "\n" + "$SC_JUMPSPEED = 9000.0"
    cnc += "\n" + "$SC_MARKSPEED = " + str(parameters.get("vitesse"))
    cnc += "\n" + "$SC_JUMPDELAY = 100.0"
    cnc += "\n" + "$SC_MARKDELAY = " + str(parameters.get("mark_delay"))
    cnc += "\n" + "$SC_POLYDELAY = 0"
    cnc += "\n" + "$SC_LASERONDELAY = 30.0"
    cnc += "\n" + "$SC_LASEROFFDELAY = 130.0"
    cnc += "\n" + "SC_TOOL_SAVE[" + str(pin_number) + "]"
    # Start "engraving"
    cnc += "\n" + "$SC_LoopCount = " + str(np)
    cnc += "\n" + "SC_Start_Entity"

    # We load the pen we will use
    cnc += "\n" + "SC_TOOL_LOAD[" + str(pin_number) + "]"
    cnc += "\n" + "SC_TOOL[0]"
    cnc += "\n" + "G0 X-25 Y-25"
    cnc += "\n" + "SC_TOOL[1]"
    cnc += "\n" + "G1 X-25 Y-25"
    cnc += "\n" + "G1 X-25 Y25"
    cnc += "\n" + "G1 X25 Y25"
    cnc += "\n" + "G1 X25 Y-25"
    cnc += "\n" + "G1 X-25 Y-25"
    cnc += "\n" + "SC_End_Entity"
    return cnc






def drawCrossToCalibrateLaser(onlyRedPointer):
    print("drawXToCalibrate INIT")
    calibrationFolder = app.getConfPath("Calibration_files_folder")
    # onlyRedPointer --> if true, we dont turn on the CO2 laser, we only move the red pointer
    if onlyRedPointer:
        fileName = app.getConf("calibration_red_filename")
        laserOn = 0
        numberOfPassages = 200
    else:
        fileName = app.getConf("calibration_CO2_filename")
        laserOn = 1
        numberOfPassages = 5
    cncFile = calibrationFolder + "/" + fileName + ".cnc"
    pinNumber = 1
    puissance = 50.0
    frequence = 30.0
    speed = 2000

    fichier = open(cncFile, 'w')
    # Commandes de base et creation variables START of CNC FILE
    fichier.write("G17")
    fichier.write("\n" + "G71")
    fichier.write("\n" + "G90")

    # Define du GCode
    fichier.write("\n" + "$SC_DEFINES = 1")
    fichier.write("\n" + "SC_SELECT_JOB[6]")
    fichier.write("\n" + "$SC_FIELDXMIN = -30")
    fichier.write("\n" + "$SC_FIELDXMAX =  30")
    fichier.write("\n" + "$SC_FIELDYMIN = -30")
    fichier.write("\n" + "$SC_FIELDYMAX =  30")
    fichier.write("\n" + "$SC_FIELDXGAIN =  1")
    fichier.write("\n" + "$SC_FIELDYGAIN =  1")
    fichier.write("\n" + "$SC_FIELDXOFFSET =  0")
    fichier.write("\n" + "$SC_FIELDYOFFSET =  0")
    fichier.write("\n" + "$SC_FIELDAXISSTATE = 3")

    # Definition of the laser characteristics
    fichier.write("\n" + "SC_TOOL_LOAD[" + str(pinNumber) + "]")
    fichier.write("\n" + "$SC_LASERPOWER = " + str(puissance))
    fichier.write("\n" + "$SC_FREQUENCY = " + str(frequence))
    fichier.write("\n" + "$SC_STDBYPERIOD = 0.0")
    fichier.write("\n" + "$SC_JUMPSPEED = 9000.0")
    fichier.write("\n" + "$SC_MARKSPEED = " + str(speed))
    fichier.write("\n" + "$SC_JUMPDELAY = 100.0")
    fichier.write("\n" + "$SC_MARKDELAY = 50")
    fichier.write("\n" + "$SC_POLYDELAY = 0")
    fichier.write("\n" + "$SC_LASERONDELAY = 30.0")
    fichier.write("\n" + "$SC_LASEROFFDELAY = 130.0")
    fichier.write("\n" + "SC_TOOL_SAVE[" + str(pinNumber) + "]")
    fichier.write("\n" + "$SC_ArcStepMode = 1")  # Permet de mettre en place le fractionnement des arc inutile ici
    fichier.write("\n" + "$SC_DEFINES = 0")

    ## ici commence la gravure
    fichier.write("\n" + "$SC_LoopCount = " + str(numberOfPassages))
    fichier.write("\n" + "SC_Start_Entity")

    # We load the pen we will use
    fichier.write("\n" + "SC_TOOL_LOAD[" + str(pinNumber) + "]")

    # Beginning of the engraving/marking --> we will draw a cross passing through the (0,0)
    # In this first line we decide it the laser is On (1) or Off (0)
    fichier.write("\n" + "SC_TOOL[" + str(laserOn) + "]")
    # G0 moves at jump speed
    # G1 moves at mark speed
    fichier.write("\n" + "G" + str(laserOn) + " X0.0 Y20.0")
    fichier.write("\n" + "G" + str(laserOn) + " X0.0 Y0.0")
    fichier.write("\n" + "G" + str(laserOn) + " X20.0 Y0.0")
    fichier.write("\n" + "G" + str(laserOn) + " X0.0 Y0.0")
    fichier.write("\n" + "G" + str(laserOn) + " X0.0 Y-20.0")
    fichier.write("\n" + "G" + str(laserOn) + " X0.0 Y0.0")
    fichier.write("\n" + "G" + str(laserOn) + " X-20.0 Y0.0")
    fichier.write("\n" + "G" + str(laserOn) + " X0.0 Y0.0")

    fichier.write("\n" + "SC_End_Entity")

    ##Fermeture du fichier
    fichier.write("\n" + "G0 X0 Y0")
    fichier.write("\n" + "M2")
    fichier.close()

    return createUnfFile(calibrationFolder, fileName)

def drawLadderToFindFocalDistance():
    pathDossierCalibration = app.getConfPath("Calibration_files_folder")
    FileName = "calibration_ladder"
    path = pathDossierCalibration+"/"+FileName+".cnc"
    LOG.info("path of file cnc to create for drawing ladder is",path)
    fichier = open(path, "w+")

    fichier.write("G17")
    fichier.write("\n" + "G71")
    fichier.write("\n" + "G90")

    fichier.write("\n" + "$SC_DEFINES = 1")
    fichier.write("\n" + "SC_SELECT_JOB[6]")
    fichier.write("\n" + "$SC_FIELDXMIN = -30")
    fichier.write("\n" + "$SC_FIELDXMAX =  30")
    fichier.write("\n" + "$SC_FIELDYMIN = -30")
    fichier.write("\n" + "$SC_FIELDYMAX =  30")
    fichier.write("\n" + "$SC_FIELDXGAIN =  1")
    fichier.write("\n" + "$SC_FIELDYGAIN =  1")
    fichier.write("\n" + "$SC_FIELDXOFFSET =  0")
    fichier.write("\n" + "$SC_FIELDYOFFSET =  0")
    fichier.write("\n" + "$SC_FIELDAXISSTATE = 3")

    fichier.write("\n" + "SC_TOOL_LOAD[1]")
    fichier.write("\n" + "$SC_LASERPOWER = 20.0")
    fichier.write("\n" + "$SC_FREQUENCY = 50.0")
    fichier.write("\n" + "$SC_STDBYPERIOD = 0.0")
    fichier.write("\n" + "$SC_JUMPSPEED = 9000.0")
    fichier.write("\n" + "$SC_MARKSPEED = 800")
    fichier.write("\n" + "$SC_JUMPDELAY = 100.0")
    fichier.write("\n" + "$SC_MARKDELAY = 50")
    fichier.write("\n" + "$SC_POLYDELAY = 0")
    fichier.write("\n" + "$SC_LASERONDELAY = 30.0")
    fichier.write("\n" + "$SC_LASEROFFDELAY = 130.0")
    fichier.write("\n" + "SC_TOOL_SAVE[1]")

    fichier.write("\n" + "$SC_ArcStepMode = 1")
    fichier.write("\n" + "$SC_DEFINES = 0")
    fichier.write("\n" + "$SC_LoopCount = 10")

    fichier.write("\n" + "SC_Start_Entity")
    fichier.write("\n" + "SC_TOOL_LOAD[1]")
    fichier.write("\n" + "SC_TOOL[1]")
    fichier.write("\n" + "G0 X-24 Y-10.0")
    fichier.write("\n" + "G1 X24.0 Y-10.0")
    fichier.write("\n" + "G0 X-24 Y10.0")
    fichier.write("\n" + "G1 X24.0 Y10.0")
    fichier.write("\n" + "G0 X-24 Y-10.0")
    fichier.write("\n" + "G1 X-24.0 Y10.0")
    fichier.write("\n" + "G0 X-20.0 Y-10.0")
    fichier.write("\n" + "G1 X-20.0 Y10.0")
    fichier.write("\n" + "G0 X-16.0 Y-10.0")
    fichier.write("\n" + "G1 X-16.0 Y10.0")
    fichier.write("\n" + "G0 X-12.0 Y-10.0")
    fichier.write("\n" + "G1 X-12.0 Y10.0")
    fichier.write("\n" + "G0 X-8.0 Y-10.0")
    fichier.write("\n" + "G1 X-8.0 Y10.0")
    fichier.write("\n" + "G0 X-4.0 Y-10.0")
    fichier.write("\n" + "G1 X-4.0 Y10.0")
    fichier.write("\n" + "G0 X0.0 Y-10.0")
    fichier.write("\n" + "G1 X0.0 Y10.0")
    fichier.write("\n" + "G0 X4.0 Y-10.0")
    fichier.write("\n" + "G1 X4.0 Y10.0")
    fichier.write("\n" + "G0 X8.0 Y-10.0")
    fichier.write("\n" + "G1 X8.0 Y10.0")
    fichier.write("\n" + "G0 X12.0 Y-10.0")
    fichier.write("\n" + "G1 X12.0 Y10.0")
    fichier.write("\n" + "G0 X16.0 Y-10.0")
    fichier.write("\n" + "G1 X16.0 Y10.0")
    fichier.write("\n" + "G0 X20.0 Y-10.0")
    fichier.write("\n" + "G1 X20.0 Y10.0")
    fichier.write("\n" + "G0 X24 Y-10.0")
    fichier.write("\n" + "G1 X24 Y10.0")
    fichier.write("\n" + "SC_End_Entity")

    fichier.write("\n" + "G0 X0 Y0")
    fichier.write("\n" + "M2")

    fichier.close()
    LOG.info("cnc file for drawing ladder to determinate focal distance have been created")

def drawHLineToDeterminateAngle():
    pathDossierCalibration = app.getConfPath("Calibration_files_folder")
    FileName = "calibration_angle"
    path = pathDossierCalibration+"/"+FileName+".cnc"
    LOG.info("path of file cnc to create for drawing horizontal line is" + str(path))
    fichier = open(path, "w+")

    fichier.write("G17")
    fichier.write("\n" + "G71")
    fichier.write("\n" + "G90")

    fichier.write("\n" + "$SC_DEFINES = 1")
    fichier.write("\n" + "SC_SELECT_JOB[6]")
    fichier.write("\n" + "$SC_FIELDXMIN = -30")
    fichier.write("\n" + "$SC_FIELDXMAX =  30")
    fichier.write("\n" + "$SC_FIELDYMIN = -30")
    fichier.write("\n" + "$SC_FIELDYMAX =  30")
    fichier.write("\n" + "$SC_FIELDXGAIN =  1")
    fichier.write("\n" + "$SC_FIELDYGAIN =  1")
    fichier.write("\n" + "$SC_FIELDXOFFSET =  0")
    fichier.write("\n" + "$SC_FIELDYOFFSET =  0")
    fichier.write("\n" + "$SC_FIELDAXISSTATE = 3")

    fichier.write("\n" + "SC_TOOL_LOAD[1]")
    fichier.write("\n" + "$SC_LASERPOWER = 20.0")
    fichier.write("\n" + "$SC_FREQUENCY = 50.0")
    fichier.write("\n" + "$SC_STDBYPERIOD = 0.0")
    fichier.write("\n" + "$SC_JUMPSPEED = 9000.0")
    fichier.write("\n" + "$SC_MARKSPEED = 800")
    fichier.write("\n" + "$SC_JUMPDELAY = 100.0")
    fichier.write("\n" + "$SC_MARKDELAY = 50")
    fichier.write("\n" + "$SC_POLYDELAY = 0")
    fichier.write("\n" + "$SC_LASERONDELAY = 30.0")
    fichier.write("\n" + "$SC_LASEROFFDELAY = 130.0")
    fichier.write("\n" + "SC_TOOL_SAVE[1]")

    fichier.write("\n" + "$SC_ArcStepMode = 1")
    fichier.write("\n" + "$SC_DEFINES = 0")
    fichier.write("\n" + "$SC_LoopCount = 10")

    fichier.write("\n" + "SC_Start_Entity")
    fichier.write("\n" + "SC_TOOL_LOAD[1]")
    fichier.write("\n" + "SC_TOOL[1]")
    fichier.write("\n" + "G0 X-29 Y0.0")
    fichier.write("\n" + "G1 X29.0 Y0.0")
    fichier.write("\n" + "SC_End_Entity")

    fichier.write("\n" + "G0 X0 Y0")
    fichier.write("\n" + "M2")

    fichier.close()
    LOG.info("cnc file for drawing horizontal line to determinate angle have been created")


############################################## NEW ENGRAVING METHODS ##############################################
# New method to create a CNC file from an image:
# We can have multiple pins, multiple hatchs, contour, hatching, etc...

# We will stop calling this --> imageToCncFile(imagePath,filePath, pinNumber)
# here --> process_image_thread(base64img, pinMode)
# and call this new function

# colorAndPENConf = {
# 	"color1": Array<int>,
# 	"color2": Array<int>,
# 	"color3": Array<int>,
# 	"color4": Array<int>,
# }

def new_methods_image_to_cnc(imagePath, colorAndPENConf):
    print("INIT new_methods_image_to_cnc")
    # Step 1: Laser Path Calculation: Start loop for each PEN.
    # Returns objects with PEN config, paths and red square coordinates
    used_PENS, calculated_paths, square_coordinates, expected_engraving_time = laser_path_calculations(colorAndPENConf, imagePath)
    print("--- used_PENS:", used_PENS)
    # Step 2: Create red rectangle CNC with last square coordinates
    app.createRectangleCNC(square_coordinates[0], square_coordinates[1], square_coordinates[2], square_coordinates[3])
    # Step 3: Write beginning of CNC file: Standard beginning and PEN configuration.
    # This also adds warm up if needed
    extra_warmup_time = create_beginning_of_cnc(used_PENS)
    expected_engraving_time += extra_warmup_time
    # Step 4: Loading default.cnc file to write the remaining parts: figures and CALL
    create_end_of_cnc(calculated_paths)
    # TODO Step 6: Calculate calculation times
    cnc_sub_times = {
        "L2D_time": 0,
        "PD_time": 0,
        "CNC_write_time": 0,
        "PD2_time": 0,
    }
    # Storing expected engraving time for display
    app.storeOrderTempVariables("engravingTime", expected_engraving_time)
    engravingTimeSeconds, engravingTimeMinutes = math.modf(expected_engraving_time/60)
    engravingTimeFormat = str(int(engravingTimeMinutes)) + " minutes and " + str(int(engravingTimeSeconds*60)) + " seconds."
    app.storeOrderTempVariables("engravingTimeFormat", engravingTimeFormat)
    return cnc_sub_times


# Creates a CNC file in the desired folder, adding only the Optic and PEN definition
def create_beginning_of_cnc(used_PENS):
    # If we warmup, the engraving will take a few seconds more
    extra_warmup_time = 0
    order_folder = app.getConfPath("order_folder")
    destination_file = order_folder + "/default.cnc"
    with open(destination_file, "w") as new_cnc:
        print("Creating CNC with Optic and PEN definition")
        new_cnc.write("G17")
        new_cnc.write("\n" + "G71")
        new_cnc.write("\n" + "G90")
        # -------------- OPTIC DEFINITION --------------
        new_cnc.write("\n" + "$SC_DEFINES = 1")
        new_cnc.write("\n" + "SC_SELECT_JOB[6]")
        # We changed this to 35 so when rotating the images for diagonal engraving, if there was a part of the engraving
        # with more than 30, it doesnt break the CNC
        # This causes a slight resizing of the engraving, which doesnt seem important from the preliminary tests
        # The resizing was a problem in the end, we come back to 30 since the problem we had that made us change to 35 is
        # now solved. The problem was with pos_init, that would move coordinates to the right and down, causing
        # some values to go over 30
        new_cnc.write("\n" + "$SC_FIELDXMIN = -30")
        new_cnc.write("\n" + "$SC_FIELDXMAX =  30")
        new_cnc.write("\n" + "$SC_FIELDYMIN = -30")
        new_cnc.write("\n" + "$SC_FIELDYMAX =  30")
        new_cnc.write("\n" + "$SC_FIELDXGAIN =  1")
        new_cnc.write("\n" + "$SC_FIELDYGAIN =  1")
        new_cnc.write("\n" + "$SC_FIELDXOFFSET =" + "  " + str(app.getX_Offset()))
        new_cnc.write("\n" + "$SC_FIELDYOFFSET =  0")
        new_cnc.write("\n" + "$SC_FIELDAXISSTATE = 3")

        # -------------- PEN DEFINITION --------------
        for PEN in used_PENS.keys():
            # Protection set of parameters
            new_cnc.write("\n" + "SC_TOOL_LOAD[" + str(PEN) + "]")
            new_cnc.write("\n" + "$SC_LASERPOWER = " + str(used_PENS[PEN].get("puissance")))
            new_cnc.write("\n" + "$SC_FREQUENCY = " + str(used_PENS[PEN].get("frequence")))
            new_cnc.write("\n" + "$SC_STDBYPERIOD = 0.0")
            new_cnc.write("\n" + "$SC_JUMPSPEED = 9000.0")
            new_cnc.write("\n" + "$SC_MARKSPEED = " + str(used_PENS[PEN].get("vitesse")))
            new_cnc.write("\n" + "$SC_JUMPDELAY = 100.0")
            new_cnc.write("\n" + "$SC_MARKDELAY = " + str(used_PENS[PEN].get("mark_delay")))
            new_cnc.write("\n" + "$SC_POLYDELAY = 0")
            new_cnc.write("\n" + "$SC_LASERONDELAY = 30.0")
            new_cnc.write("\n" + "$SC_LASEROFFDELAY = 130.0")
            new_cnc.write("\n" + "SC_TOOL_SAVE[" + str(PEN) + "]")

        new_cnc.write("\n" + "$SC_ArcStepMode = 1")
        new_cnc.write("\n" + "$SC_DEFINES = 0")

        # Laser needs warm up?
        if app.laser_needs_warm_up():
            warmup_time_in_seconds = app.get_warmup_time()
            laser_data_warmup = app.get_warm_up_params()
            LOG.info("Laser needs warmup")
            print("Laser needs warmup")
            # We only load the waiting time in the first iteration
            extra_warmup_time = float(warmup_time_in_seconds)
            new_cnc.write(get_double_engraving_wait_cnc(warmup_time_in_seconds, laser_data_warmup, "WARM UP"))

        # End CNC creation
    return extra_warmup_time


# Creates the final part of CNC file putting together all the other files and calling PENs
def create_end_of_cnc(calculated_paths):
    order_folder = app.getConfPath("order_folder")
    destination_file = order_folder + "/default.cnc"
    with open(destination_file, "a") as cnc_file:
        cnc_file.write("\n")
        # Step 4.1: Write CNC execution using only CALL function to call figures.
        for laser_path in calculated_paths:
            cnc_file.write("\n")
            cnc_file.write("$SC_LoopCount = " + str(laser_path.get("loopcount")) + "\n")
            cnc_file.write("SC_Start_Entity\n")
            cnc_file.write("SC_TOOL_LOAD[" + str(laser_path.get("PEN")) + "]\n")
            cnc_file.write("CALL " + str(laser_path.get("figure_name")) + "\n")
            cnc_file.write("SC_End_Entity\n")


        # TODO REMOVE this custom logic for Hennessy
        add_tiger = app.append_tiger_to_cnc()
        if add_tiger:
            print("HENNESSY - Custom logic Hennessy for Tiger")
            figure_name, cnc_path, nbpassage, PEN = app.create_hennessy_tiger_cnc_figure()
            cnc_file.write("\n")
            cnc_file.write("$SC_LoopCount = " + str(nbpassage) + "\n")
            cnc_file.write("SC_Start_Entity\n")
            cnc_file.write("SC_TOOL_LOAD[" + str(PEN) + "]\n")
            cnc_file.write("CALL " + str(figure_name) + "\n")
            cnc_file.write("SC_End_Entity\n")




        # Step 4.2: Finish CNC file
        cnc_file.write("\n" + "SC_TOOL[0]")
        cnc_file.write("\n" + "G0 X0 Y0")
        cnc_file.write("\n" + "M2")
        # Step 4.3: Append all CNC FIGURES temporary files created during look
        for laser_path in calculated_paths:
            if laser_path.get("append_DFS"):
                cnc_file.write("\n")
                appendable_file = order_folder + "/" + laser_path.get("figure_name") + ".cnc"
                # Appending figures to main CNC
                f = open(appendable_file, "r")
                cnc_file.write(f.read())
                f.close()

        # TODO REMOVE this custom logic for Hennessy
        if add_tiger:
            print("HENNESSY - Custom logic Hennessy for Tiger")
            product_id = app.getProductData().get("product_id")
            print("HENNESSY - product_id:", product_id)
            figure_name, cnc_path, nbpassage, PEN = app.create_hennessy_tiger_cnc_figure()
            cnc_file.write("\n")
            # Appending figures to main CNC
            f = open(cnc_path, "r")
            cnc_file.write(f.read())
            f.close()


# Calculates all the paths for all the colors
def laser_path_calculations(colorAndPENConf, imagePath):
    # Step 1: Creating objects to modify later
    # Creating main values for square containing engraving
    x0, x1, y0, y1 = 30.0, -30.0, 30.0, -30.0
    # This object will contain all the PEN configurations needed for the engraving
    used_PENS = {}
    # This object will contain all the laser paths (Figures) created, so we know if one is repeated or not and we can
    # append them later.
    calculated_paths = []
    # This is the expected time that the engraving will take
    expected_engraving_time = 0
    # Step 2.2: We iterate each color to calculate the paths
    for color in colorAndPENConf.keys():
        print("Starting calculations for color:", color)
        # Step 2.1: Iterate through all PENs in color
        for PEN in colorAndPENConf[color]:
            print("\tColor PEN:", PEN)
            # Step 2.2: Get PEN configuration
            PEN_config = app.getLaserParams(PEN)
            print("\tPEN_config:", PEN_config)
            # Step 2.3: If PEN_config is empty, we cant continue
            if PEN_config:
                figure_name = get_figure_name(color, PEN_config)
                append_DFS = False
                print("\t\tfigure_name:", figure_name)
                # Step 2.4: Check if current laser path has already been calculated
                if figure_name not in [(test_obj["figure_name"]) for test_obj in calculated_paths]:
                    print("\t\tFigure was not yet calculated")
                    append_DFS = True
                    # We need to create a new image for specific hatch of the PEN, since the dimensions of the image
                    # change the laser coordinates
                    imagePath_with_hatch = new_image_for_hatch(imagePath, PEN_config, color)
                    # Step 2.5: Calculate laser path and get square coordinates for red rectangle.
                    # This function already stores the CNC figure file in the order folder.
                    overwritten_PEN_config, square_coordinates, time = create_cnc_figure(color, imagePath_with_hatch, figure_name, PEN_config)
                    expected_engraving_time += time
                    print("\t\tfigure_name:", figure_name)
                    print("\t\toverwritten_PEN_config:", overwritten_PEN_config)
                    # Step 2.6 Overwrite main square_coordinates for red rectangle
                    x0 = min(x0, square_coordinates[0])
                    x1 = max(x1, square_coordinates[1])
                    y0 = min(y0, square_coordinates[2])
                    y1 = max(y1, square_coordinates[3])
                    # Step 2.7: Checking if PEN parameters must be overwritten by protection mode or wait time
                    if overwritten_PEN_config is not None:
                        # Protection needed
                        if "protection_mode_pen" in PEN_config:
                            print("\t\tProtection mode needed")
                            PEN = PEN_config.get("protection_mode_pen")
                        PEN_config = overwritten_PEN_config
                else:
                    print("\t\tFigure was already calculated!")
                # Step 2.8 Add PEN configuration to used_PENS
                if PEN not in used_PENS:
                    used_PENS[PEN] = PEN_config
                # Step 2.9 Add laser path parameters to calculated_paths
                calculated_paths.append({
                    "figure_name": figure_name,
                    "PEN": PEN,
                    "loopcount": get_nbpassage_for_PEN(PEN_config),
                    "append_DFS": append_DFS
                })
            else:
                print("\tERROR! received PEN does not exist in fichier info")
    square_coordinates = [x0, x1, y0, y1]




    # TODO REMOVE this custom logic for Hennessy
    add_tiger = app.append_tiger_to_cnc()
    if add_tiger:
        a, common_PEN = app.get_tiger_size_and_pos_init_from_product_id("")
        used_PENS[common_PEN] = app.getLaserParams(common_PEN)




    return used_PENS, calculated_paths, square_coordinates, expected_engraving_time


# Create a new image for specific hatch of the PEN and filter the color
# TODO when merging with DEV, take the function from DEV, not this one
def new_image_for_hatch(imagePath, PEN_config, color):
    hatch = float(PEN_config.get("hatch"))
    destination_path = imagePath.replace(".png", str(hatch) + ".png")
    img = Image.open(imagePath)
    # Calculating new dimensions for the image
    canvas_px = float(app.getConf("dmm")) / hatch
    size = canvas_px, canvas_px

    # Storing only this color
    color_int = [int(num) for num in color.split(",")]
    # destination_path = destination_path.replace(".png", "-" + color + ".png")

    # We store a filtered image only with the current color
    # This way when we resize and the pixels get blurred, we can just get all pixels with alpha != 0
    # and we dont need to worry about the blur
    orig_color = (color_int[0], color_int[1], color_int[2], color_int[3])
    replacement_color_background = (0, 0, 0, 0)
    data = np.array(img)
    #condition = np.where((data == orig_color).all(axis=-1))
    # Alpha condition
    opacity_filter = int(app.getConf("default_opacity_filter"))
    # TODO REMOVE we overwritte the opacity_filter for 2 Dior products for the PCD WW
    product_name = app.getOrderTempVariables("product_name")
    print("@@@ product_name:", product_name)
    if product_name == "jadore_edt/100ml" or product_name == "PERFUMES/125ml":
        print("@@@ JADORE ou LCP")
        opacity_filter = 125
        if app.getOrderTempVariables("fontToEngrave") == "Kunstler_Script.ttf":
            print("@@@ KUNSTLER")
            opacity_filter = 110
    # TODO REMOVE this is commented only for the Dior RIWW
    # condition = (data[:, :, 0] == color_int[0]) & (data[:, :, 1] == color_int[1]) & (data[:, :, 2] == color_int[2]) & (data[:, :, 3] > opacity_filter)
    condition = (data[:, :, 3] > opacity_filter)
    # Overwriting all pixels with backgound
    data[:] = replacement_color_background
    # Overwriting engraving pixels with color
    data[condition] = orig_color

    img = Image.fromarray(data, mode='RGBA')

    # Important that we leave the resizing use the default interpolation NEAREST. Otherwise it breaks the drawing
    img.thumbnail(size)
    img.save(destination_path, "PNG")


    # If contour, we need to create a JPG image filtering the color
    if PEN_config.get("engraving_mode") == "contour":
        # FIRST we create a new PNG only with the wanted color
        # Getting color in numbers
        #color_int = [int(num) for num in color.split(",")]
        data = np.copy(np.array(img))
        # Temporarily unpack the bands for readability
        red, green, blue, alpha = data.T
        wanted_area = (red == color_int[0]) & (blue == color_int[1]) & (green == color_int[2]) & (alpha != 0)

        data[..., :] = (255, 255, 255, 0)  # setting all pixels to 0
        data[..., :][wanted_area.T] = (color_int[0], color_int[1], color_int[2], 255)  # Transpose back needed

        im1 = Image.fromarray(data)
        # SECOND we transform this PNG to JPG
        # Storing received image as JPG in /Order folder
        bg = Image.new("RGB", im1.size, (255, 255, 255))
        bg.paste(im1, im1)
        jpg_filename = destination_path.replace(".png", "-JPG.jpg")
        bg.save(jpg_filename)
        bg.close()
        im1.close()
    img.close()
    #img2.close()
    #img_color.close()

    return destination_path






# Calculates the figure name given a set of parameters of the PEN
def get_figure_name(color, PEN_config):
    # Name: <color>_<hatch>_<engraving_mode>_<FEM>_<doublehatch>_<n_jump_lines>_<angle>.cnc
    replaced_color_for_cnc = color.replace(",", "_")
    replaced_hatch = PEN_config["hatch"].replace(".", "_")
    figure_name = replaced_color_for_cnc
    figure_name += "_" + replaced_hatch
    figure_name += "_" + PEN_config["engraving_mode"]
    figure_name += "_" + str(PEN_config["use_new_cnc_method"])
    figure_name += "_" + str(PEN_config["doublehatch"])
    figure_name += "_" + str(PEN_config["n_jump_lines"])
    figure_name += "_" + str(PEN_config["hatch_angle"])
    return figure_name


# This function creates a CNC file with a figure, corresponding to the filtered color and PEN CONFIG
def create_cnc_figure(color, imagePath, figure_name, PEN_config):
    expected_engraving_time = 0
    # We can overwrite the PEN config with protection mode or with wait time
    overwritten_PEN_config = None
    hatch = float(PEN_config.get("hatch"))
    L2D, pos_init_original = get_image_L2D_posinit(imagePath, PEN_config, hatch, color)

    # Removing empty pixels from borders of the engraving to improve performance
    # TODO TODO when hatch_angle is not 0, the red square is not correct
    x0, x1, y0, y1, L2D, pos_init, ids = app.get_square_pixel(L2D, pos_init_original, hatch)
    square_coordinates = [x0, x1, y0, y1]

    # Checking engraving type
    print("Checking engraving mode:", PEN_config.get("engraving_mode"))
    if PEN_config.get("engraving_mode") == "contour":
        print("Engraving mode contour")
        expected_engraving_time = write_contour_in_cnc(figure_name, square_coordinates, ids, PEN_config, pos_init_original)
    elif PEN_config.get("engraving_mode") == "wait":
        print("Engraving mode wait")
        overwritten_PEN_config = write_wait_in_cnc(figure_name, PEN_config)
        expected_engraving_time = float(PEN_config.get("wait_time"))
    else:
        # Hatching is the default engraving mode
        print("Engraving mode hatching")
        # ------------------------- Protection check ------------------------- #
        xl = math.fabs(x0 - x1)
        yl = math.fabs(y0 - y1)
        engraving_needs_protection = xl * yl < float(PEN_config["protection_mode_area"])
        engraving_needs_protection = engraving_needs_protection and PEN_config.get("use_new_cnc_method")
        engraving_needs_protection = engraving_needs_protection and "protection_mode_pen" in PEN_config
        if engraving_needs_protection:
            print("\t\t\tEngraving needs protection")
            overwritten_PEN_config = app.getProtectionLaserParams(PEN_config.get("pinLoad"), PEN_config.get("protection_mode_pen"))
            PEN_config = overwritten_PEN_config
        # ------------------------- END protection check ------------------------- #
        # If engraving needs protection, the engraving time will be different
        expected_engraving_time = engraving_mode_hatching(figure_name, PEN_config, L2D, pos_init)
    return overwritten_PEN_config, square_coordinates, expected_engraving_time






def get_image_L2D_posinit(imagePath, PEN_config, hatch, color):
    # Opening received image already with right hatch
    outT = Image.open(imagePath)
    # We add a 1 pixel border to the image to avoid error in CNC generation
    outT = add_margin(outT)
    outT = outT.transpose(Image.FLIP_TOP_BOTTOM)
    # TODO REMOVE ONLY FOR MARTELL
    pos_init_corrector_Martell = 0
    pn = app.getOrderTempVariables("product")
    if "Martell" in app.getConf("collection"):
        if pn == "cordon_bleu_extra/box_70cl":
            angle = 180
            pos_init_corrector_Martell = 0  # 2
            outT = outT.rotate(angle)
    # END TODO REMOVE ONLY FOR MARTELL
    # If mode is hatching, we must rotate the image to the desired angle
    rotation_hatching_needed = "hatch_angle" in PEN_config and PEN_config["hatch_angle"] and PEN_config["hatch_angle"] is not "0"
    angle = 0
    if PEN_config.get("engraving_mode") == "hatching" and rotation_hatching_needed:
        # Since rotation hatching is needed, we rotate image and then do a rotation transformation when
        # calculating the coordinates in function "obtainHorizontalPD()"
        print("##@# hatch_angle:", PEN_config.get("hatch_angle"))
        angle = int(PEN_config["hatch_angle"])
        outT = outT.rotate(angle, resample=0, expand=1, center=None, translate=None, fillcolor=None)
    # Storing variable to rotate laser
    app.storeOrderTempVariables("hatch_angle", angle)

    # Step X: Calculating shared properties
    # Calculating origin coordinates for laser
    pos_init = []
    pos_init.append(-outT.size[0] / 2 * hatch)
    pos_init.append(-outT.size[1] / 2 * hatch)

    # TODO REMOVE ONLY FOR MARTELL
    pos_init[1] = pos_init[1] + pos_init_corrector_Martell
    # END TODO REMOVE ONLY FOR MARTELL

    # Getting the pixel positions with engraving (L2D)
    start_L2D_time = time.time()
    L2D = getL2DfromImage_color_filter(outT, color)
    end_L2D_time = time.time()
    L2D_time = end_L2D_time - start_L2D_time
    LOG.info("TIME - L2D time:" + str(L2D_time))
    outT.close()
    return L2D, pos_init







# ------------------- INIT write figures CNC ------------------- #
# This functions has the logic for the engraving mode hatching
def engraving_mode_hatching(figure_name, PEN_config, L2D, pos_init):
    order_folder = app.getConfPath("order_folder")
    destination_file = order_folder + "/" + figure_name + ".cnc"
    expected_engraving_time = 0
    with open(destination_file, "w") as cncFigure:
        cncFigure.write("DFS " + str(figure_name) + "\n")
        # Do first hatching passages for single hatch
        expected_engraving_time += write_single_hatch_in_cnc(cncFigure, PEN_config, L2D, pos_init)
        # Checking if double hatch is needed
        modeDoubleHatch = PEN_config.get("doublehatch") == "1"
        if modeDoubleHatch:
            # Do second hatching passage for double hatch
            print("Double hatch needed")
            expected_engraving_time += write_single_hatch_in_cnc(cncFigure, PEN_config, L2D, pos_init, False)
        cncFigure.write("\n" + "ENDDFS" + "\n")
    print("expected_engraving_time --> hatching per loop:", expected_engraving_time)
    expected_engraving_time = expected_engraving_time * float(PEN_config.get("nbpassage"))
    print("expected_engraving_time --> hatching total:", expected_engraving_time)
    return expected_engraving_time


# This functions writes the laser movements for the hatching mode
def write_single_hatch_in_cnc(cncFigure, PEN_config, L2D, pos_init, horizontal=True):
    hatch = float(PEN_config.get("hatch"))
    cnc_sub_times = {}

    # Transforming True/False "pixels" to coordinates (PD)
    start_PD_time = time.time()
    # PD is different if the hatch is horizontal or vertical
    if horizontal:
        PD = app.obtainHorizontalPD(L2D, pos_init, hatch, PEN_config.get("use_new_cnc_method"))
    else:
        PD = app.obtainVerticalPD(L2D, pos_init, hatch, PEN_config.get("use_new_cnc_method"))
    end_PD_time = time.time()
    PD_time = end_PD_time - start_PD_time
    cnc_sub_times["PD_time"] = PD_time
    LOG.info("TIME - PD time:" + str(PD_time))

    lastPoint = [0, 0, 0]
    mark_delay_time = float(PEN_config["mark_delay"]) / 1000000

    start_CNC_write_time = time.time()
    expected_engraving_time = 0
    i_lines = 0
    j_lines = 0
    init_j = 0
    n_jump_lines = int(PEN_config.get("n_jump_lines"))
    vitesse = float(PEN_config.get("vitesse"))
    while i_lines < len(PD):
        for item in PD[j_lines]:
            # Should we add this if?
            #if item[1] > 30 or item[2] > 30:
            #    print("ALERT!!!! < 30")
            # we can change G1 by SC_TOOL if we want the laser to move at jump speed when there is no engraving
            cncFigure.write("\n" + "SC_TOOL[" + str(item[0]) + "]")
            cncFigure.write("\n" + "G" + str(item[0]) + " X" + str(item[1]) + " Y" + str(item[2]))
            speed = 9000 * (1 - item[0]) + vitesse * item[0]
            distance = round(((lastPoint[1] - item[1]) ** 2 + (lastPoint[2] - item[2]) ** 2) ** 0.5, 4)
            expected_engraving_time += round(distance / speed, 4)
            lastPoint = item
        # Adding mark delay time for each point
        expected_engraving_time += len(PD[j_lines]) * mark_delay_time / 2
        i_lines = i_lines + 1
        j_lines = j_lines + n_jump_lines
        if j_lines >= len(PD):
            init_j = init_j + 1
            j_lines = init_j
    end_CNC_write_time = time.time()
    CNC_write_time = end_CNC_write_time - start_CNC_write_time
    cnc_sub_times["CNC_write_time"] = CNC_write_time
    LOG.info("TIME - CNC write time:" + str(CNC_write_time))

    return expected_engraving_time


# This functions writes the laser movements for the contour mode
def write_contour_in_cnc(figure_name, square_coordinates, ids, PEN_config, pos_init):
    # We should calculated the expected engraving time for the contour
    expected_engraving_time = 0
    lastPoint = [0, 0]
    hatch = float(PEN_config.get("hatch"))
    mark_speed = float(PEN_config.get("vitesse"))
    jump_speed = 9000
    # Starting the calculations...
    order_folder = app.getConfPath("order_folder")
    destination_file = order_folder + "/" + figure_name + ".cnc"
    jpg_filename = "default" + str(hatch) + "-JPG.jpg"
    # Getting contours to engrave
    contours = get_image_contours(order_folder, jpg_filename)
    pos_init_contour = []
    # For some reason, the indexes coming from contours are shifted twice
    x0 = square_coordinates[0]
    y0 = square_coordinates[2]
    #pos_init_contour.append(-((ids[0] - 2) * hatch) + x0)
    #pos_init_contour.append(-((ids[2] - 2) * hatch) + y0)
    pos_init_contour = [pos_init[0] + 2*hatch, pos_init[1] + 2*hatch]
    with open(destination_file, "w") as cncFigure:
        # --------------------------- CONTOURS --------------------------- #
        cncFigure.write("DFS " + str(figure_name) + "\n")
        for c in contours:
            cncFigure.write("\n" + "SC_TOOL[0]")
            idx0, idy0 = c[0][0]
            x0, y0 = app.get_xmm_ymm(idx0, idy0, pos_init_contour, hatch)
            cncFigure.write(f"\nG0 X{x0} Y{y0} ")
            distance = round(((lastPoint[0] - x0) ** 2 + (lastPoint[1] - y0) ** 2) ** 0.5, 4)
            expected_engraving_time += round(distance / jump_speed, 4)
            cncFigure.write("\n" + "SC_TOOL[1]")
            for i in range(len(c)):
                idx, idy = c[i][0]
                x, y = app.get_xmm_ymm(idx, idy, pos_init_contour, hatch)
                # f.write("\n" + "G" + str(item[0]) + " X" + str(item[1]) + " Y" + str(item[2]))
                cncFigure.write(f"\nG1 X{x} Y{y} ")
                distance = round(((lastPoint[0] - x) ** 2 + (lastPoint[1] - y) ** 2) ** 0.5, 4)
                expected_engraving_time += round(distance / mark_speed, 4)
                lastPoint = [x, y]

        cncFigure.write("\n" + "SC_TOOL[0]")
        # ------------------------- END CONTOURS ------------------------- #
        cncFigure.write("\n" + "ENDDFS" + "\n")
        print("expected_engraving_time --> contour per loop:", expected_engraving_time)
        expected_engraving_time = expected_engraving_time * float(PEN_config.get("nbpassage"))
        print("expected_engraving_time --> contour total:", expected_engraving_time)
    return expected_engraving_time


# This functions writes the laser movements for the hatching mode
def write_wait_in_cnc(figure_name, PEN_config):
    order_folder = app.getConfPath("order_folder")
    destination_file = order_folder + "/" + figure_name + ".cnc"

    # PEN configuration for wait time is pre configured
    wait_time_params = app.get_wait_time_params(PEN_config.get("pinLoad"))
    for param in wait_time_params:
        PEN_config[param] = wait_time_params[param]

    wait_time = PEN_config.get("wait_time")
    new_nbpassages = get_nbpassage_for_PEN(PEN_config, 200)
    PEN_config["nbpassage"] = new_nbpassages

    with open(destination_file, "w") as cncFigure:
        cncFigure.write("(---------------- waiting time: " + str(wait_time) + " seconds --------------)\n")
        cncFigure.write("DFS " + str(figure_name) + "\n")
        # Writing figure for waiting time
        cncFigure.write("SC_TOOL[0]\n")
        cncFigure.write("G0 X-25 Y-25\n")
        cncFigure.write("SC_TOOL[1]\n")
        cncFigure.write("G1 X-25 Y-25\n")
        cncFigure.write("G1 X-25 Y25\n")
        cncFigure.write("G1 X25 Y25\n")
        cncFigure.write("G1 X25 Y-25\n")
        cncFigure.write("G1 X-25 Y-25\n")
        cncFigure.write("\n" + "ENDDFS" + "\n")
    return PEN_config
# ------------------- END write figures CNC ------------------- #


# Returns the number of passages for a certain PEN
def get_nbpassage_for_PEN(PEN_config, square_distance=200):
    if "engraving_mode" in PEN_config and PEN_config.get("engraving_mode") == "wait":
        wait_time = PEN_config.get("wait_time")
        if not wait_time:
            LOG.warning("wait_time was not defined in PEN, we get the default value")
            wait_time = app.getConf("default_wait_time")
        # We are drawing a 50mm square --> 200mm permiter
        speed = float(PEN_config.get("vitesse"))
        loop_time = square_distance / speed
        nbpassage = float(wait_time) / loop_time
    else:
        nbpassage = PEN_config.get("nbpassage")
    return nbpassage
############################################## END NEW ENGRAVING METHODS ##############################################

