import Common.functions as app
import Common.ServerConnection.Connection as server
import Common.ServerConnection.updateSystem as updateSystem
import json, os
import Common.LogsDWS as LOG


def getUserInfo():
    uc = {}
    if os.path.exists(app.getConfPath("user_conf_file")):
        with open(app.getConfPath("user_conf_file"), "r", encoding="utf-8") as json_file:
            uc = json.load(json_file)
            if uc.get("token"):
                print("tokenId:", uc.get("token"))
                uc["user_is_configured"] = True
    return uc


def saveUserInfo(fi_data):
    print("SAving info:", fi_data)
    with open(app.getConfPath("user_conf_file"), "w+", encoding="utf-8") as fi:
        # Replacing default values with values from table
        fi.seek(0)
        json.dump(fi_data, fi, indent=2, ensure_ascii=False)
        fi.truncate()


def validateToken(token):
    print("token:", token)
    url = app.getConf("tanpet_api_base_url") + app.getConf("tanpet_api_get_user_data")
    headers = {
        "Authorization": token
    }
    body = {
        "token": token
    }
    res = server.makeReq(url, body, headers)
    if res and res.status_code == 200:
        user_info_from_server = res.json()
        LOG.info("User data retrieved correctly: " + str(user_info_from_server))
        user_info_from_server["token"] = user_info_from_server["company"]
        saveUserInfo(user_info_from_server)
        user_info_from_server["status"] = "OK"
    else:
        LOG.error("Error getting user data")
        user_info_from_server = {"status": "KO"}
    return user_info_from_server

def getAvailableProducts(token):
    url = app.getConf("tanpet_api_base_url") + app.getConf("tanpet_api_get_products")
    headers = {
        "Authorization": token
    }
    res = server.makeReq(url, {}, headers)
    json_response = {}
    if res and res.status_code == 200:
        json_response = res.json()
        LOG.info("Products retrieved correctly: " + str(len(json_response)))
    else:
        json_response = {"status": res.status_code}
        LOG.error("Error getting list of products")
    print("json_response:", json_response)
    return json_response


def getDisplayParametersProduct():
    prod_params_display = [
        {
            "id": "hatch",
            "display_name": "Hatch",
            "class": "",
            "hint": "In mm with a . as decimal separator. Might be overwritten by laser parameters."
        },
        {
            "id": "max_number_of_lines",
            "display_name": "Max number of lines",
            "class": "",
            "hint": "Number of lines of text allowed to be engraved on the product. DEFAULT: 1000"
        },
        {
            "id": "text_color",
            "display_name": "Text color",
            "class": "",
            "hint": "Color of the text displayed on the app. Can be text, HEX or RGBA. DEFAULT: black"
        },
        {
            "id": "platform_number",
            "display_name": "Platform number",
            "class": "",
            "hint": "Number of the platform for DS1 machine: 0, 1, 2, 3 or -1. DEFAULT: 2"
        },
        {
            "id": "platform_base_height",
            "display_name": "Platform base height",
            "class": "",
            "hint": "For custom platforms, height of the base measuring from the \"level 0\" of the DS1 machine."
        },
        {
            "id": "platform_base_height_xl",
            "display_name": "Platform base height XL",
            "class": "",
            "hint": "For custom platforms, height of the base measuring from the \"level 0\" of the DS2 machine."
        },
        {
            "id": "engraving_angle",
            "display_name": "Text angle (engraving_angle)",
            "class": "",
            "hint": "Angle of the text in degrees. DEFAULT: 0"
        },
        {
            "id": "maxWidthMM",
            "display_name": "Max Width",
            "class": "",
            "hint": "Max allowed width in mm for the engraving."
        },
        {
            "id": "maxHeightMM",
            "display_name": "Max Heigth",
            "class": "",
            "hint": "Max allowed heigth in mm for the engraving."
        },
        {
            "id": "radius",
            "display_name": "Radius",
            "class": "",
            "hint": "If the product is curved, radius in mm. DEFAULT: 999999"
        },
        {
            "id": "X_Offset",
            "display_name": "X offset",
            "class": "",
            "hint": "In mm, applies an horizontal offset to the engraving. Not visible on the app, it is done when creating the CNC. DEFAULT: 0"
        },
        {
            "id": "double_engraving_wait_time",
            "display_name": "double_engraving_wait_time",
            "class": "hidden deprecated",
            "hint": "double_engraving_wait_time"
        },
        {
            "id": "show_slider",
            "display_name": "Show slider",
            "class": "",
            "hint": "\"True\" or \"False\". If \"True\" and the font has autosizing, the app will display a slider to change the font size at will. DEFAULT: False"
        },
        {
            "id": "emojis",
            "display_name": "Emojis",
            "class": "hidden not-ready",
            "hint": "List of emojis allowed on this product. Overwrites the collection emojis. DEFAULT: empty"
        },
        {
            "id": "show_font_size_buttons",
            "display_name": "Show font size buttons",
            "class": "hidden not-ready",
            "hint": "\"True\" or \"False\". If \"True\" and the font has autosizing, the app will display buttons to change the fontsize. DEFAULT: False"
        },
        {
            "id": "defaultKeyboardFont",
            "display_name": "Default keyboard font",
            "class": "",
            "hint": "\"True\" or \"False\". If \"True\", the font used for the text area of the product is the default one, not the selected font. DEFAULT: False"
        },
        {
            "id": "custom_position_image",
            "display_name": "Custom position image",
            "class": "",
            "hint": "Name of the media (photo or video) to display in 3rd screen for positioning this product. Overwrites previous configuration. DEFAULT: managed by platform number"
        },
        {
            "id": "Keyboard",
            "display_name": "Keyboard",
            "class": "hidden not-ready",
            "hint": "Name of the keyboard to use for this product. Overwrites the value of the collection. DEFAULT: Empty"
        },
        {
            "id": "propotion_img",
            "display_name": "Propotion image",
            "class": "",
            "hint": "Decimal that crops the image from the top proportionally. For tall products. DEFAULT: 1"
        }
    ]
    return prod_params_display


# Returns all the available fonts in the fonts folder
def getAvailableFonts(folders_path=app.getConfPath("fonts_folder")):
    fonts_list = []
    files = os.listdir(folders_path)
    for file in files:
        file_l = file.lower()
        if file_l.endswith(".ttf") or file_l.endswith(".otf") or file_l.endswith(".ttc") or file_l.endswith(".woff")\
                or file_l.endswith(".woff2"):
            fonts_list.append(file)
        elif os.path.isdir(folders_path + "/" + file):
            print("\t is folder")
            fonts_in_folder = getAvailableFonts(folders_path + "/" + file)
            fonts_in_folder = [file + "/" + font for font in fonts_in_folder]
            print("\t fonts_in_folder:", fonts_in_folder)
            fonts_list = fonts_list + fonts_in_folder
    return fonts_list


def return_help():
    return {
        "laserPin": "PENs à utiliser avec cette police. Tu peux mettre plusieurs PENs séparés par une virgule (pas de spaces).",
        "penColor": "La couleur doit être en RGBA, comme ça: 0,0,0,255",
        "prodDimensions": "Width and height of the product in mm",
        "hatch": "Ce hatch aplique uniquement à ce PEN",
        "engravingMode": "Hatching: Gravure avec hatch\nContour: Gravure uniquement contour\nWait: Ne grave pas, attend X secondes",
        "puissance": "Entre 1 et 100",
        "frequence": "Entre 0.001 et 50",
        "vitesse": "Vitesse du laser quand il grave. Normalement entre 500 et 4000",
        "nbpassage": "Nombre de passages à faire.",
        "doublehatch": "Uniquement disponible si \"hatching\" est sélectioné.\nSi actif, le hatch sera en horizontal et vertical",
        "use_new_cnc_method": "Uniquement disponible si \"hatching\" est sélectioné.\nActiver le FEM (Fast Engraving Mode). Essayer de l'activer par défaut",
        "hatch_angle": "Uniquement disponible si \"hatching\" est sélectioné.\nAngle des movements du laser. 0 par défaut",
        "mark_delay": "Temps que le laser attends après une ligne gravé avant commencer à se déplacer. Entre 50 et 2000",
        "n_jump_lines": "Uniquement disponible si \"hatching\" est sélectioné.\nSaut de ligne pour le hatch.",
        "protection_mode_area": "Uniquement disponible si \"hatching\" est sélectioné.\nSi la gravure est plus petite que cette area, le PEN protection mode ecrasera le PEN selectioné.",
        "protection_mode_pen": "Uniquement disponible si \"hatching\" est sélectioné.\nPEN ID pour ecraser les paramètres s'il faut protection",
        "wait_time": "Uniquement disponible si \"wait\" est sélectioné.\nTemps en secondes que le laser va attendre entre."
    }

# TODO change these parameters for good materials
def predefind_laser_parameters_grid():
    return {
        "glass": {
            "puissance": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": True
            },
            "vitesse": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": True
            },
            "frequence": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            },
            "mark_delay": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            },
            "n_jump_lines": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            },
            "nbpassage": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            }
        },
        "paper": {
            "puissance": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": True
            },
            "vitesse": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": True
            },
            "frequence": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            },
            "mark_delay": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            },
            "n_jump_lines": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            },
            "nbpassage": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            }
        },
        "metal": {
            "puissance": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": True
            },
            "vitesse": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": True
            },
            "frequence": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            },
            "mark_delay": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            },
            "n_jump_lines": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            },
            "nbpassage": {
                "min": 0,
                "max": 100,
                "def": 70,
                "activeGrid": False
            }
        }
    }
