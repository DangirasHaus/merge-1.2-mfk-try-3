import os, time, json, io
import math, random
from PIL import Image, ImageDraw, ImageFont
import traceback
import numpy as np
from shutil import copyfile
from datetime import date, datetime
import Common.LogsDWS as LOG
import Common.LiteralsDWS as L
import socket
import os.path
from os import path
from sys import platform
from ftplib import FTP
import subprocess

import requests as req

# Returns the IP in windows and linux, python 2 and 3
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = "127.0.0.1"
    finally:
        s.close()
    return IP


# Returns the laser IP depending on the router IP
def getLaserIP():
    host_ip = get_ip().split(".")
    endIP = getConf("laser_IP")
    laser_ip = host_ip[0] + "." + host_ip[1] + "." + host_ip[2] + "." + endIP
    return "192.168.168.2"


def getIpAdress():
    host_ip = get_ip()
    print(host_ip)
    half_RpiIp = "."+host_ip.split(".")[2]+"."+host_ip.split(".")[3]
    print(half_RpiIp)
    return (half_RpiIp)

# GLOBAL VARIABLES to store in memory and avoid opening configuration files multiple times
fullTextData = None
globalConfData = None
globalLaserConfData = None
globalCollectionConfData = None
currentProduct = None
productData = None
# This should be the only time we need to obtain the dir name
dir_path = os.path.dirname(os.path.realpath(__file__))
global_conf_file_path = dir_path + "/../App_configuration/conf.properties"
################Functions of fixLiterals##########################################################################
####################################################################################################################
def prepareData(FilePath):
    File = io.open(FilePath, mode="r", encoding="utf-8")
    Data = File.read()
    List = Data.splitlines()
    one_lang_dict = {}
    for x in List:
        if "=" in x and x.split("=")[1]:
            one_lang_dict[x.split("=")[0]] = x.split("=")[1]
        else:
            LOG.info("Char = not found! Please check if the file "+FilePath+" is well written")
    File.close()
    return (one_lang_dict)


def addMissingLiteralsToFile(M_dict,filepath):
    file = io.open(filepath, mode="a", encoding="utf-8")
    for key in M_dict:
        if M_dict[key]:
            logmessage = "\tM_dict[key] is: "+M_dict[key]
            LOG.info(logmessage.encode("utf-8"))
            file.write("\n"+key+"="+M_dict[key])
    file.close()

def getMissingLiterals(ff):
    Mdict = {}
    for key in ff:
        if "+" in key:
            f = key.split("+")[0]
            n = str(key.split("+")[1])
            if f in Mdict.keys():
                Mdict[f][n] = ff[key][0]
            else:
                Mdict[f] = {}
                Mdict[f][n] = ff[key][0]
    return(Mdict)

#This function give as a list of name of files from a path folder
def filesLister(path):
    list = []
    for x in os.listdir(path):
        LOG.info(os.listdir(path))
        LOG.info(x)
        if "." in x:
            if x.split(".")[1] == "txt":
                list.append(x)
    return(list)


def show_option_for_red_rectangle():
    collection_value = getCollectionConf("engravingWithOnlyRedPointer")
    if not collection_value:
        collection_value = getConf("engravingWithOnlyRedPointer")
    return collection_value

def get_emojis_for_product(product_data, SAFE_product=False):
    # By default, no emojis
    emojis = None
    # If emojis is defined in product, we overwrite them
    if product_data.get("emojis"):
        emojis = product_data.get("emojis")
    # If emojis is NOT defined in product, we get from collection
    # If we are loading a SAFE product, we dont read collection emojis
    elif getCollectionConf("emojis") and not SAFE_product:
        emojis = getCollectionConf("emojis")
    return emojis


#this function compare all the literal files with the original one
def prepareGlobalData(originalFilePath,path):
#first we get the list of literal files except the original one "Literals.txt"
    listOfFiles = filesLister(path)
    LOG.info("listOfFiles: "+str(listOfFiles))
#then we prepare the global dictionnary that contains a dictionnary for each file iterating the files
    main_dict = {}
    LOG.info("for each file we prepare a dictionary")
    for file in listOfFiles:
        LOG.info("#######Now preparing the "+file+" file##########")
        file_dict = prepareData(path+"\\"+file)
        main_dict[file] = file_dict
        logmessage = "the file_dict of the "+file+" is: "+str(file_dict)
        LOG.info(logmessage.encode("utf-8"))
        LOG.info("#####END preparing the "+file+" file##########")
    LOG.info("main dictionary that contains all dictionaries (ech name of file is the key and each dictionary language is the value)  is: ", main_dict)

    return (main_dict)

######################### end for fucntions Fixliterals####################################
#############################################################################################

#####Fucntions for launchPylint#########"
def getPylintResultOfOneModule(module_path):
    output = subprocess.run(["pylint", module_path], stdout=subprocess.PIPE)
    ouputAsstring = str(output.stdout)
    if "Your code has been rated at" in ouputAsstring:
        result = ouputAsstring.split("Your code has been rated at ")[1]
        result = result.replace("\\r", "")
        result = result.replace("\\n", "")
        result = result.replace("\\x1b[0m'", "")
        ResultNumber = float(result.split("/")[0])
    else:
        #if we don't get teh result we send "0"
        ResultNumber = 0
    return ResultNumber

#this function give as all AAlyce python files and make the test pylint on them
def getPylintFolderResult(path, result_dict):
    os.system("cd " + path)
    local_list = os.listdir( path )
    LOG.info("the local list is " + str(local_list))
    for element in local_list:
        LOG.info("we will see if this " + element + "is a folder or a file..")
        if os.path.isdir(path + "/" + str(element)):
            LOG.info("Its a folder")
            result_dict = getPylintFolderResult(path+"/"+str(element), result_dict)
        elif element.endswith(".py"):
            LOG.info("it's a pyhton file")
            result_dict[element] = getPylintResultOfOneModule(path + "/" + str(element))

    return result_dict

#######end of functions pylint#########


# Checks if there are no trainees stored
def do_general_training():
    training = False
    trainees_list_path = getConfPath("trainees_list_path")
    if getConf("debug_node") == "False" and not path.exists(trainees_list_path):
        LOG.info("General training is needed")
        training = True
    return training

# Check file exist and if it is get the file as list of lines
def askForLaserFileFromMachine(laser_conf_file): #the name given to this fucntion must be the same as the file laser parameters stored on the tablet
    destination = getLaserIP()  #Ip du USCserver (statique)
    FTPTimeout = int(getConf("laser_FTP_timeout"))
    LOG.info("laser_conf_file:" + str(laser_conf_file))
    try:
        with FTP(destination, timeout=FTPTimeout) as ftp:
            try:
                ftp.login()
                ftp.cwd("misc") #folder where the file was stored when machine got its intialisation by hardware team
                LOG.info("ftp connected and in folder ok")
                conf_lines = []
                res = ftp.retrlines('RETR ' + laser_conf_file, conf_lines.append)
                LOG.info(res)
                ftp.close()
                fileExist = True
                return (fileExist, conf_lines)
            except Exception as e:
                conf_lines = []
                LOG.info("Error opening conf file:" + str(e)) #file doesn't exist or the name given not good
                ftp.close()
                fileExist = False
                return (fileExist, conf_lines)
    except Exception as error:
        LOG.info("Error connecting on FTP:" + str(error))

# function to transform list of lines to a dict
def getConflineASDict(conf_lines):
    laserParams = {}
    for i in range(len(conf_lines)):
        #print("iteration", i ,"laserParams is", laserParams)
        if len(conf_lines[i].split("=")) > 1:
            value = conf_lines[i].split("=")[1]
            laserParams[conf_lines[i].split("=")[0]] = value
    return laserParams

# function to get laser parameter from laser Conf File and return it as list
def askForLaserFileFromTablet():
    laserConfFilePath = getConfPath("laserConfFilePath")
    try:
        file = open(laserConfFilePath)
        data = file.read().replace("\r", "")
        file.close()
        Conf_lines = data.split('\n')
        fileExist = True
    except Exception as error:
        LOG.info("An error was found:" + str(error))
        Conf_lines = []
        fileExist = False
    return (fileExist, Conf_lines)


def getConf(fieldToRetrieve):
    # Retrieves the value of a property stored in the generic configuration file
    global globalConfData
    global globalLaserConfData
    if globalConfData is None:
        fichier = open(global_conf_file_path)
        full_text = fichier.read().replace("\r", "")
        fichier.close()
        lines = full_text.split('\n')
        print(lines)
        LOG.info("getConf - Product information = " + str(lines))
        globalConfData = {}
        for i in range(len(lines)):
            if len(lines[i].split("=")) > 1:
                value = lines[i].split("=")[1]
                globalConfData[lines[i].split("=")[0]] = value
        return globalConfData[fieldToRetrieve]
    elif fieldToRetrieve in globalConfData:
        if "<COLLECTION>" in globalConfData[fieldToRetrieve]:
            collection_name = globalConfData["collection"]
            globalConfData[fieldToRetrieve] = globalConfData[fieldToRetrieve].replace("<COLLECTION>", collection_name)
        return globalConfData[fieldToRetrieve]
    #if the element we are loking for doesn't exist in globaConfData we look for it in globalLaserConfData
    else:
        #if globalLaserConfData is empty because no of FTP connection or lack of file, we send None
        if globalLaserConfData is None:
            print("globalLaserConfData is None")
            return None
        else:
            return globalLaserConfData[fieldToRetrieve]


# Common method to remove files
def remove_file(path, msg):
    res = ""
    try:
        if os.path.exists(path):
            os.remove(path)
    except Exception:
        LOG.error(msg)
        res = msg
        LOG.error(str(traceback.format_exc()))
    return res

#function to remove old laserConf and put the new one given by the laser
def updateLaserConfFileInTablet():
    global globalLaserConfData
    #first we remove the old file
    path = getConfPath("laserConfFilePath")
    print("Removing laserConf file: ", str(path))
    remove_file(path, "Error removing laserConf file")
    print("laser conf file removed")
    #then we wright the new file
    try:
        with open(path,"w") as file:
            for key in globalLaserConfData.keys():
                file.write(str(key) + "=" + str(globalLaserConfData[key]) + "\n")
        print("file succefully written")
    except Exception as er:
        print("Error writing the file:", er)


# function to check if file exist
def checkLaserConfFile():
    LOG.info("init function checkLaserConfFile")
    laser_conf_file = getConf("laser_conf_file")
    global globalLaserConfData
    CheckConfFileFromLaser = False
    try:
        # get parameters data from machine
        MachineFileExist, Machine_Conf_lines = askForLaserFileFromMachine(laser_conf_file)
        LOG.info("MachineFileExist" + str(MachineFileExist))
        # if we have machine laser parameters without any problem
        if MachineFileExist:
            MachineLaserParams = getConflineASDict(Machine_Conf_lines)
            # get parameters data from tablet
            TabletFileExist, tablet_Conf_line = askForLaserFileFromTablet()
            LOG.info("TabletFileExist" + str(TabletFileExist))
            # if we have laser parameters from tablet(laser parameter file exist)
            if TabletFileExist:
                TabletLaserParams = getConflineASDict(tablet_Conf_line)
                LOG.info("TabletLaserParams are " + str(TabletLaserParams))
                laserIDMachine = MachineLaserParams["id_machine"]
                LOG.info("laser ID machine is: " + str(laserIDMachine))
                laserIDTablet = TabletLaserParams["id_machine"]
                LOG.info("laser ID tablet is: " + str(laserIDMachine))
                # If laser ID from both tablet and Machine have the same value we load laser params from tablet
                if(laserIDMachine == laserIDTablet):
                    # load laser params from tablet
                    globalLaserConfData = TabletLaserParams
                    CheckConfFileFromLaser = True
                # If laser ID from both tablet and Machine have different values we load laser params from machine and update the tablet file
                else:
                    # load laser params from machine
                    globalLaserConfData = MachineLaserParams
                    CheckConfFileFromLaser = True
                    #update the file in tbalet with new data from machine
                    updateLaserConfFileInTablet()
            # if there's no tablet file we load laser params from machine and update the tablet file
            else:
                globalLaserConfData = MachineLaserParams
                CheckConfFileFromLaser = True
                updateLaserConfFileInTablet()
    except Exception as e:
        LOG.info("Error Communication laser" + str(e))

    return CheckConfFileFromLaser


# Loads the laser conf properties from the local file
def load_laser_conf_from_local_file():
    global globalLaserConfData
    try:
        fichier = open(getConfPath("laserConfFilePath"))
        full_text = fichier.read().replace("\r", "")
        fichier.close()
        lines = full_text.split('\n')
        print(lines)
        LOG.info("getConf - Product information = " + str(lines))
        globalLaserConfData = {}
        for i in range(len(lines)):
            if len(lines[i].split("=")) > 1:
                value = lines[i].split("=")[1]
                globalLaserConfData[lines[i].split("=")[0]] = value
        print("##### - globalLaserConfData:", globalLaserConfData)
    except Exception as e:
        LOG.error("Error reading laser conf properties file: " + str(e))



def getConfPath(fieldToRetrieve):
    # Retrieves the path from a value of a property stored in the generic configuration file
    value = getConf(fieldToRetrieve)
    if "<COLLECTION>" in value:
        collection_name = getConf("collection")
        value = value.replace("<COLLECTION>", collection_name)
    full_path = dir_path + "/.." + value

    print('::: FULL PATH = ', full_path)
    return full_path


# Returns all the collection data
def getAllCollectionConf():
    global globalCollectionConfData
    return globalCollectionConfData


def getCollectionConf(fieldToRetrieve, force_json=False):
    collection_conf_path = getConfPath("collection_conf_path")
    global globalCollectionConfData
    # TODO remove this part of code, this is just for retrocompatibility
    if not os.path.isfile(collection_conf_path):
        getConf(fieldToRetrieve)
    else:
        if globalCollectionConfData is None or force_json:
            with open(collection_conf_path, encoding="utf-8") as json_file:
                globalCollectionConfData = json.load(json_file)
            # If the product doesnt have radius defined, we consider it is plane, hence, R is "infinite"
            if globalCollectionConfData.get("radius") is None:
                globalCollectionConfData["radius"] = "999999"
            if globalCollectionConfData.get("app_lang") is None:
                globalCollectionConfData["app_lang"] = ""
            if globalCollectionConfData.get("keyboard_list") is None:
                globalCollectionConfData["keyboard_list"] = [{
                    "name": "Default",
                    "value": "defaultDWS"
                }]

        else:
            if fieldToRetrieve in globalCollectionConfData and "<COLLECTION>" in str(globalCollectionConfData.get(fieldToRetrieve)):
                collection_name = globalCollectionConfData["collection"]
                globalCollectionConfData[fieldToRetrieve] = globalCollectionConfData[fieldToRetrieve].replace("<COLLECTION>", collection_name)
        return globalCollectionConfData.get(fieldToRetrieve)


def getApiUrl(endpointName, prod=False):
    if prod:
        url = getConf("api_prod_base_url") + getConf(endpointName)
    else:
        url = getConf("api_base_url") + getConf(endpointName)
    return url


# Returns the list of products for a given "products_structure" in collection_conf.json
def get_products_from_products_structure(products_structure):
    prods = []
    # We use a recursive function
    for group in products_structure:
        if "structure" in group:
            # This group is a set of categories
            prods = prods + get_products_from_products_structure(group.get("structure"))
        elif "subProducts" in group:
            # This group is a category
            # Categories with "getAllCategory" are not accepted here
            prods = prods + get_products_from_products_structure(group.get("subProducts"))
        elif "folderName" in group:
            # This group is a single product (or an empty space for display)
            prod_id = group.get("folderName")
            if prod_id is not "":
                prods = prods + [prod_id]
        else:
            LOG.warning("Group: " + str(group) + " didnt specify a product ID")
    return prods


def getStaticVersion():
    # Retrieves the path from a value of a property stored in the generic configuration file
    version = getConf("html_static_version")
    return version

#returns the teamviewerID if it's a raspberry if not returns 0
def getTeamviewerID():
    try:
        if platform == "linux" or platform == "linux2":
            #TODO make better splitting the string
            ss = str(subprocess.check_output('sudo teamviewer info', shell=True))
            ss = ss.replace(" ", "")
            ss = ss.replace("\n", "")
            ss = ss.replace("\\x1b[", "")
            ss = ss.replace("1m", "")
            ss = ss.replace("0m", "")
            indiceID = ss.find("TeamViewerID:") + len("TeamViewerID") + 1
            #TODO replace the "10" by something more general
            return str(ss[indiceID:indiceID + 10])
        else:
            return str(0)
    except Exception as e:
        LOG.error("No teamviewer installed: " + str(e))
        return str(0)


# If we are getting the products from their IDs, we need to overwrite the parameters they might have for a certain
# brand id and shop id
def transform_product_data(fi_json):
    brand_id = getCollectionConf("id_brand")
    shop_id = getCollectionConf("id_shop")
    brand_shop_id = str(brand_id) + "_" + str(shop_id)
    res = fi_json.get("default")
    if fi_json.get(brand_shop_id):
        new = fi_json.get(brand_shop_id)
        print("we need to overwrite parameters:", new)
        for param in new:
            print(param, ":", new[param])
            res[param] = new[param]
    return res

# Sets the product used on the app.
# If we are loading a SAFE product, we might want to read some parameters differently
def setCurrentProduct(product, SAFE_product=False):
    global currentProduct, productData
    currentProduct = product
    res = True
    if product is None:
        res = None
    else:
        if productData is None or getConf("debug_node") == "True":
            fichier_info_path = getConfPath("fichier_info_prod_id_path").replace("<PRODUCT_NAME>", currentProduct)
            if os.path.exists(fichier_info_path):
                with open(fichier_info_path, encoding="utf-8") as json_file:
                    productData = transform_product_data(json.load(json_file))
                # If the product doesnt have radius defined, we consider it is plane, hence, R is "infinite"
                if productData.get("radius") is None:
                    productData["radius"] = "999999"

                # Getting emojis to display in keyboard for this product
                # If we are loading a SAFE product, we dont read collection emojis
                productData["emojis"] = get_emojis_for_product(productData, SAFE_product)

                # Iterable Iteration through all laser parameters
                for i in range(len(productData["laser"])):
                    # We only load default values if the set of laser parameters is not for protection
                    if productData["laser"][i].get("pinLoad") != "8":
                        # We will have two engraving methos, the old and the new. If not specified, we use the old
                        if productData["laser"][i].get("use_new_cnc_method") is None:
                            productData["laser"][i]["use_new_cnc_method"] = False
                        # If the product n_jump_lines is not defined AND we have old method, we take the value from conf properties
                        if productData["laser"][i].get("n_jump_lines") is None and not productData["laser"][i]["use_new_cnc_method"]:
                            productData["laser"][i]["n_jump_lines"] = str(getConf("def_n_jump_lines"))
                        # If the product mark_delay is not defined AND we have old method, we take the value from conf properties
                        if productData["laser"][i].get("mark_delay") is None and not productData["laser"][i]["use_new_cnc_method"]:
                            productData["laser"][i]["mark_delay"] = str(getConf("def_mark_delay"))

                prodpath = get_products_folder_path() + '/' + product + '/Pictures'
                # If the product doesn't have Image name or the file with this name is not existing we take the first image we find in the folder
                if productData.get("ImageName") is None or not path.exists(prodpath+"/"+productData.get("ImageName")):
                    LOG.info("imageName or his image not found")
                    prodimg = os.listdir(prodpath)
                    productData["ImageName"] = prodimg[0]
                    LOG.info("product Data name is "+productData["ImageName"])
                proportion = productData.get("propotion_img")
                productData["DefImageName"] = productData["ImageName"]
                if(CreateCroppedImageForBigProductIfNotExist(proportion, prodpath)):
                    upadateDimensions(proportion)
                else:
                    re_update_dimensions()
                LOG.info(str(productData))
                res = productData
            else:
                res = False
        else:
            res = productData
    return res



#create from the original image of product a cropped image using a proprety in fichierInfo (if the cropped image isn't
# already existing) called "proportion" and update the ImageName taken to the display, if we crop image it returns True of not it returns False
#TODO ADD PROPORTION FO WIDTH SO WE CAN CROP FROM WIDTH ALSO
def CreateCroppedImageForBigProductIfNotExist(proportion,OriginalImageFolderPath):
    global productData
    isCroppedWithPrportion = False
    if proportion is not None:
        newImageName = proportion + productData.get("ImageName")
        if path.exists(OriginalImageFolderPath + "/" + newImageName):
            LOG.info("cropped image found !!")
            productData["ImageName"] = newImageName
            LOG.info("product Data name is "+productData["ImageName"])
            isCroppedWithPrportion = True
        else:
            LOG.info("Cropped image not found!! creating one ...")
            originalImage = Image.open(OriginalImageFolderPath + "/" + productData["ImageName"])
            croppedImage = originalImage.copy()
            width, height = croppedImage.size
            croppedImage = croppedImage.crop((0, (1 - float(proportion)) * height, width, height))
            croppedImage.save(OriginalImageFolderPath + "/" + newImageName)
            LOG.info("Cropped mage created and saved succeffuly !!")
            productData["ImageName"] = newImageName
            LOG.info("product Data name is "+productData["ImageName"])
            isCroppedWithPrportion = True
    return(isCroppedWithPrportion)


# Update dimensions of the product if we want a cropped image
# TODO Add update width
def upadateDimensions(proportion):
    global productData
    LOG.info("We will change new dimensions..")
    proportion = float(proportion)
    if "dimensions_orig" not in productData:
        productData["dimensions_orig"] = productData["dimensions"]
    height = float(productData["dimensions_orig"].split(",")[1])
    productData["dimensions"] = str(productData["dimensions_orig"].split(",")[0])+","+str(height * proportion)
    LOG.info("The new dimensions are : " + productData["dimensions"])


# This function un-does the changes if a product had a proportion configured and then removed
def re_update_dimensions():
    global productData
    if "dimensions_orig" in productData:
        productData["dimensions"] = productData["dimensions_orig"]

# Return the object where the fichier info is stored
def getProductData():
    global productData
    return productData


# Returns the platform number associated to the product
# If the product doesn't have one, we get the default value from collection
def getProductPlatformNumber():
    global productData, globalCollectionConfData
    if productData.get("platform_number") is not None:
        return productData.get("platform_number")
    elif globalCollectionConfData.get("def_platform_number") is not None:
        return globalCollectionConfData.get("def_platform_number")
    else:
        # We are missing the platform number from the product and the collection
        return 2

# Returns cale name associated to the product
# If the product doesn't have one, we get the default value from collection
def getProductCale():
    global productData, globalCollectionConfData
    if productData.get("cale_name") is not None:
        return productData.get("cale_name")
    else:
        # We are missing the cale name  from the product and the collection
        return "generale"
#Return image removing disposition block
def getImageRemoveCale():
    global productData, globalCollectionConfData
    if productData.get("cale_remove_img") is not None:
        return productData.get("cale_remove_img")
    
# Returns the list of keyboards for the colletion
def selectKeyboardList():
    global productData
    if getCollectionConf("keyboard_list") is None or len(getCollectionConf("keyboard_list")) < 1:
        return [{
            "name": "Default",
            "value": "defaultDWS"
        }]
    else:
        return getCollectionConf("keyboard_list")


# Returns the height in mm associated to the product
# If platform_base_height is defined, it has priority over platform_number
# and we use platform_number as an ID for the image to display the positioning
def getProductBaseHeight():
    product_data = getProductData()
    # Checking machine model
    machine_model = getConf("machine_type")
    print("##2# machine_model:", machine_model)
    if not machine_model:
        machine_model = "standard"
    if machine_model.upper() == "XL":
        print("##2# Machine is XL:")
        platform_base_height_xl = product_data.get("platform_base_height_xl")
        if platform_base_height_xl:
            print("##2# platform_base_height_xl:", platform_base_height_xl)
            mm = platform_base_height_xl
        else:
            xl_platform_distance = getConf("xl_platform_distance")
            print("##2# xl_platform_distance:", xl_platform_distance)
            mm = str(float(getProductBaseHeightStandard(product_data)) + float(xl_platform_distance))
    else:
        print("##2# machine is not XL:")
        mm = getProductBaseHeightStandard(product_data)
    print("##2# mm:", mm)
    return mm


# Gets the product base height for the standard ALYCE machine
def getProductBaseHeightStandard(product_data):
    platform_base_height = product_data.get("platform_base_height")
    if platform_base_height:
        mm = platform_base_height
    else:
        platformnumber = getProductPlatformNumber()
        # If platform number is -1, the product is placed on the bottom of the cabine, no platform
        if platformnumber is -1:
            platformnumber = -float(getConf("first_slot_height"))/float(getConf("distance_between_slots"))
        mm = float(getConf("first_slot_height")) + float(platformnumber) * float(getConf("distance_between_slots"))  # Platform height
    LOG.info("FINAL base height mm:" + str(mm))
    return mm

# Returns an object with the laser parameters for the selected pin mode
def getLaserParams(pinNumber):
    global productData
    laser_params = {}
    for params in productData.get("laser"):
        if params.get("pinLoad") == str(pinNumber):
            laser_params["pinLoad"] = params.get("pinLoad")
            laser_params["puissance"] = params.get("puissance")
            laser_params["frequence"] = params.get("frequence")
            laser_params["vitesse"] = params.get("vitesse")
            laser_params["nbpassage"] = params.get("nbpassage")
            laser_params["doublehatch"] = params.get("doublehatch")
            laser_params["n_jump_lines"] = params.get("n_jump_lines")
            laser_params["mark_delay"] = params.get("mark_delay")
            if "use_new_cnc_method" in params:
                laser_params["use_new_cnc_method"] = params.get("use_new_cnc_method")
            else:
                laser_params["use_new_cnc_method"] = "False"
            laser_params["for_performance_csv"] = params.get("for_performance_csv")
            if "hatch_angle" in params:
                laser_params["hatch_angle"] = params.get("hatch_angle")
            else:
                laser_params["hatch_angle"] = "0"
            if "diagonal_engraving" in params:
                laser_params["diagonal_engraving"] = params.get("diagonal_engraving")
            else:
                laser_params["diagonal_engraving"] = False
            if "protection_mode_area" in params and is_number(params.get("protection_mode_area")):
                laser_params["protection_mode_area"] = params.get("protection_mode_area")
            else:
                # Default value for protection_mode_area
                laser_params["protection_mode_area"] = "25"
            if "SECOND_SET_PARAMETERS" in params:
                laser_params["SECOND_SET_PARAMETERS"] = params.get("SECOND_SET_PARAMETERS")
            if "hatch" in params:
                laser_params["hatch"] = params.get("hatch")
            else:
                laser_params["hatch"] = productData.get("hatch")
            if "protection_mode_pen" in params:
                laser_params["protection_mode_pen"] = params.get("protection_mode_pen")
            if "engraving_mode" in params:
                laser_params["engraving_mode"] = params.get("engraving_mode")
            else:
                laser_params["engraving_mode"] = "hatching"
            if "wait_time" in params:
                laser_params["wait_time"] = params.get("wait_time")
            else:
                laser_params["wait_time"] = getConf("default_wait_time")
    return laser_params


# Returns the default warmup laser parameters
def get_warm_up_params():
    return {
        "description": "Parameters that will NOT engrave, only for warm up",
        "pinLoad": str(getConf("warm_up_pin_number")),
        "puissance": "5",
        "frequence": "15",
        "vitesse": "1500",
        "mark_delay": "1000"
    }


# Returns the time in seconds for the laser to warm up
# TODO make this value dynamic?
def get_warmup_time():
    try:
        warm_up_time = float(getConf("warm_up_engraving_time"))
    except Exception as e:
        LOG.error("Incorrect warm_up_time" + str(e))
        warm_up_time = 10
    return warm_up_time


# Returns the laser parameters for double engraving waiting time
def get_wait_time_params(pin_number=getConf("double_engraving_waiting_time_pin_number")):
    return {
        "description": "Parameters that will NOT engrave, only to wait some seconds for a 2nd engraving",
        "pinLoad": str(pin_number),
        "puissance": "1",
        "frequence": "1",
        "vitesse": "2000",
        "mark_delay": "1000"
    }


# Checks if received value is a number
def is_number(value):
    try:
        float(value)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(value)
        return True
    except (TypeError, ValueError):
        pass
    return False

# Returns the protection laser parameters
def getProtectionLaserParams(pin_number, protection_pin_number=8):
    laser_params = getLaserParams(pin_number)
    protection_params = getLaserParams(protection_pin_number)
    for item in protection_params:
        if protection_params[item]:
            laser_params[item] = protection_params[item]
    return laser_params


def getX_Offset():
    global productData, globalLaserConfData
    X_Offset = 0
    try:
        if "X_Offset" in productData.keys():
            X_Offset = X_Offset + float(productData.get("X_Offset"))
            LOG.info("asdf PRODUCT XOffset is " + str(X_Offset))
        if "X_Offset" in globalLaserConfData.keys():
            X_Offset = X_Offset + float(globalLaserConfData.get("X_Offset"))
            LOG.info("asdf LASER XOffset is " + str(X_Offset))
    except Exception as e:
        LOG.error("Error getting x off set:" + str(e))
    LOG.info(" XOffset is " + str(X_Offset))
    return str(X_Offset)




# Returns the curvature of the product
def getProductRadius():
    global productData
    return float(productData.get("radius"))


def createCNCFileToReturnRedPointerToOrigin():
    cnc_path = getConfPath("cncReturnLaserToOrigin")
    f = open(cnc_path, "w+")
    f.write("G17")
    f.write("\n" + "G71")
    f.write("\n" + "G90")
    f.write("\n" + "$SC_DEFINES = 1")
    f.write("\n" + "SC_SELECT_JOB[6]")
    f.write("\n" + "$SC_FIELDXMIN = -30")
    f.write("\n" + "$SC_FIELDXMAX =  30")
    f.write("\n" + "$SC_FIELDYMIN = -30")
    f.write("\n" + "$SC_FIELDYMAX =  30")
    f.write("\n" + "$SC_FIELDXGAIN =  1")
    f.write("\n" + "$SC_FIELDYGAIN =  1")
    f.write("\n" + "$SC_FIELDXOFFSET =  0")
    f.write("\n" + "$SC_FIELDYOFFSET =  0")
    f.write("\n" + "$SC_FIELDAXISSTATE = 3")
    f.write("\n" + "SC_TOOL_LOAD[1]")
    f.write("\n" + "$SC_LASERPOWER = 2")
    f.write("\n" + "$SC_FREQUENCY = 50")
    f.write("\n" + "$SC_STDBYPERIOD = 0.0")
    f.write("\n" + "$SC_JUMPSPEED = 9000.0")
    f.write("\n" + "$SC_MARKSPEED = 3000")
    f.write("\n" + "$SC_JUMPDELAY = 100.0")
    f.write("\n" + "$SC_MARKDELAY = 50")
    f.write("\n" + "$SC_POLYDELAY = 0")
    f.write("\n" + "$SC_LASERONDELAY = 30.0")
    f.write("\n" + "$SC_LASEROFFDELAY = 130.0")
    f.write("\n" + "SC_TOOL_SAVE[1]")
    f.write("\n" + "$SC_DEFINES = 0")
    f.write("\n" + "$SC_LoopCount = 20")
    f.write("\n" + "SC_Start_Entity")
    f.write("\n" + "SC_TOOL_LOAD[1]")
    f.write("\n" + "SC_TOOL[0]")
    f.write("\n" + "SC_TOOL[0]")
    f.write("\n" + "G0 X0 Y0")
    f.write("\n" + "SC_End_Entity")
    f.write("\n" + "M2")
    f.close()


'''
INIT FUNCTIONS
'''
def init():
    logName = getConfPath("app_log_path")
    LOG.init(logName, getConf("debug_node"))

init()
'''
END OF INIT
'''



'''
    Literals
'''

def initLiterals():
    # Initiating the language defined in the app
    lang = getCollectionConf("app_lang")
    print("-.- lang:", lang)
    if lang is None:
        lang = ""
    customFilePath = getConfPath("custom_literals_path") + "/" + getConf("literals_filename").replace("<LANG>",lang)
    errorsFilePath = getConfPath("json_errors_path")
    generalFilePath = getConfPath("literals_path") + "/" + getConf("literals_filename").replace("<LANG>",lang)
    #this three lines are for being sure that we are loading the defaut literals first to not allow empty literals when chosing another language
    DefautLang = ""
    generalFilePathWithDefaultLiterals = getConfPath("literals_path") + "/" + getConf("literals_filename").replace("<LANG>", DefautLang)
    # Back up literals from main literals file
    L.init(generalFilePathWithDefaultLiterals, errorsFilePath, generalFilePath)
    # Back up literals from main literals file for current language
    L.init(generalFilePath, errorsFilePath, generalFilePath)
    # Loading custom literals
    L.init(customFilePath, errorsFilePath, generalFilePath)
    # Initiating questions also
    L.initQuestions(getConfPath("questions_path"))
    return L.getAllLits()

def changeLiteralLang(lang):
    global globalCollectionConfData
    collection_conf_path = getConfPath("collection_conf_path")
    #open collection_conf_path
    with open(collection_conf_path, "r+", encoding="utf-8") as fi:
        fi_dict = json.load(fi)
        fi_dict["app_lang"] = lang
        fi.seek(0)
        json.dump(fi_dict, fi, indent=2)
        fi.truncate()
        globalCollectionConfData["app_lang"] = lang
    customFilePath = getConfPath("custom_literals_path") + "/" + getConf("literals_filename").replace("<LANG>",lang)
    errorsFilePath = getConfPath("json_errors_path")
    generalFilePath = getConfPath("literals_path") + "/" + getConf("literals_filename").replace("<LANG>",lang)
    # In order to have the main literals as backup, we should init twice
    L.init(generalFilePath, errorsFilePath, generalFilePath)
    L.init(customFilePath, errorsFilePath, generalFilePath)
    return L.getAllLits()

def getLangList():
    collection_lang_list = getCollectionConf("language_list")
    if collection_lang_list:
        langlist = collection_lang_list
    else:
        languageList_conf_path = getConfPath("languageList_conf_path")
        with open(languageList_conf_path) as fl:
            langlist = json.load(fl)
            print("langlist is ",langlist)
    return langlist

# When loading the first screen, we remove previous product parameters from memory
def resetProductData():
    global fullTextData, productData
    setCurrentProduct(None)
    productData = None
    fullTextData = None

# When a product is selected, we store its paramters in memory
def getDataFromFile(fichierInfo, fieldToRetrieve):
    global fullTextData
    if fullTextData is None or getConf("debug_node") == "True":
        fichier = open(fichierInfo)
        fullText = fichier.read().replace("\r", "")
        fichier.close()
        lines = fullText.split('\n###Comments###')[0].split('\n')
        #LOG.info("getDataFromFile - Product information = " + str(lines))
        fullTextData = {}
        for i in range(len(lines)):
            if len(lines[i].split("=")) > 1:
                value = lines[i].split("=")[1]
                fullTextData[lines[i].split("=")[0]] = value
    if fieldToRetrieve in fullTextData:
        return fullTextData[fieldToRetrieve]
    else:
        return ""


# Returns a dictionary with "fontname": "line space value"
def get_line_spacing_fonts():
    lsf = get_fonts_configuration()
    b = {}
    for key in lsf.keys():
        b[key] = lsf[key].get("line_spacing")
    return b


# Returns the font configuration
def get_fonts_configuration():
    fonts_configuration = getConfPath("font_configuration_path")
    with open(fonts_configuration) as fl:
        flc = json.load(fl)
        for font in flc.keys():
            if not flc.get(font).get("line_spacing"):
                flc.get(font)["line_spacing"] = flc.get("default").get("line_spacing")
    return flc

# Returns the relation betwee the real product dimensions and the pixel dimensions of the product's image
def getrapport(pathimg):
    global fullTextData
    im = Image.open(pathimg)
    widthpx, heightpx = im.size
    widthmm, heightmm = fullTextData["dimensions"].split(",")
    rapportheight = float(heightmm) / float(heightpx)
    rapportwidth = float(widthmm) / float(widthpx)
    return rapportwidth, rapportheight


# Stores the 3 files from the order folder in the "Failed" or "Jobs" folder, order with the date
def storeEngravingFiles(folderPath, OrderDirPath):
    # Defining Year, Month, Day and Hour+Minute to store historic of orders
    currentYear = str(date.today().year)
    currentMonth = str(date.today().month)
    currentDay = str(date.today().day)
    minute = str(datetime.now().minute)
    if len(minute) == 1:
        minute = "0" + minute
    hour = str(datetime.now().hour)
    if len(hour) == 1:
        hour = "0" + hour
    currentTime = hour + "-" + minute

    DayDirPath = folderPath + "/" + currentYear + "/" + currentMonth + "/" + currentDay + "/"
    if not os.path.isdir(folderPath):
        os.makedirs(folderPath)
    if not os.path.isdir(folderPath + "/" + currentYear):
        os.makedirs(folderPath + "/" + currentYear)
    if not os.path.isdir(folderPath + "/" + currentYear + "/" + currentMonth):
        os.makedirs(folderPath + "/" + currentYear + "/" + currentMonth)
    if not os.path.isdir(DayDirPath):
        os.makedirs(DayDirPath)

    # Copy files to /Jobs, since the engraving went well
    fileOrigTXT = OrderDirPath + "/default0.png"
    fileDestTXT = DayDirPath + currentTime + ".png"
    fileOrigCNC = OrderDirPath + "/default.cnc"
    fileDestCNC = DayDirPath + currentTime + ".cnc"
    fileOrigUNF = OrderDirPath + "/default.unf"
    fileDestUNF = DayDirPath + currentTime + ".unf"
    try:
        LOG.info("Copying files: " + str("cp " + fileOrigTXT + " " + fileDestTXT))
        LOG.info("Copying files: " + str("cp " + fileOrigCNC + " " + fileDestCNC))
        LOG.info("Copying files: " + str("cp " + fileOrigUNF + " " + fileDestUNF))
        copyfile(fileOrigTXT, fileDestTXT)
        copyfile(fileOrigCNC, fileDestCNC)
        copyfile(fileOrigUNF, fileDestUNF)
    except:
        LOG.error("ERROR copying files")
        LOG.error(str(traceback.format_exc()))
        return L.returnResponse("005")

    return L.returnResponse(None)


# Restarts or turns off tablet depending on the OS
def screen1TabletOption(option):
    res = "Error doing " + str(option)
    if platform == "linux" or platform == "linux2":
        LOG.info("We are in linux")
        if str(option) == "restart":
            LOG.info("RESTART = " + str(option))
            os.system('pkill chromium')
            time.sleep(2)
            os.system('sudo reboot')
            res = "Restarting... RESTART"
        if str(option) == "shutdown":
            LOG.info("SHUTDOWN = " + str(option))
            os.system('pkill chromium')
            time.sleep(2)
            os.system('sudo shutdown -h now')
            res = "Restarting... SHUTDOWN"
    elif platform == "win32":
        LOG.info("We are in Windows")
        if str(option) == "restart":
            LOG.info("RESTART = " + str(option))
            os.system('pkill chromium')
            time.sleep(1)
            os.system("shutdown -t 0 -r -f")
            res = "Restarting... RESTART"
        if str(option) == "shutdown":
            LOG.info("SHUTDOWN = " + str(option))
            os.system('pkill chromium')
            time.sleep(1)
            os.system("shutdown -t 0 -s")
            res = "Restarting... SHUTDOWN"
    return res

# Facade for method storeEngravingFiles
def storefiles():
    OrderDirPath = getConfPath("order_folder")
    # Storing in Jobs
    FolderJobsDirPath = getConfPath("jobs_folder")
    result = storeEngravingFiles(FolderJobsDirPath, OrderDirPath)
    if result["result"] == "KO":
        LOG.error("Result of storing in folder: " + str(result["msg"]))
        result["msg"] = result["msg"] + "----" + result["msg"]

    return result






def addPNGtoImage(dataFolderPath, outT):
    try:
        outT.save(dataFolderPath + "/ImageToEngraveBEFORE.png")
    except OSError:
        time.sleep(0.1)
        outT.save(dataFolderPath + "/ImageToEngraveBEFORE.png")

    print("\t\t2 width, height =", outT.size)
    foreground = Image.open(dataFolderPath + "/PNG.png").convert("RGBA")
    print("\t\t3 width, height =", foreground.size)

    basewidth = 100
    wpercent = (basewidth / float(foreground.size[0]))
    hsize = int((float(foreground.size[1]) * float(wpercent)))
    foreground = foreground.resize((basewidth, hsize), Image.ANTIALIAS)

    print("\t\t3 width, height =", foreground.size)

    try:
        outT.paste(foreground, (outT.size[0]-150, outT.size[1]-50), foreground)
    except Exception:
        print(str(traceback.format_exc()))

    try:
        outT.save(dataFolderPath + "/ImageToEngraveAFTER.png")
    except OSError:
        time.sleep(0.1)
        outT.save(dataFolderPath + "/ImageToEngraveAFTER.png")

    return outT


# Returns the first font that matches with the list of keyboards
def get_default_font_for_keyboard(font_list):
    keyboardList = selectKeyboardList()
    for font in font_list:
        # If font does not have a list of allowed keyboards, it is allowed in every keyboard
        if not font.get("keyboard_list"):
            return font
        else:
            # If the first keyboard is in the list of keyboards of the font, this font is selected
            if keyboardList[0].get("value") in font.get("keyboard_list"):
                return font
    LOG.error("Fonts and keyboards dont match correctly!")
    return font_list[0]


# Removes all unf, cnc, png and jpg files from order folder
def remove_order_files():
    order_folder = getConfPath("order_folder")
    engraving_file_name = "default"
    list_of_extensions = [".png", ".jpg", ".cnc", ".unf", ".unfCHECK"]
    for file_name in os.listdir(order_folder):
        for extension in list_of_extensions:
            if file_name.lower().endswith(extension.lower()):
                LOG.info("REMOVE old file:" + str(file_name))
                os.remove(order_folder + "/" + file_name)

    # Removing file from FTP also
    delete_ftp_file(engraving_file_name + ".unf")


# Function to send laser configuration file
def delete_ftp_file(file_name):
    destination = getLaserIP()
    FTPTimeout = int(getConf("laser_FTP_timeout"))
    res = False
    try:
        LOG.info("FTP("+destination+")")
        ftp = FTP(destination, timeout=FTPTimeout)
        ftp.login()
        ftp.cwd("jobs")
        ftp.delete(file_name)
        ftp.close()
        res = True
        LOG.info("UNF file removed correctly from the FTP")
    except Exception as e:
        errorMsg = "ERROR trying to delete file on the FTP "
        print(errorMsg)
        LOG.error(errorMsg + str(e))
    return res

# Checks that the timestamp of object a_server (coming from server) is newer than object b_local (in local)
def check_timestamps(a_server, b_local, backup_parameter):
    res = False
    print("\t\t\ta_server:", a_server)
    print("\t\t\tb_local:", b_local)
    if a_server and "timestamp" in a_server and b_local and "timestamp" in b_local:
        print("\t\tCASE A:")
        # Both objects have timestamps to compare
        print("\t\ta_server[timestamp]:", a_server["timestamp"])
        print("\t\tb_local[timestamp]:", b_local["timestamp"])
        # Server timestamp is more recent than local timestamp
        res = float(a_server["timestamp"]) > float(b_local["timestamp"])
        print("\t\tCASE A:", res)
    elif backup_parameter in a_server:
        print("\t\tCASE B:", True)
        # We update if the received object has the backup parameter
        print("\t\ta_server[", backup_parameter, "]:", a_server[backup_parameter])
        res = True
    return res


# Given a product, calculates the needed parameters for the correct display of engraving area on the app
def calculate_data(fullpathimg, overwriteProdData=None, use_orig_dimensions=False):
    global productData
    if overwriteProdData:
        print("Received overwriteProdData:", overwriteProdData)
        productData = overwriteProdData.get("parameters")
    # INPUTS
    inputs = {}
    img = Image.open(fullpathimg)
    width, height = img.size
    # inputs["dimensionsMM"][0] --> WIDTH
    # inputs["dimensionsMM"][1] --> HEIGHT
    inputs["X_Offset"] = getX_Offset()
    if use_orig_dimensions:
        inputs["dimensionsMM"] = productData.get("dimensions_orig").split(",")
    else:
        inputs["dimensionsMM"] = productData.get("dimensions").split(",")
    inputs["emojis"] = productData.get("emojis")

    inputs["fix_engraving_location"] = productData.get("engraving_area").get("fix_engraving_location") == "True"
    inputs["engraving_restriction"] = productData.get("engraving_area").get("engraving_restriction") == "True"
    inputs["signatureEngraving"] = productData.get("signatureEngraving") == "True"
    inputs["vertical_engraving"] = productData.get("engraving_angle")
    inputs["dmm"] = float(getConf("dmm"))
    inputs["maxHeightMM"] = productData.get("maxHeightMM")
    inputs["maxWidthMM"] = productData.get("maxWidthMM")
    inputs["max_number_of_lines"] = productData.get("max_number_of_lines")
    if inputs["maxWidthMM"] is "":
        inputs["maxWidthMM"] = min(inputs["dmm"], float(inputs["dimensionsMM"][0]))
    if inputs["maxHeightMM"] is "":
        inputs["maxHeightMM"] = min(inputs["dmm"], float(inputs["dimensionsMM"][1]))
    if not inputs["max_number_of_lines"]:
        inputs["max_number_of_lines"] = 1000
    if not productData.get("defaultKeyboardFont"):
        inputs["defaultKeyboardFont"] = "False"
    else:
        inputs["defaultKeyboardFont"] = productData.get("defaultKeyboardFont")
    # DEPRECATED inputs["max_number_of_chars"] = productData.get("max_number_of_chars")
    # DEPRECATED inputs["max_number_of_lines"] = productData.get("max_number_of_lines")
    # DEPRECATED if inputs["max_number_of_chars"] is "":
        # Default max number of characters
        # DEPRECATED inputs["max_number_of_chars"] = 10
    # DEPRECATED if inputs["max_number_of_lines"] is "":
        # Default max number of characters
        # DEPRECATED inputs["max_number_of_lines"] = 1

    if inputs["fix_engraving_location"]:
        # Overwritting the value of engraving restriction if the location is fixed
        inputs["engraving_restriction"] = False
    inputs["engraving_box_x_0"] = float(productData.get("engraving_area").get("engraving_box_x_0"))
    inputs["engraving_box_y_0"] = float(productData.get("engraving_area").get("engraving_box_y_0"))
    inputs["engraving_box_x_len"] = float(productData.get("engraving_area").get("engraving_box_x_len"))
    inputs["engraving_box_y_len"] = float(productData.get("engraving_area").get("engraving_box_y_len"))
    # Checking if we allow the font size buttons
    inputs["show_slider"] = productData.get("show_slider") == "True"
    if "show_font_size_buttons" in productData:
        inputs["show_font_size_buttons"] = productData.get("show_font_size_buttons")
    elif "show_font_size_buttons" in getAllCollectionConf():
        inputs["show_font_size_buttons"] = getCollectionConf("show_font_size_buttons")
    else:
        inputs["show_font_size_buttons"] = False
    # Getting font parameters
    def_font = get_default_font_for_keyboard(productData.get("fonts"))
    inputs["fontsize"] = float(def_font.get("fontsize"))
    if def_font.get("fontsize_max") is not None and def_font.get("fontsize_max") != "":
        inputs["fontsize_max"] = float(def_font.get("fontsize_max"))
    else:
        inputs["fontsize_max"] = inputs["fontsize"]
        # If no fontsize max is defined, we dont allow the font size buttons
        inputs["show_font_size_buttons"] = False

    # This hatch is used for the canvas, and has to be the smallest hatch of all the fonts
    inputs["hatch"] = get_smallest_hatch_from_laser_params(productData.get("laser"), productData.get("hatch"))
    inputs["hmm"] = float(getConf("laser_height_from_base"))  # Height in mm of the center of the laser
    inputs["dimensionsPX"] = width, height  # Image size in pixels
    inputs["platformmm"] = float(getProductBaseHeight())  # Platform height

    inputs["hauteur"] = float(productData.get("hauteur"))  # Square dimensions

    # INTERMEDIOS
    intermedios = {}
    intermedios["ratio"] = float(height) / float(width)
    intermedios["vertical_rapport"] = float(inputs["dimensionsMM"][1])  # we send the height of the object to divide it later by the height of the image
    #rapportwidth, rapportheight = getrapport(fullpathimg)  # mm / px

    # Transformation of engraving box dimensions into percentage of the
    intermedios["engraving_box_x_0"] = 1 * inputs["engraving_box_x_0"] / float(inputs["dimensionsMM"][0])
    intermedios["engraving_box_y_0"] = 1 * inputs["engraving_box_y_0"] / float(inputs["dimensionsMM"][1])
    intermedios["engraving_box_x_len"] = 1 * inputs["engraving_box_x_len"] / float(inputs["dimensionsMM"][0])
    intermedios["engraving_box_y_len"] = 1 * inputs["engraving_box_y_len"] / float(inputs["dimensionsMM"][1])

    intermedios["default_height"] = inputs["hauteur"] / float(inputs["dimensionsMM"][1])



    # (laser_height_from_base - Platform height)/ height_of_flask_MM
    laserpoint = 100 * (inputs["hmm"] - inputs["platformmm"]) / float(inputs["dimensionsMM"][1])
    intermedios["laserpoint"] = str(laserpoint) + "%"
    squareheight = 100 * inputs["dmm"] / float(inputs["dimensionsMM"][1])
    intermedios["squareheight"] = str(squareheight) + "%"
    squarewidth = 100 * inputs["dmm"] / float(inputs["dimensionsMM"][0])
    intermedios["squarewidth"] = str(squarewidth) + "%"




    # Getting first text color
    if "color" in def_font:
        inputs["text_color"] = def_font.get("color")
    elif "text_color" in productData:
        inputs["text_color"] = productData.get("text_color")
    else:
        inputs["text_color"] = getConf("default_text_color")


    # OUTPUTS
    outputs = {}

    return inputs, intermedios, outputs


def get_collection_display_name(collection_path):
    fichier_info_filename = getConf("fichier_info_filename")
    fichier_info_path = collection_path + "/" + fichier_info_filename
    collection_name = ""
    with open(fichier_info_path) as json_file:
        productData = transform_product_data(json.load(json_file))
        collection_name = productData.get("ProductName")
    return collection_name


def get_products_folder_path():
    res = getConfPath("products_id_folder")
    return res

def get_products_folder_conf():
    res = getConf("products_id_folder")
    return res


# Returns the structure of the current client collections
def obtain_products_structure():
    products_structure = getCollectionConf("products_structure")
    if products_structure[0].get("category") is not None:
        for i, fold in enumerate(products_structure):
            # Normal category button
            if fold.get("category"):
                fold["structure"] = obtain_category_products_structure(fold.get("structure"))
            # Empty category name to display space
            else:
                fold["structure"] = []
                fold["button-class"] = "hidden-visibility"
                # Giving a dummy name to take the space, even though with the class "hidden-visibility", the item will not be shown
                fold["category"] = "dummy"
        return products_structure
    else:
        # There are no categories, we return the old structure
        return [{
          "category": "Cat 1",
          "class": "col-sm-4",
          "structure": obtain_category_products_structure(products_structure)
        }]


# Returns the structure of products inside a category
def obtain_category_products_structure(products_structure):
    products_path = get_products_folder_path()
    for i, fold in enumerate(products_structure):
        if fold["folderName"]:
            path = products_path + "/" + fold["folderName"]
            if os.path.isdir(path):
                prod_name = get_collection_display_name(path)
                if not fold.get("displayName"):
                    products_structure[i]["displayName"] = prod_name
                if not fold.get("class"):
                    products_structure[i]["class"] = "col-sm-3"
                if fold.get("subProducts"):
                    # Folder is a category
                    for j, prod in enumerate(fold.get("subProducts")):
                        prod_path = products_path + "/" + prod.get("folderName")
                        prod_name = get_collection_display_name(prod_path)
                        if not prod.get("displayName"):
                            products_structure[i]["subProducts"][j]["displayName"] = prod_name
                elif fold.get("getAllCategory"):
                    print("Get all category")
                    sub_folders = [x for x in os.listdir(path) if os.path.isdir(path + "/" + x)]
                    print("sub_folders:", sub_folders)
                    products_structure[i]["subProducts"] = []
                    for j, prod in enumerate(sub_folders):
                        prod_name = get_collection_display_name(path + "/" + prod)
                        elem = {"folderName": fold.get("folderName") + "/" + prod, "displayName": prod_name}
                        products_structure[i]["subProducts"].append(elem)
                        print("prod:", prod)
                        print("prod_name:", prod_name)

                else:
                    # Folder is a product
                    products_structure[i]["subProducts"] = []
        else:
            # Product name is empty, we use it to display empty space
            products_structure[i]["subProducts"] = []
            products_structure[i]["button-class"] = "hidden-visibility"
            # Giving a dummy name to take the space, even though with the class "hidden-visibility", the item will not be shown
            products_structure[i]["displayName"] = "dummy"
    # Single element: {'displayName' : '100 mL', 'folderName' : '100mL', 'subProducts' : []}
    # Single element with subfolders: {'displayName' : '100 mL', 'folderName' : '100mL', 'subProducts' : [{singleElement1}, {singleElement2}]}
    # Full response: [{singleElement1}, {singleElement3}, {singleElement3}]
    return products_structure


def update_last_engraving_date():
    # Editing file last_engraving_date_path to know the last time we did an engraving and know if we need to warm up
    last_engraving_date_path = getConfPath("last_engraving_date_path")
    with open(last_engraving_date_path, "w"):
        LOG.info("Editing last_engraving_date_path")


# Checks when was the last engraving done
def laser_needs_warm_up():
    warm_up_minutes = float(getConf("warm_up_minutes"))
    last_engraving_date_path = getConfPath("last_engraving_date_path")
    now = datetime.now()

    try:
        # Last Engraving Date got from the last modified date of file "last_engraving_date_path"
        led = datetime.fromtimestamp(os.path.getmtime(last_engraving_date_path))
        difference = (now - led).total_seconds() / 60
        if warm_up_minutes < difference:
            LOG.info("laser needs warm up")
            laser_needs_warm_up = True
        else:
            LOG.info("laser is warmed up")
            laser_needs_warm_up = False
    except:
        LOG.error("File LastEngravingDate.txt didnt exist, so we need to warmup")
        laser_needs_warm_up = True
    return laser_needs_warm_up



'''
-------------------------------------- CREATE COLLECTION AND PRODUCTS -------------------------------
'''

def createImageForProduct(pictures_folder_path, width, height):
    factor = 5
    img = Image.new('RGB', (int(width)*factor, int(height)*factor), (255, 255, 255))
    img.save(pictures_folder_path + "/image.png", "PNG")


# Creates the fichier info JSON of a product
def createProductFichierInfo(fichier_info_path, prod_data, name_prefix=""):
    copyfile(getConfPath("example_fichier_info_path"), fichier_info_path)
    fi_data = {}
    with io.open(fichier_info_path, "r+", encoding="utf-8") as fi:
        fi_data = json.load(fi)
        # Replacing default values with values from table
        for param in (get_product_creation_needed_parameters()+get_product_creation_needed_laser_parameters()):
            val = prod_data.get(str(name_prefix) + param["name"])
            if val:
                if "laser" in param["name"]:
                    print("parameter inside laser")
                    splitted = param["name"].split("-")
                    fi_data["default"][splitted[0]][0][splitted[1]] = val
                else:
                    fi_data["default"][param["name"]] = val
        fi.seek(0)
        json.dump(fi_data, fi, indent=2)
        fi.truncate()
    return fi_data


# Creates the fichier info JSON of a category folder
def createCategoryFichierInfo(fichier_info_path, category_name, id_brand):
    with io.open(fichier_info_path, "w+", encoding="utf-8") as fi:
        fi_data = {
            "default": {
                "ProductName": category_name,
                "id_brand": id_brand
            }
        }
        # Replacing default values with values from table
        fi.seek(0)
        json.dump(fi_data, fi, indent=2)
        fi.truncate()


# Laser parameters needed for the creation of a product
def get_product_creation_needed_laser_parameters():
    laser_params = [{
        "class": "",
        "name": "laser-puissance",
        "td_text": "puissance",
        "placeholder": "puissance",
        "default_value": ""
    }, {
        "class": "",
        "name": "laser-frequence",
        "td_text": "frequence",
        "placeholder": "frequence",
        "default_value": ""
    }, {
        "class": "",
        "name": "laser-vitesse",
        "td_text": "vitesse",
        "placeholder": "vitesse",
        "default_value": ""
    }, {
        "class": "",
        "name": "laser-nbpassage",
        "td_text": "nbpassage",
        "placeholder": "nbpassage",
        "default_value": ""
    }, {
        "class": "",
        "name": "laser-doublehatch",
        "td_text": "doublehatch",
        "placeholder": "doublehatch",
        "default_value": ""
    }, {
        "class": "",
        "name": "laser-pinLoad",
        "td_text": "pinLoad",
        "placeholder": "pinLoad",
        "default_value": "1"
    }]
    return laser_params


# Parameters needed for the creation of a product
def get_product_creation_needed_parameters():
    prod_params = [{
        "class": "required",
        "name": "id_brand",
        "td_text": "Brand ID",
        "placeholder": "The id of the brand where this product belongs",
        "default_value": "brand_id1",
        "pattern": "[0-9A-Za-z-_]{3,}"
    },{
        "class": "",
        "name": "product_category_id",
        "td_text": "Product Category ID",
        "placeholder": "Name of the folder that will contain the product",
        "default_value": "",
        "pattern": "[0-9a-z-_]{3,}"
    }, {
        "class": "",
        "name": "product_category_name",
        "td_text": "Product Category Name",
        "placeholder": "Name of the folder to display in the app",
        "default_value": ""
    }, {
        "class": "required",
        "name": "product_id",
        "td_text": "Product ID",
        "placeholder": "Name of the product folder",
        "default_value": "",
        "pattern": "[0-9a-z-_]{3,}"
    }, {
        "class": "required",
        "name": "ProductName",
        "td_text": "Product Name",
        "placeholder": "Name of the product to display in the app",
        "default_value": ""
    }, {
        "class": "",
        "name": "engraving_angle",
        "td_text": "Engraving angle",
        "placeholder": "Engraving angle in display",
        "default_value": "0",
        "pattern": "[\-0-9]+"
    }, {
        "class": "",
        "name": "text_color",
        "td_text": "Text color",
        "placeholder": "Color of the text in the display",
        "default_value": "black"
    }, {
        "class": "required",
        "name": "hauteur",
        "td_text": "Hauteur of text",
        "placeholder": "Height of the text from the bottom (mm)",
        "default_value": "",
        "pattern": "[0-9]+"
    }, {
        "class": "required",
        "name": "dimensions",
        "td_text": "Product dimensions",
        "placeholder": "Width,Height in mm",
        "default_value": ""
    }, {
        "class": "required",
        "name": "maxHeightMM",
        "td_text": "Max Height",
        "placeholder": "Max height in mm",
        "default_value": "",
        "pattern": "[0-9]+"
    }, {
        "class": "required",
        "name": "maxWidthMM",
        "td_text": "Max Width",
        "placeholder": "Max width in mm",
        "default_value": "",
        "pattern": "[0-9]+"
    }, {
        "class": "required",
        "name": "hatch",
        "td_text": "Hatch",
        "placeholder": "Hatch in mm with . as separator",
        "default_value": "0.05",
        "pattern": "[0-9\.]+"
    }, {
        "class": "",
        "name": "radius",
        "td_text": "Product radius",
        "placeholder": "Radius in mm of the product",
        "default_value": "",
        "pattern": "[0-9]+"
    }, {
        "class": "",
        "name": "signatureEngraving",
        "td_text": "Allow signature",
        "placeholder": "Allow signature engraving if 'True'",
        "default_value": "",
        "pattern": "True|False"
    }, {
        "class": "",
        "name": "platform_number",
        "td_text": "Platform number",
        "placeholder": "Platform number where the product will be placed",
        "default_value": "",
        "pattern": "[\-]{0,1}[0-9]{0,1}"
    }, {
        "class": "required",
        "name": "X_Offset",
        "td_text": "X Offset",
        "placeholder": "Distance in mm to off set horizontally",
        "default_value": "0",
        "pattern": "[\-]{0,1}[0-9]{0,3}"
    }]
    return prod_params


# Parameters needed for the creation of a collection
def get_collection_creation_needed_parameters():
    col_params = [{
        "class": "required",
        "name": "id_brand",
        "td_text": "Brand ID",
        "placeholder": "The id of the brand",
        "default_value": "brand_id1",
        "pattern": "[0-9A-Za-z-]{3,}"
    }, {
        "class": "required",
        "name": "id_shop",
        "td_text": "Shop/Event ID",
        "placeholder": "The id of the shop or event",
        "default_value": "shop_id1",
        "pattern": "[0-9a-zA-Z-_]{3,}"
    }, {
        "class": "required",
        "name": "code_version",
        "td_text": "Code version",
        "placeholder": "Current version of the code",
        "default_value": str(getConf("code_version")),
        "pattern": "[0-9\.]+"
    }, {
        "class": "",
        "name": "app_lang",
        "td_text": "Default language",
        "placeholder": "Default language for the app",
        "default_value": "",
        "pattern": "[a-z_]+"
    }, {
        "class": "",
        "name": "keyboard",
        "td_text": "Default keyboard",
        "placeholder": "Default keyboard for the app",
        "default_value": "",
        "pattern": "[a-zA-Z-_]+"
    }, {
        "class": "",
        "name": "screen_saver_time",
        "td_text": "Screen saver time",
        "placeholder": "Time in seconds for screen saver",
        "default_value": "",
        "pattern": "[0-9]+"
    }, {
        "class": "",
        "name": "def_platform_number",
        "td_text": "Default platform number",
        "placeholder": "Default platform number for products",
        "default_value": "",
        "pattern": "[\-]{0,1}[0-9]{0,1}"
    }, {
        "class": "",
        "name": "question_id",
        "td_text": "Default question ID",
        "placeholder": "Default question for 3rd screen",
        "default_value": "",
        "pattern": "[a-z0-9-_]+"
    }]
    return col_params


# Changes the html static version of the app to a random value
def change_static_version():
    v = getConf("code_version")
    new_conf_values = {}
    new_conf_values["html_static_version"] = v + "." + str(random.randint(0, 10000))
    change_conf_prop(new_conf_values)


# Common method to change conf.propertis file
def change_conf_prop(p_conf):
    path = getConfPath("conf_properties_path")
    LOG.info("Changing values: " + str(p_conf))
    lines = []
    res = ""
    try:
        with open(path, "r") as orig:
            lines = orig.readlines()

        with open(path, "w+") as new:
            for line in lines:
                # If line has property and it must be edited
                if "=" in line and line.split("=")[0] in p_conf.keys():
                    prop = line.split("=")[0]
                    LOG.info("Changing conf.properties: " + str(prop) + " to " + str(p_conf[prop]))
                    LOG.info("Current value: " + str(getConf(prop)))
                    new.write(str(prop) + "=" + str(p_conf[prop]) + "\n")
                    LOG.info("Value changed!")
                else:
                    new.write(line)
    except Exception:
        res = "Error editing conf properties file"
        LOG.error(res)
        LOG.error(str(traceback.format_exc()))
    return res
'''
-------------------------------------- GCODE -------------------------------
'''





def createRectangleCNC(x0, x1, y0, y1):
    # A partir de PD obtenemos x0, x1, y0, y1
    cncPathFile = str(getConfPath("order_folder")) + "/" + str(getConf("red_rectangle")) + ".cnc"
    print("createRectangleCNC")
    print("cncPathFile = " + cncPathFile)
    print("(" + str(x0) + "," + str(x1) + "," + str(y0) + "," + str(y1) + ")")

    fichier = open(cncPathFile, 'w')
    ##Commandes de base et creation variables START of CNC FILE
    fichier.write("G17")
    fichier.write("\n" + "G71")
    fichier.write("\n" + "G90")
    fichier.write("\n" + "$SC_DEFINES = 1")
    fichier.write("\n" + "SC_SELECT_JOB[6]")
    fichier.write("\n" + "$SC_FIELDXMIN = -30")
    fichier.write("\n" + "$SC_FIELDXMAX =  30")
    fichier.write("\n" + "$SC_FIELDYMIN = -30")
    fichier.write("\n" + "$SC_FIELDYMAX =  30")
    fichier.write("\n" + "$SC_FIELDXGAIN =  1")
    fichier.write("\n" + "$SC_FIELDYGAIN =  1")
    fichier.write("\n" + "$SC_FIELDXOFFSET =  0")
    fichier.write("\n" + "$SC_FIELDYOFFSET =  0")
    fichier.write("\n" + "$SC_FIELDAXISSTATE = 3")
    fichier.write("\n" + "SC_TOOL_LOAD[1]")
    fichier.write("\n" + "$SC_LASERPOWER = 50.0")
    fichier.write("\n" + "$SC_FREQUENCY = 30.0")
    fichier.write("\n" + "$SC_STDBYPERIOD = 0.0")
    jump_speed = 9000
    fichier.write("\n" + "$SC_JUMPSPEED = " + str(jump_speed))
    fichier.write("\n" + "$SC_MARKSPEED = 2000")
    fichier.write("\n" + "$SC_JUMPDELAY = 100.0")
    fichier.write("\n" + "$SC_MARKDELAY = 50")
    fichier.write("\n" + "$SC_POLYDELAY = 0")
    fichier.write("\n" + "$SC_LASERONDELAY = 30.0")
    fichier.write("\n" + "$SC_LASEROFFDELAY = 130.0")
    fichier.write("\n" + "SC_TOOL_SAVE[1]")
    fichier.write("\n" + "$SC_ArcStepMode = 1")
    fichier.write("\n" + "$SC_DEFINES = 0")
    # Path ABDACBDC
    AB = math.fabs(y1 - y0)
    DA = math.fabs(x1 - x0)
    BD = math.pow(AB*AB + DA*DA, 0.5)
    AC = BD
    CB = DA
    DC = AB
    CA = AC
    total_length_eng = AB + BD + DA + AC + CB + BD + DC + CA
    # TODO configure this time in seconds from input
    t_seconds = 30
    n_loops = t_seconds / (total_length_eng / jump_speed)
    print(" --!- t_seconds:", t_seconds)
    print(" --!- total_length_eng:", total_length_eng)
    print(" --!- jump_speed:", jump_speed)
    print(" --!- n_loops:", n_loops)
    fichier.write("\n" + "$SC_LoopCount = " + str(n_loops))
    fichier.write("\n" + "SC_Start_Entity")
    fichier.write("\n" + "SC_TOOL_LOAD[1]")
    fichier.write("\n" + "SC_TOOL[0]")

    # Diagonals to the square
    # Path ABDACBDC
    fichier.write("\n" + "G0 X" + str(x0) + " Y" + str(y0))  # A
    fichier.write("\n" + "G0 X" + str(x0) + " Y" + str(y1))  # B
    fichier.write("\n" + "G0 X" + str(x1) + " Y" + str(y0))  # D
    fichier.write("\n" + "G0 X" + str(x0) + " Y" + str(y0))  # A
    fichier.write("\n" + "G0 X" + str(x1) + " Y" + str(y1))  # C
    fichier.write("\n" + "G0 X" + str(x0) + " Y" + str(y1))  # B
    fichier.write("\n" + "G0 X" + str(x1) + " Y" + str(y0))  # D
    fichier.write("\n" + "G0 X" + str(x1) + " Y" + str(y1))  # C



    '''fichier.write("\n" + "G0 X" + str(x0) + " Y" + str(y0)) # A
    fichier.write("\n" + "G0 X" + str(x0) + " Y" + str(y1)) # B
    fichier.write("\n" + "G0 X" + str(x1) + " Y" + str(y1)) # C
    fichier.write("\n" + "G0 X" + str(x1) + " Y" + str(y0)) # D'''

    fichier.write("\n" + "SC_End_Entity")
    fichier.write("\n" + "G0 X0 Y0")
    fichier.write("\n" + "M2")













import Common.Order as Order
def storeOrderTempVariables(varName, varValue):
    # Opens or creates a order.txt file in the folder /Order to store temporary data like estimated engraving time
    print("storeOrderTempVariables")
    print(Order.order_config)
    Order.order_config[varName] = varValue


def getOrderTempVariables(varName):
    # Returns the value of a variable in the order.txt file inside tjhe /Order folder
    if varName in Order.order_config:
        return Order.order_config[varName]
    else:
        return ""


def arr(nombre, arrondi=2):
    return round(nombre, arrondi)



def rotate_matrix(x, y, angle, x_shift=0, y_shift=0, units="DEGREES"):
    """
    Rotates a point in the xy-plane counterclockwise through an angle about the origin
    https://en.wikipedia.org/wiki/Rotation_matrix
    :param x: x coordinate
    :param y: y coordinate
    :param x_shift: x-axis shift from origin (0, 0)
    :param y_shift: y-axis shift from origin (0, 0)
    :param angle: The rotation angle in degrees
    :param units: DEGREES (default) or RADIANS
    :return: Tuple of rotated x and y
    """

    # Shift to origin (0,0)
    x = x - x_shift
    y = y - y_shift

    # Convert degrees to radians
    if units == "DEGREES":
        angle = math.radians(angle)

    # Rotation matrix multiplication to get rotated x & y
    xr = (x * math.cos(angle)) - (y * math.sin(angle)) + x_shift
    yr = (x * math.sin(angle)) + (y * math.cos(angle)) + y_shift

    return xr, yr





def projectXinCircle(xmm, F, R):
    angle = xmm/R
    x = F * R * math.sin(angle) / (F + R * (1 - math.cos(angle)))
    return round(x, 2)


def projectYinCircle(xmm, ymm, F, R):
    angle = xmm/R
    # Auto-adjustment for curved products that we usually place closer to the laser point
    F = F - 5
    y = ymm * F / (F + R - R * math.cos(angle))
    return round(y, 2)


def get_xmm_ymm(idx, idy, pos_init, hatch, R=None):
    # If we engrave in horizontal, we rotate the axis
    F = float(getConf("focal_distance"))
    if not R:
        R = getProductRadius()
    x = pos_init[0] + idx * hatch
    xmm = projectXinCircle(x, F, R)
    ymm = projectYinCircle(x, (pos_init[1] + idy * hatch), F, R)
    # We dont call this function get_xmm_ymm_horizontal because we dont want to rotate the coordinates in this function
    #return get_xmm_ymm_horizontal(idx, idy, True, pos_init, hatch, R)
    return xmm, ymm


def get_xmm_ymm_horizontal(idx, idy, vertical, pos_init, hatch, R=None):
    # If we engrave in horizontal, we rotate the axis
    F = float(getConf("focal_distance"))
    # TODO change this getOrderTempVariables with a variable as input for the function
    angle = getOrderTempVariables("hatch_angle")
    if not R:
        R = getProductRadius()
    if vertical:
        x = pos_init[0] + idx * hatch
        y = pos_init[1] + idy * hatch
        x, y = rotate_matrix(x, y, angle)
        xmm = projectXinCircle(x, F, R)
        ymm = projectYinCircle(x, y, F, R)
    else:
        x = pos_init[0] + idy * hatch
        y = pos_init[1] + idx * hatch
        x, y = rotate_matrix(x, y, angle)
        xmm = projectXinCircle(x, F, R)
        ymm = projectYinCircle(x, y, F, R)
    return round(xmm, 2), round(ymm, 2)


# Returns the size of the engravable square and reduces the size of the image to treat to engrave
# removing empty lines
def get_square_pixel(L2D, pos_init, hatch):
    idx0, idx1, idy0, idy1 = 0, 0, 0, 0
    x0 = 30.0
    x1 = -30.0
    y0 = 30.0
    y1 = -30.0
    new_pos_init = pos_init
    newL2D = []

    #overwrite_R = getProductRadius()
    overwrite_R = 9999999

    #print("\t\t\t AAAA - pos_init INIT:", pos_init)
    # FINDING FIRST row with engraving
    for idx, linex in enumerate(L2D):
        if sum(linex) != 0:
            #print("\t\t\t AAAA - pos_init INIT - 2:", pos_init)
            xmm, ymm = get_xmm_ymm(idx, 0, pos_init, hatch, overwrite_R)
            #new_pos_init[0] = xmm
            #new_pos_init[0] = -25.65
            #print("\t\t\t\t AAAA - RRRRR:", getProductRadius(), "-----")
            #print("\t\t\t\t AAAA - idx:", idx)
            #print("\t\t\t\t AAAA - pos_init:", pos_init)
            #print("\t\t\t\t AAAA - hatch:", hatch)
            #print("\t\t\t\t AAAA - xmm:", xmm)
            #new_pos_init[0] = get_xmm_ymm(idx, 0, pos_init, hatch, 99999)[0]
            #print("\t\t\t\t AAAA - new_pos_init[0]:", new_pos_init[0])
            idx0 = idx
            x0 = xmm
            break

    # FINDING LAST row with engraving
    for idx in range(len(L2D)-1, 0, -1):
        if sum(L2D[idx]) != 0:
            xmm, ymm = get_xmm_ymm(idx, 0, pos_init, hatch, overwrite_R)
            idx1 = idx
            x1 = xmm
            break
    newL2D.append([0] * len(L2D[0]))

    # FINDING FIRST AND LAST columns with engraving
    for idx in range(idx0, idx1+1):
        linex = L2D[idx]
        newL2D.append(linex)
        if sum(linex) != 0:
            for idy, val in enumerate(linex):
                if val != 0:
                    xmm, ymm = get_xmm_ymm(idx, idy, pos_init, hatch, overwrite_R)
                    if ymm < y0:
                        y0 = ymm
                        #new_pos_init[1] = 23.1
                        #print("\t\t\t\t y0:", y0)
                        #print("\t\t\t\t ymm:", ymm)
                        #new_pos_init[1] = get_xmm_ymm(idx, idy, pos_init, hatch, 99999)[1]
                        #print("\t\t\t\t new_pos_init[1]:", new_pos_init[1])
                        idy0 = idy
                    elif ymm > y1:
                        y1 = ymm
                        idy1 = idy

    # Adding an empty square around the engraving
    for i in range(len(newL2D)):
        newL2D[i] = newL2D[i][idy0-1:idy1+2]
        newL2D[i].append(0)
    # We need even numbers of rows and columns for the traspose
    # We add an element to each line
    if len(newL2D[0]) % 2 is not 0:
        for row in newL2D:
            row.append(0)
    # We need odd number because we need to add a new line after
    if len(newL2D) % 2 is not 1:
        newL2D.append([0] * len(newL2D[0]))
    newL2D.append([0] * len(newL2D[0]))
    new_pos_init = x0, y0
    # The returned pos init should be without the projection correction.
    # otherwise the engraving is shifted
    ids = [idx0, idx1, idy0, idy1]

    # Adjusting red rectangle coordinates
    F = float(getConf("focal_distance"))
    R = getProductRadius()
    new_y0 = projectYinCircle(x0, y0, F, R)
    new_y1 = projectYinCircle(x0, y1, F, R)
    if new_y0 < y0:
        y0 = new_y0
    if new_y1 > y1:
        y1 = new_y1

    x0 = projectXinCircle(x0, F, R)
    x1 = projectXinCircle(x1, F, R)
    return x0, x1, y0, y1, newL2D, new_pos_init, ids


def obtainHorizontalPD(L2Darray, pos_init, hatch, use_new_cnc_method):
    LOG.info("obtainHorizontalPD HORIZONTAL INIT")
    print("-------------------------------------------------obtainHorizontalPD HORIZONTAL INIT")
    L2D = np.array(L2Darray).transpose()
    return obtainVerticalPD(L2D, pos_init, hatch, use_new_cnc_method, False)


def obtainVerticalPD(L2D, pos_init, hatch, use_new_cnc_method, vertical=True):
    LOG.info("obtainVerticalPD VERTICAL INIT")
    print("-------------------------------------------------obtainVerticalPD VERTICAL INIT")
    print("pos_init - 3 - ", pos_init)
    verticalPD = []
    xmm, ymm = get_xmm_ymm_horizontal(0, 0, vertical, pos_init, hatch)
    prev_pui = 0

    for idx, linex in enumerate(L2D):
        #print(idx, "of ", len(L2D))
        LPD = []  # Line Pattern Definition --> This line [[a, b, c], [a, b, c], [a, b, c]...]
        # a --> 0 (white, dont engrave) or 1 (black, engrave)
        # b, c --> location of point to move in mm
        # If the linex sums 0, it means there is no engraving, so we skip the calculations
        if sum(linex) != 0:
            for idy, pixel in enumerate(linex):

                # If pixel is 0, pui must be 0 because pixel is white, so we dont engrave. Otherwise, is 1
                pui = 0 if pixel == 0 else 1

                if pui != prev_pui:
                    # If laser is going to turn on, we move to the current point with the laser off, and then turn on
                    if pui == 1:
                        xmm, ymm = get_xmm_ymm_horizontal(idx, idy, vertical, pos_init, hatch)
                        # First iteration we dont have previous, so the laser is Off and the position is the initial
                        curr_pixelPD = [pui, xmm, ymm]
                        LPD.append([0, xmm, ymm])
                        LPD.append(curr_pixelPD)
                    else:
                    # If laser is going to turn off, we move to the previous pixel, the last "ON" pixel in this segment
                        xmm, ymm = get_xmm_ymm_horizontal(idx, idy-1, vertical, pos_init, hatch)
                        LPD.append([prev_pui, xmm, ymm])
                prev_pui = pui
            # These 3 lines will make the engraving be the old method, going to the end of the engraving zone
            # TODO to fully minimize the CNC generation, we can create 2 new "obtainVerticalPD" functions
            # TODO moving this condition and the red rectangle to different functions
            if not use_new_cnc_method:
                xmm, ymm = get_xmm_ymm_horizontal(idx, len(linex), vertical, pos_init, hatch)
                if vertical:
                    prev_pixelPD = [pui, xmm, 30]
                else:
                    prev_pixelPD = [pui, 30, ymm]
                LPD.append(prev_pixelPD)
        if LPD:
            verticalPD.append(LPD)
    return verticalPD


# Stores the CNC and UNF times in a CSV to analyse performance
def storeTimesInCSV(cnc_time, unf_time, pinNumber, cst):
    # cst = cnc_sub_times
    # Type, double hatch, CNC time, UNF time
    file_path = getConfPath("performance_path")
    laser_data = getLaserParams(pinNumber)
    doubleHatch = laser_data.get("doublehatch") == "1"
    ##### CHANGE THIS VALUE DEPENDING ON THE TAG YOU WANT TO PUT
    custom_label = laser_data.get("for_performance_csv")
    add_header = not os.path.exists(file_path)
    with open(file_path, 'a') as f:
        if add_header:
            f.write("Double hatch,CNC time,UNF time,New CNC method,Hatch angle,Engraving time,")
            f.write("L2D_time,PD_time,CNC_write_time,PD2_time,Custom text\n")
        # Double hatch, CNC time, UNF time, new cnc method, Hatch angle, custom label
        f.write(str(doubleHatch) + "," + str(cnc_time) + "," + str(unf_time) + ",")
        f.write(str(laser_data.get("use_new_cnc_method")) + "," + str(laser_data.get("hatch_angle")) + ",")
        f.write(str(getOrderTempVariables("engravingTime")) + ",")
        f.write(str(cst["L2D_time"]) + "," + str(cst["PD_time"]) + "," + str(cst["CNC_write_time"]) + ",")
        f.write(str(cst["PD2_time"]) + ",")
        f.write(str(custom_label) + "\n")


def get_smallest_hatch_from_laser_params(lasers, product_hatch):
    # If fonts dont have a defined hatch, we use the one from the old configuration where it was at product level
    hatch = float(product_hatch)
    for laser in lasers:
        if "hatch" in laser and float(laser.get("hatch")) < hatch:
            hatch = float(laser.get("hatch"))
    return hatch

'''
-------------------------------------- INIT E-COMMERCE mode functions ----------------------------------------------
'''
e_commerce_active_now = None
e_commerce_admin_active_now = False


def allow_switch_modes():
    return getCollectionConf("allow_switch_modes")


def e_commerce_active():
    global e_commerce_active_now
    if e_commerce_active_now is None:
        e_commerce_active_now = getCollectionConf("e_commerce_active")
    return e_commerce_active_now


def switch_e_commerce_active_state():
    global e_commerce_active_now
    e_commerce_active_now = not e_commerce_active_now


# Returns if the user has admin mode active at the moment
def e_commerce_admin_active():
    global e_commerce_admin_active_now
    return e_commerce_admin_active_now


# Changes the admin mode status for e-commerce
def change_e_commerce_admin_state(new_state):
    global e_commerce_admin_active_now
    e_commerce_admin_active_now = new_state


# Checks if the received password matches the stored password
def check_admin_password(password):
    print("check_admin_password")
    admin_password = getCollectionConf("admin_password")
    print("received password:", password)
    print("admin_password:", admin_password)
    return password == admin_password

'''
-------------------------------------- END E-COMMERCE mode functions ----------------------------------------------
'''
'''
-------------------------------------- INIT custom clients functions ----------------------------------------------
'''

import svgpathtools

# takes in a svg files and outputs a list of paths, themselves made of a list of segments, themselves listing points
def svgToListOfPoints(svg_file):
    # loads the SVG file and the paths its made of
    paths, attributes = svgpathtools.svg2paths(svg_file)
    # accuracy of path drawing
    accuracy = 30
    # goes through all the paths and segments to turn them into points
    pathsToPoints = []
    for thesePaths in paths:
        paths = []
        for thisPath in thesePaths:
            points = []
            for i in range(accuracy):
                points.append([thisPath.point(i/(accuracy-1)).real, thisPath.point(i/(accuracy-1)).imag])
            paths.append(points)
        pathsToPoints.append(paths)

    return pathsToPoints

# takes in a list of paths, themselves made of a list of segments, themselves listing points
# takes in the size of the square the engraving should fit into
# outputs a cnc file
def pathToGCode(paths, engravingSize, pos_init, figure_name, rotate=0):
    # dirty way to find the max and min in X and Y but the files are usually pretty small so whatever
    allPoints = []
    for path in paths:
        for segment in path:
            for points in segment:
                allPoints.append(points)

    allx = [item[0] for item in allPoints]
    ally = [item[1] for item in allPoints]

    maxX = max(allx)
    minX = min(allx)
    maxY = max(ally)
    minY = min(ally)

    # gets the center of the drawing
    pathSizePxX = maxX - minX
    pathSizePxY = maxY - minY
    pathCenterX = minX + pathSizePxX/2
    pathCenterY = minY + pathSizePxY/2

    # gets the drawing's ratio = ratio between the discretized svg and the size of the engraving
    if pathSizePxX > pathSizePxY:
        ratio = engravingSize/pathSizePxX
    else:
        ratio = engravingSize/pathSizePxY

    # writing gcode
    gcode = "DFS " + str(figure_name) + "\n"
    for path in paths:
        for segment in path:
            # getting the X,Y for the first pixel of the segment, taking into account the ratio to fit the size we want and centering
            # fast move to it
            x0 = (segment[0][0] - pathCenterX) * ratio
            y0 = (segment[0][1] - pathCenterY) * ratio
            if rotate != 0:
                x0, y0 = rotate_matrix(x0, y0, rotate)
            firstPixX = round(x0 + pos_init[0], 2)
            firstPixY = -round(y0 + pos_init[1], 2)
            gcode = gcode + 'SC_TOOL[0]\nG0 X' + str(firstPixX) + ' Y' + str(firstPixY) + '\nSC_TOOL[1]\n'

            # idem for each following points in the engraving except the laser is on
            for thisPoint in segment:
                x0 = (thisPoint[0] - pathCenterX) * ratio
                y0 = (thisPoint[1] - pathCenterY) * ratio
                if rotate != 0:
                    x0, y0 = rotate_matrix(x0, y0, rotate)
                thisPixX = round(x0 + pos_init[0], 2)
                thisPixY = -round(y0 + pos_init[1], 2)
                gcode = gcode + 'G1 X' + str(thisPixX) + ' Y' + str(thisPixY) + '\n'

    gcode = gcode + "ENDDFS\n"
    return gcode


def create_hennessy_tiger_cnc_figure():
    # Getting product ID
    prod_data = getProductData()
    product_id = prod_data.get("product_id")
    if "product_category_id" in prod_data:
        product_id = prod_data.get("product_category_id") + "/" + product_id
    # Getting engraving text
    textToEngrave = getOrderTempVariables("textToEngrave")
    n_lines = len(textToEngrave.split("\n"))
    response, common_PEN = get_tiger_size_and_pos_init_from_product_id(product_id)
    print("HENNESSY - response:", response)
    angle = response.get("angle")
    sizeEngraving = response.get("sizeEngraving")
    pos_init = response.get("pos_init")
    new_line_step = response.get("new_line_step")
    for i in range(0, n_lines-1):
        pos_init = list(map(sum, zip(pos_init, new_line_step)))
    print("HENNESSY - END pos_init:", pos_init)
    figure_name = "tiger_" + str(sizeEngraving) + "_" + str(pos_init[0]) + "_" + str(pos_init[1]) + "_" + str(angle)
    figure_name = figure_name.replace("-", "minus")
    figure_name = figure_name.replace(".", "point")
    cnc_path = getConfPath("order_folder") + "/Tigers/" + figure_name + ".cnc"

    if not os.path.isdir(getConfPath("order_folder") + "/Tigers"):
        os.mkdir(getConfPath("order_folder") + "/Tigers")

    if not os.path.exists(cnc_path):
        # select the SVG and sets parameters
        svg_file = getConfPath("Common_folder") + "/CustomFiles/Hennessy/TIGRE.svg"

        # génération du gcode
        print('Generating GCode...')
        print('Generating GCode for image, DFS figure...')
        listOfStuffToEngrave = svgToListOfPoints(svg_file)
        gcode = pathToGCode(listOfStuffToEngrave, sizeEngraving, pos_init, figure_name, response.get("angle"))
        print('GCode generated...')

        # sauvegarde du fichier gcode
        gcodefile = open(cnc_path, 'w')
        gcodefile.write(gcode)
        gcodefile.close()
        print('GCode saved.')
    else:
        print("This tiger has already been calculated")

    return figure_name, cnc_path, response.get("nbpassage"), response.get("PEN")



def get_tiger_size_and_pos_init_from_product_id(product_id):
    # Common PEN number for all produts with tiger
    common_PEN = 111
    # Position and size of the tiger are the same no matter the color
    properties_tiger = {
        # ----------- COL CLIPSE celestial_blue-----------
        "paradise_accessories/col_clipse_paradise_celestial_blue": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 3,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- COL CLIPSE magnetic_grey-----------
        "paradise_accessories/col_clipse_paradise_magnetic_grey": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 1,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- COL CLIPSE sunlight_yellow -----------
        "paradise_accessories/col_clipse_paradise_sunlight_yellow": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 2,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- COL CLIPSE stone_beige -----------
        "paradise_accessories/col_clipse_paradise_stone_beige": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 1,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- COL CLIPSE joy_red -----------
        "paradise_accessories/col_clipse_paradise_joy_red": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 2,
            "angle": -90,
            "PEN": common_PEN
        },

        # ----------- COL CLIPSE elixir_brown -----------
        "paradise_accessories/col_clipse_paradise_elixir_brown": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 4,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- COL CLIPSE dandy_green -----------
        "paradise_accessories/col_clipse_paradise_dandy_green": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 1,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- COL CLIPSE prestige_red  -----------
        "paradise_accessories/col_clipse_paradise_prestige_red": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 2,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- COL CLIPSE outfit Harrods  -----------
        "paradise_accessories/col_clipse_outfit__paradise_green_harrods": {
            "sizeEngraving": 5,
            "pos_init": [0, -18],
            "nbpassage": 6,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- COL CLIPSE outfit col_clipse_outfit__prestige-red  -----------
        "paradise_accessories/col_clipse_outfit__prestige-red": {
            "sizeEngraving": 5,
            "pos_init": [0, -18],
            "nbpassage": 6,
            "angle": -90,
            "PEN": common_PEN
        },
                # ----------- COL CLIPSE outfit col_clipse_outfit__celestial_blue  -----------
        "paradise_accessories/col_clipse_outfit__celestial_blue": {
            "sizeEngraving": 5,
            "pos_init": [0, -18],
            "nbpassage": 6,
            "angle": -90,
            "PEN": common_PEN
        },
                # ----------- COL CLIPSE outfit col_clipse_outfit__elixir_brown  -----------
        "paradise_accessories/col_clipse_outfit__elixir_brown": {
            "sizeEngraving": 5,
            "pos_init": [0, -18],
            "nbpassage": 6,
            "angle": -90,
            "PEN": common_PEN
        },
                # ----------- COL CLIPSE outfit col_clipse_outfit__stone_beige  -----------
        "paradise_accessories/col_clipse_outfit__stone_beige": {
            "sizeEngraving": 5,
            "pos_init": [0, -18],
            "nbpassage": 6,
            "angle": -90,
            "PEN": common_PEN
        },
                # ----------- COL CLIPSE outfit col_clipse_outfit__joy_red  -----------
        "paradise_accessories/col_clipse_outfit__joy_red": {
            "sizeEngraving": 5,
            "pos_init": [0, -18],
            "nbpassage": 6,
            "angle": -90,
            "PEN": common_PEN
        },
                # ----------- COL CLIPSE outfit col_clipse_outfit__magnetic_grey  -----------
        "paradise_accessories/col_clipse_outfit__magnetic_grey": {
            "sizeEngraving": 5,
            "pos_init": [0, -18],
            "nbpassage": 6,
            "angle": -90,
            "PEN": common_PEN
        },
                # ----------- COL CLIPSE outfit col_clipse_outfit__sunlight_yellow  -----------
        "paradise_accessories/col_clipse_outfit__sunlight_yellow": {
            "sizeEngraving": 5,
            "pos_init": [0, -18],
            "nbpassage": 6,
            "angle": -90,
            "PEN": common_PEN
        },
                # ----------- COL CLIPSE outfit col_clipse_outfit__dandy_green  -----------
        "paradise_accessories/col_clipse_outfit__dandy_green": {
            "sizeEngraving": 5,
            "pos_init": [0, -18],
            "nbpassage": 6,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- COL CLIPSE  Harrods  -----------
        "paradise_accessories/col_clipse_paradise_green_harrods": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 15,
            "angle": -90,
            "PEN": common_PEN
        },

        # ----------- CRAVATE -----------
        "paradise_accessories/cravate_paradise_": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 1,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- LINK -----------
        "paradise_accessories/link_": {
            "sizeEngraving": 5,
            "pos_init": [0, -5],
            "nbpassage": 1,
            "angle": -90,
            "PEN": common_PEN
        },
        # ----------- hennessy XO 70 cl -----------
        "hennessy_XO/70cl": {
            "sizeEngraving": 6,
            "pos_init": [0, 7],
            "new_line_step": [0, 5],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
        # ----------- hennessy XO 100 cl -----------
        "hennessy_XO/100cl": {
            "sizeEngraving": 6,
            "pos_init": [0, 7.5],
            "new_line_step": [0, 5],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
        # ----------- hennessy XO 150 cl -----------
        "hennessy_XO/150cl": {
            "sizeEngraving": 6,
            "pos_init": [0, -7],
            "new_line_step": [0, 5],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
        # ----------- hennessy XO 300 cl -----------
        "hennessy_XO/300cl": {
            "sizeEngraving": 6,
            "pos_init": [0, -2.5],
            "new_line_step": [0, 6.9],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
        # ----------- hennessy XXO 100 cl -----------
        "hennessy_XXO/100cl": {
            "sizeEngraving": 6,
            "pos_init": [0, 8],
            "new_line_step": [0, 3.75],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
        # ----------- hennessy JAMES 100 cl -----------
        "Hennessy-James/100cl": {
            "sizeEngraving": 6,
            "pos_init": [0, 8],
            "new_line_step": [0, 3.75],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
        # ----------- hennessy Paradis 150 cl -----------
        "paradise/Paradis_150cl": {
            "sizeEngraving": 6,
            "pos_init": [0, -1],
            "new_line_step": [0, 4.9],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
        # ----------- hennessy Paradis 70 cl -----------
        "paradise/Paradis": {
            "sizeEngraving": 6,
            "pos_init": [0, 4.5],
            "new_line_step": [0, 4.75],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
        # ----------- hennessy Richard 100 cl -----------
        "hennessy_richard/100cl": {
            "sizeEngraving": 6,
            "pos_init": [0, 9],
            "new_line_step": [0, 5],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
                # ----------- hennessy Richard 70cl_2 -----------
        "hennessy_richard/70cl_2": {
            "sizeEngraving": 6,
            "pos_init": [0, -10],
            "new_line_step": [0, 5],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
                        # ----------- hennessy James 100cl -----------
        "Hennessy-James/100cl": {
            "sizeEngraving": 6,
            "pos_init": [0, 9],
            "new_line_step": [0, 5],
            "nbpassage": 4,
            "angle": 0,
            "PEN": common_PEN
        },
    }

    # Default sizes and position
    response = {}

    for key in properties_tiger.keys():
        # Checking if product starts with the key
        if product_id.startswith(key):
            response = properties_tiger.get(key)
            if "new_line_step" not in response:
                response["new_line_step"] = [0, 0]
            break
    return response, common_PEN

# Function that says if we should add the tiger to the engraving
def append_tiger_to_cnc():
    add_tiger = getCollectionConf("id_brand") == "Hennessy"
    # Tiger could be used in all products except in cravate
    add_tiger = add_tiger and not getProductData().get("product_id").startswith("cravate_")
    # Checking if user added tiger
    pictogram = getOrderTempVariables("pictogram")
    add_tiger = add_tiger and "tigre" in pictogram
    return add_tiger


'''
-------------------------------------- END custom clients functions ----------------------------------------------
'''

def get_positioning_media_name():
    global productData
    # Checking if product has a specific media
    if "custom_position_image" in productData:
        media_name = productData.get("custom_position_image")
    else:
      if "custom_position_image_DS2" in productData:
          media_name = productData.get("custom_position_image_DS2")
      else:
          # We use the default images depending on the machine model
          machine_model = getConf("machine_type")
          if machine_model.upper() == "XL":
              media_name = "platformNumberXL.png"
          else:
              media_name = "platformNumber" + str(getProductPlatformNumber()) + ".png"
    return media_name


'''
-------------------------------------- END custom clients functions ----------------------------------------------
'''

def get_cale_name():
    global productData
    # Checking if product has a specific cale
    if "cale_name" in productData:
        cale_name = productData.get("cale_name")
    else:
          
        cale_name = str(getProductCale())
    return cale_name
