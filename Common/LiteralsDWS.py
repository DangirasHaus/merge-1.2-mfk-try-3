import io, os
import ast
import json, traceback
#from . import functions as app

LiteralsObject = {}
LiteralsDefaultObject = {}
errorsObject = {}


def getLit(literalname):
    global LiteralsObject
    try:
        return LiteralsObject[literalname]
    except:
        try:
            return LiteralsDefaultObject[literalname]
        except:
            return "TEXT MISSING..."


def getAllLits():
    return LiteralsObject


def init(customFilePath, errorsFilePath, generalFilePath):
    global LiteralsObject
    # We cant put this paths here because we will get an error from importing module between themselves
    #customFilePath = app.getConfPath("custom_literals_path")
    #errorsFilePath = app.getConfPath("json_errors_path")
    if os.path.exists(customFilePath):
        filePath = customFilePath
    else:
        filePath = generalFilePath
    initErrors(errorsFilePath)
    try:
        file = io.open(filePath, mode="r", encoding="utf-8")
        data = file.read()
        literals = data.split("\n")
        for k in range(0, len(literals)):
            if "=" in literals[k]:
                serie = literals[k].split('=')
                LiteralsObject[serie[0]] = serie[1]
        file.close()
        return "ok"
    except Exception:
        print("Error reading literals file:", str(traceback.format_exc()))
        return "not ok"


# ----------------- QUESTIONS 3rd SCREEN -----------------

def initQuestions(questionsFilePath):
    global questionsObject
    with io.open(questionsFilePath, encoding="utf-8") as data_file:
        questionsObject = json.load(data_file)


def getQuestion(qid):
    global questionsObject
    qids = [item["qid"] for item in questionsObject]
    if not qid or qid not in qids:
        # Default question id if collection doesnt have any defined
        qid = "question_1"
    q = [item for item in questionsObject if item.get("qid") == qid]
    return q[0]

# ----------------- ERROR messages -----------------
def initErrors(errorsFilePath):
    global errorsObject
    with io.open(errorsFilePath) as data_file:
        errorsObject = json.load(data_file)


def returnResponse(errorCode):
    if errorCode is not None:
        try:
            errorName = errorsObject[errorCode]["name"]
            errorMsg = errorsObject[errorCode]["description"]
            errorInstr = getLit("error" + errorCode + "_instructions")
            return {"result": "KO", "msg": str(errorMsg), "code": str(errorCode), "errorName": str(errorName),
                    "errorInstr": str(errorInstr)}
        except Exception:
            errorCode = "000"
            errorName = errorsObject[errorCode]["name"]
            errorMsg = errorsObject[errorCode]["description"]
            errorInstr = getLit("error" + errorCode + "_instructions")
            return {"result": "KO", "msg": str(errorMsg), "code": str(errorCode), "errorName": str(errorName),
                    "errorInstr": str(errorInstr)}
    else:
        return {"result": "OK", "msg": "", "code": ""}


def getErrorInstr(errorCode):
    global errorsObject
    import Common.ServerConnection.Connection as server
    # Test if the error code exists
    if errorCode in errorsObject:
        server.informError(errorCode, errorsObject[errorCode])
        instructions = ast.literal_eval(getLit("error" + errorCode + "_instructions"))
        instructions.append(getLit("scree3_modal_contact_admin"))
        return instructions
    else:
        instructions = ast.literal_eval(getLit("error000_instructions"))
        instructions.append(getLit("scree3_modal_contact_admin"))
        print("No instruction for error: " + str(errorCode))
        return instructions
