from ftplib import FTP
from telnetlib import Telnet
import os, traceback, io
import time
import Common.LogsDWS as LOG
import Common.LiteralsDWS as L
import Common.functions as app


# Returns true if the laser is engraving at the moment
def is_laser_on():
    laser_off = True
    try:
        destination = app.getLaserIP()  #Ip du USCserver (statique)
        telnetTimeout = int(app.getConf("laser_Telnet_timeout"))
        tn = Telnet(destination, timeout=telnetTimeout)
        tn.read_until(b"INET> ", telnetTimeout)
        tn.write(b"M \r\n")
        # We need to wait a bit for the response from the laser.
        time.sleep(0.5)
        laser_status = tn.read_eager()
        # If laser_status contains 0:1, the laser is on
        #laser_on = "0:1" in str(laser_status)
        # If laser_status contains 0:0, the laser is off
        laser_off = "0:0" in str(laser_status)
    except Exception as e:
        LOG.error("Laser does not answer: " + str(e))
    return not laser_off

import socket

# Returns true if the laser is Ready External (ready to engrave)
def is_laser_ready():
    try:
        destination = app.getLaserIP()  #Ip du USCserver (statique)
        telnetTimeout = int(app.getConf("laser_Telnet_timeout"))
        tn = Telnet(destination, timeout=telnetTimeout)
        tn.read_until(b"INET> ", telnetTimeout)
        tn.write(b"OIU \r\n")
        # We need to wait a bit for the response from the laser.
        time.sleep(0.5)
        laser_status = tn.read_eager()
        LOG.info("laser ready external? " + str(laser_status))
        # If laser_status contains 0:0 or 0:4, the laser is Ready External
        # it depends on the laser version the telnet response can be "0:0" or "0:2" when it is interlock
        laser_interlock = str(app.getConf("laser_interlock"))
        print("laser_interlock iss " + str(laser_interlock))
        print("laser_status is " + str(laser_status))
        laser_ready = laser_interlock not in str(laser_status)
    except socket.timeout:
        LOG.error("LASER did not answer in time, must be off")
        laser_ready = "TimeOut"
    except Exception as e:
        LOG.error("No internet connection: " + str(e))
        laser_ready = "Error"
    return laser_ready


# Function to send laser configuration file
def sendLaserConfToLaser(laserConfFilePath):
    destination = app.getLaserIP()  #Ip du USCserver (statique)
    FTPTimeout = int(app.getConf("laser_FTP_timeout"))

    try:
        ## Upload FTP du fichier situe dans le repertoire Ordre
        LOG.info("FTP("+destination+")")
        ftp = FTP(destination, timeout=FTPTimeout)
        ftp.login()
        ftp.cwd("misc") #repertoire ou copier le fichier
        print("connected and folder found")
        #unfFileCheck = str(unfFilePath) + "CHECK"
        if os.path.exists(laserConfFilePath):
            f_dir, f_name = os.path.split(os.path.abspath(laserConfFilePath))
            f = io.open(laserConfFilePath, "rb")  # lecture du fichier sous la forme de bytes
            ftp.storbinary('STOR ' + f_name, f)  # upload du fichier
            f.close()
            ftp.close()
            print("file sent via ftp correctly ")
            return ("Ok file has been sent")
        else:
            print("Couldnt find laser file " + str(laserConfFilePath))
            return "Couldnt find laser file"

    except Exception:
        errorMsg = "ERROR trying to connect via FTP to the laser"
        print(errorMsg)
        #LOG.error(str(traceback.format_exc()))
        return (errorMsg)


def sendUnfToLaser(unfFilePath):
    stringNomFichier = ""
    destination = app.getLaserIP()  #Ip du USCserver (statique)
    FTPTimeout = int(app.getConf("laser_FTP_timeout"))
    telnetTimeout = int(app.getConf("laser_Telnet_timeout"))
    try:
        ## Upload FTP du fichier situe dans le repertoire Ordre
        LOG.info("FTP("+destination+")")
        ftp = FTP(destination, timeout=FTPTimeout)
        ftp.login()
        ftp.cwd("jobs") #repertoire ou copier le fichier
        #unfFileCheck = str(unfFilePath) + "CHECK"
        if os.path.exists(unfFilePath):
            f_dir, f_name = os.path.split(os.path.abspath(unfFilePath))
            f = io.open(unfFilePath, "rb")  # lecture du fichier sous la forme de bytes
            ftp.storbinary('STOR ' + f_name, f)  # upload du fichier
            f.close()
            ftp.close()
            stringNomFichier = f_name[:-4].encode()
            LOG.info("stringNomFichier(" + str(stringNomFichier) + ") sent correclty to laser")
        else:
            LOG.error("Couldnt find file .unf in folder" + str(unfFilePath))
            return L.returnResponse("003")

    except Exception:
        errorMsg = "ERROR trying to connect via FTP to the laser"
        LOG.error(errorMsg)
        LOG.error(str(traceback.format_exc()))
        return L.returnResponse("001")

    try:
        ## Demarrage de la gravure en commande Telnet
        LOG.info("Telnet("+destination+")")
        tn = Telnet(destination, timeout=telnetTimeout)
        tn.read_until(b"INET> ", telnetTimeout) #derniers caracteres de dialogue du server avant action utilisateur
        tn.write(b"FM 0\r\n") #Ne sert pas specialement mais empeche les bugs
        k = tn.read_eager()#peu importe le resutalt (en general 3: 0:)
        time.sleep(1)
        k = tn.read_eager()
        tn.write(b"FM 0\r\n") #Idem, pour etre sur d avoir 0:
        tn.read_until(b'0:\r\n')
        tn.write(b"JLA 6 "+stringNomFichier+b"\r\n") #positionnement du fichier sur le job 6 du server
        tn.read_until(b"0:\r\n")
        tn.write(b"JN 6\r\n") #selection du job 6 pour la gravure
        tn.read_until(b"0:\r\n")
        # If hatch_angle is defined and not 0, we add it to the rotation of the laser
        rotation_correction = str(app.getConf("rotation_correction")).encode()
        print("##@# rotation_correction:", rotation_correction)
        tn.write(b"RT " + rotation_correction + b"\r\n") # rotation correction
        tn.read_until(b"0:\r\n")
        tn.write(b"M 1\r\n") #gravure
        tn.close()
    except Exception:
        errorMsg = "ERROR trying to connect via Telnet to the laser"
        LOG.error(errorMsg)
        LOG.error(str(traceback.format_exc()))
        return L.returnResponse("002")


        #il reste a implementer des fonctionalites de depart controles via un bouton physique
        #ajouter la commande ET 1 pour attendre le trigger externe

    return L.returnResponse(None)


def init():
    """
    Ce programme est execute automatiquement par l application une fois le fichier Gcode/unf genere par le script GcodeLutens
    et que le fichier unf a ete copie dans le repertoire ordre (precedemment vide)

    Il permet de lancer directement la gravure si les conditions precdentes sont reunies.

    Le raspberry doit etre sur le meme reseau ethernet que le serveur, ce qui n est pas encore tres certain
    pour le moment les tests ont ete faits avec rasp+app connectes sur le wifi marquize et le routeur en etherne sur le serveur.

    A voir si mettre le rasp en mode hotspotwifi ne modifie pas tout niveau ip (surtout qu il a la tendance de se mettre sur le 192.168.1)
    """
    OrderDirPath = app.getConfPath("order_folder")

    unfFilePath = OrderDirPath + "/default.unf"
    return sendUnfToLaser(unfFilePath)
