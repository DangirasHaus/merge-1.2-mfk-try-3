import io, os
import json
from jsonschema import validate
import jsonschema



schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "fichierInfo.json",
    "description": "Fichier info in JSON",
    "type": "object",
    "properties": {
        "ProductName": {
            "description": "Name of the displayed product",
            "type": "string"
        },
        "engraving_angle": {
            "description": "Text angle",
            "type": "string"
        },
        "text_color": {
            "description": "Display color of the engraving text in the app",
            "type": "string"
        },
        "hauteur": {
            "description": "Height in mm of the engraving text from the bottom of the product",
            "type": "string"
        },
        "dimensions": {
            "description": "width and height in mm of the product",
            "type": "string"
        },
        "maxHeightMM": {
            "description": "max height of the engraving text",
            "type": "string"
        },
        "maxWidthMM": {
            "description": "max width of the engraving text",
            "type": "string"
        },
        "hatch": {
            "description": "Distance between engraving lines",
            "type": "string"
        },
        "laser": {
            "type": "array",
            "minItems": 1,
            "items": {
                "type": "object",
                "properties": {
                    "description": {
                        "description": "Description of the parameters",
                        "type": "string"
                    },
                    "pinLoad": {
                        "description": "Pin number, should match with laserPin of font",
                        "type": "string"
                    },
                    "puissance": {
                        "description": "Power of the laser in %",
                        "type": "string"
                    },
                    "frequence": {
                        "description": "Frecuency of the laser in Hz",
                        "type": "string"
                    },
                    "vitesse": {
                        "description": "Speed of the engraving laser in mm/s",
                        "type": "string"
                    },
                    "nbpassage": {
                        "description": "Number of passes the laser will make",
                        "type": "string"
                    },
                    "doublehatch": {
                        "description": "If 0, only horizontal hatch, if 1, also vertical",
                        "type": "string"
                    }
                },
                "required": [
                    "pinLoad",
                    "puissance",
                    "frequence",
                    "vitesse",
                    "nbpassage",
                    "doublehatch"
                ]
            }
        },
        "fonts": {
            "type": "array",
            "minItems": 1,
            "items": {
                "type": "object",
                "properties": {
                    "police": {
                        "description": "File name of the font",
                        "type": "string"
                    },
                    "policeName": {
                        "description": "Name that will be displayed in the app",
                        "type": "string"
                    },
                    "fontsize": {
                        "description": "Height of the text in mm",
                        "type": "string"
                    },
                    "laserPin": {
                        "description": "Matches with the pinLoad to assign this font to a set of parameters",
                        "type": "string"
                    }
                },
                "required": [
                    "police",
                    "policeName",
                    "fontsize",
                    "laserPin"
                ]
            }
        },
        "engraving_area": {
            "type": "object",
            "properties":{
                "fix_engraving_location": {
                    "description": "If True, doesnt allow the user to move the text on the tablet",
                    "type": "string"
                },
                "engraving_restriction": {
                    "description": "When fix_engraving_location is False, if this is True, we set limits to where the user can move the text",
                    "type": "string"
                },
                "engraving_box_x_0": {
                    "description": "In mm, horizontal distance between the border of the product and the limit of the restricted area",
                    "type": "string"
                },
                "engraving_box_y_0": {
                    "description": "In mm, vertical distance between the border of the product and the limit of the restricted area",
                    "type": "string"
                },
                "engraving_box_x_len": {
                    "description": "In mm, horizontal length of the restricted area",
                    "type": "string"
                },
                "engraving_box_y_len": {
                    "description": "In mm, vertical length of the restricted area",
                    "type": "string"
                },
            },
            "required": [
                "fix_engraving_location",
                "engraving_restriction",
                "engraving_box_x_0",
                "engraving_box_y_0",
                "engraving_box_x_len",
                "engraving_box_y_len"
            ]
        }
    },
    "required": [
        "ProductName",
        "engraving_angle",
        "text_color",
        "hauteur",
        "dimensions",
        "maxHeightMM",
        "maxWidthMM",
        "hatch",
        "laser",
        "fonts",
        "engraving_area"
    ]
}


# Reads the old fichier info file and stores it in a dictionary
def get_old_format(fichier_info_name):
    old_format = {}
    #print(fichier_info_name)
    old_format["folder_name"] = os.path.basename(os.path.dirname(fichier_info_name))
    with io.open(fichier_info_name, 'r') as f:
        lines = f.read().replace("\r", "").replace("\t","")

        lines = lines.split('\n###Comments###')[0].split('\n')
        f.seek(0)
        for l in lines:
            if "=" in l:
                key = l.split("=")[0]
                value = l.split("=")[1].replace("\n", "")
                old_format[key] = value
    return old_format

# Returns a dictionary with the new format
def old_foramt_to_new(old_format):
    new_format = {}
    print(old_format)
    if old_format.get("ProductName") is None:
        new_format["ProductName"] = old_format.get("folder_name")
    else:
        new_format["ProductName"] = old_format.get("ProductName")
    if old_format.get("vertical_engraving") is None:
        new_format["engraving_angle"] = "0"
    else:
        new_format["engraving_angle"] = old_format.get("vertical_engraving")
    if old_format.get("text_color") is None:
        new_format["text_color"] = "black"
    else:
        new_format["text_color"] = old_format.get("text_color")
    new_format["hauteur"] = old_format.get("hauteur")
    new_format["dimensions"] = old_format.get("dimensions")
    if old_format.get("maxHeightMM") is None:
        new_format["maxHeightMM"] = "20"
    else:
        new_format["maxHeightMM"] = old_format.get("maxHeightMM")
    if old_format.get("maxWidthMM") is None:
        new_format["maxWidthMM"] = "20"
    else:
        new_format["maxWidthMM"] = old_format.get("maxWidthMM")
    new_format["hatch"] = old_format.get("hatch")
    new_format["laser"] = []
    new_format["laser"].append({})
    new_format["laser"][0]["description"] = "You can write a description here"
    new_format["laser"][0]["pinLoad"] = "1"
    new_format["laser"][0]["puissance"] = old_format.get("puissance")
    new_format["laser"][0]["frequence"] = old_format.get("frequence")
    new_format["laser"][0]["vitesse"] = old_format.get("vitesse")
    new_format["laser"][0]["nbpassage"] = old_format.get("nbpassage")
    new_format["laser"][0]["doublehatch"] = old_format.get("doublehatch")


    new_format["fonts"] = []
    old_fonts = old_format.get("police").split(",")
    if old_format.get("policeName"):
        old_font_names = old_format.get("policeName").split(",")
    else:
        old_font_names = []
        for font in old_format.get("police").split(","):
            old_font_names.append(font.split(".")[0])

    it = 0
    for font in old_fonts:
        new_format["fonts"].append({})
        new_format["fonts"][it]["police"] = font
        new_format["fonts"][it]["policeName"] = old_font_names[it]
        if old_format.get("fontsize") is None:
            new_format["fonts"][it]["fontsize"] = old_format.get("Hauteurmax").split(",")[0]
        else:
            new_format["fonts"][it]["fontsize"] = old_format.get("fontsize")
        new_format["fonts"][it]["laserPin"] = "1"
        it = it + 1

    new_format["engraving_area"] = {}
    new_format["engraving_area"]["fix_engraving_location"] = old_format.get("fix_engraving_location")
    new_format["engraving_area"]["engraving_restriction"] = old_format.get("engraving_restriction")
    new_format["engraving_area"]["engraving_box_x_0"] = old_format.get("engraving_box_x_0")
    new_format["engraving_area"]["engraving_box_y_0"] = old_format.get("engraving_box_y_0")
    new_format["engraving_area"]["engraving_box_x_len"] = old_format.get("engraving_box_x_len")
    new_format["engraving_area"]["engraving_box_y_len"] = old_format.get("engraving_box_y_len")
    new_format["engraving_area"]["dmm"] = old_format.get("dmm")
    return new_format


# Stores the new fichier info JSON file in a location
def store_new_format(new_format, new_file_path):
    new_name = "fichierInfo.json"
    with open(new_file_path + "/" + new_name, 'w') as fp:
        json.dump(new_format, fp, indent=2)


def get_all_fichier_info(folder_path, name):
    list = []
    files = os.listdir(folder_path)
    for file in files:
        file_full_path = folder_path + "/" + file
        if file == name and "Pictures" in files:
            list = list + [folder_path + "/" + file]
        elif os.path.isdir(file_full_path):
            list = list + get_all_fichier_info(folder_path + "/" + file, name)
    return list


def get_all_fichier_info_txt(folder_path):
    return get_all_fichier_info(folder_path, "fichierInfo.txt")


def get_all_fichier_info_json(folder_path):
    return get_all_fichier_info(folder_path, "fichierInfo.json")


def transform_old_fichier_info(folder_path):

    fichier_info_list = get_all_fichier_info_txt(folder_path)
    for file in fichier_info_list:
        #print("file = ", file)
        old_format = get_old_format(file)
        json_format = old_foramt_to_new(old_format)
        new_file_path = os.path.dirname(os.path.realpath(file))
        store_new_format(json_format, new_file_path)

    fichier_info_json_list = get_all_fichier_info_json(folder_path)
    incorrect_json = []
    for json_file in fichier_info_json_list:
        resp = check_new_fichier_info(json_file)
        if resp != "OK":
            incorrect_json.append(resp)
    return incorrect_json

    #print("END")


def check_new_fichier_info(file):
    print("checking fichier info file:", file)
    with open(file, 'r') as fp:
        try:
            productData = json.load(fp)
            validate(productData, schema)
            print("Record #{}: OK")
            return "OK"
        except jsonschema.exceptions.ValidationError as ve:
            print("\n\n\nRecord #{}: ERROR")
            #print(str(ve.message))
            #print(str(ve.path))
            #print(str(ve) + "\n\n\n\n")
            return {"file:": str(file), "error": str(ve.message) + " | " + str(ve.path)}


#print(check_new_fichier_info("C:\DWS\GIT\DecorWorldServices\AAlYCE\Collections\VivaTech_mikel/Products/flyer/fichierInfo.json"))



folder_path = "C:\DWS\GIT\DecorWorldServices\AAlYCE"
folder_path = "C:\DWS\GIT\DecorWorldServices\Collection_repository"
#folder_path = "C:\DWS\GIT\DecorWorldServices\AAlYCE\Collections\DiorDemo10_19"
files_with_errors = transform_old_fichier_info(folder_path)
print(files_with_errors)


