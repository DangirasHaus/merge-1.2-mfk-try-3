import os, json, time, io
import glob
import requests as req

path = os.path.dirname(os.path.realpath(__file__))
col_conf = "collection_conf.properties"

def getFileParametersJSON(filePath):
    with io.open(filePath, encoding="utf-8") as json_file:
        productData = json.load(json_file)
    return productData


def getFileParameters(filePath):
    parameters = {}
    file = open(filePath)
    full_text = file.read().replace("\r", "").replace("\t","")
    file.close()
    lines = full_text.split('\n')
    for i in range(len(lines)):
        if len(lines[i].split("=")) > 1:
            value = lines[i].split("=")[1]
            parameters[lines[i].split("=")[0]] = value
    return parameters

def getIdBrand(collection):
    conf_prop = getFileParameters(path + "/" + collection + "/" + col_conf)
    return conf_prop.get("id_brand")

collections = os.listdir(path)
print(collections)

productos = []

for col in collections:
    if os.path.isdir(path + "/" + col) and os.path.exists(path + "/" + col + "/" + col_conf):
        id_brand = getIdBrand(col)
        prod_folders = os.listdir(path + "/" + col + "/Products")
        print("id_brand:", id_brand, prod_folders)
        for folder in prod_folders:
            fol_path = path + "/" + col + "/Products/" + folder
            if os.path.exists(fol_path + "/Pictures"):
                fi = fol_path + "/fichierInfo.json"
                if os.path.exists(fi):
                    prod_data = getFileParametersJSON(fi)
                    id_product = folder
                    productName = prod_data.get("ProductName")
                    # Add producto
                    print("\tAdding product:", id_product)
                    prod_json = {}
                    prod_json["idBrand"] = id_brand
                    prod_json["idShop"] = id_brand + "_" + id_product
                    prod_json["idProduct"] = id_product
                    prod_json["description"] = "Description"
                    prod_json["productName"] = productName
                    prod_json["volume"] = productName
                    prod_json["parameters"] = prod_data
                    productos.append(prod_json)
            else:
                prod_2_folders = os.listdir(fol_path)
                for prod in prod_2_folders:
                    fi = fol_path + "/" + prod + "/fichierInfo.json"
                    if os.path.exists(fi):
                        prod_data = getFileParametersJSON(fi)
                        id_product = folder + "/" + prod
                        productName = prod_data.get("ProductName")
                        # Add producto
                        print("\tAdding product:", id_product)
                        prod_json = {}
                        prod_json["idBrand"] = id_brand
                        prod_json["idShop"] = id_brand + "_" + id_product
                        prod_json["idProduct"] = id_product
                        prod_json["description"] = "Description"
                        prod_json["productName"] = productName
                        prod_json["parameters"] = prod_data
                        productos.append(prod_json)

print(productos)

with open("lista_products.txt", "w") as dataFile:
    for product in productos:
        dataFile.write(str(product) + "\n")



def makeReq(url, payload):
    headers = {'content-type': 'application/json'}
    response = {}
    # Payload will be a DTO the first time called, and will be a JSON for the retrying
    if type(payload) is dict:
        data = json.dumps(payload)
    else:
        data = payload.toJSON()
    try:
        response = req.post(url, data=data, headers=headers, timout=3)
    except req.exceptions.Timeout:
        print("First request got an error, trying a second time")
        try:
            response = req.post(url, data=data, headers=headers, timeout=3)
        except req.exceptions.Timeout:
            print("Second request failed")
    except req.exceptions.TooManyRedirects:
        print("The url used is not correct, please use another one")
    except Exception as e:
        # catastrophic error. bail.
        print("Error sending request: " + str(e))
    finally:
        return response


print("\n\ndoing request...")
url = "https://api-dev.my-dws.com/put/addproducts"

print("dat:", productos[95])
res = makeReq(url, productos[95])
print(res)
print(res.text)
for prod in productos:
    print("dat:", prod)
    res = makeReq(url, prod)
    print(res)
    print(res.text)
    if "Incorrect" in res.text:
        print("BRAND DOESNT EXIST in DB")
    time.sleep(1)

