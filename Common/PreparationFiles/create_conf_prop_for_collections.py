import os, io, sys

def get_all_files(folder_path):
    list = []
    files = os.listdir(folder_path)
    for file in files:
        file_full_path = folder_path + "/" + file
        if "Products" in files:
            list = list + [folder_path]
            return list
        elif os.path.isdir(file_full_path):
            list = list + get_all_files(folder_path + "/" + file)
    return list


def has_conf_properties(path):
    return "collection_conf.properties" in os.listdir(path)




#folder_path = "C:\DWS\GIT\DecorWorldServices\AAlYCE"
folder_path = "/home/pi/Documents/AAlYCE/Collections"
res = get_all_files(folder_path)
for collection in res:
    if has_conf_properties(collection):
        print("Properties")
    else:
        print("Collection", collection, "doesn't have properties")
        with io.open(collection + "/collection_conf.properties", "w", encoding="utf-8") as conf:
            conf.write(u"# Server database information\n")
            print("Collection: " + str(collection) + "\n")
            print("id_brand:")
            id_brand = sys.stdin.readline()
            conf.write(u"id_brand=" + str(id_brand))
            print("id_shop:")
            id_shop = sys.stdin.readline()
            conf.write(u"id_shop=" + str(id_shop))
            conf.write(u"code_version=" + str("3.1") + "\n")
            conf.write(u"app_lang=\n")
            print("creating file")
        if os.path.exists(collection + "/collection_conf.txt"):
            os.remove(collection + "/collection_conf.txt")
            print("deleted test files. txt")


print(res)