from svgpathtools import *

############ Main GCode generation ##############

# takes in a svg files and outputs a list of paths, themselves made of a list of segments, themselves listing points
def svgToListOfPoints(svg_file):
    # loads the SVG file and the paths its made of
    paths, attributes = svg2paths(svg_file)
    # accuracy of path drawing
    accuracy = 30

    # goes through all the paths and segments to turn them into points
    pathsToPoints = []
    for thesePaths in paths:
        paths=[]
        for thisPath in thesePaths:
            points=[]
            pathParsed = Path(thisPath)
            for i in range(accuracy):
                points.append([thisPath.point(i/(accuracy-1)).real, thisPath.point(i/(accuracy-1)).imag])
            paths.append(points)
        pathsToPoints.append(paths)

    return pathsToPoints

# takes in a list of paths, themselves made of a list of segments, themselves listing points
# takes in the size of the square the engraving should fit into
# outputs a cnc file
def pathToGCode(paths, engravingSize, pos_init, figure_name):
    # dirty way to find the max and min in X and Y but the files are usually pretty small so whatever
    allPoints = []
    for path in paths:
        for segment in path:
            for points in segment:
                allPoints.append(points)

    allx = [item[0] for item in allPoints]
    ally = [item[1] for item in allPoints]

    maxX = max(allx)
    minX = min(allx)
    maxY = max(ally)
    minY = min(ally)

    # gets the center of the drawing
    pathSizePxX = maxX - minX
    pathSizePxY = maxY - minY
    pathCenterX = minX + pathSizePxX/2
    pathCenterY = minY + pathSizePxY/2

    # gets the drawing's ratio = ratio between the discretized svg and the size of the engraving
    if pathSizePxX > pathSizePxY:
        ratio = engravingSize/pathSizePxX
    else:
        ratio = engravingSize/pathSizePxY

    # writing gcode
    gcode = "DFS nombre_figura\n"
    # paths[index of paths][index of segments making up the paths][index of points making up the segments]
    for path in paths:
        for segment in path:
            # getting the X,Y for the first pixel of the segment, taking into account the ratio to fit the size we want and centering
            # fast move to it
            firstPixX = round(((segment[0][0] - pathCenterX) * ratio), 2) + pos_init[0]
            firstPixY = -round(((segment[0][1] - pathCenterY) * ratio), 2) + pos_init[1]
            gcode = gcode + 'SC_TOOL[0]\nG0 X' + str(firstPixX) + ' Y' + str(firstPixY) + '\nSC_TOOL[1]\n'

            # idem for each following points in the engraving except the laser is on
            for thisPoint in segment:
                thisPixX = round(((thisPoint[0] - pathCenterX) * ratio), 2) + pos_init[0]
                thisPixY = -round(((thisPoint[1] - pathCenterY) * ratio), 2) + pos_init[1]
                #if thisPixX != prevX and thisPixY != prevY:
                gcode = gcode + 'G1 X' + str(thisPixX) + ' Y' + str(thisPixY) + '\n'
                #prevX = thisPixX
                #prevY = thisPixY

    gcode = gcode + "ENDDFS\n"
    return gcode

########### Basic GCode stuff #################""





######## Example use ##########




def create_hennessy_tiger_cnc_figure(sizeEngraving, pos_init):
    figure_name = str(sizeEngraving) + "_" + str(pos_init[0]) + "_" + str(pos_init[1]) + ".cnc"
    cnc_path = "" + figure_name

    # select the SVG and sets parameters
    svg_file = "/CustomFiles/Hennessy/TIGRE.svg"

    # génération du gcode
    print('Generating GCode...')
    print('Generating GCode for image, DFS figure...')
    listOfStuffToEngrave = svgToListOfPoints(svg_file)
    gcode = pathToGCode(listOfStuffToEngrave, sizeEngraving, pos_init, figure_name)
    print('GCode generated...')

    # sauvegarde du fichier gcode
    gcodefile = open(figure_name, 'w')
    gcodefile.write(gcode)
    gcodefile.close()
    print('GCode saved.')

    return cnc_path
