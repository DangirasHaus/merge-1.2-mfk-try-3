import json
import Common.functions as app

# DTO of addEngravingEngravings_request
class informError_request:
    def __init__(self, idBrand, idShop, errorLog, url=""):
        self.idBrand = idBrand
        self.idShop = idShop
        self.errorLog = errorLog
        self.url = url
        self.failedRequest = False
        self.code_version = app.getConf("code_version")

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.toJSON()

# DTO of files to send
class files_request:
    def __init__(self, cnc):
        #TODO add unf and png
        self.cnc = cnc

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.toJSON()

# DTO of getShopExists_request
class shopExists_request:
    def __init__(self, idBrand, idShop):
        self.idBrand = idBrand
        self.idShop = idShop
        self.code_version = app.getConf("code_version")

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.toJSON()

# DTO of getShopExists_request
class addNewProduct_request:
    def __init__(self, idBrand, idProduct, productName, description, parameters, volume, productCategoryId="", categoryName=""):
        self.dto = {
            "idBrand": idBrand,
            "idProduct": idProduct,
            "productName": productName,
            "categoryName": categoryName,
            "productCategory": productCategoryId,
            "description": description,
            "parameters": parameters,
            "volume": volume,
            "failedRequest": False,
            "code_version": app.getConf("code_version")
        }

    def toJSON(self):
        return json.dumps(self.dto, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.toJSON()

    def __setitem__(self, key, value):
        #explicitly defined __setitem__
        self.dto[key] = value

#DTO of lasTimeShutDown (sended when shutwon tablet)
class lasTimeShutDown_dto:
    def __init__(self, idBrand, idShop, Date, lastTimeShutdown, url=""):
        self.idBrand = idBrand
        self.idShop = idShop
        self.Date = Date
        self.lastTimeShutdown = lastTimeShutdown
        self.url = url
        self.failedRequest = False
        self.code_version = app.getConf("code_version")

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.toJSON()

# a class to create response
class mock_http_response:
    def __init__(self, status_code, message):
        self.status_code = status_code
        self.message = message

    def json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.json()

# DTO of addEngravingEngravings_request
class addEngraving_request:
    def __init__(self, idBrand, idTask, idProduct, idShop, Text, Time, Date, Satisfaction, parameters, font="",
                 idQuestion="question_1", questionType="stars", errorMessage="", stopped=False,relaunch=False, url="",
                 engravingComment=None, storeIdProduct=None):
        self.idBrand = idBrand
        self.idShop = idShop
        self.idTask = idTask
        self.idProduct = idProduct
        self.Text = Text
        self.font = font
        self.Time = Time
        self.Date = Date
        self.Satisfaction = Satisfaction
        self.Version = app.getConf("code_version")
        self.idQuestion = idQuestion
        self.questionType = questionType
        self.errorMessage = errorMessage
        self.stopped = bool(stopped)
        self.relaunch = bool(relaunch)
        self.url = url
        self.parameters = parameters
        self.failedRequest = False
        if engravingComment:
            self.comment = engravingComment
        if storeIdProduct:
            self.storeIdProduct = storeIdProduct
        self.code_version = app.getConf("code_version")

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.toJSON()

# DTO of update e-engraving
class e_engraving_request:
    def __init__(self, idBrand, ts, url, headers):
        self.idBrand = idBrand
        self.ts = ts
        self.url = url
        self.headers = headers
        self.failedRequest = False

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.toJSON()


'''
-----------------------------------------------------------------------------------------------------------
------------------------------------------------NOT IN USE-------------------------------------------------
-----------------------------------------------------------------------------------------------------------
'''

# DTO of getShopEngravings_request
class getShopEngravings_request:
    def __init__(self, idBrand, idShop):
        self.idBrand = idBrand
        self.idShop = idShop
        self.code_version = app.getConf("code_version")

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.toJSON()



# DTO of getShopEngravings_response
class getShopEngravings_response:
    def __init__(self, response):
        self.engravings = json.loads(response.text)
        self.code_version = app.getConf("code_version")

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.toJSON()

    #responseObject.__init__(self, attribut["idBrand"], attribut["Date"], attribut["Satisfaction"], attribut["idShopTask"], attribut["idProduct"], attribut["idShop"], attribut["Version"], attribut["idTask"], attribut["Text"], attribut["Time"])
