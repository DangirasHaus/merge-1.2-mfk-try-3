import json, os
from datetime import datetime
import Common.functions as app
import Common.ServerConnection.Connection as server
import Common.LogsDWS as LOG
import urllib.request
import shutil
import urllib.parse
# This file will contain the functions to update collections, products and potentially code


# Stores and overwrites the downloaded configuration of a product
def storeDownloadedProduct(prod_data_server, overwrite_existing_product=True):
    LOG.info("storeDownloadedProduct INIT")
    LOG.info("prod_data_server: " + str(prod_data_server))
    LOG.info("overwrite_existing_product: " + str(overwrite_existing_product))
    print("-----------------overwrite_existing_product: " + str(overwrite_existing_product))
    json_to_store = {}
    json_to_store["default"] = prod_data_server["parameters"]
    LOG.info("storeDownloadedProduct error 1")
    json_to_store["default"]["product_id"] = prod_data_server["parameters"]["product_id"]
    LOG.info("storeDownloadedProduct error 2")
    json_to_store["default"]["product_category_id"] = prod_data_server["parameters"].get("product_category_id")
    LOG.info("storeDownloadedProduct error 3")
    json_to_store["default"]["product_category_name"] = prod_data_server.get("categoryName")
    LOG.info("storeDownloadedProduct error 4")
    json_to_store["default"]["id_brand"] = prod_data_server["realIdBrand"]
    LOG.info("storeDownloadedProduct error 5")
    if "timestamp" in prod_data_server:
        json_to_store["default"]["timestamp"] = prod_data_server["timestamp"]
    if "editionDate" in prod_data_server:
        json_to_store["default"]["editionDate"] = prod_data_server["editionDate"]
    LOG.info("storeDownloadedProduct error 6")


    # If we dont want to overwrite an existing product, we store is a temporary product
    if not overwrite_existing_product:
        LOG.info("We want to create a temporary product")
        createBasicProductStructureFolder(json_to_store, prod_data_server["idProduct"])
    # We can overwrite and existing product if we want to update it
    else:
        LOG.info("We want to overwrite the product")
        overwriteProductConf(json_to_store, prod_data_server["idProduct"])


def sort_fichier_info_json(fi_conf):
    fi_sorted = {}
    fi = fi_conf["default"]
    for key in sorted(fi.keys(), key=lambda s: s.lower()):
        # Sorting fonts and laser
        if key == "fonts" or key == "laser":
            fi_sorted[key] = []
            for font in fi[key]:
                fontSorted = {}
                for fontkey in sorted(font.keys(), key=lambda s: s.lower()):
                    fontSorted[fontkey] = font[fontkey]
                fi_sorted[key].append(fontSorted)
        # Sorting engraving area
        elif key == "engraving_area":
            fi_sorted[key] = {}
            for eakey in sorted(fi[key].keys(), key=lambda s: s.lower()):
                fi_sorted[key][eakey] = fi[key][eakey]
        else:
            fi_sorted[key] = fi[key]
    return {"default": fi_sorted}


# Orders a collection_conf.json file
def sort_collection_conf_json(cc_json):
    fi_sorted = {}
    for key in sorted(cc_json.keys(), key=lambda s: s.lower()):
        fi_sorted[key] = cc_json[key]
        print(key, " :: ", cc_json[key])
    return fi_sorted


# Creates the basic structure for a temporary product for SAFE
def createBasicProductStructureFolder(prod_data, real_prod_id, temp_prod=app.getConf("use_temp_prod_safe") == "True"):
    LOG.info("Storing product data in local for [" + str(real_prod_id) + "]")
    if temp_prod:
        folder_name = app.getConf("temp_prod_id")
    else:
        folder_name = real_prod_id
        prod_data["default"]["DefImageName"] = "default.png"
        prod_data["default"]["ImageName"] = "default.png"
    temp_prod_path = app.getConfPath("products_id_folder") + "/" + folder_name
    temp_prod_img_path = temp_prod_path + "/Pictures"
    # Creating main folder
    if not os.path.isdir(temp_prod_path):
        os.makedirs(temp_prod_path)
    # Creating pictures folder
    if not os.path.isdir(temp_prod_img_path):
        os.makedirs(temp_prod_img_path)
    # Creating fichier info
    fichier_info_path = app.getConfPath("products_id_folder") + "/" + folder_name + "/" + app.getConf("fichier_info_filename")
    with open(fichier_info_path, "w", encoding="utf-8") as nfi:
        json.dump(sort_fichier_info_json(prod_data), nfi, indent=2, ensure_ascii=False)
    # Creating product image
    image_path_online = "https://www.my-dws.com/assets/img/Product/"
    image_path_online = image_path_online + prod_data.get("default").get("id_brand") + "/"
    image_path_online = image_path_online + real_prod_id + ".png"
    store_img = temp_prod_img_path# + prod_data.get("DefImageName")
    download_image_from_server(image_path_online, store_img, "default.png")


# Downloads an image from the server
def download_image_from_server(download_url, store_path, img_name):
    try:
        if not os.path.isdir(store_path):
            os.makedirs(store_path)
        full_path = store_path + "/" + img_name
        r = urllib.request.urlopen(download_url)
        LOG.info("Downloading image from server")
        with open(full_path, "wb") as f:
            f.write(r.read())
    except Exception as e:
        LOG.error("Error downloading image file: " + str(e))


# This function overwrites an existing product configuration
def overwriteProductConf(prod_data, prod_id):
    prod_cat = None
    if prod_data["default"].get("product_category_id"):
        prod_cat = prod_data["default"].get("product_category_id")
    prod_path = app.getConfPath("products_id_folder") + "/" + prod_id
    LOG.info("Prod path to overwrite: " + str(prod_path))
    # We make a back up of the category folder just in case
    productBackUp(prod_path, prod_cat)
    # Now we overwrite the product configuration
    createBasicProductStructureFolder(prod_data, prod_id, False)


# This function creates a backup of a soon-to-be-overwritten product folder
def productBackUp(prod_path, prod_cat=None):
    LOG.info("productBackUp prod_path:" + str(prod_path))
    backup_folder = app.getConfPath("backups_folder")
    products_backups_folder = app.getConfPath("products_backups_folder")
    # Creating backup folders if they dont exist
    if not os.path.isdir(backup_folder):
        os.mkdir(backup_folder)
    if not os.path.isdir(products_backups_folder):
        os.mkdir(products_backups_folder)
    # Copying product category folder to backup
    date_stamp = str(datetime.now().strftime("%Y-%m-%d_%H-%M"))
    # Adding time stamp subfolder to be able to come back to older versions
    folder_name = os.path.basename(prod_path) + "/" + date_stamp
    # We add prod_cat as parent folder if received
    if prod_cat:
        folder_name = prod_cat + "/" + folder_name
    copy_and_overwrite(prod_path, products_backups_folder + "/" + folder_name)


# Using shutil, we will copy and overwrite the folder
def copy_and_overwrite(from_path, to_path):
    if os.path.exists(from_path):
        if os.path.exists(to_path):
            shutil.rmtree(to_path)
        shutil.copytree(from_path, to_path)
    else:
        LOG.error("No product to make a backup")
        print("No product to make a backup")


# Downloads a product from the server
def getProductDataFromServer(idBrand, idProduct, token, user_system_update=False):
    url = app.getConf("tanpet_api_base_url") + app.getConf("tanpet_api_download_product")
    headers = {
        "Authorization": token
    }
    body = {
        "idBrand": idBrand,
        "idProduct": idProduct,
        "code_version": app.getConf("code_version")
    }
    # We are in SAFE, so we add a new parameter to get the whole product configuration
    if not user_system_update:
        body["forSAFE"] = True
    LOG.info("Getting product from server.")
    LOG.info("url:" + str(url))
    LOG.info("body:" + str(body))
    res = server.makeReq(url, body, headers)
    json_response = {}
    is_there_an_update = False
    if res and res.status_code == 200:
        json_response = res.json()
        json_response["status"] = res.status_code
        LOG.info("Product data retrieved correctly: " + str(len(json_response)))
        if user_system_update:
            print("user_system_update:", user_system_update)
            print("json_response.get:", json_response)
            local_prod = app.setCurrentProduct(idProduct)
            # If we have "timestamp" in response, this means we have an available version for our code
            #  - json_response is from server
            #  - local_prod is local
            if app.check_timestamps(json_response, local_prod, "idProduct"):
                storeDownloadedProduct(json_response)
                is_there_an_update = True
        else:
            storeDownloadedProduct(json_response)
    else:
        try:
            json_response = {"status": res.status_code}
        except Exception as e:
            LOG.error("no status_code in res: " + str(res))
        LOG.error("Error getting product data")
    print("json_response:", json_response)
    return json_response, is_there_an_update

''' ---------------------- Functions for system update for collection and products ---------------------- '''


# Turns a collection received from the server into a collection_conf.json - like file
def server_collection_to_conf_json(server_col, current_col):
    res = {}
    fields_to_have = [
        "app_lang", "screen_saver_time", "engravingWithOnlyRedPointer", "timestamp",
        "allow_switch_modes", "e_commerce_active", "admin_password",
        "def_platform_number", "question_id", "products_structure", "emojis", "keyboard_list"
    ]
    LOG.info("Merging collection from server and from tablet to generate collection_conf.json")
    for item in fields_to_have:
        if item in server_col:
            # We add the item from the server (if empty, we must also add it)
            print("1 - item:", item, "is in server data")
            res[item] = server_col[item]
        else:
            if item in current_col:
                # We add the item from the current collection (if empty, we must also add it)
                print("2 - item:", item, "is in current data")
                res[item] = current_col[item]
            else:
                print("\t\t3 - item:", item, "is not configured, we dont add it")
    if "products_structure" in res:
        res["products_structure"] = json.loads(res["products_structure"])
    LOG.info("collection_conf.json file generated: " + str(res))
    return res


# This function creates a backup of a soon-to-be-overwritten collection folder
def collection_back_up(col_name):
    col_path = app.getConfPath("collections_folder") + "/" + col_name
    LOG.info("collection_back_up for collection [" + col_name + "] col_path:" + str(col_path))
    backup_folder = app.getConfPath("backups_folder")
    collections_backups_folder = app.getConfPath("collections_backups_folder")
    # Creating backup folders if they dont exist
    if not os.path.isdir(backup_folder):
        os.mkdir(backup_folder)
    if not os.path.isdir(collections_backups_folder):
        os.mkdir(collections_backups_folder)
    # Copying product category folder to backup
    date_stamp = str(datetime.now().strftime("%Y-%m-%d_%H-%M"))
    # Adding time stamp subfolder to be able to come back to older versions
    folder_name = os.path.basename(col_path) + "/" + date_stamp
    copy_and_overwrite(col_path, collections_backups_folder + "/" + folder_name)


# This function overwrites an existing collection folder
def overwrite_collection_conf(col_folder_name, col_conf_json, custom_js="", custom_css=""):
    LOG.info("Storing collection data in local for [" + str(col_folder_name) + "]")
    col_path = app.getConfPath("collections_folder") + "/" + col_folder_name
    # STEP 1: Overwrite collection conf json
    # Creating folder if it does not exist
    if not os.path.isdir(col_path):
        os.makedirs(col_path)
    # Editing/Creating collection_conf.json if it does not exist
    col_conf_json_path = col_path + "/" + app.getConf("collection_conf_filename")
    with open(col_conf_json_path, "w+", encoding="utf-8") as ccj:
        LOG.info("Editing Collection_conf.json")
        json.dump(sort_collection_conf_json(col_conf_json), ccj, indent=2, ensure_ascii=False)

    # STEP 2: Overwrite collection background image
    # path to image: https://www.my-dws.com/assets/img/<ID_BRAND>/<ID_SHOP>/display_image.png
    image_path_online = "https://www.my-dws.com/assets/img/"
    image_path_online = image_path_online + col_conf_json.get("id_brand") + "/" + col_conf_json.get("id_shop") + "/"
    image_path_online = image_path_online + "display_image.png"
    store_img_path = app.getConfPath("collection_static_folder") + "/images"
    download_image_from_server(image_path_online, store_img_path, "Background_screen_1.jpg")
    # STEP 3: Overwrite custom css and custom js --> add parameters in the future coming from server
    if custom_css:
        # Editing/Creating custom.css if it does not exist
        custom_css_path = app.getConfPath("collection_static_folder") + "/custom.css"
        # Creating folder if it does not exist
        if not os.path.isdir(app.getConfPath("collection_static_folder")):
            os.makedirs(app.getConfPath("collection_static_folder"))
        with open(custom_css_path, "w+", encoding="utf-8") as ccss:
            LOG.info("Editing custom.css: " + str(custom_css))
            ccss.write(custom_css)
    if custom_js:
        # Editing/Creating custom.js if it does not exist
        custom_js_path = app.getConfPath("collection_static_folder") + "/custom.js"
        # Creating folder if it does not exist
        if not os.path.isdir(app.getConfPath("collection_static_folder")):
            os.makedirs(app.getConfPath("collection_static_folder"))
        with open(custom_js_path, "w+", encoding="utf-8") as ccjs:
            LOG.info("Editing custom.js: " + str(custom_js))
            ccjs.write(custom_js)


# This function downloads all the missing fonts from the server
def download_missing_fonts_from_server(fonts):
    # First we check what fonts we are missing from the list
    fonts_path = app.getConfPath("fonts_folder")
    available_fonts = os.listdir(fonts_path)
    for needed_font in fonts:
        if needed_font not in available_fonts:
            LOG.info("Font [" + needed_font + "] is not in device, we need to download it")
            # Then we download from the server the missing fonts and store them in staticFonts
            # path to image: https://www.my-dws.com/assets/Fonts/<FONT_NAME>
            # We need to encode the font file name to URL
            image_path_online = "https://www.my-dws.com/assets/Fonts/" + urllib.parse.quote(needed_font)
            store_path = app.getConfPath("fonts_folder")
            download_image_from_server(image_path_online, store_path, needed_font)


# Downloads a list of products from the server and stores them in local if they need to be updated
def update_products_from_server(products, id_brand):
    fonts = []
    LOG.info("update_products_from_server INIT")
    is_there_an_update = False
    for product in products:
        print("product:", product)
        LOG.info("product: " + str(product))
        # user_system_update means that the update does not come from SAFE, so we need to check the value of
        # "newUpdateAvailable" if it is TRUE, we will overwrite the product, if any other case, we don't change anything
        prod_path = app.getConfPath("products_id_folder") + "/" + product
        LOG.info("prod_path: " + str(prod_path))
        if os.path.isdir(prod_path):
            # If product is in device, we check if an update is available on the server
            user_system_update = True
        else:
            # If product is new to device, we download it always
            user_system_update = False
        LOG.info("user_system_update: " + str(user_system_update))
        # This function downloads and updates product data if needed from server
        pc, is_there_an_update_product = getProductDataFromServer(id_brand, product, app.getConf("id_machine"), user_system_update)
        # is_there_an_update is True if any product has been updated
        is_there_an_update = is_there_an_update or is_there_an_update_product
        LOG.info("pc: " + str(pc))
        # Getting all fonts use for this product
        if pc.get("parameters") and pc.get("parameters").get("fonts"):
            LOG.info("Getting all fonts use for this product")
            LOG.info("pc.get(parameters): " + str(pc.get("parameters")))
            LOG.info("pc.get(parameters).get(fonts): " + str(pc.get("parameters").get("fonts")))
            LOG.info("map: " + str(map(lambda n: n.get("police"), pc.get("parameters").get("fonts"))))
            LOG.info("list: " + str(list(map(lambda n: n.get("police"), pc.get("parameters").get("fonts")))))
            fonts = fonts + list(map(lambda n: n.get("police"), pc.get("parameters").get("fonts")))
    LOG.info("Fonts to download: " + str(fonts))
    return fonts, is_there_an_update

# Downloads a new collection from the server when the collection doesnt exist locally
def download_new_collection_from_server(id_brand, id_shop):
    '''
        If we edit the logic of these steps, pay attention to function "updateSystemCollection"
        since it has a very similar logic
    '''
    print("downloading new collection:", id_brand, id_shop)
    # STEP 1: Download collection
    col_data = server.getServerCollectionData(id_brand, id_shop)
    print("Downloaded col data:", col_data)
    # STEP 2: Create collection JSON file
    col_conf_json = server_collection_to_conf_json(col_data, {})
    col_conf_json["id_brand"] = id_brand
    col_conf_json["id_shop"] = id_shop
    # STEP 3: Create backup of current collection folder
    LOG.info("Creating backup for collection")
    col_name = app.getConf("collection")
    collection_back_up(col_name)
    # STEP 4: Store downloaded collection
    LOG.info("Overwriting collection with server data")
    css = col_data.get("customCSS")
    js = col_data.get("customJS")
    overwrite_collection_conf(col_name, col_conf_json, js, css)
    # STEP 5: Get all products from server for new collection data, if needed, backup and update them
    # The function safeApp.getProductData downloads the product, creates a back up and saves the downloaded conf
    LOG.info("Updating products in collection")
    products = app.get_products_from_products_structure(col_conf_json["products_structure"])
    # Parameter is_there_an_update is not needed here
    fonts, is_there_an_update = update_products_from_server(products, id_brand)
    # STEP 6: Check if all fonts are available and download missing fonts
    # Removing repeated fonts by creating a set
    fonts = list(set(fonts))
    LOG.info("Downloading missing fonts for products")
    download_missing_fonts_from_server(fonts)


''' ---------------------- Functions for system backup for collection and products ---------------------- '''


# Returns a list with all the backups done on the tablet since installation for COLLECTIONS
def get_all_col_backups():
    # Collection back up folder
    cbf = app.getConfPath("collections_backups_folder")
    col = app.getConf("collection")
    # Back up folder for collection
    buf = cbf + "/" + col
    if os.path.isdir(buf):
        # Getting All Collection Folders backups
        acf = os.listdir(buf)
        res = []
        for folder in acf:
            res.append({
                "date": folder.replace("_", " "),
                "path": folder
            })
    else:
        res = {}
    return res


# Returns a list with all the backups done on the tablet since installation for PRODUCTS
def get_all_prod_backups():
    # Products back up folder
    pbf = app.getConfPath("products_backups_folder")
    col_conf_json = app.getAllCollectionConf()
    # We get the list of available products for this collection
    products = app.get_products_from_products_structure(col_conf_json["products_structure"])
    print(products)
    res = []
    for prod in products:
        # Product Backup Path
        pbp = pbf + "/" + prod
        if os.path.isdir(pbp):
            # Getting All Product Folders backups
            apf = os.listdir(pbp)
            print("\t\tThis product has backup -->", prod)
            for folder in apf:
                # TODO change prod_name by the display name of colle
                res.append({
                    "date": folder.replace("_", " "),
                    "prod_name": prod.replace("_", " ").replace("/", " - "),
                    "prod_id": prod,
                    "path": prod + "/" + folder
                })
    return res


# Reinstalls a backup
def reinstall_backup(path, is_collection, prod_id):
    res = {
        "status": "OK",
        "code": 200
    }
    try:
        # STEP 1: Get backup folder
        if is_collection:
            dest_folder = app.getConfPath("data_folder")
            backup_folder = app.getConfPath("collections_backups_folder") + "/" + app.getConf("collection")
        else:
            dest_folder = app.getConfPath("products_id_folder") + "/" + prod_id
            backup_folder = app.getConfPath("products_backups_folder")
        backup_folder = backup_folder + "/" + path
        LOG.info("Restoring back up from: " + str(backup_folder))
        LOG.info("Restoring back up to: " + str(dest_folder))
        # STEP 2: Overwrite current folder with new folder
        copy_and_overwrite(backup_folder, dest_folder)
    except Exception as e:
        LOG.error("Error backing up system: " + str(e))
        res = {
            "status": "KO",
            "code": 500
        }
    return res
