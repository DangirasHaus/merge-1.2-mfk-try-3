import requests as req
import uuid, json, os, time
import Common.functions as app
import Common.ServerConnection.DTOS as dto
import Common.LogsDWS as LOG
from PIL import Image
from datetime import datetime
from threading import Thread

import traceback


def makeReq_old(url, payload, add_headers={}, files={}):
    start_req_time = time.time()
    if not files:
        headers = {'content-type': 'application/json'}
    else:
        headers = {}
    for key in add_headers.keys():
        headers[key] = add_headers[key]
    response = {}
    # Payload will be a DTO the first time called, and will be a JSON for the retrying
    if files:
        LOG.info("## request with files")
        print("## request with files")
        data = payload.__dict__
        files = files.__dict__
    elif type(payload) is dict:
        data = json.dumps(payload)
        LOG.info("## request without files ")
        print("## request without files ")
    else:
        data = payload.toJSON()
        LOG.info("## request without files ")
        print("## request without files ")
    try:
        response = req.post(url, data=data, headers=headers, files=files, timeout=3)
    except req.exceptions.Timeout:
        print("First request got an error, trying a second time")
        LOG.warning("First request got an error, trying a second time")
        try:
            response = req.post(url, data=data, headers=headers, timeout=1.5, files=files)
        except req.exceptions.Timeout:
            print("Second request failed")
            LOG.error("Second request failed")
    except req.exceptions.TooManyRedirects:
        LOG.error("The url used is not correct, please use another one")
        print("The url used is not correct, please use another one")
    except req.exceptions.ConnectionError as e:
        LOG.error("This error should be HTTPSConnectionPool: " + str(e))
        print("This error should be HTTPSConnectionPool: " + str(e))
    except Exception as e:
        traceback_output = traceback.format_exc()
        print(traceback_output)
        # catastrophic error. bail.
        print("Error sending request: " + str(e))
        LOG.error("Error sending request: " + str(e))
    finally:
        end_req_time = time.time()
        req_time = end_req_time - start_req_time
        print("Time it took to make the request: " + str(req_time))
        LOG.error("Time it took to make the request: " + str(req_time))
        return response





def makeReq_real(results, url, payload, add_headers={}, files={}):
    start_req_time = time.time()
    if not files:
        headers = {'content-type': 'application/json'}
    else:
        headers = {}
    for key in add_headers.keys():
        headers[key] = add_headers[key]
    response = {}
    # Payload will be a DTO the first time called, and will be a JSON for the retrying
    if files:
        LOG.info("Request with files")
        data = payload.__dict__
        files = files.__dict__
    elif type(payload) is dict:
        data = json.dumps(payload)
        LOG.info("Request without files ")
    else:
        data = payload.toJSON()
        LOG.info("Request without files ")
    try:
        response = req.post(url, data=data, headers=headers, timeout=3, files=files)
    except req.exceptions.Timeout:
        print("First request got an error, trying a second time")
        LOG.warning("First request got an error, trying a second time")
        try:
            response = req.post(url, data=data, headers=headers, timeout=1.5, files=files)
        except req.exceptions.Timeout:
            print("Second request failed")
            LOG.error("Second request failed")
    except req.exceptions.TooManyRedirects:
        LOG.error("The url used is not correct, please use another one")
        print("The url used is not correct, please use another one")
    except req.exceptions.ConnectionError as e:
        LOG.error("This error should be HTTPSConnectionPool: " + str(e))
        print("This error should be HTTPSConnectionPool: " + str(e))
    except Exception as e:
        # catastrophic error. bail.
        print("Error sending request: " + str(e))
        LOG.error("Error sending request: " + str(e))
    finally:
        end_req_time = time.time()
        req_time = end_req_time - start_req_time
        print("Time it took to make the request: " + str(req_time))
        LOG.error("Time it took to make the request: " + str(req_time))
        results[0] = response



def makeReq(url, payload, add_headers={}, files={}):
    print("makeReq INIT")
    general_timeout = 5
    threads = [None]
    results = [None]

    threads[0] = Thread(target=makeReq_real, args=(results, url, payload, add_headers, files, ))
    threads[0].start()

    # do some other stuff
    print("\t joining with", general_timeout, "seconds...")
    threads[0].join(general_timeout)
    print("\t", general_timeout, "seconds have passed")

    if results[0] is not None and results[0]:
        return results[0]  # what sound does a metasyntactic locomotive make?
    else:
        print("Timeout before finishing function")
        return dto.mock_http_response(408, 'Error timeout')














def addSAFEEngraving(prod_parameters, username, Text, Time, Date, font, engravingComment, storeIdProduct):
    # Some items are written by default for SAFE engravings
    idQuestion = "NA"
    questionType = "NA"
    Satisfaction = "NA"
    errorMessage = ""
    stopped = False
    relaunched = False

    # SAFE engravings always go to PRO
    url = app.getApiUrl("add_engraving_url", True)
    idProduct = prod_parameters.get("idProduct")
    idBrand = prod_parameters.get("realIdBrand")
    idTask = str(uuid.uuid4())
    # We store as "shop", the person who did the engraving
    idShop = username
    parameters = prod_parameters.get("parameters")

    requestDTO = dto.addEngraving_request(idBrand, idTask, idProduct, idShop, Text, str(Time), Date, Satisfaction,
                                          parameters, font, idQuestion, questionType, errorMessage, stopped, relaunched,
                                          engravingComment=engravingComment, storeIdProduct=storeIdProduct)

    print("SAFE Adding engraving: " + str(requestDTO))
    LOG.info("SAFE Adding engraving: " + str(requestDTO))
    response = makeReq(url, requestDTO)
    if response and response.status_code == 200:
        json_response = response.json()
        print("SAFE Engraving added correctly: " + str(json_response))
        LOG.info("SAFE Engraving added correctly: " + str(json_response))
    else:
        # Since there was an error, we store the failed request to try again in the next launch
        storeFailedRequestForLater(url, requestDTO)
        print("Error making request SAFE add_engraving_url: " + str(response))
        LOG.error("Error making request SAFE add_engraving_url")
    return response




def addEngraving(idProduct, Text, Time, Date, Satisfaction, font, idQuestion="question_1",
                 questionType="stars", errorMessage="", stopped=False,relaunched=False):
    url = app.getApiUrl("add_engraving_url", app.getConf("debug_node") != "True")
    idBrand = app.getCollectionConf("id_brand")
    idTask = str(uuid.uuid4())
    idShop = app.getCollectionConf("id_shop")
    # Now we only store parameters in SAFE
    #if app.getConf("debug_node") == "True":
    #    parameters = app.getProductData()
    #else:
    parameters = {}
    requestDTO = dto.addEngraving_request(idBrand, idTask, idProduct, idShop, Text, Time, Date, Satisfaction,
                                          parameters, font, idQuestion, questionType, errorMessage, stopped, relaunched)
    LOG.info("requestDTO: " + str(requestDTO))

    if errorMessage and errorMessage != "":
        LOG.info("Client informed an error")
        errorReceived = "TaskID: " + idTask
        informError(errorReceived, errorMessage, True, sendFiles=True)

    response = makeReq(url, requestDTO)
    if response and response.status_code == 200:
        json_response = response.json()
        print("Engraving added correctly: " + str(json_response))
        LOG.info("Engraving added correctly: " + str(json_response))
    else:
        # Since there was an error, we store the failed request to try again in the next launch
        storeFailedRequestForLater(url, requestDTO)
        LOG.error("Error making request add_engraving_url")
    return idTask


# Inform error request, sends an email with the error information
def informError(errorReceived, errorDescription, sentByShop = False, sendFiles = False):
    result = {"result": "OK", "msg": "", "code": "", "errorName": "", "errorInstr": ""}
    errorReceived = str(errorReceived)
    errorDescription = str(errorDescription)
    print("\t\terrorReceived = " + str(errorReceived))
    url = app.getApiUrl("inform_error_url", app.getConf("debug_node") != "True")
    idBrand = str(app.getCollectionConf("id_brand"))
    idShop = str(app.getCollectionConf("id_shop"))
    Version = str(app.getConf("code_version"))
    logFilePath = app.getConfPath("app_log_path")
    print("\t\tlogFilePath = " + str(logFilePath))
    logs = "ERROR READING LOGS FROM FILE"
    try:
        # We open the log file to read the last 1000 lines and send them via email
        with open(logFilePath, "r", encoding="utf-8") as log_file:
            logs = list(log_file)
            n_lines = 1000
            if len(logs) > n_lines:
                init_number = (len(logs)-n_lines)
                logs = logs[init_number:]
        logs = "".join(logs)
    except Exception as e:
        LOG.error("Error getting logs to send to server " + str(e))

    # Generation of the mail body
    errorLog = "Brand: " + idBrand + ", Shop: " + idShop + "\n"
    if sentByShop:
        errorLog = errorLog + "ERROR INFORMED BY THE SHOP\n"
    errorLog = errorLog + "Code error: " + errorReceived + "\n"
    errorLog = errorLog + "Code description: " + errorDescription + "\n"
    errorLog = errorLog + "\n"
    errorLog = errorLog + "LOG: " + logs + "\n"
    if sendFiles:
        # prepare files
        OrderDirPath = app.getConfPath("order_folder")
        cnc = open(OrderDirPath + "/" + "default.cnc", "rb")
        print("file cnc path ok")
        LOG.info("file cnc path ok")
        # TODO get also png and unf files
        # png = open(OrderDirPath + "/" + "default.png", "rb")
        # unf = open(OrderDirPath + "/" + "default.unf", "rb")
        # files = [("defaultCNC", ("default.cnc", cnc, "cnc"))
        #          # TODO send also png and unf files
        #          # ,("defaultPNG", ("default.png", png, "png")),
        #          # ("defaultUNF", ("default.unf", unf, "unf"))
        #          ]
        files = dto.files_request(cnc)
        # TODO close also png and unf files
        # png.colse()
        # unf.close()
    else:
        files = {}
    requestDTO = dto.informError_request(idBrand, idShop, errorLog)
    response = makeReq(url, requestDTO, files=files)
    if sendFiles:
        cnc.close()
    print("request sent to server and response is ready")
    LOG.info("request sent to server and response is ready")
    if response and response.status_code == 200:
        LOG.info("200 the aws response status code is " + str(response.status_code))
        result['code'] = str(response.status_code)
        result["result"] = "OK"
        result["msg"] = "Message est bien envoye"
        json_response = response.json()
        print("Error informed correctly: " + str(json_response))
        LOG.info("Error informed correctly: " + str(json_response))
    elif response and response.status_code == 404:
        result['code'] = str(response.status_code)
        LOG.info("404 the aws response status code is " + str(response.status_code))
    else:
        result['code'] = "504"
        LOG.info("the aws response status code is " + str(response))
        result["result"] = "KO"
        result["msg"] = "Message n'est pas envoyé"
        # Since there was an error, we store the failed request to try again in the next launch
        storeFailedRequestForLater(url, requestDTO)
        LOG.error("Error making request add_engraving_url")
    print("result response is ", result)
    return result


def storeFailedRequestForLater(endpointUrl, dto, headers=None):
    print("storeFailedRequestForLater INIT")
    dto.url = endpointUrl
    if headers is not None:
        dto.headers = headers
    dto.failedRequest = True
    LOG.info("failedRequest is true")
    filePath = app.getConfPath("failed_requests_path")
    # Opening file that contains previous failed requests, and appending this last request in the end
    try:
        file = open(filePath, 'a')
        file.write(str(dto.toJSON()) + "\n")
    finally:
        file.close()
    LOG.info("Failed request stored correctly: " + str(endpointUrl))
    print("Failed request stored correctly: " + str(endpointUrl))


def retryFailedRequest(requestDTO):
    print("retryFailedRequest")
    if requestDTO.get("headers"):
        response = makeReq(requestDTO["url"], requestDTO, requestDTO.get("headers"))
    else:
        response = makeReq(requestDTO["url"], requestDTO)
    requestFailedAgain = True
    if response and response.status_code == 200:
        json_response = response.json()
        print("retryFailedRequest done correctly: " + str(json_response))
        LOG.info("retryFailedRequest done correctly: " + str(json_response))
        requestFailedAgain = False
    return requestFailedAgain


def tryOldFailedRequests():
    # First, we check if we have connectivity, if we dont have, we don't ever bother retrying
    # We also check if the file "FailedRequests exists, if it doesnt, we dont have any failed request to retry
    print("tryOldFailedRequests")
    #baseApiUrl = app.getConf("api_base_url")
    #print("baseApiUrl = " + str(baseApiUrl))
    filePath = app.getConfPath("failed_requests_path")
    if os.path.exists(filePath):
        try:
            #req.get(url=baseApiUrl, timeout=3)
            there_is_internet = lasTimeShutDown()
            if there_is_internet:
                # Once internet connection has been confirmed, we retry all failed requests
                # Opening file that contains previous failed requests, and appending this last request in the end
                with open(filePath, 'r+', encoding="utf-8") as file:
                    lines = file.readlines()
                    file.seek(0)
                    i = 0
                    for line in lines:
                        print("Executing request", i, "of", len(lines))
                        time.sleep(0.5)
                        # Iteration over every failed request
                        try:
                            dto = json.loads(line)
                            print(dto["url"])
                            requestFailedAgain = retryFailedRequest(dto)
                        except Exception as e1:
                            requestFailedAgain = True
                            LOG.error("Failed request or JSON converstion: " + str(e1))
                        if requestFailedAgain:
                            # If it fails again, we re-store it in the file
                            LOG.error("request to " + dto["url"] + " failed again")
                            file.write(line)
                        else:
                            LOG.info("request to " + dto["url"] + " was correct!!")
                        i = i+1
                    file.truncate()
        except Exception as e:
            LOG.error("NO INTERNET CONNECTION: " + str(e))


# Checks if a brand and shop exist on the server
def checkIfBrandAndShopExist(idBrand, idShop, prod=False):
    url = app.getApiUrl("shop_exists_url", prod)
    requestDTO = dto.shopExists_request(idBrand, idShop)
    LOG.info("requestDTO: " + str(requestDTO))
    response = makeReq(url, requestDTO)
    res = {"result": "KO", "msg": "", "code": "", "errorName": "", "errorInstr": ""}
    if response and response.status_code == 200:
        json_response = response.json()
        LOG.info("Shop Exists?: " + str(json_response))
        if json_response:
            res["result"] = "OK"
        else:
            res["msg"] = "Shop does not exist"
            res["errorInstr"] = "Ask admin to create shop in the server"
    else:
        LOG.error("Error making request to " + str(url))
        if response:
            LOG.error("Response status: " + str(response.status_code))
            res["code"] = str(response.status_code)
        res["msg"] = "Error making request to " + str(url)
        res["errorInstr"] = "Check logs to see what was the problem"
    return res


# Creates a product in the server TODO CAMBIAR ESTA FUNCION

def storeProdInDB(prod_id, PROD=app.getConf("debug_node") != "True", path_to_mydws="", parametrizationFinished=False,
                  newUpdateAvailable=False):
    LOG.info("Sending product ID:" + str(prod_id))
    res = {"result": "KO", "msg": "", "code": "", "errorName": "", "errorInstr": ""}
    SAFE_product = True
    app.setCurrentProduct(prod_id, SAFE_product)
    LOG.info("SETTING product ID:" + str(prod_id))
    prod_data = app.getProductData()
    LOG.info("prod data to store:" + str(prod_data))

    url = app.getApiUrl("add_product_url", PROD)
    idBrand = prod_data.get("id_brand")
    idProduct = prod_data.get("product_id")
    productName = prod_data.get("ProductName")
    description = prod_data.get("ProductName")
    volume = prod_data.get("ProductName")
    productCategoryId = prod_data.get("product_category_id")
    if productCategoryId != "" and productCategoryId:
        idProduct = productCategoryId + "/" + idProduct
    categoryName = prod_data.get("product_category_name")
    requestDTO = dto.addNewProduct_request(idBrand, idProduct, productName, description, prod_data, volume,
                                           productCategoryId, categoryName)
    if parametrizationFinished:
        print("### Conncetion parametrizationFinished:" + str(parametrizationFinished))
        requestDTO["parametrizationFinished"] = parametrizationFinished
        #requestDTO.force_set("parametrizationFinished", parametrizationFinished)
    if newUpdateAvailable:
        print("### Conncetion newUpdateAvailable:" + str(newUpdateAvailable))
        requestDTO["newUpdateAvailable"] = newUpdateAvailable
        #requestDTO.force_set("newUpdateAvailable", newUpdateAvailable)
    print("### Sending product:" + str(requestDTO))
    LOG.info("Sending product:" + str(requestDTO))
    response = makeReq(url, requestDTO)

    # Moving image to MYDWS
    copy_image_to_mydws = not not path_to_mydws
    if copy_image_to_mydws:
        # Product image path
        prod_path = app.get_products_folder_path()
        img_path = prod_path + "/" + prod_id + "/Pictures/" + prod_data.get("DefImageName")
        img_ext = os.path.splitext(prod_data.get("ImageName"))[1]
        # Destination folder
        destination_folder = path_to_mydws + "/src/assets/img/Product/"
        destination_folder = destination_folder + "/" + idBrand + "/" + idProduct
        # If folder doesnt exist, we create it
        if not os.path.exists(os.path.dirname(destination_folder)):
            os.makedirs(os.path.dirname(destination_folder))
        new_file_path = destination_folder + img_ext
        # Copying file if it doesnt already exist

        new_file_path_png = new_file_path.replace(img_ext, ".png")
        if not os.path.isfile(new_file_path_png):
            im1 = Image.open(img_path)
            im1.save(new_file_path_png)

    if response and response.status_code == 200:
        json_response = response.json()
        LOG.info("Product added?: " + str(json_response))
        if json_response.get("Result") == "Product added correctly":
            res["result"] = "OK"
        else:
            res["msg"] = "Response was 200 but product was not stored"
            res["errorInstr"] = "Check the logs"
    else:
        # Since there was an error, we store the failed request to try again in the next launch
        storeFailedRequestForLater(url, requestDTO)
        LOG.error("Error making request to " + str(url))
        if response:
            LOG.error("Response status: " + str(response.status_code))
            res["code"] = str(response.status_code)
        res["msg"] = "Error making request to " + str(url)
        res["errorInstr"] = "Check logs to see what was the problem and start from the beginning"
    return res

# This function will send a reaquest to the sever if we click shutdown
def lasTimeShutDown():
    # prepare data for dto
    idBrand = app.getCollectionConf("id_brand")
    idShop = app.getCollectionConf("id_shop")
    Date = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    lastTimeShutdown = True
    there_is_internet = False

    url = app.getApiUrl("lasTimeShutDown_url", app.getConf("debug_node") != "True")
    # prepare data for request
    lasTimeShutDown_dto = dto.lasTimeShutDown_dto(idBrand, idShop, Date, lastTimeShutdown, url)
    LOG.info("lasTimeShutDown_dto is :"+str(lasTimeShutDown_dto))

    # make request
    response = makeReq(url, lasTimeShutDown_dto)

    if response and response.status_code == 200:
        json_response = response.json()
        print("lastTimeShutDown added correctly: " + str(json_response))
        LOG.info("lastTimeShutDown added correctly: " + str(json_response))
        there_is_internet = True
    else:
        storeFailedRequestForLater(url, lasTimeShutDown_dto)
        LOG.error("Error making request lasTimeShutDown_url")
    return there_is_internet


# Add or modifies a new trainee
def addNewTrainee(traineeDTO):
    url = app.getApiUrl("add_new_trainee_url", app.getConf("debug_node") != "True")
    stored = False
    LOG.info("traineeDTO: " + str(traineeDTO))

    response = makeReq(url, traineeDTO)
    status_code = response.status_code
    if response and status_code == 200:
        json_response = response.json()
        print("Trainee added correctly: " + str(json_response))
        LOG.info("Trainee added correctly: " + str(json_response))
        stored = True
    else:
        # Since there was an error, we store the failed request to try again in the next launch
        storeFailedRequestForLater(url, traineeDTO)
        LOG.error("Error making request to add new trainee " + str(status_code))
    return stored


# Gets a collection from the server
def getServerCollectionData(idBrand=app.getCollectionConf("id_brand"), idShop=app.getCollectionConf("id_shop")):
    try:
        url = app.getConf("tanpet_api_base_url") + app.getConf("tanpet_api_download_collection")
        token = app.getConf("id_machine")
        headers = {
            "Authorization": token
        }
        body = {
            "idBrand": idBrand,
            "idShop": idShop,
            "code_version": app.getConf("code_version")
        }
        print("body", body)
        res = makeReq(url, body, headers)
        json_response = {}
        if res and res.status_code == 200:
            json_response = res.json()
            json_response["status"] = res.status_code
            LOG.info("Collection data retrieved correctly: " + str(len(json_response)))
        else:
            json_response = {"status": res.status_code}
            LOG.error("Error getting collection data from server")
            LOG.error("res:" + str(res))
        print("json_response:", json_response)
    except Exception as e:
        LOG.error("Error making request to server")
        LOG.error("res:" + str(e))
        json_response = {"status": "KO"}
    return json_response

'''
-------------------------------------- E-COMMERCE mode functions ----------------------------------------------
'''


# Gets all the pending engravings from the e-commerce api
def get_pending_e_engravings():
    try:
        api_key = app.getConf("api_key")
        idStore = app.getCollectionConf("idStore")
        idBrand = app.getCollectionConf("id_brand")
        last_e_engravings_days = int(app.getConf("last_e_engravings_days"))
        url = app.getConf("eapi_prod_base_url") + app.getConf("get_last_e_engravings_url")
        headers = {
            "X-Api-Key": api_key
        }
        now = int(datetime.now().timestamp() * 1000000)
        date0 = now - last_e_engravings_days * 24*60*60*1000000
        body = {
            "idBrand":idBrand,
            "date0": date0,
            "date1": now,
        }
        print("date0",date0)
        print("date1",now)
        if idStore:
            print("idStore exists:", idStore)
            body["idStore"] = idStore
        print("body:", body)
        response = makeReq(url, body, headers)
        print(response.json())
        return response.json()
    except Exception as e:
        LOG.error("Error sending request: " + str(e))
        return None

# Gets all the  engraved by dates api lastecommercedates
def get_engraved():
    try:
        api_key = app.getConf("api_key2")
        idStore = app.getCollectionConf("idStore")
        last_e_engravings_days = int(app.getConf("last_e_engravings_days"))
        url = app.getConf("eapi_prod_base_url2") + app.getConf("get_last_e_engravings_url2")
        headers = {
            "X-Api-Key": api_key
        }
        now = int(datetime.now().timestamp() * 1000000)
        date0 = now - last_e_engravings_days * 24*60*60*1000000
        body = {
            
            "date0": date0,
            "date1": now,
            "userId": "trainee_gep",
        }
        if idStore:
            print("idStore exists:", idStore)
            body["idStore"] = idStore
        #print("body:", body)
        response = makeReq(url, body, headers)
        print(response.json())
        return response.json()
    except Exception as e:
        LOG.error("Error sending request: " + str(e))
        return None

# Makes a request to the server to get an engraving from an ID
def get_e_engraving_by_id(orderId):
    try:
        api_key = app.getConf("api_key")
        idBrand = app.getCollectionConf("id_brand")
        url = app.getConf("eapi_prod_base_url") + app.getConf("get_e_engravings_url")
        headers = {
            "X-Api-Key": api_key
        }
        body = {
            "idBrand": idBrand,
            "orderId": orderId
        }
        print("body:", body)
        response = makeReq(url, body, headers)
        print(response.json())
        return response.json(), response.status_code
    except Exception as e:
        LOG.error("Error sending request: " + str(e))
        return None


# Marks an e-engraving as engraved in the server
def update_engraving_order(ts):
    print("Updating engraving order in server ts:", ts)
    api_key = app.getConf("api_key")
    idBrand = app.getCollectionConf("id_brand")
    url = app.getConf("eapi_prod_base_url") + app.getConf("update_e_engravings_url")
    headers = {
        "X-Api-Key": api_key
    }
    requestDTO = dto.e_engraving_request(idBrand, ts, url, headers)

    try:
        response = makeReq(url, requestDTO, headers)
        print(response.json())
        return response.json(), response.status_code
    except Exception as e:
        storeFailedRequestForLater(url, requestDTO, headers)
        LOG.error("Error sending request: " + str(e))
        return None
'''
-------------------------------------- END E-COMMERCE mode functions ----------------------------------------------
'''

