import json, io
import random
from os import path
from datetime import datetime
import Common.functions as app
import Common.ServerConnection.Connection as server
import Common.LogsDWS as LOG


# DTO of addEngravingEngravings_request
class trainee:
    def __init__(self, userId, firstName, lastName, dateAppGeneralTraining,
                 dateEngravingTraining, generalTrainingDuration, engravingTrainingDuration, firstEngraving,
                 generalTraining, engravingTraining, storedInServer):
        self.userId = userId
        idBrand = app.getCollectionConf("id_brand")
        idShop = app.getCollectionConf("id_shop")
        self.company = idBrand + "_" + idShop + "_" + userId
        self.idBrand = idBrand
        self.idShop = idShop
        self.firstName = firstName
        self.lastName = lastName
        self.dateAppGeneralTraining = dateAppGeneralTraining
        self.dateEngravingTraining = dateEngravingTraining
        # Seconds it took to do the general training
        self.generalTrainingDuration = generalTrainingDuration
        # Seconds it took to do the engraving training
        self.engravingTrainingDuration = engravingTrainingDuration
        # First engraving task ID
        self.firstEngraving = firstEngraving
        # Booleans
        self.generalTraining = generalTraining
        self.engravingTraining = engravingTraining
        self.storedInServer = storedInServer

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)

    def __repr__(self):
        return self.toJSON()


# Searches in the trainees file for the trainee ID
def get_trainee(traineeID):
    LOG.info("get_trainee ID:" + str(traineeID))
    trainees_list_path = app.getConfPath("trainees_list_path")
    if path.isfile(trainees_list_path):
        with io.open(trainees_list_path, "r", encoding="utf-8") as fi:
            fi_data = json.load(fi)
            for person in fi_data.get("trainees"):
                if person.get("userId") == traineeID:
                    LOG.info("Trainee found: " + str(person))
                    return person
    else:
        LOG.error("File trainees doesnt exist")
    LOG.info("Trainee doesnt exist")
    return {}


# Stores a new trainee in the collection file
def add_new_trainee(dto):
    LOG.info("add_new_trainee:")
    LOG.info(dto)
    add_this_trainee = True
    trainees_list_path = app.getConfPath("trainees_list_path")

    # If trainees file doesnt exist, we create it first
    if not path.exists(trainees_list_path):
        with io.open(trainees_list_path, "w+", encoding="utf-8") as fi:
            fi_data = {"trainees": []}
            # Replacing default values with values from table
            fi.seek(0)
            json.dump(fi_data, fi, indent=2)
            fi.truncate()

    # We add the new trainee to the file
    with io.open(trainees_list_path, "r+", encoding="utf-8") as fi:
        # Replacing default values with values from table
        fi_data = json.load(fi)
        # Checking if trainee already exists
        for person in fi_data.get("trainees"):
            if person.get("userId") == dto.userId:
                add_this_trainee = False
        # We add the trainee only if they dont yet exist
        if add_this_trainee:
            fi_data["trainees"].append(dto.__dict__)
        fi.seek(0)
        json.dump(fi_data, fi, indent=2)
        fi.truncate()


# Transforms the http from received from the general training to DTOs and then stores them in the JSON file
def add_recevied_trainees(form):
    LOG.debug("add_recevied_trainees")
    n_trainees = int(form.get('n_trainees'))
    date_app = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    date_engraving = "PENDING"
    general_training_duration = form.get("general_training_duration")
    engraving_training_duration = "PENDING"
    first_engraving = "PENDING"
    general_training = True
    engraving_training = False
    stored_in_server = False
    for i in range(n_trainees):
        first_name = form.get("firstName-" + str(i+1))
        last_name = form.get("lastName-" + str(i+1))
        # Making sure the random ID is not repeated
        trainee_id = str(random.randint(0, 500))
        while get_trainee(trainee_id):
            trainee_id = str(random.randint(0, 500))
        traineeDTO = trainee(trainee_id, first_name, last_name, date_app, date_engraving,
                             general_training_duration, engraving_training_duration, first_engraving,
                             general_training, engraving_training, stored_in_server)
        stored = server.addNewTrainee(traineeDTO)
        traineeDTO.storedInServer = stored
        LOG.info("Trainee stored in server?: " + str(stored))
        add_new_trainee(traineeDTO)


# Returns a dictionary with all trainees
def get_all_trainees():
    trainees_list_path = app.getConfPath("trainees_list_path")
    if path.exists(trainees_list_path):
        with io.open(trainees_list_path, "r", encoding="utf-8") as fi:
            fi_data = json.load(fi)
            trainees = fi_data.get("trainees")
    else:
        trainees = []
    return trainees


# Modifies an existing trainee
def modify_trainee(traineeID, newParametersJSON):
    trainees_list_path = app.getConfPath("trainees_list_path")
    with io.open(trainees_list_path, "r+", encoding="utf-8") as fi:
        fi_data = json.load(fi)
        for index, person in enumerate(fi_data["trainees"]):
            if person.get("userId") == traineeID:
                LOG.info("Updating:" + str(traineeID))
                LOG.info("With new parameters:" + str(newParametersJSON))
                for k, v in newParametersJSON.items():
                    fi_data["trainees"][index][k] = v
                updated = update_trainee_in_server(fi_data["trainees"][index])
                fi_data["trainees"][index]["storedInServer"] = updated

        fi.seek(0)
        json.dump(fi_data, fi, indent=2)
        fi.truncate()


def update_trainee_in_server(traineeData):
    print("trainee data:", traineeData)
    traineeDTO = trainee(traineeData["userId"], traineeData["firstName"], traineeData["lastName"],
                            traineeData["dateAppGeneralTraining"],
                            traineeData["dateEngravingTraining"],
                            traineeData["generalTrainingDuration"], traineeData["engravingTrainingDuration"],
                            traineeData["firstEngraving"], traineeData["generalTraining"],
                            traineeData["engravingTraining"], traineeData["storedInServer"])
    updated = server.addNewTrainee(traineeDTO)
    LOG.info("Trainee updated in server?: " + str(updated))
    return updated


# A trainee finished the engraving training
def finish_engraving_training(traineeID, engravingTrainingDuration, idTask):
    newParametersJSON = {
        "engravingTrainingDuration": engravingTrainingDuration,
        "dateEngravingTraining": str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
        "firstEngraving": idTask,
        "engravingTraining": True
    }
    modify_trainee(traineeID, newParametersJSON)


# Checks if at least one person has done both trainings
def do_engraving_training():
    trainees = get_all_trainees()
    training = True
    for trainee in trainees:
        if trainee.get("generalTraining") and trainee.get("engravingTraining"):
            training = False
            break
    if app.getConf("debug_node") == "True":
        training = False
    return training