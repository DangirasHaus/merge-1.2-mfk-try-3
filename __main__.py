from web.app import app as alyce
from SAFE.SAFEapp import app as safe
import sys, os
import Common.functions as functions
import Common.LogsDWS as LOG
import Common.ServerConnection.updateSystem as updateSystem

debug_mode = functions.getConf("debug_node") == "True"

if __name__ == "__main__":
    LOG.info("LAUNCHING APP")
    print("LAUNCHING APP")
    # If we are in debug mode, we load the parameters from the local file
    # If there is a machine ready, these parameters will be overwritten later
    if debug_mode:
        functions.load_laser_conf_from_local_file()
        print("DEBUG MODE --> using local file for laser conf")
    CheckConfFileFromLaser = functions.checkLaserConfFile()
    # Check if collection exists
    if not os.path.isdir(functions.getConfPath("data_folder")):
        if "_" in functions.getConf("collection"):
            print("****************************************\n\n\nERROR - INCORRECT COLLECTION NAME, trying to download collection\n\n\n****************************************")
            LOG.error("ERROR - INCORRECT COLLECTION NAME, trying to download collection")
            id_brand, id_shop = functions.getConf("collection").split("_")
            updateSystem.download_new_collection_from_server(id_brand, id_shop)
        else:
            print("****************************************\n\n\nERROR - INCORRECT COLLECTION NAME, not even in server\n\n\n****************************************")
            LOG.error("ERROR - INCORRECT COLLECTION NAME, not even in server")
        functions.initLiterals()
    else:
        functions.initLiterals()
    # Creating order folder
    order_folder = functions.getConfPath("order_folder")
    if not os.path.isdir(order_folder):
        os.makedirs(order_folder)
    if "SAFE" in sys.argv or functions.getConf("force_SAFE") == "True":
        LOG.info("EXECUTE SAFE")
        print("EXECUTE SAFE")
        safe.run(debug=True, host='0.0.0.0', port=5000)
    else:
        alyce.run(debug=debug_mode, host='0.0.0.0', port=5000, use_reloader=not debug_mode)
        #alyce.run(debug=debug_mode, threaded=True)