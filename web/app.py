from flask import Flask, redirect, url_for
from flask import jsonify, request
from web.views.ecommerce1 import bp as ecommerce1_bp
from web.views.screen1 import bp as screen1_bp
from web.views.screen2 import bp as screen2_bp
from web.views.screen3 import bp as screen3_bp
from web.views.backUpPage import bp as backUpPage_bp
from web.views.dataManagement import bp as dataManagement_bp
from web.views.downloads import bp as donwload_bp
from web.views.createCollection import bp as createCollection_bp
#from web.views.createProduct import bp as createProduct_bp
from web.views.training import bp as training_bp
import Common.LogsDWS as LOG
import Common.functions as functions
import webbrowser, os
from sys import platform

app = Flask(__name__)

app.register_blueprint(ecommerce1_bp)
app.register_blueprint(screen1_bp)
app.register_blueprint(screen2_bp)
app.register_blueprint(screen3_bp)
app.register_blueprint(backUpPage_bp)
app.register_blueprint(dataManagement_bp)
app.register_blueprint(donwload_bp)
app.register_blueprint(createCollection_bp)
#app.register_blueprint(createProduct_bp)
app.register_blueprint(training_bp)

app.secret_key = ".."


if platform == "linux" or platform == "linux2":
    LOG.info("Setting raspberry clock to RTC")
    os.system("sudo hwclock -s")  # Give gconv execution permissions
    #os.system("sudo teamviewer passwd DWS-1618")

@app.errorhandler(404)
def page_not_found(e):
    #LOG.error("404 detected: " + request.url)
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp


@app.errorhandler(500)
def error500(e):
    LOG.error("Error 500 detected: " + str(e))
    return redirect(url_for('web.views.screen1.show', errorcode="500"))

