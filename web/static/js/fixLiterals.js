
$( document ).ready(function() {
	// Setting on change functions for checkboxes
	$('input[type=checkbox]').change(function () {
		var className = "." + $(this).val();
		if($(this).prop("checked")){
			$(className).show();
		} else {
			$(className).hide();
		}
	});
	// Hiding literals by default
	$('input[type=checkbox]').each(function(){
		var className = "." + $(this).val();
		$(className).hide();
		isLanguageComplete($(this).val())
	});
});

function isLanguageComplete(litFileName){
	var query = "." + litFileName + " input";
	var isComplete = $(query).length == 0;
	if(!isComplete){
		$('input[type=checkbox][value=' + litFileName + ']').parent().addClass("incomplete");
	}
}