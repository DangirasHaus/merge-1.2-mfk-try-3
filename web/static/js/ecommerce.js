
$( document ).ready(function() {
    keyboardInit("search-e-engraving");
	keyboardInit("search-e-engraving2");
	if(/ecommerce/i.test(location.href)){
        $("body").addClass("e-commerce");
    }
});


// Changes modes from e-commerce to in-store and viceversa
function switchModes(pass){
	console.log("Password received:", pass);
    $.ajax({
        type : 'POST',
        url : "./checkAdminPassword",
        data : {
            pass: pass
        },
        error : function(e) {
            alert("Incorrect password")
            $('.adminPassOk').addClass("hidden");
            console.error(e)
        },
        success : function(data) {
            console.log(data);
            $('.adminPassOk').removeClass("hidden");
			var dest = "?switchMode=True"
			if(/screen/i.test(location.href)){
				console.log("Switch to ecommerce")
				dest = "/ecommerce1" + dest
			} else {
				console.log("Switch to instore")
				dest = "/screen1" + dest
			}
			console.log("redirect to :", dest)
			location.href = dest;
        },
        complete: function(){
            $('#password').val("");
        }
    });
}


// Opens the keyboard to write the engraving ID
function openEEngravingKeyboard(){
	console.log("openEEngravingKeyboard")
    $('#search-e-engraving').removeClass("hidden");
    $('#search-e-engraving').focus();
	$('#search-e-engraving2').removeClass("hidden");
    $('#search-e-engraving2').focus();
}


function loadEngravingById(value) {	

	const HIDDEN = 'hidden';
	const selector = '#e-engravings-table tbody tr'

	value = value.trim();

	if (!value || value == '') {
		$(selector).each((_, item) => item.classList.remove(HIDDEN));
	} else {
		$(selector)
			.each((_, item) => item.classList[0]?.includes(value) ? item.classList.remove(HIDDEN) : item.classList.add(HIDDEN));
	}

	var selectorVisible = selector + ":visible";
	var nRes = $(selectorVisible).length;
    if(nRes < 1){
        console.log("no engravings with this id locally, getting from server");
        getEngravingFromServer(value)
        console.log("value:",value);
    }
}

function loadEngravingById2(value) {	
	
	const HIDDEN = 'hidden';
	const selector = '#e-engravings-table2 tbody tr'

	value = value.trim();

	if (!value || value == '') {
		$(selector).each((_, item) => item.classList.remove(HIDDEN));
	} else {
		$(selector)
			.each((_, item) => item.classList[0]?.includes(value) ? item.classList.remove(HIDDEN) : item.classList.add(HIDDEN));
	}

	var selectorVisible = selector + ":visible";
	var nRes = $(selectorVisible).length;
		if(nRes < 1){
			console.log("no engravings with this id locally, getting from server");
			getEngravingFromServer(value)
			console.log("value:",value);
		}
}

function reload_ecom() {
	console.log("Reloading ecommerce page.");
    location.href = "/ecommerce1"
}

// Get engraving from server knowing ID
function getEngravingFromServer(id){
	console.log("getEngravingFromServer:", id);
    $.ajax({
        type : 'POST',
        url : "./getEEngraving",
        data: {
            "orderId": id
        },
        success : function(data) {
            console.log("data:", data)
            addEngravingsToTable(data)
        },
        complete : function() {
        }
    });
}

// Transforms an engraving into a new row in the table
function addEngravingsToTable(engravings){
	for(let eng of engravings){
		console.log("eng:", eng);
		pee.push(eng)
		var tds = "<td>" + eng.ts + "</td>";
		tds += "<td>" + eng.clientProdName + "</td>";
		tds += "<td>" + eng.clientProductId + "</td>";
		tds += "<td>" + eng.clientOrderId + "</td>";
		//tds += "<td>" + eng.productId + "</td>";
		tds += "<td>" + eng.idStore + "</td>";
		var button = '<button class="btn btn-default" type="button" onclick="loadEEngraving(\'' + eng.ts + '\')">Launch</button>'
		tds += "<td class='center'>" + button + "</td>";
		var classes = "" + eng.ts + " " + eng.clientOrderId;
		var tr = "<tr class=" + classes + ">" + tds + "</tr>";
		if($('#e-engravings-table tbody tr:last').length > 0) {
			$('#e-engravings-table tbody tr:last').after(tr);
		} else {
			$('#e-engravings-table tbody').html(tr);
		}
	}
}

// loads an engraving in screen 2
function loadEEngraving(engTs){
	var eng = getEEngravingFromMemory(engTs)
	$("input[name=product]").val(eng.productId);
	$("input[name=clientProdName]").val(eng.clientProdName);
	$("input[name=clientProductId]").val(eng.clientProductId);
	$("input[name=clientOrderId]").val(eng.clientOrderId);
	$("input[name=textToEngrave]").val(eng.text);
	$("input[name=font]").val(eng.font);
	$("input[name=ts]").val(eng.ts);
	$("input[name=color]").val(eng.color);
	$("input[name=position]").val(eng.position);
	$("input[name=position]").val(eng.position);
	$("#ecommerce1Form").submit();
}

// Returns an engraving object from its TS ID
function getEEngravingFromMemory(engTs){
	console.log(" je filtre")
	filtered = pee.filter(function(eng) {
	  return eng.ts == engTs;
	});
	return filtered[0]
}

// Checks if the password is correct and then activates admin mode for one engraving
function activateAdminMode(pass){
	console.log("activateAdminMode pass:", pass);
    $.ajax({
        type : 'POST',
        url : "./activateAdminMode",
        data : {
            pass: pass
        },
        error : function(e) {
            alert("Incorrect password")
            $('.adminPassOk').addClass("hidden");
            console.error(e)
        },
        success : function(data) {
            console.log(data);
            $('.adminPassOk').removeClass("hidden");
        },
        complete: function(){
            $('#password').val("");
        }
    });
}

function jsonEscape(str)  {
    return str.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
}
