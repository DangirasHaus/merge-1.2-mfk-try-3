console.log("functionalities js INIT")
var keepCheckingLaserReady = true;
var chineseAutocomplete = false;
var traditionalChineseAutocomplete = false;
var zhuyinChineseAutocomplete = false;
var lastLogo = undefined;
var mydwsisConnected = false; // global variable boolean that will be true if mydws is accessible
var TVisConnected = false; // gloibal variable boolean that will be true if teamviewer is accessible
function formOk() {
  var isOk = false;
  if (!textareafield.checkValidity()) {
    //textareafield.className = "invalid";
    textareafield.classList.add("invalid");
    $(".errorMessage").show();
    $("#goForwardA").prop("disabled", true);
  } else {
    textareafield.className = "";
    $(".errorMessage").hide();
    $("#goForwardA").prop("disabled", false);
  }
  isOk = textareafield.checkValidity();
  return isOk;
}

var engravingFontName = "engravingFont";

function changeEngravingFont(
  fontName,
  pinMode,
  engravingFont,
  new_fontsize,
  new_fontsize_max,
  rec_color
) {
  var color = rec_color ? rec_color : $("input[name=textColor]").val();
  var prevFont = $("input[name='font']").val();
  var isDifferentFont = !(prevFont == fontName);
  $("input[name=font]").val(fontName);
  if (isDifferentFont) {
    console.log("#@ changing font to:", fontName);
    setEngravingFont(fontName);
    engravingFontName = engravingFont;
    recalculateOptimalFontSize(new_fontsize, new_fontsize_max);
    if (isTextInBoundaries($("#textToEngrave").val())) {
      $("input[name=textColor]").val(color);
      $("input[name=pinMode]").val(pinMode);
      $("input[name=font]").val(fontName);
      $("#textToEngrave").attr("previous-val", "");
      $("#textToEngrave").change();
      $("#goForwardA").prop("disabled", false);
    } else {
      $("#goForwardA").prop("disabled", true);
      console.warn("object didnt fit after changing font");
    }
    // Changing slider values for new font if there is slider
    if ($("#myRange").length) {
      changeSliderValuesForFont();
    }
  }
}

function changeSliderValuesForFont() {
  if (fontsize > fontsize_min) {
    $("#slidecontainer").show();
    // We multiply by 10 to have a smoother slider when the font size is small
    $("#myRange").attr("min", fontsize_min * 10);
    $("#myRange").attr("max", fontsize * 10);
    $("#myRange").val(fontsize * 10);
  } else {
    $("#slidecontainer").hide();
  }
}

function addFontFace(fontName, engravingFontName) {
  var fontUrl = "/customStatic/Fonts/" + fontName;
  var newStyle = document.createElement("style");
  newStyle.appendChild(
    document.createTextNode(
      "\
    @font-face {\
        font-family: " +
        engravingFontName +
        ";\
        src: url(" +
        fontUrl +
        ");\
    }\
    "
    )
  );
  document.head.appendChild(newStyle);
}

function setEngravingFont(fontName) {
  var oldElem = document.getElementById("engravingFontId");
  if (oldElem) oldElem.remove();
  var fontUrl = "/customStatic/Fonts/" + fontName;
  var newStyle = document.createElement("style");
  newStyle.setAttribute("id", "engravingFontId");
  newStyle.setAttribute("value", fontName);
  newStyle.appendChild(
    document.createTextNode(
      "\
    @font-face {\
        font-family: engravingFont;\
        src: url(" +
        fontUrl +
        ");\
    }\
    "
    )
  );
  document.head.appendChild(newStyle);
}

function recalculateOptimalFontSize(
  recFontsize = defaultFontsize,
  recFontsize_max = defaultFontsize_max
) {
  fontsize = obtainFontSizeGivenMM(vertical_rapport, recFontsize);
  fontsize_min = fontsize;
  fontsize_max = obtainFontSizeGivenMM(vertical_rapport, recFontsize_max);
}

function eqHeightPredefTexts() {
  var maxHeight = 0;
  $(".predef-text").each(function () {
    thisHeight = $(this).height();
    console.log(thisHeight);
    if (maxHeight < thisHeight) {
      maxHeight = thisHeight;
    }
  });
  $(".predef-text").height(maxHeight);
  $(".predef-text").outerWidth("30%");
  $(".predef-buttons-container").height(maxHeight * 1.2);
}

function textToEngravePreview(value, logoUrl) {
  lastMove = null;
  previousValue = $("#textToEngrave").attr("previous-val");
  if (typeof logoUrl === "undefined") {
    //if ($("#goForwardA").attr("hideRestrictionSquare") === "true"){
    if (formOk() || value.length == 0) {
      // Prints text in image
      createImageForDisplay(
        value,
        fontsize,
        undefined,
        default_height,
        "printCanvas"
      );
      removeElementsWhenWriting(previousValue);
      // Slider logic
      if ($(".slidecontainer").length !== 0) {
        fontsize = SliderFontSize(undefined, default_height, 0);
        changeSliderValuesForFont();
      }
      // Font size buttons logic
      if ($("#fontsize-buttons-container").length) {
        changeFontSizeButtonValues();
      }
    }
    $("#textToEngrave").attr("previous-val", value);
    //}
  } else {
    // Prints text in image
    createImageForDisplay(
      value,
      fontsize,
      undefined,
      default_height,
      "printCanvas",
      logoUrl
    );
    removeElementsWhenWriting(previousValue);
    if ($(".slidecontainer").length !== 0) {
      fontsize = SliderFontSize(undefined, default_height, 0);
    }
    $("#textToEngrave").attr("previous-val", value);
  }
}

// Checks if the text is touching the border of the engraving area
function textTouchesBorder(canvasId) {
  var c = document.getElementById(canvasId);
  var ctx = c.getContext("2d");
  var imgData = ctx.getImageData(0, 0, c.width, c.height);
  // First row
  var row = 0;
  for (col = 0; col < c.width; col++) {
    index = (col + row * imgData.width) * 4;
    var pixVal =
      imgData.data[index] +
      imgData.data[index + 1] +
      imgData.data[index + 2] +
      imgData.data[index + 3];
    if (pixVal !== 0) return true;
  }
  // Last row
  var row = c.height - 1;
  for (col = 0; col < c.width; col++) {
    index = (col + row * imgData.width) * 4;
    var pixVal =
      imgData.data[index] +
      imgData.data[index + 1] +
      imgData.data[index + 2] +
      imgData.data[index + 3];
    if (pixVal !== 0) return true;
  }
  // Rest of rows
  for (row = 1; row < c.height - 1; row++) {
    // First item of row
    var col = 0;
    index = (col + row * imgData.width) * 4;
    var pixVal =
      imgData.data[index] +
      imgData.data[index + 1] +
      imgData.data[index + 2] +
      imgData.data[index + 3];
    if (pixVal !== 0) return true;
    // Last item of row
    var col = c.width - 1;
    index = (col + row * imgData.width) * 4;
    var pixVal =
      imgData.data[index] +
      imgData.data[index + 1] +
      imgData.data[index + 2] +
      imgData.data[index + 3];
    if (pixVal !== 0) return true;
  }
  return false;
}

// Checks if the text is inside the boundary conditions for the product and the engraving area
function isTextInBoundaries(
  textValue,
  forced_fontsize_max = fontsize_max,
  forced_fontsize_min = fontsize_min
) {
  var textFits = true;
  if (
    max_number_of_lines &&
    parseInt(max_number_of_lines) &&
    textValue.trim().split("\n").length > parseInt(max_number_of_lines)
  ) {
    console.warn("Max number of lines passed");
    $(".errorNumberLines").show();
    return false;
  }

  if (engravingFontName == "engravingFont_DINOT-CondBold") {
    textValue = textValue.toUpperCase();
  }

  if (textValue !== "") {
    $(".errorMessage").hide();
    textFits = false;
    //for(var i = forced_fontsize_max; i >= forced_fontsize_min; i--){
    for (var i = forced_fontsize_min; i <= forced_fontsize_max; i++) {
      canvasID = "textCanvas";
      if (engravingFontName == "engravingFont_DINOT-CondBold") {
        $(".ui-keyboard-preview.ui-widget-content.ui-corner-all").click(
          function () {
            $(this).val($(this).val().toUpperCase());
          }
        );
      }
      removeElementsWhenWriting($("#textToEngrave").attr("previous-val"));
      createImage(canvasID, textValue, i, undefined, default_height);
      if (
        isTextTooHigh(textValue, i) |
        isTextTooWide(XOffset, textValue) |
        textTouchesBorder("textCanvas")
      ) {
        if (textTouchesBorder("textCanvas"))
          console.warn("Text is touching border");
        console.warn(
          "---- Text out of boundaries:--- final fontsize:",
          fontsize
        );
        if (!textFits) {
          $(".errorNumberCharacters").show();
        } else {
          $(".errorNumberCharacters").hide();
        }
        removeElementsWhenWriting(textValue);
        var canvas = document.getElementById(canvasID);
        var c = canvas.getContext("2d");
        c.clearRect(0, 0, canvas.width, canvas.height);
        return textFits;
      }
      removeElementsWhenWriting(textValue);
      var canvas = document.getElementById(canvasID);
      var c = canvas.getContext("2d");
      c.clearRect(0, 0, canvas.width, canvas.height);
      fontsize = i;
      textFits = true;
      console.warn(
        "---- Text out of boundaries??:",
        textFits,
        "--- final fontsize:",
        i
      );
    }
  } else {
    $(".errorMessage").show();
    $(".errorNumberLines").hide();
    $(".errorNumberCharacters").hide();
  }
  return textFits;
}

function degreeToRad(degree) {
  var rad = (degree * Math.PI) / 180;
  return rad;
}

function getPixelHeight(canvasID, text, fontSize) {
  var canvas = document.getElementById(canvasID);
  var c = canvas.getContext("2d");
  xCenter = canvas.width / 2;
  yCenter = canvas.height / 2;
  c.clearRect(0, 0, canvas.width, canvas.height);
  c.fillStyle = "rgba(255, 255, 255, 1)";
  c.font = fontSize + "px " + engravingFontName;
  c.textAlign = "center";
  c.textBaseline = getTextBaseline();
  c.fillText(text, xCenter, yCenter);

  var bbox = getBoundingBox(c, 0, 0, canvas.width, canvas.height);

  c.clearRect(0, 0, canvas.width, canvas.height);
  // PIXELS
  return bbox.height;
}

function calcInitialFontSize() {
  return 80;
}

var mainfontsize;

function obtainFontSizeGivenMM(vertical_rapport, desiredHeight) {
  currFontSize = calcInitialFontSize();
  objective = desiredHeight / vertical_rapport;
  precission = (1 / window.innerHeight) * 100;

  currError = getPixelHeight("textCanvas", "A", currFontSize) - objective;
  var count = 0;
  // We are trying to find the zero of the function "getPixelHeight" which is more or less lineal
  // so each step we substract the error.
  // Also, the precission can't be great because the function to optimize returns only integers
  while (Math.abs(currError) > precission / vertical_rapport && count < 100) {
    if (currError > currFontSize) currError = currFontSize / 2;
    currFontSize = currFontSize - currError;
    currError = getPixelHeight("textCanvas", "A", currFontSize) - objective;
    count++;
  }
  console.log("converged after: " + count + " with font = " + currFontSize);
  return Math.round(currFontSize);
}

function getImageContainerHeight() {
  // Before it was this by default
  // return "75%"
  return $("#imgcontainer").height();
}

function prepareDisplayCanvas() {
  calculateCanvasRestriction();

  setTimeout(function () {
    var canvas_height = $("div.redsquare").height();
    canvasFactor_height = canvas_px / canvas_height;
    canvasFactor_width = canvas_px / canvas_height;
    canvasFactor = canvasFactor_height;
    $("div.redsquare").width(canvas_height);
    canvas_text = document.getElementById("textCanvas");
    if (canvas_text) {
      canvas_text.style.width = "100%";
      canvas_text.style.height = "100%";
      canvas_text.width = canvas_text.offsetWidth;
      canvas_text.height = canvas_text.offsetHeight;
      canvas_text.width = canvas_text.offsetWidth * getScale("textCanvas");
      canvas_text.height = canvas_text.offsetHeight * getScale("textCanvas");
    }
    canvas_print = document.getElementById("printCanvas");
    if (canvas_print) {
      canvas_print.width = canvas_text.offsetWidth * canvasFactor_width;
      canvas_print.height = canvas_text.offsetHeight * canvasFactor_height;
      canvas_print.width =
        canvas_text.offsetWidth * canvasFactor_width * getScale("printCanvas");
      canvas_print.height =
        canvas_text.offsetHeight *
        canvasFactor_height *
        getScale("printCanvas");
    }
    if ($("#textCanvas").attr("fixEngravingLocation") === "True") {
      console.log("NOT binding events");
    } else {
      console.log("binding events");
      bindEventsToCanvas();
    }

    // Screen 2 button vertical alignment
    $("#right-panel").height(getImageContainerHeight());
    //$('#right-panel').height("75%")
    $("#right-panel").show();

    if (!/screen3/i.test(location.href)) {
      var desiredHeightInMM = fontsize;
      var desiredHeightInMM_min = fontsize_min;
      var desiredHeightInMM_max = fontsize_max;
      fontsize = obtainFontSizeGivenMM(vertical_rapport, desiredHeightInMM);
      fontsize_min = fontsize;
      fontsize_max = obtainFontSizeGivenMM(
        vertical_rapport,
        desiredHeightInMM_max
      );
      console.log("optimized fontsize = " + fontsize);
      console.log("desiredHeightInMM = " + desiredHeightInMM);
      console.log("optimized fontsize_min = " + fontsize_min);
      console.log("desiredHeightInMM_min = " + desiredHeightInMM_min);
      console.log("optimized fontsize_max = " + fontsize_max);
      console.log("desiredHeightInMM_max = " + desiredHeightInMM_max);
    }
  }, 200);
}

function checkRestrictionIsInsideCanvas(canvasWidth, canvasHeight) {
  //console.log("checkRestrictionIsInsideCanvas: " + canvasWidth + " " + canvasHeight);
  if (canvas_restriction[0] < 0) {
    //alert("Left side is out of the canvas");
    console.warn("Left side is out of the canvas");
  }
  if (canvas_restriction[1] < 0) {
    //alert("Top side is out of the canvas");
    console.warn("Top side is out of the canvas");
  }
  if (canvas_restriction[0] + canvas_restriction[2] > canvasWidth) {
    //alert("Right side is out of the canvas");
    console.warn("Right side is out of the canvas");
  }
  if (canvas_restriction[1] + canvas_restriction[3] > canvasHeight) {
    //alert("Bottom side is out of the canvas");
    console.warn("Bottom side is out of the canvas");
  }
}

function getProductImageProportion() {
  return 0.9;
}

function resizeProductImage() {
  newHeight =
    ($(window).outerHeight() - $("#header").outerHeight(true)) *
    getProductImageProportion();
  fatherWidth = $("#imgcontainer").width();
  // ratio = height/width
  ratio = $("#productImage").attr("ratio");
  maxHeightWithRatio = ratio * fatherWidth;
  if (newHeight > maxHeightWithRatio) {
    newHeight = maxHeightWithRatio;
  }
  $("#productImage").height(newHeight);
  // Calculation of the rapport mm / px
  vertical_rapport = vertical_rapport / $("#productImage").height();

  // Function that displays correctly the canvas according to the product dimensions
  prepareDisplayCanvas();
}

function calculateCanvasRestriction() {
  var origin_move = [];
  var canvas_element;
  var img_element;
  if (
    typeof canvas_restriction === "undefined" ||
    !/screen3/i.test(location.href)
  ) {
    setTimeout(function () {
      // Default text height
      default_height = default_height * $("#productImage").height();
      // Origin of coordinates of the canvas
      canvas_element = document.getElementById("textCanvas");
      var rect = canvas_element.getBoundingClientRect();
      var canvas_orig = [rect.left, rect.top];

      // Origin of coordinates of the image
      img_element = document.getElementsByClassName("product-img")[0];
      var rect = img_element.getBoundingClientRect();
      var img_orig = [rect.left, rect.bottom];

      // Change of coordinates origin
      origin_move = [
        img_orig[0] - canvas_orig[0],
        img_orig[1] - canvas_orig[1],
      ];
      default_height = -default_height + origin_move[1];
      maindefault_height = default_height;
      console.log("default_height origin_move: " + default_height);
    }, 250);
  }

  if (typeof canvas_restriction === "undefined") {
    console.log("canvas_restriction DOESNT exists");
  } else if (!/screen3/i.test(location.href)) {
    console.log("canvas_restriction exists");
    setTimeout(function () {
      // Obtaining canvas restriction in pixels from %
      canvas_restriction[0] =
        canvas_restriction[0] * $("#productImage").width();
      canvas_restriction[1] =
        canvas_restriction[1] * $("#productImage").height();
      canvas_restriction[2] =
        canvas_restriction[2] * $("#productImage").width();
      canvas_restriction[3] =
        canvas_restriction[3] * $("#productImage").height();

      // Change of coordinates origin
      canvas_restriction[0] = canvas_restriction[0] + origin_move[0];
      canvas_restriction[1] = -canvas_restriction[1] + origin_move[1];
      canvas_restriction[2] = canvas_restriction[2];
      canvas_restriction[3] = canvas_restriction[3];

      checkRestrictionIsInsideCanvas(
        canvas_element.width,
        canvas_element.height
      );

      // Drawing the rectangle
      $("#textToEngrave").change();
      textareafield.classList.remove("invalid");
      $(".errorMessage").hide();
    }, 250);
  }
}

function onResize() {
  resizeProductImage();
}

$(window).resize(function () {
  if (!/screen1/i.test(location.href)) {
    //onResize();
  }
});
var slider;

function modeTrainingSteps(movingStep) {
  var className = "modal-t-instruction";
  var numberOfSteps = $("." + className).length - 1;
  var nextStep = 1;
  $("." + className).each(function (index) {
    console.log(index + ": " + $(this).is(":visible"));
    if ($(this).is(":visible")) {
      nextStep = index + movingStep;
      $(this).hide();
    }
  });
  // Changing buttons when getting to the last step
  console.log("numberOfSteps: " + numberOfSteps);
  console.log("nextStep: " + nextStep);
  if (numberOfSteps === nextStep) {
    console.log("1");
    $(".next-step-training").hide();
  } else if (nextStep === 0) {
    console.log("2");
    $(".prev-step-training").hide();
  } else {
    console.log("3");
    $(".prev-step-training").show();
    $(".next-step-training").show();
  }
  var idToShow = "#" + className + "-" + nextStep;
  $(idToShow).show();
}

function modeErrorSteps(movingStep) {
  var className = "modal-instruction";
  var numberOfSteps = $("." + className).length - 1;
  var nextStep = 1;
  $("." + className).each(function (index) {
    console.log(index + ": " + $(this).is(":visible"));
    if ($(this).is(":visible")) {
      nextStep = index + movingStep;
      $(this).hide();
    }
  });
  // Changing buttons when getting to the last step
  console.log("numberOfSteps: " + numberOfSteps);
  console.log("nextStep: " + nextStep);
  if (numberOfSteps === nextStep) {
    console.log("1");
    $(".next-step-error").hide();
    $(".back-to-start-button").css("display", "inline-block");
  } else if (nextStep === 0) {
    console.log("2");
    $(".prev-step-error").hide();
  } else {
    console.log("3");
    $(".prev-step-error").show();
    $(".next-step-error").show();
    $(".back-to-start-button").hide();
  }
  var idToShow = "#" + className + "-" + nextStep;
  $(idToShow).show();
}

// Check of font is correctly loaded
function checkCustomFont(fontUrl) {
  $.ajax({
    type: "GET",
    url: fontUrl,
    success: function (data) {
      console.log("SUCCESS getting font");
      $("#font-not-found-msg").addClass("hidden");
    },
    error: function (request, status, error) {
      console.log("ERROR getting font");
      $("#font-not-found-msg").removeClass("hidden");
    },
  });
}

function setInitEngravingTraining() {
  $('input[name$="engravingTrainingDuration"]').val(
    Math.round(new Date().getTime() / 1000)
  );
}

// Buttons functions
$(document).ready(function () {
  //$('textarea#textToEngrave').val($('#textToEngrave').toUpperCase())
  // Function to change from steps in the training instructions
  $(".next-step-training").click(function () {
    modeTrainingSteps(1);
  });
  $(".prev-step-training").click(function () {
    modeTrainingSteps(-1);
  });

  // Function to change from steps in the error instructions
  $(".next-step-error").click(function () {
    modeErrorSteps(1);
  });
  $(".prev-step-error").click(function () {
    modeErrorSteps(-1);
  });

  $("#screen-saver-img").click(function () {
    console.log("click in screen save");
    switchScreenSaver();
  });

  // Clicks for chinese autocomplete
  $(".IMEControl .left").click(function () {
    console.log("click left");
    var visibleRow = $(".IMEControl.IMEContent table").attr("value");
    var nRows = $(".IMEControl.IMEContent table").attr("nRows");
    console.log("visibleRow:", visibleRow);
    if (visibleRow > 0 && !$(".showAll").hasClass("showPage")) {
      var hideRow = ".chi-row-" + visibleRow;
      var newRow = Number(visibleRow) - 1;
      var showRow = ".chi-row-" + newRow;
      $(hideRow).hide();
      $(showRow).show();
      $(".IMEControl.IMEContent table").attr("value", newRow);
      $(".IMEPageCounter").html(newRow + 1 + "/" + nRows);
    }
  });

  $(".IMEControl .right").click(function () {
    console.log("click right");
    var visibleRow = $(".IMEControl.IMEContent table").attr("value");
    var nRows = $(".IMEControl.IMEContent table").attr("nRows");
    console.log("visibleRow:", visibleRow);
    console.log("nRows:", nRows);
    if (visibleRow < nRows - 1 && !$(".showAll").hasClass("showPage")) {
      var hideRow = ".chi-row-" + visibleRow;
      var newRow = Number(visibleRow) + 1;
      var showRow = ".chi-row-" + newRow;
      console.log("hideRow:", hideRow);
      console.log("showRow:", showRow);
      $(hideRow).hide();
      $(showRow).show();
      $(".IMEControl.IMEContent table").attr("value", newRow);
      $(".IMEPageCounter").html(newRow + 1 + "/" + nRows);
    }
  });

  $(".IMEControl .arrow").click(function () {
    console.log("arrow down click");
    if (!$(".showAll").hasClass("showPage")) {
      $(".IMEControl.IMEContent tr").show();
      $(".showAll").addClass("showPage");
    } else {
      $(".IMEControl.IMEContent tr").hide();
      var visibleRow = $(".IMEControl.IMEContent table").attr("value");
      var showRow = ".chi-row-" + visibleRow;
      $(showRow).show();
      $(".showAll").removeClass("showPage");
    }
  });

  slider = document.getElementById("myRange"); // slider as a global variable

  $("#goBackA").click(function () {
    // If we have a trainee ID we have to reselect the trainee if we come back to screen 1
    if ($('input[name$="traineeID"]').val()) {
      location.href = "./screen1?do_engraving_training=true";
    } else {
      location.href = "./screen1";
    }
  });

  $("#goForwardA").click(function () {
    fromScreen2ToScreen3();
  });

  //$("#textToEngrave").keyup(function(){
  $("#textToEngrave").change(function () {
    customTextToEngraveChangeFunction();
    previousValue = $("#textToEngrave").attr("previous-val");
    value = $(this).val();
    value = value.trim();
    textToEngravePreview(value);
  });

  // Screen 3
  $("#redRectangleButton").click(function () {
    console.log("redRectangleButton button");
    showRedRectangle();
  });

  $("#redRectangleButtonModal").click(function () {
    console.log("redRectangleButton button");
    showRedRectangle();
  });

  $("#printButton").click(function () {
    console.log("Print button");
    // TODO CHANGE THIS submit, to make an AJAX to /engraveOrder
    // Then, depending on the answer, load the modal with progress bar or show error messages and retry
    engraveOrder();
  });

  $("#relaunchButton").click(function () {
    console.log("Relaunch button");
    keepCheckingLaserReady = true;
    loadModal("modal-engraving");

    document.getElementById("myBar").style.width = "0%";
    $("#myBar").text("0%");
    $("#myBar").removeClass("progress-bar-danger");
    $("#myBar").removeClass("progress-bar-success");
    $("#myBar").addClass("active");
    $(".order-finished-error").hide();
    $("#modal-engraving .modal-title").hide();
    $(".modal-title.relaunch").show();

    checkLaserIsReady();
    showRelaunchMessage(true);
  });

  $("#confirmRelaunchButton").click(function () {
    $("#modal-engraving .modal-title").hide();
    $(".modal-title.finishText").show();
    storeRating(engraveOrder);
    $('input[name="engraving_relaunched"]').val("true");
  });

  $("#relaunchButton2").click(function () {
    $("#relaunchButton").click();
  });

  $(".stopButton").click(function () {
    console.log("STOP button");
    $('input[name="engraving_stopped"]').val("true");
    stopLaser();
    moveProgressBar(false);
    $(".modal-title.order-finished-error").show();
    $(".modal-title.finishText").hide();
    $("#relaunchButton").prop("disabled", false);
    $(".modal-body").click();
  });
});

function showRelaunchMessage(show) {
  if (show) {
    // We are trying to relaunch
    $(".finishText.stars-container").hide();
    $(".finishText.retry-relaunch").show();
    $("#relaunchButton").hide();
    $("#confirmRelaunchButton").show();
    $("#redRectangleButtonModal").show();
  } else {
    // We come back to the original one
    $(".finishText.stars-container").show();
    $(".finishText.retry-relaunch").hide();
    $("#relaunchButton").show();
    $("#confirmRelaunchButton").hide();
    $("#redRectangleButtonModal").hide();
  }
}

//function to stop engraving
function stopLaser() {
  $.ajax({
    type: "POST",
    url: "./stopLaser",
    error: function () {
      $(".errorMessage").show();
    },
    success: function () {
      $(".errorMessage").hide();
    },
  });
}

//function to verify number of characters is overpassing the max or not
function verifyNbreChar(text, max) {
  var bool;
  console.log("le maximum est", max);
  console.log("le texte à vérifier est : ", text);
  if (text.length > max) {
    console.log("le nbre max de caractères par ligne est dépassé");
    // console.log(resText[i])
    $(".errorNumberCharacters").show();
    $("#goForwardA").prop("disabled", true);
    bool = false;
  } else {
    $(".errorNumberCharacters").hide();
    console.log("nombre max de caractères est ok");
    bool = true;
  }
  return bool;
}
//function to verify number of lines is overpassing the max or not
function verifyNbreLine(Tabtext, max) {
  var bool;
  if (Tabtext.length > max) {
    console.log("le nbre max de ligne est dépassé");
    console.log(max_number_of_lines);
    console.log("le nbre de ligne est", Tabtext.length);
    // console.log(Tabtext[i])
    $(".errorNumberLines").show();
    $("#goForwardA").prop("disabled", true);
    bool = false;
  } else {
    $(".errorNumberLines").hide();
    console.log("nombre max de lignes ok");
    bool = true;
  }
  return bool;
}

// function to get fonside from slide range value and then update the image immediatly
function SliderFontSize(x, y, objectId) {
  var fontSize;
  var output = document.getElementById("demo");
  output.innerHTML = $("#myRange").val();
  // With jQuery we need to delete previous event listener to overwrite them
  $("#myRange")
    .off("input touchend mouseup")
    .on("input touchend mouseup", function (event) {
      if (
        event.type == "touchend" ||
        event.type == "mouseup" ||
        $("#myRange").val() % 10 == 0
      ) {
        var text = $("#textToEngrave").val();
        output.innerHTML = this.value;
        //make the slider value as the a fontsize
        fontSize = this.value / 10;
        //get width an height of current object
        var lastWidth = objInCanvas[objectId].width;
        var lastHeight = objInCanvas[objectId].height;
        //[x, y] = checkIfObjectInside(objInCanvas[objectId], x, y);
        //console.trace("# text:", text)
        console.log("# previousValue:", previousValue);
        createImageForDisplay(text, fontSize, x, y, "printCanvas", lastLogo);
        removeElementsWhenWriting(previousValue);
        if (objInCanvas.length != 0) {
          removeElementsWhenDragging(objectId);
        }
        try {
          var newWidth = objInCanvas[objectId].width;
          var newHeight = objInCanvas[objectId].height;
          [x, y] = checkResizableFontObjectInside(
            objInCanvas[objectId],
            lastWidth,
            lastHeight,
            newWidth,
            newHeight,
            x,
            y
          );
          createImageForDisplay(text, fontSize, x, y, "printCanvas", lastLogo);
          removeElementsWhenWriting(previousValue);
          if (objInCanvas.length != 0) {
            removeElementsWhenDragging(objectId);
          }
        } catch (error) {
          console.warn(error);
        }
        if (typeof fontSize !== "undefined") {
          fontsize = fontSize;
        }
      }
    });
  return fontsize;
}

function engraveOrder() {
  console.log("engraveOrder");
  keepCheckingLaserReady = false;
  loadModal("modal-sending-order");
  $('input[name="engraving_stopped"]').val("");
  $('input[name="error_message"]').val("");
  $(".stopButton").prop("disabled", false);
  $("#relaunchButton").prop("disabled", true);
  $("#error_title").hide();
  $(".stars-container").show();
  $("#list_of_errors li").remove();
  $("#retry_text").hide();
  $.ajax({
    type: "POST",
    url: "./engraveOrder",
    beforeSend: function () {},
    success: function (data) {
      // Parsing data to JSON
      data = data.split('"').join('\\"');
      data = data.split("'").join('"');
      var json = JSON.parse(data);
      console.log(json.result);
      if (json.result == "KO") {
        console.log("Some error occured");
        console.log(json);
        console.log(json.msg);
        console.log(json.errorInstr);
        var errorSteps = JSON.parse(json.errorInstr);

        loadModal("modal-error");
        // TODO logic for modal error
        healthCheckLaser(errorSteps, json.code);
      } else {
        console.log("Engraving should be starting");
        loadModal("modal-engraving");
        startProgressBar(json.engravingTime, json.engravingTimeFormat);
      }
    },
    error: function (request, status, error) {
      console.log("error engraveOrder: " + error);
    },
  });
}

/* function to change the display color of the blinking circle depending on the connection to mydws and teamviewer:
 red no connection, yellow only mydws and green both of them */
function changeHealthChechCircleColor() {
  if (mydwsisConnected) {
    if (TVisConnected) {
      console.log("GREEN complete connection ");
      $("#healthCheckCircle").removeClass("yellow");
      $("#healthCheckCircle").removeClass("redBlinking");
      $("#healthCheckCircle").addClass("green");
    } else {
      console.log("YELLOW partial connection ");
      $("#healthCheckCircle").removeClass("redBlinking");
      $("#healthCheckCircle").removeClass("green");
      $("#healthCheckCircle").addClass("yellow");
    }
  } else {
    $("#healthCheckCircle").removeClass("yellow");
    $("#healthCheckCircle").removeClass("green");
    $("#healthCheckCircle").addClass("redBlinking");
  } //end if
} // end changeHealthChechCircleColor

// function to check both connections teamviewer and Mydws and change circle color
function healthCheckCommunication() {
  console.log("healthCheck communication");
  ChecMydwsConnection(); // checking mydws
  CheckTeamviewerConnection();
} // end healthCheckCommunication

//function to check access to mydws
function ChecMydwsConnection() {
  $.ajax({
    type: "POST",
    url: "./ChecMydwsConnection",
    contentType: "application/json; charset=utf-8",
    beforeSend: function () {},
    success: function (data) {
      console.log(" data is ", data);
      mydwsisConnected = true;
      console.log("connection to mydws succeed");
      changeHealthChechCircleColor();
    },
    error: function (request, status, error) {
      console.log("error to request flask server");
      mydwsisConnected = false;
      changeHealthChechCircleColor();
    },
  });
} // end ChecMydwsConnection

//function to check access to teamvewer
function CheckTeamviewerConnection() {
  $.ajax({
    type: "POST",
    url: "./CheckTeamviewerConnection",
    beforeSend: function () {},
    success: function (data) {
      console.log(" data is ", data);
      TVisConnected = true;
      console.log("connection to Teamviewer succeed");
      changeHealthChechCircleColor();
    },
    error: function (request, status, error) {
      console.log("error to request flask server");
      TVisConnected = false;
      changeHealthChechCircleColor();
    },
  });
} // end CheckTeamviewerConnection

function healthCheckLaser(errorSteps, prevErrorCode) {
  $.ajax({
    type: "POST",
    url: "./healthCheckLaser",
    beforeSend: function () {
      $("#retry_title").show();
    },
    success: function (data) {
      console.log("health =", data);
      data = data.split('"').join('\\"');
      data = data.split("'").join('"');
      var json = JSON.parse(data);
      if (json.result == "KO") {
        var errorStepsNew = JSON.parse(json.errorInstr);
        if (json.code.indexOf("003") != -1) {
          console.log("indes of != -1");
          // Some error with the generation of files, we regenerate and try again
          reSendPictureToProcess();
          $("#error_title").show();
          showErrorStepsModal(errorStepsNew, json.code);
        } else {
          $("#error_title").show();
          // We couldn't solve the issue, showing what to do...
          showErrorStepsModal(errorStepsNew, json.code);
        }
      } else {
        // Everything is ready, tell the user to relaunch
        $("#error_title").show();
        $("#retry_text").show();
        console.log("TODO Everything is ready, tell the user to relaunch");
      }
    },
    error: function (request, status, error) {
      console.log(error);
      $("#error_title").show();
      showErrorStepsModal(errorSteps, prevErrorCode);
    },
    complete: function () {
      $("#retry_title").hide();
      $("#relaunchButton2").prop("disabled", false);
    },
  });
}

function showErrorStepsModal(errorSteps, errorCode) {
  $("#list_of_errors li").remove();
  for (i in errorSteps) {
    $("#list_of_errors").append("<li>" + errorSteps[i] + "</li>");
  }
  $("#list_of_errors").append("<li>Error code: " + errorCode + "</li>");
  $("#retry_text").show();
}

function startProgressBar(engravingTime, engravingTimeFormat) {
  $("#engravingTime").attr("value", engravingTime);
  $("#engravingTime").html(
    $("#engravingTime").html().replace("#TIME_FORMAT#", engravingTimeFormat)
  );
  console.log("#engravingTime:", engravingTime);
  moveProgressBar(true);
}

function loadModal(modalId) {
  // We hide all modals to show the desired one afterwards
  $("#modal-sending-order").hide();
  $("#modal-engraving").hide();
  $("#modal-error").hide();
  $("#" + modalId).show();
  showRelaunchMessage(false);
  // If the modal is hidden, we display it
  if (!$("#myModal").hasClass("in")) $("#modalButton").click();
}
//golbal variable
var myInterval;

function moveProgressBar(CarryOnProg) {
  console.log(CarryOnProg);
  var engravingTime = $("#engravingTime").attr("value") * 10;
  console.log("times is ", engravingTime);
  var width = 1;
  var elem = document.getElementById("myBar");
  if (CarryOnProg) {
    myInterval = setInterval(frame, engravingTime);
    function frame() {
      if (width >= 98) {
        checkLaserStatus();
        clearInterval(myInterval);
      } else {
        width++;
        $("#myBar").text(width + "%");
        elem.style.width = width + "%";
      }
    }
  } else {
    clearInterval(myInterval);
    if (width < 100) {
      elem.className = "progress-bar progress-bar-danger progress-bar-striped";
    }
  }
}

// Executed when we change the keyboard selection
function changeKeyboardFromList(keyboard, extraConf, firstLoad = false) {
  console.log("# changeKeyboardFromList to ", keyboard);
  console.log("# extraConf:", extraConf);
  console.log("# firstLoad:", firstLoad);
  $("input[name='keyboard']").val(keyboard);
  $("input[name='keyboardConf']").val(extraConf);
  // Hide/show fonts
  $(".font-keyboard").hide();
  $(".all-keyboards").show();
  var keyboardClass = "." + keyboard;
  $(keyboardClass).show();
  // Change selected font
  if (!firstLoad) {
    $(".font-keyboard").each(function () {
      // We select the first font for this keyboard
      if ($(this).css("display") !== "none") {
        $(this).find("a").click();
        return false;
      }
    });
    // Change keyboard display
    changeKb(keyboard);
  } else {
    // Change keyboard display
    customJSLayoutName = keyboard;
  }
  // Remove current engraving text
  $("#textToEngrave").val("");
  $("#textToEngrave").change();
  $("#textToEngrave").removeClass("invalid");
  $(".errorMessage").hide();
  // Specific logic for extraConf
  if (extraConf === "chinese") {
    chineseAutocomplete = true;
    traditionalChineseAutocomplete = false;
    zhuyinChineseAutocomplete = false;
  } else if (extraConf === "chinese_trad") {
    chineseAutocomplete = true;
    traditionalChineseAutocomplete = true;
    zhuyinChineseAutocomplete = false;
  } else if (extraConf === "chinese_zhuyin") {
    chineseAutocomplete = true;
    traditionalChineseAutocomplete = false;
    zhuyinChineseAutocomplete = true;
  } else {
    chineseAutocomplete = false;
    traditionalChineseAutocomplete = false;
    zhuyinChineseAutocomplete = false;
  }
}

function turnOffRaspberry() {
  if (
    /1/i.test(window.location.pathname) ||
    /training/i.test(window.location.pathname)
  ) {
    healthCheckCommunication();
    $(".admin-pop-up-1, .warning-msg").removeClass("hidden");
    $("#ButtonContactDWS").removeClass("hidden");
    $(".contact-form, .system-update").addClass("hidden");
    customJSLayoutName = "password";
    keyboardInit("password");
    $("#modalButtonTurnOff").click();
  }
}

function backToStart() {
  loadModal("modal-sending-order");
  $(".modal-title").hide();
  $(".modal-title.back-to-start").show();
  // NOW
  location.href = "./screen1";
  // Test 1
  //window.location.href = "./screen1";
  // Test 2
  //window.location.assign("./screen1")
}

function backToStartStoringEngraving() {
  loadModal("modal-sending-order");
  $(".modal-title").hide();
  $(".modal-title.back-to-start").show();
  storeRating(backToStart);
}

function storeRating(cb) {
  var text = JSON.stringify($("input[name=textToEngrave]").val());
  text = text.substring(1, text.length - 1);
  var stopped;
  var relaunched;
  if ($('input[name="engraving_stopped"]').val()) stopped = true;
  if ($('input[name="engraving_relaunched"]').val()) relaunched = true;
  if ($(".q-response").attr("type") === "YN") {
    q_resp = $("input[name=rating]:checked").val();
    if (!q_resp) q_resp = "N/A";
  } else if ($(".q-response").attr("type") === "data") {
    q_resp = $("input[name=rating]").val();
  } else {
    q_resp = $(".rating > input:checked ~ label").length;
  }

  var ajax_data = {
    stars: q_resp,
    product: $("input[name=product]").val(),
    text: text,
    engravingTime: $("#engravingTime").attr("value"),
    idQuestion: $("#question_id").attr("question_id"),
    questionType: $("#question_id").attr("questionType"),
    font: $('input[name="font"]').val(),
    ts: $('input[name="ts"]').val(),
    errorMessage: $('input[name="error_message"]').val(),
    stopped: stopped,
    relaunched: relaunched,
  };

  // Checking if there was a trainee who did the engraving training
  if ($('input[name$="traineeID"]').val()) {
    console.log("Trainee did engraving training");
    ajax_data["traineeID"] = $('input[name$="traineeID"]').val();
    var etd =
      Math.round(new Date().getTime() / 1000) -
      $('input[name$="engravingTrainingDuration"]').val();
    ajax_data["engravingTrainingDuration"] = etd;
  }

  $.ajax({
    type: "POST",
    url: "./dataManagement",
    data: ajax_data,
    timeout: 1000,
    beforeSend: function () {},
    success: function (data) {
      console.log(data);
    },
    error: function (request, status, error) {},
    complete: function () {
      cb();
    },
  });
}

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(
    /[?&]+([^=&]+)=([^&]*)/gi,
    function (m, key, value) {
      vars[key] = value;
    }
  );
  return vars;
}

function sendPictureToProcess(img, pinMode, color) {
  var ajax_data = {
    base64img_process: img,
    pinMode: pinMode,
    color: color,
  };

  $.ajax({
    type: "POST",
    url: "./image",
    data: ajax_data,
    beforeSend: function () {},
    success: function (data) {
      console.log(data);
      if (data.trim() == "True") {
        // Laser is engraving
      }
    },
    error: function (request, status, error) {},
  });
}

function checkLaserStatus() {
  // Returns true if the laser is ON
  $.ajax({
    type: "POST",
    url: "./checkLaserStatus",
    beforeSend: function () {},
    success: function (data) {
      console.log(data);
      if (/True/i.test(data)) {
        console.log("LASER ON");
        // If laser is still ON, we check again
        checkLaserStatus();
      } else if (/False/i.test(data)) {
        console.log("LASER OFF");
        document.getElementById("myBar").style.width = "100%";
        $("#myBar").text("100%");
        document.getElementById("myBar").className =
          "progress-bar progress-bar-success progress-bar-striped";
        $(".modal-title.finishText").hide();
        //$('.stopButton').hide();
        $(".stopButton").prop("disabled", true);
        $(".modal-title.order-finished").show();
        $("#relaunchButton").prop("disabled", false);
      } else if (/Error/i.test(data)) {
        console.log("ERROR checking laser status");
        checkLaserStatus();
        //errorCheckingLaserStatus();
      } else {
        console.log("Unexpected response");
        errorCheckingLaserStatus();
      }
    },
    error: function (request, status, error) {
      console.log("Ajax error: " + error);
      errorCheckingLaserStatus();
    },
  });
}

function checkLaserIsReady() {
  console.log("\tCHECKING IF LASER IS READY");
  // Returns true if the laser is ON
  var timeIfLaserReady = 3000;
  var timeIfLaserNotReady = 1000;
  var timeIfTimeOut = 1000;
  var timeIfErrorChecking = 3000;
  var timeIfAjaxError = 3000;
  $.ajax({
    type: "POST",
    url: "./checkLaserIsReady",
    beforeSend: function () {},
    success: function (data) {
      console.log("\t\t " + data);
      if (/True/i.test(data)) {
        console.log("\t\t LASER READY");
        // If laser is ready, we enable the print button
        $("#printButton").prop("disabled", false);
        $("#redRectangleButton").prop("disabled", false);
        $("#redRectangleButtonModal").prop("disabled", false);
        $("#confirmRelaunchButton").prop("disabled", false);
        $(".warning-msg").hide();
        $(".info").hide();
        $("#laser-ready-msg").show();
        setTimeout(function () {
          console.log("\t\t checking again");
          if (keepCheckingLaserReady) checkLaserIsReady();
        }, timeIfLaserReady);
      } else if (/False/i.test(data)) {
        console.log("\t\t LASER NOT READY");
        $("#printButton").prop("disabled", true);
        $("#redRectangleButton").prop("disabled", true);
        $("#redRectangleButtonModal").prop("disabled", true);
        $("#confirmRelaunchButton").prop("disabled", true);
        // If laser is not ready, we check again
        $(".info").hide();
        $("#laser-off-msg").hide();
        $("#laser-interlock-msg").show();
        setTimeout(function () {
          console.log("\t\t checking again");
          if (keepCheckingLaserReady) checkLaserIsReady();
        }, timeIfLaserNotReady);
      } else if (/TimeOut/i.test(data) || /Error/i.test(data)) {
        console.log("\t\t Timeout, laser must be OFF");
        $(".info").hide();
        $(".warning-msg").hide();
        $("#laser-off-msg").show();
        // If laser is OFF, we check again
        setTimeout(function () {
          console.log("\t\t checking again");
          if (keepCheckingLaserReady) checkLaserIsReady();
        }, timeIfTimeOut);
      } else {
        console.log("\t\t Unexpected response");
        // Error management
        setTimeout(function () {
          console.log("\t\t Error checking laser");
          if (keepCheckingLaserReady) checkLaserIsReady();
        }, timeIfAjaxError);
        //$('#printButton').prop('disabled', false);
        //$('#confirmRelaunchButton').prop('disabled', false);
        //errorCheckingLaserStatus();
      }
    },
    error: function (request, status, error) {
      console.log("Ajax error: " + error);
      // Error management
      setTimeout(function () {
        console.log("Error checking laser");
        if (keepCheckingLaserReady) checkLaserIsReady();
      }, timeIfAjaxError);
      //$('#printButton').prop('disabled', false);
      //$('#confirmRelaunchButton').prop('disabled', false);
    },
    complete: function () {
      if ($("#redRectangleButton").hasClass("allow-always")) {
        $("#redRectangleButton").prop("disabled", false);
      }
    },
  });
}
//function to check if the laser is on (is responding)
function checkLaserIsON() {
  // Returns true if the laser is ON
  $.ajax({
    type: "POST",
    url: "./checkLaserIsOn",
    beforeSend: function () {},
    success: function (data) {
      console.log(data);
      if (/True/i.test(data)) {
        console.log("LASER ON");
      } else if (/False/i.test(data)) {
        console.log("LASER OFF");
        location.href = "/screen1?errorcode=201";
      } else {
        console.log("Unexpected response");
      }
    },
    error: function (request, status, error) {
      console.log("Ajax error: " + error);
    },
  });
}

function errorCheckingLaserStatus() {
  $("#circle").addClass("error-connecting");
  $(".modal-title.order-finished").addClass("hidden");
  $(".order-finished-error").show();
  $(".finishText.stars-container").hide();
}

function objInCanvasInMM(obj) {
  heightInMM = obj.height * vertical_rapport;
  widthInMM = obj.width * vertical_rapport;
  console.log("heightInMM = " + heightInMM);
  console.log("widthInMM = " + widthInMM);
  var resp = {};
  resp.height = heightInMM;
  resp.width = widthInMM;
  return resp;
}

function isTextTooHigh(text, fontsize) {
  //console.log("isTextTooHigh");
  var resText = text.trim().split("\n");
  var textHMM =
    fontsize * getLineSeparationValue() * resText.length * vertical_rapport;

  var outOfBoundaries = true;
  if (typeof objInCanvas !== "undefined" && objInCanvas.length > 0) {
    objHeightMM = objInCanvas[0].height * vertical_rapport;
    console.log(objInCanvas[0]);
    //console.log("objHeightMM = " + objHeightMM)
    //console.log("maxHeightMM = " + maxHeightMM)
    if (objHeightMM < maxHeightMM) {
      outOfBoundaries = false;
    }
  } else {
    outOfBoundaries = false;
  }
  // TODO this 1.075 is a bit random, we should improve this
  if (textHMM > maxHeightMM * 1.075) {
    outOfBoundaries = true;
    console.warn("Text too high");
  }
  outOfBoundaries
    ? $(".errorNumberLines").show()
    : $(".errorNumberLines").hide();
  return outOfBoundaries;
}

function isTextTooWide(XOffset, text) {
  //console.log("isTextTooWide");
  console.log("XOffset is ", XOffset);
  XOffset = parseInt(XOffset, 10);
  var outOfBoundaries = false;
  var canvas = document.getElementById("textCanvas");
  var c = canvas.getContext("2d");
  var resText = text.split("\n");
  for (var i = 0; i < resText.length; i++) {
    // TODO we should find a way to make the condition "vertical_engraving === 0" general
    if (vertical_engraving == 0) {
      console.log("WARNING vertical_engraving:", vertical_engraving);
      var objWidthMM = c.measureText(resText[i]).width * vertical_rapport;
    } else {
      console.log("WARNING vertical_engraving !0:", vertical_engraving);
      var objWidthMM = objInCanvas[0].width * vertical_rapport;
    }
    // Scaling down the real width of the engraving
    objWidthMM = objWidthMM / getScale("textCanvas");
    //TODO change the value of 30 by a general variable of half width of the lasera engraving area
    if (maxWidthMM < objWidthMM || objWidthMM / 2 + Math.abs(XOffset) > 30) {
      outOfBoundaries = true;
      console.warn("Text too wide");
    }
  }
  outOfBoundaries
    ? $(".errorNumberCharacters").show()
    : $(".errorNumberCharacters").hide();
  return outOfBoundaries;
}

// Example below
function changePreDefTexts(jsonText) {
  for (prod in jsonText) {
    if (product_name.indexOf(prod) !== -1) {
      for (button in jsonText[prod]) {
        var it = parseInt(button) + 1;
        DisplayText = jsonText[prod][button]["DisplayText"];
        JSText = jsonText[prod][button]["JSText"];
        $("#predefBut" + it).attr(
          "onClick",
          "writePredefVal('" + JSText + "')"
        );
        $("#predefBut" + it).text(DisplayText);
      }
    }
  }
}
// Example of JSON for function above
/*
var customPreDefTexts = {"Midnight": [{"DisplayText": "I LOVE YOU", "JSText": "I\\nLOVE\\nYOU"},
                                    {"DisplayText": "FOR YOU", "JSText": "FOR\\nYOU"},
                                    {"DisplayText": "MARRY ME", "JSText": "MARRY\\nME"}],
                          "Brave": [{"DisplayText": "I LOVE YOU", "JSText": "I\\nLOVE\\nYOU"},
                                    {"DisplayText": "FOR YOU", "JSText": "FOR\\nYOU"},
                                    {"DisplayText": "MARRY ME", "JSText": "MARRY\\nME"}],
                          "Yes": [{"DisplayText": "I LOVE YOU", "JSText": "I\\nLOVE\\nYOU"},
                                    {"DisplayText": "FOR YOU", "JSText": "FOR\\nYOU"},
                                    {"DisplayText": "MARRY ME", "JSText": "MARRY\\nME"}]}
*/

function writePredefVal(predefinedVal) {
  $("#textToEngrave").val(predefinedVal);
  $("#textToEngrave").change();
}

function printLogo(logoUrl, logoText = $("#textToEngrave").val()) {
  $("input[name='pictogram']").val(logoUrl);
  console.log(logoUrl);
  textToEngravePreview(logoText, logoUrl);
  $("#textToEngrave").val(logoText);
  $("#textToEngrave").change();
}

//function to stop engraving
function showRedRectangle() {
  $.ajax({
    type: "POST",
    url: "./showRedRectangle",
    beforeSend: function () {
      $("#redRectangleButton").addClass("hidden");
      $("#redRectangleButton-loading").removeClass("hidden");
      $("#printButton").prop("disabled", true);
      keepCheckingLaserReady = false;
    },
    error: function (error) {
      console.log(error);
    },
    success: function (data) {
      console.log(data);
    },
    complete: function () {
      $("#redRectangleButton").removeClass("hidden");
      $("#redRectangleButton-loading").addClass("hidden");
      keepCheckingLaserReady = true;
      checkLaserIsReady();
      console.log("complete");
    },
  });
}

function changeLang() {
  var selectBox = document.getElementById("selectLangBox");
  var lang = selectBox.options[selectBox.selectedIndex].value;
  console.log("changing lang to: " + lang);
  location.href = location.origin + location.pathname + "?new_lang=" + lang;
}

function changeToLang(lang) {
  location.href = location.origin + location.pathname + "?new_lang=" + lang;
}

function switchScreenSaver() {
  if ($("#background-img").is(":visible")) {
    $("#background-img").hide();
    $("#screen-saver-img").show();
  } else {
    $("#background-img").show();
    $("#screen-saver-img").hide();
  }
}

var timeoutId;

function resetTimer() {
  window.clearTimeout(timeoutId);
  startTimer();
}

function startTimer() {
  // window.setTimeout returns an Id that can be used to start and stop a timer
  timeoutId = window.setTimeout(doInactive, timeoutInMiliseconds);
}

function doInactive() {
  // does whatever you need it to actually do - probably signs them out or stops polling the server for info
  console.log("user inactive");
  switchScreenSaver();
}

// This function starts a timer to check for inactivity and display a screen saver
function setupTimersForInactivity(time) {
  if (time) {
    timeoutInMiliseconds = time;
  }
  document.addEventListener("mousemove", resetTimer, false);
  document.addEventListener("mousedown", resetTimer, false);
  document.addEventListener("keypress", resetTimer, false);
  document.addEventListener("touchmove", resetTimer, false);

  startTimer();
}

/****************************************************************************/

function showShutdownGif(text) {
  $("#sendFeedBack").addClass("hidden");
  $("h1.loadingGif").html(text);
  $(".warning-msg").hide();
  $(".loadingGif").show();
  $(".contact-form").addClass("hidden");
}

var regularFontSize;
function changeFontSize(proportion) {
  regularFontSize = fontsize;
  fontsize = fontsize * proportion;
  $("#textToEngrave").attr("previous-val", proportion + "aaaa");
  $("#textToEngrave").change();
  fontsize = fontsize / proportion;
}

function closeApp(pass) {
  console.log("Closing app if pass is correct");
  console.log("fonction closeapp after add class hidden");
  $.ajax({
    type: "POST",
    url: "./closeApp",
    data: {
      pass: pass,
    },
    error: function (e) {
      console.error(e);
    },
    success: function (data) {
      if (!/true/i.test(data)) alert("Incorrect Password");
      console.log(data);
    },
    complete: function () {
      $("#password").val("");
    },
  });
}

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

function validateCreateCollectionForm() {
  var formOk = true;
  var textArea = isJson($("textarea[name=products_structure]").val());
  if (!textArea) {
    $("textarea[name=products_structure]").focus();
    $("textarea[name=products_structure]").addClass("invalid");
    alert("products_structure is not a JSON");
  }
  formOk = textArea;
  return formOk;
}

function validateCreateProductForm() {
  var formOk = true;
  var prod_id_name = "-product_id";
  var cat_id_name = "-product_category_id";
  // Checking that product IDs are all different
  var prod_ids = [];
  $("form input[name$=" + prod_id_name + "]").each(function (a) {
    var prod_id = $(this).val();
    console.log($(this).attr("name"));
    var id = $(this).attr("name").split("-")[0];
    var category = id + cat_id_name;
    var cat_id = $("form input[name=" + category + "]").val();
    var cat_prod_id = cat_id + "_" + prod_id;
    if (prod_ids.indexOf(cat_prod_id) === -1) {
      prod_ids.push(cat_prod_id);
    } else {
      alert("Repeated product ID: " + cat_prod_id);
      $(this).focus();
      $(this).addClass("invalid");
      console.error("Repeated product ID: " + cat_prod_id);
      formOk = false;
    }
  });

  // Checking dimensions
  $("form input[name$=dimensions]").each(function (a) {
    var dim = $(this).val();
    if (dim.indexOf(",") === -1 || dim.split(",").length !== 2) {
      // Dimension doesnt have , or is too long
      $(this).focus();
      $(this).addClass("invalid");
      console.error("Dimension doesnt have comma or too long");
      formOk = false;
    } else if (!isInt(dim.split(",")[0]) || !isInt(dim.split(",")[1])) {
      // Dimensions are not numbers
      console.error("Incorrect dimensions");
      alert("Incorrect dimensions");
      $(this).focus();
      $(this).addClass("invalid");
      formOk = false;
    } else {
      $(this).removeClass("invalid");
    }
  });

  return formOk;
}

function isInt(value) {
  return (
    !isNaN(value) &&
    parseInt(Number(value)) == value &&
    !isNaN(parseInt(value, 10))
  );
}

function openPassKeyboard(passType) {
  $("input[name=password-type]").val(passType);
  $("#ButtonContactDWS").removeClass("hidden");
  $(".contact-form").addClass("hidden");
  console.log("openPassKeyboard INIT");
  $("#password").removeClass("hidden");
  $("#password").focus();
}

function openKeyboard() {
  console.log("openKeyboard INIT");
  $("#informErrorMessage").removeClass("hidden");
  $("#informErrorMessage").focus();
}

function openfeedBackKeyboard() {
  $("#TextClickContactMessage").addClass("hidden");
  customJSLayoutName = "defaultDWS";
  keyboardInit("feedBackMessage");
  $("#feedBackMessage").removeClass("hidden");
  $("#feedBackMessage").focus();
}

function showFeedBackMessage() {
  $("#ButtonContactWriteMessage").removeClass("hidden");
  $("#feedBackMessage").addClass("hidden");
  if ($("#feedBackMessage").val() === "") {
    $("#TextClickContactMessage").removeClass("hidden");
    $("#sendFeedBack").prop("disabled", true);
  } else {
    $("#youCanClickSend").removeClass("hidden");
    $("#sendFeedBack").prop("disabled", false);
  }
}

function enableFeedback(show) {
  if (show) {
    $(".admin-pop-up-1, .warning-msg").addClass("hidden");
    $("a.contact-form").removeClass("hidden");
    AfficherContactDWS();
  } else {
    $(".admin-pop-up-1, .warning-msg").removeClass("hidden");
    $(".contact-form").addClass("hidden");
  }
}

function AfficherContactDWS() {
  $("#SuccessSendFeedBack").addClass("hidden");
  $("#504ErrorFeedBack").addClass("hidden");
  $("#404ErrorFeedBack").addClass("hidden");
  $("#ServerProblemFeedBack").addClass("hidden");
  $("#TextClickContactMessage").removeClass("hidden");
  $("#ButtonContactWriteMessage").removeClass("hidden");
  $("#ButtonContactDWS").addClass("hidden");
  $("#sendFeedBack").removeClass("hidden");
}

// This funtion gets the feddback message of the client and sends it the the flask server and manipulate the request via an JQuerry AJAX
function sendFeedBackMessage() {
  var FeedBackMessageAjax = { feedBack_message: $("#feedBackMessage").val() };
  console.log("FeedBackMessageAjax is ", FeedBackMessageAjax);
  $.ajax({
    type: "POST",
    url: "./sendFeedBackMessage",
    data: FeedBackMessageAjax,
    //timeout: 2000,
    beforeSend: function () {
      $(".loadingGif").show();
      $("#youCanClickSend").addClass("hidden");
    },
    success: function (data) {
      console.log("Here we are The data", data);
      // we will say to the user hey every thing is ok the message has been sent
      $("#SuccessSendFeedBack").removeClass("hidden");
    },
    error: function (xhr) {
      $("#textOfFeedBackMessage").addClass("hidden");
      $("#sendFeedBack").addClass("hidden");
      console.log("sattussasas is ", xhr.status);
      if (xhr.status === 504) {
        //we will say to the user hey there is a problem of internet connection
        $("#504ErrorFeedBack").removeClass("hidden");
      } else if (xhr.status === 404) {
        $("#404ErrorFeedBack").removeClass("hidden");
      } else {
        // a server problem
        $("#ServerProblemFeedBack").removeClass("hidden");
      }
    },
    complete: function () {
      $(".loadingGif").hide();
      $("#youCanClickSend").addClass("hidden");
      $("#textOfFeedBackMessage").addClass("hidden");
      $("#sendFeedBack").addClass("hidden");
      console.log("feed back message has sent to browser");
      $("#feedBackMessage").val("");
      $("#ButtonContactDWS").removeClass("hidden");
    },
  });
}

function showInformErrorMessage() {
  console.log(
    "showInformErrorMessage INIT: show a message saying that we will send a notification with your comment"
  );
  $("#informErrorMessage").addClass("hidden");
  $(".stars-container").hide();
  console.log("message:", $("#informErrorMessage").val());
  $('input[name="error_message"]').val($("#informErrorMessage").val());
  $(".inform-error-text").show();
}

function customFunctionBeforeGoToScreen3() {
  console.log("to be overwritten");
}
function customFunctionAfterGoToScreen3() {
  console.log("to be overwritten");
}
function customAcceptKeyboardLogic(val) {
  console.log("to be overwritten. VALUE:", val);
}

function fromScreen2ToScreen3() {
  customFunctionBeforeGoToScreen3();
  // TODO uncomment this to remove the restricted engravable area
  // TODO Delete the red square from the display canvas before sending it
  /*
    $("#goForwardA").attr("hideRestrictionSquare","true");
    if(lastMove !== null){
        var rect = lastMove.target.getBoundingClientRect();
        var xPos = lastMove.targetTouches[0].pageX - rect.left;
        var yPox = lastMove.targetTouches[0].pageY - rect.top;
        mouseDragging(xPos, yPox, "printCanvas", objectSelectedId, true);
        canvas_restriction = undefined;
    } else {
        // When the user clicks validate without moving the text on the canvas
        $("#textToEngrave").change();
    }
    */
  // Generation of the image to send to next step to display
  generateBase64img("textCanvas", "base64img_display");
  // Generation of the image to send to next step to process and engrave
  generateBase64img("printCanvas", "base64img_process");
  customFunctionAfterGoToScreen3();
  if (formOk()) {
    $("#screen2Form").submit();
  }
}

/* CHINESE LOGIC */
//displayAutocompleteCharacters(chinese_auto["a"])

function displayAutocompleteCharacters(characters) {
  var row = 0;
  var multiplo = 10;
  var nRows = Math.ceil(characters.length / multiplo);
  var table =
    "<table value='" +
    row +
    "' nRows='" +
    nRows +
    "'><tbody><tr class='chi-row-" +
    row +
    "'>";
  var endTable = "</tr></tbody></table>";
  console.log("characters:", characters);
  var charIterable = characters.split("");
  // Some chinese characters need 2 bytes and when splitting them by "", they get cut in half
  // These characters are formatted in the chinese-pynsimpl.js file with " "
  if (characters.includes(" ")) {
    charIterable = characters.split(" ");
  }
  for (var i = 0; i < charIterable.length; i++) {
    var letter =
      "<td><a><b>" +
      ((i + 1) % multiplo) +
      ": </b><span>" +
      charIterable[i] +
      "</span></a></td>";
    table += letter;
    if ((i + 1) % multiplo == 0 && i != characters.length - 1) {
      row++;
      table += "</tr><tr style='display:none' class='chi-row-" + row + "'>";
    }
  }
  table += endTable;
  $(".IMEPageCounter").html("1/" + (row + 1));
  $(".IMEControl.IMEContent").html(table);
  $("#VirtualKeyboardIME").addClass("chinese");
  $("#VirtualKeyboardIME").removeClass("hidden");
  defineClickForLetters();
}

function defineClickForLetters() {
  $("#VirtualKeyboardIME a").click(function () {
    var value = $(this).find("span").html();
    console.log("clicking button:", value);
    console.log("chineseSyllable:", chineseSyllable);
    if ($(".ui-keyboard-preview-wrapper textarea").length > 0) {
      inputElement = $(".ui-keyboard-preview-wrapper textarea");
    } else {
      inputElement = $(".ui-keyboard-preview-wrapper input");
    }
    var cur_val = inputElement.val();
    console.log("replaceLast cur_val:", cur_val);
    cur_val = replaceLast(cur_val, chineseSyllable, "");
    inputElement.val(cur_val + value);
    // Hiding the autocomplete
    $(".IMEControl.IMEContent").html("");
    $("#VirtualKeyboardIME").addClass("hidden");
    $(".showAll").removeClass("showPage");
    // Reseting syllable for next characters
    chineseSyllable = "";
  });
}

function replaceLast(str, what, replacement) {
  var response = "";
  if (str.indexOf(what) != -1) {
    var pcs = str.split(what);
    var lastPc = pcs.pop();
    response = pcs.join(what) + replacement + lastPc;
  } else {
    response = str;
  }
  return response;
}

var chineseSyllable = "";

function updateChineseSyllable() {
  chineseSyllable = chineseSyllable.substr(0, chineseSyllable.length - 1);
}

function showChineseAutocomplete(letter) {
  console.log("letter:", letter);
  chineseSyllable = chineseSyllable + letter;
  // If we have a space, we consider a new syllable
  if (letter == " ") {
    chineseSyllable = "";
  }
  console.log("chineseSyllable:", chineseSyllable);

  var listOfCharacters = chinese_auto;
  if (traditionalChineseAutocomplete == true)
    listOfCharacters = traditional_chinese_auto;
  if (zhuyinChineseAutocomplete == true) listOfCharacters = zhuyin_chinese_auto;

  if (chineseSyllable && listOfCharacters[chineseSyllable]) {
    displayAutocompleteCharacters(listOfCharacters[chineseSyllable]);
  } else {
    $(".IMEControl.IMEContent").html("");
    $("#VirtualKeyboardIME").addClass("hidden");
    //displayAutocompleteCharacters("")
  }
}

var origPos = 1;
var incrementChangeHeight;

function customTextToEngraveChangeFunction() {}

function getIncrementChangeHeightProp() {
  return 0;
}

function changeTextHeight() {
  if ($("#textToEngrave").val().length !== 0) {
    console.log("changeTextHeight INIT");
    var increment = 110;
    if (!incrementChangeHeight)
      incrementChangeHeight = getIncrementChangeHeightProp() * default_height;
    //default_height += origPos*incrementChangeHeight;
    default_height += origPos * increment;
    console.log("default_height1===", default_height);
    origPos = -origPos;
    console.log("default_height2===", default_height);

    if (isTextInBoundaries($("#textToEngrave").val())) {
      $("#textToEngrave").attr("previous-val", "");
      $("#textToEngrave").change();
      $("#goForwardA").prop("disabled", false);
      console.log(" je passe dans le if");
    } else {
      $("#goForwardA").prop("disabled", true);
      console.warn("object didnt fit after changing height");
      console.log(" je passe dans le else");
    }
  }
}

function backToCategory() {
  $(".cat-selection").show();
  $(".prod-cat").hide();
  $("#backCategories").hide();
}

function showCategory(step) {
  var catClass = ".cat-" + step;
  $(".cat-selection").hide();
  $("#backCategories").show();
  $(catClass).show();
}

function prepareAppForInstallation() {
  console.log("prepareAppForInstallation");
}

function enableSystemUpdate(show) {
  if (show) {
    $(".admin-pop-up-1, .warning-msg").addClass("hidden");
    $(".system-update").removeClass("hidden");
  } else {
    $(".admin-pop-up-1, .warning-msg").removeClass("hidden");
    $(".system-update").addClass("hidden");
    $(".system-update-result").addClass("hidden");
  }
}

function updateSystemCollection() {
  console.log("updateSystemCollection");
  $.ajax({
    type: "GET",
    url: "./updateSystemCollection",
    beforeSend: function () {
      $(".loadingGif").show();
      $(".system-update-result").addClass("hidden");
    },
    success: function (data) {
      console.log(data);
      $("#system-update-result").html(data["message"]);
    },
    error: function (request, status, error) {
      $("#system-update-result").addClass("hidden");
      $("#system-update-result-error").removeClass("hidden");
    },
    complete: function () {
      $(".loadingGif").hide();
      $(".system-update-result").removeClass("hidden");
    },
  });
}

function startBackUpProcess(folderPath, date, prodName, prod_id) {
  console.log("startBackUpProcess for:", prodName);
  // Filling HTML for display
  $("#selected_date").html(date);
  $("#folder-path").val(folderPath);
  if (prodName && prod_id) {
    // This is a product
    $("#prod_id").val(prod_id);
    $(".selected_product-p").removeClass("hidden");
    $("#selected_product").html(prodName);
  } else {
    $("#prod_id").val("");
    $(".selected_product-p").addClass("hidden");
  }
  // Showing HTML
  $(".backup-step-1").addClass("hidden");
  $(".backup-step-2").removeClass("hidden");
}

function showBackupList() {
  $(".backup-step-1").removeClass("hidden");
  $(".backup-step-2").addClass("hidden");
}

function confirmBackUp() {
  var path = $("#folder-path").val();
  console.log("confirmBackUp:", path);
  var isCollection = $("#selected_product").hasClass("hidden");
  console.log("isCollection:", isCollection);
  var prodId = $("#prod_id").val();

  $.ajax({
    type: "GET",
    url: "./reinstalBackup",
    data: {
      path: path,
      isCollection: isCollection,
      prodId: prodId,
    },
    beforeSend: function () {
      $(".backup-step-2").addClass("hidden");
      $(".backup-step-2.result").removeClass("hidden");
      $(".loadingGif").show();
    },
    success: function (data) {
      console.log(data);
      $(".backup-ok").removeClass("hidden");
    },
    error: function (request, status, error) {
      $(".backup-ko").removeClass("hidden");
    },
    complete: function () {
      $(".loadingGif").hide();
    },
  });
}

function customLogicLoadScreen2() {
  console.log("This is empty by default, collections can overwrite it");
}

function customLogicLoadScreen3() {
  console.log("This is empty by default, collections can overwrite it");
}

/****************************** INIT font size buttons + and - ******************************/
// Changes the min and max value of the buttons
// TODO Is this function needed??
function changeFontSizeButtonValues() {
  var a = 1;
}

$(document).ready(function () {
  var stepFontSizeButtons = 0.5;
  $(".btn-number").click(function (e) {
    e.preventDefault();
    fieldName = $(this).attr("data-field");
    type = $(this).attr("data-type");
    var input = $("input[name='" + fieldName + "']");
    var currentVal = parseFloat(input.val());
    if (!isNaN(currentVal)) {
      if (type == "minus") {
        if (currentVal > parseFloat(input.attr("min"))) {
          input.val(currentVal - stepFontSizeButtons).change();
        }
        if (
          parseFloat(input.val()) <
          parseFloat(input.attr("min")) + stepFontSizeButtons
        ) {
          $(this).attr("disabled", true);
        }
      } else if (type == "plus") {
        if (currentVal < parseFloat(input.attr("max"))) {
          input.val(currentVal + stepFontSizeButtons).change();
        }
        if (
          parseFloat(input.val()) >
          parseFloat(input.attr("max")) - stepFontSizeButtons
        ) {
          $(this).attr("disabled", true);
        }
      }
    } else {
      input.val(0);
    }
    // Redrawing the canvas with current value
    fontsize = obtainFontSizeGivenMM(vertical_rapport, parseFloat(input.val()));
    var allowChange = isTextInBoundaries(
      $("#textToEngrave").val(),
      fontsize,
      fontsize
    );
    if (allowChange) {
      $("#textToEngrave").change();
    }
  });
  $(".input-number").focusin(function () {
    $(this).data("oldValue", $(this).val());
  });
  $(".input-number").change(function () {
    minValue = parseFloat($(this).attr("min"));
    maxValue = parseFloat($(this).attr("max"));
    valueCurrent = parseFloat($(this).val());

    name = $(this).attr("name");
    if (valueCurrent >= minValue) {
      $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr(
        "disabled"
      );
    } else {
      alert("Sorry, the minimum value was reached");
      $(this).val($(this).data("oldValue"));
    }
    if (valueCurrent <= maxValue) {
      $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr(
        "disabled"
      );
    } else {
      alert("Sorry, the maximum value was reached");
      $(this).val($(this).data("oldValue"));
    }
  });
});
/****************************** END font size buttons + and - ******************************/
/****************************** INIT pictogram new logic ******************************/
function noPictogram() {
  $("#textToEngrave").change();
  // Removing pictogram
  $("input[name='pictogram']").val("");
}
/****************************** END pictogram new logic ******************************/
