
var chineseAutocomplete = false;
var initTime = new Date().getTime() / 1000;

$( document ).ready(function() {
	$('#finish-training-btn').hide();
});

function stopCountingTrainingTime(){
    var endTime = new Date().getTime() / 1000;
    var trainingTime = Math.round(endTime - initTime);
    $('input[name$="general_training_duration"]').val(trainingTime);
}

// Loads a GIF in the screen while loading /screen1
function loadEndGeneralTrainingGif(){
	$('#training-confirmation').hide();
	$('.row.repeat').hide();
	$('button').hide();
	$('#training-loading-gif').show();
}

function startTrainingKeyboards(id){
    keyboardInit("firstName-" + id);
    keyboardInit("lastName-" + id);
    keyboardInit("trainee_id-" + id);
}

function changeImageNumber(step){
    var body = $('.background-img-training');
    var classes = body.attr("class").split(/\s+/);
    var currentStep = 0;
    for (i in classes){
        if (/background-img-order/.test(classes[i])){
            currentStep = parseInt(classes[i].replace("background-img-order-", ""));
        }
    }
    var newStep = currentStep + step;

    if(newStep >= 0){
        $('.training.btn.back').prop('disabled', false);
        var newStep_f = newStep.toString().padStart(4, "0");
        // Formating number to string
        var currentStep_f = currentStep.toString().padStart(4, "0");
        body.removeClass("background-img-order-" + currentStep_f);
        body.addClass("background-img-order-" + newStep_f);
    }
    // We arrived to last screen
    if(newStep > n_slides){
        switchTrainingDisplay();
        $('.training.btn.next').hide();
        $("#add-new-person").hide();
        $("#continue-training").show();
        $("#training-welcome").hide();
        $("#training-confirmation").show();
    }
    // We click back from last screen
    if(step == -1 && newStep == n_slides){
        switchTrainingDisplay();
        $('.training.btn.next').show();
        $("#continue-training").hide();
        $("#training-welcome").show();
        $("#training-confirmation").hide();
    }

    // We disable the back button if we are in the first page
    if(newStep === 0) {
        $('.training.btn.back').prop('disabled', true);
    }
    if(newStep <= 1 && currentStep <= 1){
        switchTrainingDisplay();
    }
}


function switchTrainingDisplay(){
    if($('#form-trainee').is(':visible')){
        $('#form-trainee').hide();
        $('.training.btn.trainee-form').hide();
    } else {
        $('#form-trainee').show();
        $('.training.btn.trainee-form').show();
    }
}


function addNewPerson(){
    var cur_n_trainees = parseInt($("form .repeat").length);
    // We get the IDs of the people we have at the moment
    var ids = $("form .repeat").map(function (){
        return $(this).attr("about");
    }).get();

    for(var i = 1; i <= max_n_trainees; i++){
        if($.inArray( i.toString(), ids ) === -1){
            next_n = i;
            var nextRow = $('#repeat-BASE').html().split("BASE").join(next_n);
            var lastRowSelector = ".row.repeat-" + (next_n - 1);
            $( lastRowSelector ).after( nextRow );
            $( (".row.repeat-"+next_n) ).show();
            $('input[name$="n_trainees"]').val(cur_n_trainees + 1);
            startTrainingKeyboards(next_n);
            break;
        }
    }
}

function removePerson(elem){
    // We remove the element
    var class_elem = "." + elem
    $(class_elem).remove();
    var cur_n_trainees = parseInt($('input[name$="n_trainees"]').val());
    $('input[name$="n_trainees"]').val(cur_n_trainees - 1);
    // We check if the continue button should be enabled
    enableContinueButton();
}

function enableContinueButton(){
    var enableContinue = true;
    $('.repeat:not(.repeat-BASE) span.error').each(function(){
        console.log("a:",$(this).is(':visible'))
        enableContinue = enableContinue && !$(this).is(':visible')
    });
    if(enableContinue){
        $('#continue-training').prop("disabled",false);
    } else {
        $('#continue-training').prop("disabled",true);
    }
}


function checkTraineeID(trainee_id, input_id){
    console.log("trainee id:", trainee_id)
    console.log("input id:", input_id)
    var errorMsgSel = '.error.' + input_id
    // Ajax, if trainee exists, we dont allow continue to /screen1
        $.ajax({
            type: 'GET',
            url: "/traineeExists?trainee_id=" + trainee_id,
            success: function(data) {
                if(data != "{}"){
                    console.log("user exists");
                    $(errorMsgSel).show();
                } else {
                    // Check if this id is repeated on the screen
				    var valueArray = $('input[name^="trainee_id"]').not('[name$="BASE"]').map(function() {
					    return this.value;
					}).get();
				    if(hasDuplicates(valueArray)){
				        console.log("user exists");
				        $(errorMsgSel).show();
				    } else {
				        $(errorMsgSel).hide();
				    }
                }
                enableContinueButton();
            },
            error: function(request, status, error) {
                console.log(error)
            }
        });
    // Highlight repeated trainee ID
}

function hasDuplicates(array) {
    return (new Set(array)).size !== array.length;
}

/************************* Functions for training displays *************************/
function showHideKeyboard(openKeyboard){
    if(openKeyboard){
        $('#textToEngrave').focus();
    }else{
        $('.ui-keyboard-accept').mousedown();
    }
}

function showHideAdminPopup(showPopup){

    if(showPopup && !$("#myModalTurnOff").is(":visible")){
        turnOffRaspberry();
    } else if(!showPopup && $("#myModalTurnOff").is(":visible")){
        turnOffRaspberry();
    }


}



function validateGeneralTrainingForm(){
    var formOk = true;
    $("form input:visible").each(function(){
        if(!$(this).val()){
            formOk = false;
            $(this).addClass("error");
        } else {
            $(this).removeClass("error");
        }
    });
    if(formOk){
        stopCountingTrainingTime();
        loadEndGeneralTrainingGif();
    }
    return formOk;
}

function isInputValid($elem){
        if(!$elem.val()){
            $elem.addClass("error");
        } else {
            $elem.removeClass("error");
        }
    }

/************************* End of training functions for display *************************/

$(function() {
    function getEngravingTutorialConfig(item, cb) {
        var lang = app_lang ? "_" + app_lang : "";
        $.ajax({
            type: 'GET',
            url: "/testgettrainingconfig",
            data:{
                item: item,
                lang: lang
            },
            success: function(data) {
                var json = JSON.parse(data);
                startTour(json);
                if(cb) cb()
            },
            error: function(request, status, error) {
                console.log(error)
            }
        });
    }


    /*
    the json config obj.
    name: the class given to the element where you want the tooltip to appear
    bgcolor: the background color of the tooltip
    color: the color of the tooltip text
    text: the text inside the tooltip
    time: if automatic tour, then this is the time in ms for this step
    position: the position of the tip. Possible values are
    	TL	top left
    	TR  top right
    	BL  bottom left
    	BR  bottom right
    	LT  left top
    	LB  left bottom
    	RT  right top
    	RB  right bottom
    	T   top
    	R   right
    	B   bottom
    	L   left
     */
    var config,
        //define if steps should change automatically
        autoplay = false,
        //timeout for the step
        showtime,
        //current step of the tour
        step = 0,
        //total number of steps
        //total_steps	= config.length;
        total_steps;

    //show the tour controls
    //showControls();

    /*
    we can restart or stop the tour,
    and also navigate through the steps
     */
    $('#activatetour').on('click', startTour);
    $('#canceltour').on('click', endTour);
    $('#endtour').on('click', endTour);
    $('#restarttour').on('click', restartTour);
    $('#nextstep').on('click', nextStep);
    $('#prevstep').on('click', prevStep);
    // Click to start training in first screen
    $('#confirm-trainee').on('click', traineeIsSelected);
    // Click to start training in engraving modal
    $('#printButton').on('click', engravingHasStarted);


    function startTour(Rconfig) {
        total_steps = Rconfig.length;
        config = Rconfig;
        $('#activatetour').remove();
        //$('#endtour,#restarttour').show();
        if(!autoplay && total_steps > 1)
            $('#nextstep').show();
        showOverlay();
        nextStep();
    }

    function nextStep() {
        if(!autoplay) {
            if(step > 0)
                $('#prevstep').show();
            else
                $('#prevstep').hide();
            if(step == total_steps - 1) {
                $('#nextstep').hide();
                $('#endtour').show();
            } else {
                $('#nextstep').show();
            }
        }
        if(step >= total_steps) {
            //if last step then end tour
            endTour();
            return false;
        }
        ++step;
        showTooltip();
    }

    function prevStep() {
        if(!autoplay) {
            if(step > 2)
                $('#prevstep').show();
            else
                $('#prevstep').hide();
            if(step == total_steps) {
                $('#nextstep').show();
                $('#endtour').hide();
            }
        }
        if(step <= 1)
            return false;
        --step;
        showTooltip();
    }

    function endTour() {
        step = 0;
        if(autoplay) clearTimeout(showtime);
        removeTooltip();
        hideControls();
        hideOverlay();
    }

    function restartTour(this_step) {
        if(typeof(this_step) == "number"){
            step = this_step
        } else {
            step = 0
        }
        if(autoplay) clearTimeout(showtime);
        nextStep();
    }

    function showTooltip() {
        //remove current tooltip
        removeTooltip();

        var step_config = config[step - 1];
        var $elem = $('.' + step_config.name);

        if(step_config.onLoad) {
            eval(step_config.onLoad)
        }


        if(autoplay)
            showtime = setTimeout(nextStep, step_config.time);

        var bgcolor = step_config.bgcolor;
        var color = step_config.color;

        var $tooltip = $('<div>', {
            id: 'tour_tooltip',
            className: 'tooltip',
            class: 'tooltip',
            html: '<p>' + step_config.text + '</p><span class="tooltip_arrow"></span>'
        }).css({
            'display': 'none',
            'background-color': bgcolor,
            'color': color
        });

        //position the tooltip correctly:

        //the css properties the tooltip should have
        var properties = {};

        var tip_position = step_config.position;

        //append the tooltip but hide it
        $('BODY').prepend($tooltip);

        //get some info of the element
        var e_w = $elem.outerWidth();
        var e_h = $elem.outerHeight();
        var e_l = $elem.offset().left;
        var e_t = $elem.offset().top;


        switch (tip_position) {
            case 'TL':
                properties = {
                    'left': e_l,
                    'top': e_t + e_h + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_TL');
                break;
            case 'TR':
                properties = {
                    'left': e_l + e_w - $tooltip.width() + 'px',
                    'top': e_t + e_h + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_TR');
                break;
            case 'BL':
                properties = {
                    'left': e_l + 'px',
                    'top': e_t - $tooltip.height() + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_BL');
                break;
            case 'BR':
                properties = {
                    'left': e_l + e_w - $tooltip.width() + 'px',
                    'top': e_t - $tooltip.height() + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_BR');
                break;
            case 'LT':
                properties = {
                    'left': e_l + e_w + 'px',
                    'top': e_t + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_LT');
                break;
            case 'LB':
                properties = {
                    'left': e_l + e_w + 'px',
                    'top': e_t + e_h - $tooltip.height() + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_LB');
                break;
            case 'RT':
                properties = {
                    'left': e_l - $tooltip.width() + 'px',
                    'top': e_t + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_RT');
                break;
            case 'RB':
                properties = {
                    'left': e_l - $tooltip.width() + 'px',
                    'top': e_t + e_h - $tooltip.height() + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_RB');
                break;
            case 'T':
                properties = {
                    'left': e_l + e_w / 2 - $tooltip.width() / 2 + 'px',
                    'top': e_t + e_h + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_T');
                break;
            case 'R':
                properties = {
                    'left': e_l - $tooltip.width() + 'px',
                    'top': e_t + e_h / 2 - $tooltip.height() / 2 + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_R');
                break;
            case 'B':
                properties = {
                    'left': e_l + e_w / 2 - $tooltip.width() / 2 + 'px',
                    'top': e_t - $tooltip.height() + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_B');
                break;
            case 'L':
                properties = {
                    'left': e_l + e_w + 'px',
                    'top': e_t + e_h / 2 - $tooltip.height() / 2 + 'px'
                };
                $tooltip.find('span.tooltip_arrow').addClass('tooltip_arrow_L');
                break;
        }


        /*
        if the element is not in the viewport
        we scroll to it before displaying the tooltip
         */
        var w_t = $(window).scrollTop();
        var w_b = $(window).scrollTop() + $(window).height();
        //get the boundaries of the element + tooltip
        var b_t = parseFloat(properties.top, 10);

        if(e_t < b_t)
            b_t = e_t;

        var b_b = parseFloat(properties.top, 10) + $tooltip.height();
        if((e_t + e_h) > b_b)
            b_b = e_t + e_h;


        if((b_t < w_t || b_t > w_b) || (b_b < w_t || b_b > w_b)) {
            $('html, body').stop()
                .animate({
                    scrollTop: b_t
                }, 500, 'easeInOutExpo', function() {
                    //need to reset the timeout because of the animation delay
                    if(autoplay) {
                        clearTimeout(showtime);
                        showtime = setTimeout(nextStep, step_config.time);
                    }
                    //show the new tooltip
                    $tooltip.css(properties).show();
                });
        }
        else
            //show the new tooltip
            $tooltip.css(properties).show();
    }

    function removeTooltip() {
        $('#tour_tooltip').remove();
    }

    function hideControls() {
        // We change remove to hide to show the steps more than once per screen
        //$('#tourcontrols').remove();
        $('#tourcontrols').hide();
    }

    function showOverlay() {
        var $overlay = '<div id="tour_overlay" class="overlay"></div>';
        $('BODY').prepend($overlay);
    }

    function hideOverlay() {
        $('#tour_overlay').remove();
    }

    function traineeIsSelected(){
		$('#tourcontrols').show();
	    getEngravingTutorialConfig(window.location.pathname);
    }

    function engravingHasStarted(){
        // We only launch the training steps if there is a trainee
	    if($('input[name$="traineeID"]').val()){
	        console.log("Starting tour for engravingHasStarted")
		    getEngravingTutorialConfig("engravingModal", callbackTutorialEngraving);
        }
    }

    function callbackTutorialEngraving(){
    console.log("call back--------------------")
		$('#tourcontrols').show();
        $('#endtour').hide();
		//restartTour(-1);
    }

    // For screen2 and screen3 we start in the begining if there is a traineeID
    if(!/screen1/i.test(location.href) && !/training/i.test(location.href) && $('input[name$="traineeID"]').val() !== ""){
	    traineeIsSelected();
    }

});


function repeatTraining(){
    var selectBox = document.getElementById("training-selector");
    var option = selectBox.options[selectBox.selectedIndex].value;
    if(option === "engraving_training"){
	    location.href = location.origin + "/screen1?do_engraving_training=true"
    } else if(option === "general_training"){
        location.href = location.origin + "/training"
    }
}


