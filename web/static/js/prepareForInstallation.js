

$( document ).ready($(function () {
    $('.add').on('click',function(){
        var input_id = "#" + $(this).attr("id").split("_add")[0]
        var $qty=$(input_id);
        var val = parseFloat($qty.attr("dif"))
        var currentVal = parseFloat($qty.val());
        var fixed = parseInt($qty.attr("fixed"));
        if (!isNaN(currentVal)) {
            $qty.val((currentVal + val).toFixed(fixed));
        }
    });
    $('.minus').on('click',function(){
        var input_id = "#" + $(this).attr("id").split("_minus")[0]
        var $qty=$(input_id);
        var val = parseFloat($qty.attr("dif"))
        var currentVal = parseFloat($qty.val());
        var fixed = parseInt($qty.attr("fixed"));
        if (!isNaN(currentVal) && currentVal > 0) {
            $qty.val((currentVal - val).toFixed(fixed));
        }
    });
	}));