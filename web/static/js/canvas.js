/**************************** CANVAS KONTROL ************************************************/
// http://anthonyterrien.com/demo/kontrol/

function objectInCanvas(x0, x1, y0, y1, text, fontsize, canvasID, logoUrl){
    // Every object will have a rectangle that contains it, with 4 points (x0, y0), (x0, y1), (x1, y0) and (x1, y1)
    this.x0 = x0;
    this.x1 = x1;
    this.y0 = y0;
    this.y1 = y1;
    this.width = x1 - x0;
    this.height = y1 - y0;

    this.text = text;
    this.fontsize = fontsize;
    this.canvasID = canvasID;
    if(typeof logoUrl !== typeof undefined){
        this.logoUrl = logoUrl;
    }

}

var objInCanvas = [];

function generateBase64img(canvasID, inputName){
    // We turn the canvas into an image
    var canvas = document.getElementById(canvasID);
    var  c = canvas.getContext("2d");
    var img    = canvas.toDataURL("image/png");
    //document.body.innerHTML += '<a id="downloadImg" style="display:" href="'+img+'" download><img src="'+img+'" width="104" height="142"></a>';
    $('input[name='+inputName+']').val(img)
    return img;
}

function getLineSeparationValue(){
	var fontName = $('#engravingFontId').attr("value");
	return flsp[fontName] ? flsp[fontName] : flsp["default"]
}

function getTextBaseline(){
	return "middle";
}

function recenterText(){
    return 0;
}

function getNewYCenterValue(yCenter, yDeviation, canvasID){
	return (yCenter - yDeviation/2) * getScale(canvasID);
}

// Function to scale canvas to have a nice logo display
function getScale(canvasID){
	return 1;
}

function getTextAlignCanvas(){
	return "center";
}

function createImage(canvasID ,text, myfontSize, xCenter, yCenter, logoUrl){
	myfontSize = myfontSize * getScale(canvasID);
	lastLogo = undefined;
    var canvas = document.getElementById(canvasID);
    if(typeof canvas != "undefined" && canvas != null){
        if(typeof logoUrl != "undefined"){
            createLogo(canvasID, xCenter, yCenter, logoUrl, text, myfontSize);
        } else {
            var  c = canvas.getContext("2d");
            c.clearRect(0, 0, canvas.width, canvas.height);
            //c.font = myfontSize + 'px ' + engravingFontName + ', EmojisFont';
            c.font = myfontSize + 'px CustomEmojisFont, ' + engravingFontName + ', EmojisFont';
            
            // Rotation angle of the text
            var angle = getRotationAngle(canvasID, text);
            c.textAlign = getTextAlignCanvas(angle);
            if(typeof xCenter == "undefined" || Number.isNaN(xCenter)) xCenter = canvas.width/2 + canvas.width*recenterText();
            if(typeof yCenter == "undefined" || Number.isNaN(yCenter)) yCenter = canvas.height/2;
            var resText = text.split("\n");
            var xDeviation = ((resText.length - 1) * myfontSize * getLineSeparationValue()) * Math.sin(angle);
            var yDeviation = ((resText.length - 1) * myfontSize * getLineSeparationValue()) * Math.cos(angle);
            var newXCenter = xCenter - xDeviation/2;
            var newYCenter = getNewYCenterValue(yCenter, yDeviation, canvasID);
            var text_color_fill = $('input[name=textColor]').val();
            if(text_color_fill.indexOf(",") != -1){
                c.fillStyle = "rgba(" + text_color_fill + ")";
            } else {
                c.fillStyle = text_color_fill;
            }
            // textBaseline explained --> https://www.w3schools.com/tags/canvas_textbaseline.asp
            var aaaa = getTextBaseline();
            c.textBaseline = aaaa;
            c.translate( newXCenter, newYCenter );
            // Rotation of the canvas according to the rotation of the text
            c.rotate( -angle);
            for (var i=0;i<resText.length;i++){
                c.fillText(resText[i], 0, 0 + i * myfontSize * getLineSeparationValue());
            }
            c.rotate( angle);
            c.translate( -newXCenter, -newYCenter );
            var bbox = getBoundingBox(c, 0, 0, canvas.width, canvas.height);
            // This function draws the square of the object
            //drawBoundingBox(c, bbox);
            if(bbox){
                objectCreated = new objectInCanvas(bbox.left,bbox.right,bbox.top,bbox.bottom,text, myfontSize, canvasID)
            } else {
                objectCreated = new objectInCanvas(0,0,0,0,text, myfontSize, canvasID)
            }
            objInCanvas.push(objectCreated);

            // Draws the restriction rectangle on the canvas the one that will allow the text to move
            if(canvasID === "textCanvas" && typeof(canvas_restriction) !== "undefined" && $("#goForwardA").attr("hideRestrictionSquare") !== "true"){
                var c = document.getElementById("textCanvas");
                var ctx = c.getContext("2d");
                ctx.strokeStyle="#FF0000";
                ctx.rect(canvas_restriction[0], canvas_restriction[1], canvas_restriction[2], canvas_restriction[3]);
                ctx.stroke();
            }

            var img = canvas.toDataURL("image/png");
        }
    }
}

// We sned the canvas ID for some special case for GEP
function getRotationAngle(canvasID, text){
    if(isNaN(vertical_engraving)) vertical_engraving = 0;
    return degreeToRad(vertical_engraving)
}


// Prints a text on the invisible canvas with bigger and better quality
function createImageForPrinting(text, fontSize, x, y, forcedCanvasId, logoUrl){
    // canvasFactor is set on screen2.html
    canvasID = "printCanvas";
    if(typeof forcedCanvasId != "undefined") {
        canvasID = forcedCanvasId;
    }
    createImage(canvasID ,text, fontSize*canvasFactor, x*canvasFactor, y*canvasFactor, logoUrl);
}

// Prints a text on the canvas visible on screen
function createImageForDisplay(text, fontSize, x, y, forcedCanvasId, logoUrl){
    createImage("textCanvas" ,text, fontSize, x, y, logoUrl);
    createImageForPrinting(text, fontSize, x, y, forcedCanvasId, logoUrl);
}


function drawCanvasFromURL(imgURL, canvasID){
    var myCanvas = document.getElementById(canvasID);
    if(typeof myCanvas != "undefined" && myCanvas != null){
        var ctx = myCanvas.getContext('2d');
        var img = new Image;
        img.onload = function(){
          ctx.drawImage(img,0,0); // Or at whatever offset you like
        };
        img.src = imgURL;
        //console.log("drawCanvasFromURL = " + canvasID)
    }
}


function getBoundingBox(ctx, left, top, width, height) {
    var ret = {};

    // Get the pixel data from the canvas
    var data = ctx.getImageData(left, top, width, height).data;
    //console.log(data);
    var first = false;
    var last = false;
    var right = false;
    var left = false;
    var r = height;
    var w = 0;
    var c = 0;
    var d = 0;

    // 1. get bottom
    while(!last && r) {
        r--;
        for(c = 0; c < width; c++) {
            if(data[r * width * 4 + c * 4 + 3]) {
                last = r+1;
                ret.bottom = r+1;
                break;
            }
        }
    }

    // 2. get top
    r = 0;
    var checks = [];
    while(!first && r < last) {

        for(c = 0; c < width; c++) {
            if(data[r * width * 4 + c * 4 + 3]) {
                first = r-1;
                ret.top = r-1;
                ret.height = last - first - 1;
                break;
            }
        }
        r++;
    }

    // 3. get right
    c = width;
    while(!right && c) {
        c--;
        for(r = 0; r < height; r++) {
            if(data[r * width * 4 + c * 4 + 3]) {
                right = c+1;
                ret.right = c+1;
                break;
            }
        }
    }

    // 4. get left
    c = 0;
    while(!left && c < right) {

        for(r = 0; r < height; r++) {
            if(data[r * width * 4 + c * 4 + 3]) {
                left = c;
                ret.left = c;
                ret.width = right - left - 1;
                break;
            }
        }
        c++;

        // If we've got it then return the height
        if(left) {
        	return ret;
        }
    }

    // We screwed something up...  What do you expect from free code?
    return false;
}


function drawBoundingBox(ctx, bbox){
    ctx.strokeStyle="red";
    ctx.lineWidth=1;
    ctx.beginPath();

    // top
    ctx.moveTo(bbox.left+0.5, bbox.top + 0.5);
    ctx.lineTo(bbox.right+0.5 ,bbox.top + 0.5);
    ctx.stroke();

    // right
    ctx.lineTo(bbox.right+0.5 ,bbox.bottom + 0.5);
    ctx.stroke();

    // bottom
    ctx.lineTo(bbox.left+0.5, bbox.bottom + 0.5);
    ctx.stroke();

    // left
    ctx.lineTo(bbox.left+0.5, bbox.top + 0.5);
    ctx.stroke();
}

// Moves the object inside both canvas and manages the objects
function mouseDragging(eX, eY, forcedCanvasId, objectId, mouseIsDown){
    if(objectId != -1 && mouseIsDown){
        result = checkIfObjectInside(objInCanvas[objectId], eX, eY);
        x = result[0];
        y = result[1];
        // We "move" the object to the mouse location
        // Create a new element with the new position obtained from the mouse
        texte = objInCanvas[objectId].text;
        logoUrl = objInCanvas[objectId].logoUrl;
        // TODO uncomment slider
        //fontsize=SliderFontSize(x,y,objectId);//we get the fontsize from the value of  the slider range
        //fontsize = objInCanvas[objectId].fontSize;// if we want disable slide range discoment this line and comment the previous line
        // Removing the element we are draggin from the objects in DISPLAY canvas
        removeElementsWhenDragging(objectId);
        createImageForDisplay(texte, fontsize, x, y, forcedCanvasId, logoUrl);
    }
}


var lastMove = null;
var objectSelectedId = -1;
function bindEventsToCanvas(){

    if (typeof $("input[name=base64img]").val() != "undefined"){
        drawCanvasFromURL($("input[name=base64img]").val(), "textCanvas");
        //drawCanvasFromURL($("input[name=base64img]").val(), "printCanvas");
    }
    var mouseIsDown = false;
    if(typeof(canvas_restriction) === "undefined"){
        $("#textCanvas").css({'border':'1px solid #ef0202'});
    }


    $("#textCanvas").bind("mousedown", function (e) {
            e.preventDefault();
            mouseIsDown = true;
            objectSelectedId = checkIfMouseIsOnObject(e.offsetX, e.offsetY, "textCanvas");
        }
    ).bind("mousemove", function (e) {
            e.preventDefault();
            lastMove = e;
            // We send a fake canvas ID, so the printing canvas is not refreshed all the time
            mouseDragging(e.offsetX, e.offsetY, "mouse move", objectSelectedId, mouseIsDown);
        }
    ).bind("mouseup", function (e) {
            e.preventDefault();
            lastMove = e;
            mouseDragging(e.offsetX, e.offsetY, "printCanvas", objectSelectedId, mouseIsDown);
            mouseIsDown = false;
        }
    ).bind("mouseover", function (e) {
            //e.preventDefault();
        }
    ).bind("mouseout", function (e) {
            e.preventDefault();
            mouseDragging(e.offsetX, e.offsetY, "printCanvas", objectSelectedId, mouseIsDown);
            mouseIsDown = false;
        }
    ).bind("touchend", function (e) { // mouseup
            e.preventDefault();
            var rect = lastMove.target.getBoundingClientRect();
            var xPos = lastMove.targetTouches[0].pageX - rect.left;
            var yPox = lastMove.targetTouches[0].pageY - rect.top;
            mouseDragging(xPos, yPox, "printCanvas", objectSelectedId, mouseIsDown);
            mouseIsDown = false;
        }
    ).bind("touchstart", function (e) { // mousedown
            e.preventDefault();
            mouseIsDown = true;
            lastMove = e;
            // For touch screen the click position is calculated differently
            var rect = e.target.getBoundingClientRect();
            var xPos = e.targetTouches[0].pageX - rect.left;
            var yPox = e.targetTouches[0].pageY - rect.top;
            objectSelectedId = checkIfMouseIsOnObject(xPos, yPox, "textCanvas");
        }
    ).bind("touchmove", function (e) { // mousemove
            e.preventDefault();
            lastMove = e;
            var rect = e.target.getBoundingClientRect();
            var xPos = e.targetTouches[0].pageX - rect.left;
            var yPox = e.targetTouches[0].pageY - rect.top;
            // We send a fake canvas ID, so the printing canvas is not refreshed all the time
            mouseDragging(xPos, yPox, "mouse move", objectSelectedId, mouseIsDown);
        }
    );
}

// Removes 2 object from the array of objects when DRAGGING, the one selected and the equivalent in the printing canvas
function removeElementsWhenDragging(objectSelectedId){
    elemText = objInCanvas[objectSelectedId].text;
    objInCanvas.splice(objectSelectedId,1);
    for(var i = 0; i < objInCanvas.length; i++) {
        if (objInCanvas[i].text == elemText) {
            objInCanvas.splice(i,1);
            break;
        }
    }
}

// Removes 2 objects from the arra of objects when writing a NEW TEXT, the one selected and the equivalent in the printing canvas
function removeElementsWhenWriting(previousText){
    if(typeof previousText != "undefined"){
        var objInCanvasFilter = objInCanvas.filter(function(value, index, arr){
            return objInCanvas[index].text != previousText;
        });
        objInCanvas = objInCanvasFilter;
    }
}

// Returns true if the mouse is inside and object of the current canvas
function checkIfMouseIsOnObject(mouseX, mouseY, canvasID){
    var foundPosition = -1;
    var isMouseInsideObject = false;
    for(var i = 0; i < objInCanvas.length; i++) {
        if (objInCanvas[i].canvasID == canvasID) {
            isMouseInsideObjectX = mouseX >= objInCanvas[i].x0 && mouseX <= objInCanvas[i].x1;
            isMouseInsideObjectY = mouseY >= objInCanvas[i].y0 && mouseY <= objInCanvas[i].y1;
            isMouseInsideObject = isMouseInsideObjectX && isMouseInsideObjectY
            if(isMouseInsideObject) foundPosition = i;
            break;
        }
    }
    return foundPosition;
}

// Given an object, checks its bounding box and returns true if the box is inside the canvas
function checkIfObjectInside(draggedObject, x, y){
    var canvasWidth = document.getElementById("textCanvas").width;
    var canvasHeight = document.getElementById("textCanvas").height;

    if(typeof(canvas_restriction) === "undefined"){
        // Igual que antes
        x0_restriction = 0;
        y0_restriction = 0;
        x1_restriction = canvasWidth;
        y1_restriction = canvasHeight;
    } else{
        // canvas_restriction manda
        x0_restriction = canvas_restriction[0];
        y0_restriction = canvas_restriction[1];
        x1_restriction = canvas_restriction[0] + canvas_restriction[2];
        y1_restriction = canvas_restriction[1] + canvas_restriction[3];
    }

    x0Border = x - draggedObject.width/2;
    x1Border = x + draggedObject.width/2;
    // alphabetic
    y0Border = y - draggedObject.height/2;
    y1Border = y + draggedObject.height/2;
    if(x0Border < x0_restriction){
        x = x0_restriction + draggedObject.width/2;
    }
    if(y0Border < y0_restriction){
        y = y0_restriction + draggedObject.height/2;
    }
    if(x1Border > x1_restriction){
        x = x1_restriction - draggedObject.width/2;
    }
    if(y1Border > y1_restriction){
        y = y1_restriction - draggedObject.height/2;
    }
    return [x, y];
}

// function returns new x an d y (position) of the object if the font change a lot
function checkResizableFontObjectInside(draggedObject,lastWidth,lastHeight,newWidth,newHeight,x, y){
      var diffx= Math.abs(newWidth-lastWidth);
      var diffy= Math.abs(newHeight-lastHeight);
      var canvasWidth = document.getElementById("textCanvas").width;
      var canvasHeight = document.getElementById("textCanvas").height;
      x0Border = x - draggedObject.width/2;
      x1Border = x + draggedObject.width/2;
      y0Border = y - draggedObject.height/2;
      y1Border = y + draggedObject.height/2;
    if(x1Border >= canvasWidth){
        x = x-diffx-5 ;
    }
    if(x0Border <= 0){
        x = x+diffx+5 ;
    }
    if(y0Border <= 0){
        y=y+diffy+5;
     }
    if(y1Border >= canvasHeight){
        y=y-diffy-5;
    }
    return [x,y];
}




function createLogo(canvasID, xCenter, yCenter, logoUrl, logoText, myfontSize=""){
    var canvas = document.getElementById(canvasID);
    if(typeof xCenter == "undefined" || Number.isNaN(xCenter)) xCenter = canvas.width/2;
    if(typeof yCenter == "undefined" || Number.isNaN(yCenter)) yCenter = canvas.height/2;
    base_image = new Image();
    //base_image.src = 'http://localhost:5000' + logoUrl;
    base_image.src = logoUrl;
    base_image.onload = function(){
        var c = canvas.getContext("2d");
        c.clearRect(0, 0, canvas.width, canvas.height);
        var proportion = base_image.naturalWidth / base_image.naturalHeight;
        // TODO sizeOfImage can be changed
        var sizeOfImage = 0.4;
        var desiredHeight = canvas.height * sizeOfImage;
        var desiredWidth = desiredHeight * proportion;
        var imgX0 = xCenter - (desiredWidth/2);
        var imgY0 = yCenter - (desiredHeight/2);
        //c.textAlign = "center";
        c.drawImage(base_image, imgX0, imgY0, desiredWidth, desiredHeight);
        var bbox = getBoundingBox(c, 0, 0, canvas.width, canvas.height);
        // This function draws the square of the object
        //drawBoundingBox(c, bbox);

        objectCreated = new objectInCanvas(bbox.left,bbox.right,bbox.top,bbox.bottom,logoText , 0, canvasID, logoUrl)
        objInCanvas.push(objectCreated);

        // Draws the restriction rectangle on the canvas the one that will allow the text to move
        if(canvasID === "textCanvas" && typeof(canvas_restriction) !== "undefined" && $("#goForwardA").attr("hideRestrictionSquare") !== "true"){
            var c = document.getElementById("textCanvas");
            var ctx = c.getContext("2d");
            ctx.strokeStyle="red";
            ctx.rect(canvas_restriction[0], canvas_restriction[1], canvas_restriction[2], canvas_restriction[3]);
            ctx.stroke();
        }

        var img = canvas.toDataURL("image/png");
    }
}


/************************************   SIGNATURE ENGRAVING    *******************************************/
var mousePressed = false;
var lastX, lastY;
var lastX1, lastY1;
var lastX2, lastY2;
var ctxSignature;
var drawingDone = false;

function prepareSignatureEngraving(){
    setTimeout(function(){
        InitSignatureCanvas();
    }, 1000);
}

// Returns the dimensions and location of the restricted engravable area
function getRestrictedArea(){
    // X0, Y0, xlen, ylen
    var maxHeightPX = maxHeightMM/vertical_rapport;
    var maxWidthPX = maxWidthMM/vertical_rapport;
    var textCanvasWidth = document.getElementById("textCanvas").width;
    var XOffsetPX = XOffset/vertical_rapport;
    var restricted_area = [textCanvasWidth/2 - maxWidthPX/2 + XOffsetPX, default_height - maxHeightPX/2, maxWidthPX, maxHeightPX];
    return restricted_area;
}

// Calculates how big can the signature engraving be to mantain proportions and fit in the tablet
function getSignatureCanvasFactor(){
    var restricted_area = getRestrictedArea();
    var windowWTimes = $(window).width()/restricted_area[2];
    var windowHTimes = $(window).height()/restricted_area[3];
    return Math.min(windowWTimes, windowHTimes)*0.75;
}

// Returns the proportion between two canvas sizes
function getCanvasFactor(id1, id2){
    var canvas1 = document.getElementById(id1);
    var canvas2 = document.getElementById(id2);
    var factor = canvas1.width/canvas2.width;
    return factor;
}

// Checks if the restricted area fits inside the engravable area
function checkIfRestrictedAreaFits(restricted_area){
    var canvas = document.getElementById("textCanvas");
    if(restricted_area[0] < 0) console.warn("Restricted area goes out of engraving area in LEFT");
    if(restricted_area[1] < 0) console.warn("Restricted area goes out of engraving area in TOP");
    if(restricted_area[0] + restricted_area[2] > canvas.width) console.warn("Restricted area goes out of engraving area in RIGHT");
    if(restricted_area[1] + restricted_area[3] > canvas.height) console.warn("Restricted area goes out of engraving area in BOTTOM");
    if(restricted_area[2] > canvas.width) console.warn("Restricted area is TOO WIDE");
    if(restricted_area[3] > canvas.height) console.warn("Restricted area is TOO HIGH");
}

// Starts the canvas and sets all the events for the drawing
function InitSignatureCanvas() {
    var signatureCanvasID = "signatureCanvas";
    var signatureCanvasIDJQ = "#" + signatureCanvasID;
    var $signatureCanvas = $(signatureCanvasIDJQ);
    canvas = document.getElementById(signatureCanvasID);
    ctxSignature = canvas.getContext("2d");

    // Setting the dimensions of the signature canvas
    var restricted_area = getRestrictedArea();

    checkIfRestrictedAreaFits(restricted_area);

    canvas.width = restricted_area[2]*getSignatureCanvasFactor();
    canvas.height = restricted_area[3]*getSignatureCanvasFactor();

    /**************   START   **************/
    $signatureCanvas.mousedown(function (e) {
        mousePressed = true;
        Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, false);
    });
    $signatureCanvas.bind("touchstart", function (e) {
        //console.log("touchstart")
        e.preventDefault();
        //console.log("e:", e);
        var rect = e.target.getBoundingClientRect();
        //console.log("rect:", rect)
        var xPos = e.originalEvent.targetTouches[0].pageX - rect.left;
        var yPox = e.originalEvent.targetTouches[0].pageY - rect.top;
        mousePressed = true;
        //Draw(xPos - $(this).offset().left, yPox - $(this).offset().top, false);
        Draw(xPos, yPox, false);
    });

    /**************   MOVE   **************/
    $signatureCanvas.mousemove(function (e) {
        if (mousePressed) {
            Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, true);
        }
    });
    $signatureCanvas.bind("touchmove", function (e) {
        //console.log("touchmove")
        e.preventDefault();
        // We cant go out of the canvas
        var maxX = e.currentTarget.width;
        var maxY = e.currentTarget.height;

        var rect = e.target.getBoundingClientRect();
        var xPos = e.originalEvent.targetTouches[0].pageX - rect.left;
        var yPos = e.originalEvent.targetTouches[0].pageY - rect.top;

        var drawIsInside = xPos < maxX && xPos > 0 && yPos < maxY && yPos > 0;
        if (mousePressed && drawIsInside) {
            //Draw(xPos - $(this).offset().left, yPos - $(this).offset().top, true);
            Draw(xPos, yPos, true);
        }
    });

    /**************   UP   **************/
    $signatureCanvas.mouseup(function (e) {
        mousePressed = false;
    });
    $signatureCanvas.bind("touchend", function (e) {
        //console.log("touchend")
        mousePressed = false;
    });

    /**************   LEAVE   **************/
    $signatureCanvas.mouseleave(function (e) {
        mousePressed = false;
    });
    $signatureCanvas.bind("touchend", function (e) {
        //console.log("touchend 2")
        mousePressed = false;
    });
}


// Draws a line following the user's clicks/touches
function Draw(x, y, isDown) {
    if (isDown) {
        drawingDone = true;
        // We need to draw in three canvas:
        // - signatureCanvas --> x, y are this coordinates
        var ctxSignature = document.getElementById("signatureCanvas").getContext("2d");
        // - textCanvas --> 1
        var ctxTextCanvas = document.getElementById("textCanvas").getContext("2d");
        // - printCanvas --> 2
        var ctxPrintCanvas = document.getElementById("printCanvas").getContext("2d");
        var x1, y1, x2, y2;
        var lineWidth = $('#selWidth').val();

        // First we draw in the signature canvas
        ctxSignature.beginPath();
        ctxSignature.strokeStyle = "black";
        ctxSignature.lineWidth = lineWidth;
        ctxSignature.lineJoin = "round";
        ctxSignature.moveTo(lastX, lastY);
        ctxSignature.lineTo(x, y);
        ctxSignature.closePath();
        ctxSignature.stroke();

        // Now text canvas
        // We change coordinates to the text canvas
        restricted_area = getRestrictedArea();
        canvasFactor = getSignatureCanvasFactor();
        x1 = restricted_area[0] + x/canvasFactor;
        y1 = restricted_area[1] + y/canvasFactor;
        // Now we draw in the text canvas
        ctxTextCanvas.beginPath();
        ctxTextCanvas.strokeStyle = "black";
        ctxTextCanvas.lineWidth = lineWidth/canvasFactor;
        ctxTextCanvas.lineJoin = "round";
        ctxTextCanvas.moveTo(lastX1, lastY1);
        ctxTextCanvas.lineTo(x1, y1);
        ctxTextCanvas.closePath();
        ctxTextCanvas.stroke();

        // Now print canvas
        // We change coordinates to the print canvas
        canvasFactor = getCanvasFactor("textCanvas", "printCanvas");
        canvasFactor2 = getCanvasFactor("signatureCanvas", "printCanvas");
        x2 = x1/canvasFactor;
        y2 = y1/canvasFactor;
        // Finally we draw in the print canvas
        ctxPrintCanvas.beginPath();
        ctxPrintCanvas.strokeStyle = "black";
        ctxPrintCanvas.lineWidth = lineWidth/(canvasFactor2*1.5);
        ctxPrintCanvas.lineJoin = "round";
        ctxPrintCanvas.moveTo(lastX2, lastY2);
        ctxPrintCanvas.lineTo(x2, y2);
        ctxPrintCanvas.closePath();
        ctxPrintCanvas.stroke();
    }
    lastX = x; lastY = y;
    lastX1 = x1; lastY1 = y1;
    lastX2 = x2; lastY2 = y2;
}

// Clears the canvas to start drawing again
function clearSignatureCanvasArea(){
    var ctxSignature = document.getElementById("signatureCanvas").getContext("2d");
    var ctxTextCanvas = document.getElementById("textCanvas").getContext("2d");
    var ctxPrintCanvas = document.getElementById("printCanvas").getContext("2d");
    ctxSignature.clearRect(0, 0, ctxSignature.canvas.width, ctxSignature.canvas.height);
    ctxTextCanvas.clearRect(0, 0, ctxTextCanvas.canvas.width, ctxTextCanvas.canvas.height);
    ctxPrintCanvas.clearRect(0, 0, ctxPrintCanvas.canvas.width, ctxPrintCanvas.canvas.height);
    $("#textToEngrave").val("");
    $("#goForwardA").prop('disabled', true);
    drawingDone = false;
}

// Confirms and closes the modal pop up
function confirmSignature(){
    console.log("confirmSignature");
    $("#textToEngrave").val("temporal text");
    $("#goForwardA").prop('disabled', !drawingDone);
}
