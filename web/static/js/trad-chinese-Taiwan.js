var traditional_chinese_auto = {
	a:"阿啊吖嗄錒安愛暗案按",
	ai: "愛呆挨哀埃礙艾唉矮哎癌嗌噯噯嬡捱曖璦皚砹磑藹誒锿隘靄",
	an: "安暗案按岸俺埯庵揞桉氨犴胺醃諳銨鞍鵪黯",
	ang: "昂盎醃",
	ao: "奧澳傲熬凹嗷坳媼嶴廒懊拗敖燠獒翱聱螯襖遨鏊鏖驁鰲",
	b: "不本把部便被北並別比",
	ba: "把八吧巴伯罷爸拔霸壩叭扒岜捌杷湃灞疤笆粑耙芭茇菝萆跋鈀鈸靶魃鲃鮁鲌",
	bai: "白百伯敗拜擺柏佰唄捭掰稗薜鞴䙓",
	ban: "反辦半版般班板伴搬扮斑頒坂扳拌瓣瘢癍絆舨鈑阪",
	bang: "幫旁邦棒膀綁傍榜梆氆浜磅蒡蚌螃謗鎊",
	bao: "報保包寶抱暴炮薄爆胞飽剝堡鮑刨孢掊煲脬苞苴葆裒褒褓襤豹趵鉋雹鴇",
	be: "本被北備背奔悲輩杯臂",
	bei: "被北備背悲輩杯臂貝倍碑卑唄埤孛庳悖憊焙狽碚萆蓓蜚褙跋邶鐾鋇陂鞴鵯",
	ben: "本奔笨坌夯畚苯賁錛唪",
	beng: "唪嘣堋崩抨泵甏甭繃繃蚌蹦迸鏰",
	bi: "比必筆畢秘避臂幣壁閉逼鼻彼碧闢拂斃鄙蔽弊俾匕吡嗶埤妣婢嬖庇庳弼愎敝枇蘗殍毖泌滗濞狴璧畀痺睥瞥秕篳箅篦紕舭芘蓽荸菝萆蓖薜蘗蚍裨襞诐賁贔跛蹕鐾鉍陂陛陴馥髀鲾",
	bia: "便表邊變編標遍彪辯鞭",
	bian: "便邊變編遍辯鞭辨辮扁匾卞弁忭汴煸砭碥稹窆籩緶芐蝙褊貶鯿",
	biao: "表標彪鏢剽婊嫖杓灬焱瘭膘苞裱鏖鑣颮飆飚骉驃髟鰾",
	bie: "別憋癟蹩鱉",
	bin: "賓彬濱斌儐擯殯浜瀕玢禀繽臏豳鑌髕鬢",
	bing: "並兵病冰柄屏餅丙冫廩摒枋檳炳燹禀秉迸邴",
	bo: "白百般波伯博播薄勃撥柏剝玻脖泊卜駁搏魄亳佰啵孛帛悖擘蘗濼渤溥礴箔簸簿膊舶艴荸菔菠蕃薜蘗袯趵跛踣缽鈸鉑鎛雹餑餺鮁鲌鵓",
	bu: "不部步布補捕堡怖卜卟卩哺埔埠惚晡溥瓿簿逋醭钚鈽鞴",
	c: "出成從長此重次產處才",
	ca: "擦拆嚓礤才參採藏草菜",
	cai: "才採菜財材彩裁猜蔡踩睬䌽",
	can: "參殘餐慘燦孱慚摻昝璨粲蠶驂黪",
	cang: "藏蒼倉艙傖滄臧鸧",
	cao: "草操曹嘈屮槽漕澡糙艚螬",
	ce: "策測側冊廁惻柵赦䇲曾",
	cen: "參岑涔曾層噌繒蹭",
	ceng: "曾層噌繒蹭",
	ch: "出成長重產處場車常吃",
	cha: "查差察茶插剎叉吒吒喳奼岔搋搽杈楂槎檫汊痄碴苴茬荼衩詫鍤镲馇",
	chai: "差柴拆儕瘥茈蠆豺釵",
	chan: "產單顫禪纏闡囅剷嘽嬋孱廛懺撣摻攙滻潺澶羼苫蕆蟬蟾覘諂讒躔鏟鑱饞骣",
	chang: "長場常廠唱嘗償昌暢腸倡敞倀娼嫦徜悵惝昶氅猖瑒萇菖裳錩閶鬯鯧鲿",
	chao: "朝超潮吵抄炒嘲綽剿巢怊晁焯耖鈔",
	che: "車徹尺撤扯坼屮掣澈硨轍",
	chen: "稱陳沉晨塵沈臣辰趁襯琛嗔宸忱抻櫬橙湛疹眈磣秤肜胂諶讖郴齔",
	cheng: "成城程稱承槍乘盛誠撐呈懲澄丞傖噌埕塍晟棖檉樘橙湞瞠秤蟶裎赬逞郢酲鋮鐺騁",
	chi: "吃持遲赤尺池馳恥痴齒斥嗤匙侈傺叱哆哧啻坻墀媸弛彳搋敕沱熾瘛眙眵笞篪翅胝芪茌茬蚩螭褫豉踅踟郗飭魑䗖",
	cho: "重衝充抽仇崇愁醜臭籌",
	chon: "重衝充崇蟲寵忡憧潼盅",
	chong: "重衝充崇蟲寵忡憧潼盅舂艟繭茺酮銃",
	chou: "抽仇愁醜臭籌酬綢瞅儔幬惆搐燾疇瘳稠躊讎䌷",
	chu: "出處初除楚礎觸儲廚畜亍芻怵憷搐杵楮樗櫥滁矗硫絮絀蜍褚躕鋤雛黜",
	chua: "傳創穿船窗床川串闖喘",
	chuai: "嘬揣揣搋踹蹉",
	chuan: "傳穿船川串喘巛椽氚舛舡遄釧",
	chuang: "創窗床闖幢囪愴瘡舂",
	chui: "吹垂錘捶棰椎槌炊陲",
	chun: "春純唇淳蠢椿沌肫蓴醇鶉䞐",
	chuo: "綽啜戳淖焯簇綴荃蔟踔踱躇輟醛齪",
	ci: "此次差刺詞辭慈賜磁瓷茲伺嵯恣疵祠粢糍茈茨蠔螅訾趑雌鶿",
	co: "從匆聰叢湊蔥囪揍樅楱",
	con: "從匆聰叢蔥囪樅淙琮璁",
	cong: "從匆聰叢蔥囪樅淙琮璁蓯驄",
	cou: "湊揍楱簇腠蔟輳",
	cu: "促粗醋卒徂槭殂猝簇蔟蹙蹴酢",
	cua: "竄攛汆爨篡躥鑹",
	cuan: "竄攛汆爨篡躥鑹",
	cui: "脆翠崔衰催粹摧啐悴榱橇毳淬璀瘁縗萃隹",
	cun: "存村寸忖浚皴",
	cuo: "錯差措挫厝嵯搓撮痤瘥矬磋脞蹉銼鹺",
	d: "的大到地道得對多都當",
	da: "大打達答塔搭噠妲怛沓疸瘩瘩笪耷荙褡躂酇靼韃",
	dai: "大代帶待呆袋戴貸逮歹傣呔埭岱怠棣殆玳甙紿轪迨逯黛",
	dan: "但單石擔彈丹淡膽旦蛋誕儋啖憚撣簷殫氮湛澶澹疸癉癉眈簞耽聃羶萏蜒詹贍鄲鉭",
	dang: "當黨盪擋檔氹垱宕瑒璫碭簹菪襠讜鐺",
	dao: "到道倒導刀島盜稻蹈悼搗叨刂幬忉檮氘洮禱纛鱽",
	de: "的地得德锝陟等登鄧",
	dei: "得",
	den: "等登鄧燈瞪凳澄噔嶝戥",
	deng: "等登鄧燈瞪凳澄噔嶝戥橙眙磴簦蹬鐙",
	di: "的地第提弟底低敵帝適抵遞滴狄迪蒂堤笛嘀坻娣嫡柢棣氐滌睇砥碲糴締羝翟胝芍荻莜覿詆諦蹄轪邸鏑骶䗖",
	dia: "嗲點電調店掉典鳥雕殿",
	dian: "點電店典殿顛淀甸奠佃坫墊巔惦拈掂涎滇玷癜癲碘簟蜓踮鈿阽靛",
	diao: "調掉鳥雕釣吊凋刁叼碉稠窵莜蜩貂踔釕铞鯛",
	die: "爹跌疊蝶碟佚喋垤堞揲渫牒瓞窒絰耋褶諜蹀軼迭鰈鰨",
	din: "定頂訂丁釘盯鼎叮仃啶",
	ding: "定頂訂丁釘盯鼎叮仃啶汀玎町疔碇耵腚葶酊錠飣",
	diu: "丟铥",
	do: "都動東讀鬥洞懂冬豆抖",
	don: "動東洞懂冬董凍棟侗咚",
	dong: "動東洞懂冬董凍棟侗咚垌岽峒恫氡甬硐腖胴酮鶇",
	dou: "都讀鬥豆抖兜逗陡瀆痘竇窬篼蔸蚪逾餖",
	du: "都度讀獨毒督肚渡杜賭堵睹嘟妒櫝橐瀆牘犢碡竺篤纛芏蠹鍍阇髑黷㱩",
	dua: "段斷端短鍛椴煅簖緞踹",
	duan: "段斷端短鍛椴煅簖緞踹",
	dui: "對隊堆敦兌懟憝槌碓鐓",
	dun: "頓盾噸敦蹲囤墩沌燉盹砘礅豚躉遁鈍",
	duo: "多度奪躲朵舵墮嚲剁咄哆哚垛惰掇柁棰沱沲綞裰跺踱鐸鍺隋飿馱",
	e: "阿惡額俄餓哦鵝厄呃噩堊娥婀屙峨愕扼猗硪胺腭苊莪萼蛤蛾訛諤軛遏邑鄂鈳鍔閼隘顎鱷鶚",
	ei: "哎誒誒",
	en: "恩摁蒽",
	er: "而二兒爾耳佴洱濡珥貳邇鉺餌鮞鴯䌺",
	f: "發方分法反放風服非夫",
	fa: "發法乏罰伐垡琺砝筏閥",
	fan: "反飯翻犯範凡番煩繁返泛販帆幡梵樊燔畈礬蕃藩蘩蟠袢蹯釩",
	fang: "方放房訪防仿芳妨坊紡匚彷枋肪舫邡邡鈁魴鰟",
	fe: "分風非飛費份封紛峰豐",
	fei: "非飛費斐廢菲肥匪啡肺沸吠妃悱扉榧淝狒痱痱砩祓篚緋翡腓芾蜚裴誹賁鐨霏鯡",
	fen: "分份紛奮憤粉芬吩氛墳糞焚僨忿棼棼汾瀵燔玢豶賁酚鱝鼢",
	feng: "風封峰豐鋒鳳奉楓瘋逢縫馮蜂諷俸唪灃烽砜葑蚌賵逄酆",
	fo: "佛否缶",
	fou: "否缶",
	fu: "服夫父府复福副負婦富佛付附幅伏扶赴浮符腐腹咐撫覆傅弗膚芙俯拂俘賦甫縛輔敷鳧匐呋嘸孚孵宓幞怫拊掊斧桴氟涪溥滏砩祓稃紱紼罘脯腑艴芾苻茯莆莩菔蚨蜉蝠蝮袱訃賻趺跗輻郛釜阜阝鞴韨馥駙鮒鰒麩黻黼",
	g: "國個過公高工給間關感",
	ga: "界夾咖胳嘎噶尕尜尬旮軋钆骱",
	gai: "該改概蓋丐垓戤溉胲芥賅鈣閡陔骸",
	gan: "間感幹敢趕甘乾桿肝坩尷擀旰柑橄泔淦澉疳矸稈竿竿紺苷贛酐鳡",
	gang: "港剛鋼崗綱缸岡扛亢戇槓筻罡肛骯舡頏",
	gao: "高告稿搞糕膏咎杲桕槁槔皋睾篙縞羔蒿藁誥郜鋯鎬",
	ge: "個合各革格哥歌隔割閣葛戈擱胳仡咯哿嗝噶圪塥搿烙疙砝硌紇膈舸菏虼蛤袼鉻鎘闔頜骼髂鬲鴿",
	gei: "給胲",
	gen: "跟根哏艮茛更頸耕耿亙",
	geng: "更頸耕耿亙哽埂庚梗炅炔粳綆羹賡邢鯁鹒",
	go: "公工共功紅供夠構攻狗",
	gon: "公工共功紅供攻宮恭貢",
	gong: "公工共功紅供攻宮恭貢躬拱弓鞏嗊廾汞珙肱蚣蛩觥贛龔",
	gou: "夠構狗購溝勾鉤佝垢媾岣彀枸笱篝緱耩苟覯詬逅遘鞲骺",
	gu: "告古故姑顧股骨鼓谷固孤估賈僱辜咕嘏崮梏轂汩沽牯牿瓠痼皋瞽箍罟胍臌菇菰蛄蠱觚詁酤鈷鈷錮餶骰鯝鴣鵠鶻",
	gua: "掛瓜寡刮剮卦呱栝聒胍腡蝸褂詿鴰",
	guai: "怪乖拐",
	guan: "關管觀官館慣冠貫灌罐倌摜擐斡棺浣涫盥矜綸缶脘莞菅鰥鸛",
	guang: "光廣逛咣桄潢獷胱",
	gui: "規歸貴鬼桂跪櫃軌龜瑰傀劊劌匭匱圭媯宄庋撅晷檜窪炅炔癸皈眭瞶矽祈簋觖詭蹶閨隗鮭鱖",
	gun: "滾棍磙緄袞輥鯀",
	guo: "國過果郭鍋裹呙堝崞幗摑槨渦猓聒虢蜮蜾蟈蠃餜馘",
	h: "和會後好還行回話很海",
	ha: "哈蝦獬蛤鉿好還行海孩",
	hai: "還海孩害咳亥嗨氦胲醢頦駭骸",
	han: "漢喊含寒汗韓罕憾翰涵函旱撼悍憨捍撖擀旰晗泔澉瀚焊焓犴菡蚶邗邯酐酣閈頇頜頷鼾",
	hang: "行航杭巷吭夯桁沆炕絎骯酐頏鴴",
	hao: "好號毫豪浩耗嗥嚆嚎壕昊涸濠灝皋皓睾蒿薅蠔貉郝鎬顥",
	he: "和何合河喝核嚇赫荷賀盒呵鶴禾劾嗑嗬壑曷洽涸盍矽紇翮耠苛菏藿蚵蠍褐訶貉鉀閡闔頜餄鲄鶡齕䙓",
	hei: "黑嘿",
	hen: "很恨狠痕哏艮行橫哼衡",
	heng: "行橫哼衡恆亨桁珩絎蘅訇鴴黌",
	ho: "後紅候洪厚後轟鴻宏侯",
	hon: "紅洪轟鴻宏哄虹嗊弘泓",
	hong: "紅洪轟鴻宏哄虹嗊弘泓洚烘纮葒蕻薨訇訌閎黌",
	hou: "後候厚後侯喉吼猴堠灬瘊篌餱逅骺鱟鲘",
	hu: "和許乎胡戶護呼忽湖狐互核虎糊滬壺沍唬唿囫岵弧怙惚戽扈斛槲汩滸滹烀煳猢琥瑚瓠祜笏胍芋芴葫虍蝴觳轷酏醐鳠鵠鶘鶻鹱",
	hua: "話化華花劃畫滑嘩嫿樺獪猾砉稞豁踝鏵驊㟆",
	huai: "懷壞淮孬徊槐踝",
	huan: "還歡換環緩幻患喚圜垸奐宦寰擐桓洹浣渙漶煥獾瑗瘓皖眩繯脘莧萑豢逭郇鍰鐶闤鬟鯇",
	huang: "黃皇荒晃慌惶煌謊恍凰幌徨湟潢潢璜癀磺篁簧肓蝗蟥遑鍠隍鰉",
	hui: "會回揮匯灰輝惠慧毀悔恢繪徽潰賄諱卉咴噦喙彗徊恚悝戯暉晦檜洄燴琿皓眭穢繢茴薈蕙虺蛔蝰蟪褘詼誨鉞闠隳颒麾",
	hun: "婚混魂昏渾溷琿葷諢閽餛",
	huo: "和話活或火獲夥貨禍惑霍劐嚯壑夥攉瓠砉矽耠藿蠖豁钬锪鑊鳠鹱",
	j: "就家見經將進其己機給",
	ji: "其己機給幾期系計及記革即技基極際濟集級奇急紀擊既輯激寄繼積忌吉跡雞季騎疾籍擠寂績祭飢妓肌脊圾劑藉譏姬丌乩亟伎佶偈冀剞嘰咭嚌唧墼嫉屐岌嵇嵴彐悸戟戢掎揖暨棘楫殛汲洎犄猗璣畸畿疵瘠瘵睽瞿磯秸稷稽笄笈箕粢緝羈臠芨芰薺萁蒺薊蕺蟣覬齎躋跽郅钑霽驥鱾鱭鯽鶺麂齏",
	jia: "家加價假架甲佳夾駕嫁嘉賈頰稼伽嘏岬恝戛拮挾枷柙檟浹珈痂瘕笳胛茄莢葭蛺袈袷跏迦郟鉀鋏鉿鎵頡餄骱䇲",
	jian: "見間件建劍漸簡堅監健檢肩減尖兼姦箭艦艱鍵鑑剪踐薦賤撿揀煎儉僭囝戔戩搛梘柬楗檻殲毽沮澗湔湛濺牮犍犴瞼鹼鹼筧箋箴緘縑翦腱茛繭菅蒹襉謇諫譾譖趼踺蹇鈃鐧韉餞鰹鰜鶼",
	jiang: "將強江講獎降蔣疆虹匠姜僵醬漿槳洚犟礓糨絳韁耩茳螿襁豇鱂",
	jiao: "教叫覺交校腳較角焦轎嬌驕郊繳嚼膠攪澆絞佼僥僬剿噍姣嶠徼徼撟敫椒湫爝狡皎矯矯礁窖艽茭蕎菽蕉蛟跤酵醮鉸餃鮫鷦䴔",
	jie: "家界解接結價節姐街階介借屆傑截潔戒皆揭捷劫竭藉偈偕卩喈嗟婕孑廨拮櫛桀桔楷獬癤疥睫砝碣秸羯芥苴葜蚧袷訐詰誡鍇頡骱髻鮚鶡",
	jin: "進金今近盡緊僅禁勁津斤錦筋謹晉巾浸襟卺噤堇妗廑槿湛燼瑾矜縉肋藎衿覲贐钅靳饉",
	jing: "經京精竟驚境靜景警睛靖勁敬競淨鏡徑井晶頸儆兢剄婧弳憬旌晟檠涇烴獍痙箐粳肼脛腈莖荊菁蜻迳阱陘靚鯨黥䴖",
	jio: "垧扃炅炯窘迥颎䌹",
	jion: "垧扃炅炯窘迥颎䌹",
	jiong: "垧扃炅炯窘迥颎䌹",
	jiu: "就九究久酒救舊舅糾僦厩咎啾揪柩桕湫灸玖疚繆臼艽蝤赳赳鬮韭鬏鳩鷲",
	ju: "車據且局舉句具居劇巨聚距拒懼俱櫃矩拘菊倨咀屨掬枸桔椐櫸榘橘沮炬犋狙琚疽瞿窶苣苴莒菹蛆裾詎趄踞踽遽鄹醵鉅鋦鋸雎鞠鞫颶駒鬻鮍齟䴗",
	jua: "卷圈捐倦娟桊泫涓狷甄",
	juan: "卷圈捐倦娟桊泫涓狷甄眩眷絹蕊蜷蠲鄄錈鐫阮雋鵑䌸",
	jue: "覺決絕腳角爵掘嚼倔劂厥嗟噘噱孓崛抉撅攫檉桷梏橛爝獗珏矍蕞蕨蛙蠼觖觳訣譎蹶镢闕鱖",
	jun: "軍均君俊龜峻菌捃浚狻皸睃竣筠訇逡郡鈞雋馂駿鲪麇",
	k: "會可看開口科快空克客",
	ka: "卡刮咖喀佧咔咯胩髂看",
	kai: "開凱慨剴塏愾愷揩楷溘蒈鎧锎鍇闓雉",
	kan: "看刊砍堪坎侃勘戡檻瞰莰闞龕",
	kang: "康抗慷扛亢伉沆炕糠骯鈧閌",
	kao: "考靠烤尻拷栲槁犒銬鲓",
	ke: "可科克客刻課顆柯渴棵磕咳殼哿嗑坷岢恪氪溘珂疴盍瞌碣稞窠緙苛蚵蝌軻鈳鉿錁頦騍髁",
	kei: "克",
	ken: "肯懇啃墾裉頎龂齦坑吭",
	keng: "坑吭硜硎脛鏗",
	ko: "口空恐控孔扣佝倥叩寇",
	kon: "空恐控孔倥崆穹箜",
	kong: "空恐控孔倥崆穹箜",
	kou: "口扣佝叩寇摳挎筘芤蔻",
	ku: "苦哭庫褲酷枯刳嚳堀挎窟絝绹軲骷",
	kua: "跨誇垮侉挎胯錁髁會快",
	kuai: "會快塊筷儈呙噲栝檜澮獪膾蕢蒯鄶魁鱠㧟㱮",
	kuan: "款寬髖況狂礦曠框匡哐",
	kuang: "況狂礦曠框匡哐壙夼湟眶磺筐纊誆誆誑貺鄺",
	kui: "虧愧潰奎傀匱喟喹夔巋悝憒揆暌盔睽瞶窺簣聵臾葵蕢蝰觖跬逵隗頃饋馗騤魁㱮",
	kun: "困昆坤壼巛悃捆琨褌醌錕閫髡鯤鶤麇",
	kuo: "括擴闊廓栝蛞",
	l: "了來里老兩理力立路利",
	la: "落拉啦辣喇臘蠟剌垃摺旯瘌癩砬邋鑞",
	lai: "來厲賴萊俫崍徠梾淶瀨癩睞籟賚錸黧",
	lan: "蘭欄藍爛覽攔懶籃濫纜啉婪嵐廩攬斕欖漤瀾罱襤讕郴鑭闌㨫䍀",
	lang: "浪郎朗狼廊啷榔瑯稂羹莨蒗螂踉鋃閬閬",
	lao: "老落勞絡牢姥撈佬嘮嶗櫟栳澇潦潦烙獠癆耢蓼酪醪銠铹",
	le: "了樂勒仂叻捋泐肋餎鳓",
	lei: "類淚雷累勒壘儡嘞埒嫘擂擂檑漯磊縲羸耒肋蕾誄酹鐳",
	len: "冷愣塄棱楞",
	leng: "冷愣塄棱楞",
	li: "裡理力立利李歷離麗禮例厲勵黎璃哩莉粒隸梨栗俐俚儷傈厘吏嚦唳哩壢娌嫠悝捩櫪櫟瀝溧漓澧犛犁狸猁珞癘癧痢砬礪礫硌笠篥籬糲褵罹翮藶荔蒞蘺藜蠣蜊蠡詈躒轢邐酈釃醴鋰鎘靂霾驪髦鬲鱺鯉鱧鸝黧䲞",
	lia: "倆了兩連聯量臉料亮練",
	lian: "連聯臉練戀憐蓮煉廉簾鏈奩孌斂槤楝殮漣瀲濂璉瞵碾羸膦臁苓薟蘞蠊褳襝鐮鰱",
	liang: "兩量亮良糧輛梁涼倆諒唡墚晾椋粱莨踉辌閬魎",
	liao: "了料療聊遼僚寥嘹寮尥廖撂撩撩潦燎燎獠繆繚蓼釕鐐鷚鷯",
	lie: "列烈裂劣獵冽咧戾捩洌膊趔躐邋鬣䴕",
	lin: "林臨鄰琳淋凜吝啉嶙廩懍檁瞵磷禀粼膦藺賃躪轔遴霖鱗麟",
	ling: "領令另靈零齡凌玲鈴陵嶺伶呤呤囹拎柃櫺棱泠瓴磷綾羚翎聆苓菱蛉酃鯪鴒㻏",
	liu: "六流留陸劉柳溜碌旒榴泖泵瀏熘琉瘤硫綹蔞蓼遛鎏鉚锍鎦鏐飀餾騮鶹",
	lo: "咯龍樓露弄隆籠漏摟陋",
	lon: "龍弄隆籠攏朧聾壟嚨壟",
	long: "龍弄隆籠攏朧聾壟嚨壟曨櫳瀧瓏癃矓礱窿蘢隴",
	lou: "樓露漏摟陋僂嘍婁嶁瘺窶簍耬蔞螻鏤髏䁖",
	lü: "律率旅綠慮呂履縷侶僂屢嶁捋櫚氯濾瘺稆膂褸鋁閭驢",
	lu: "路陸露錄綠魯盧爐鹿碌蘆廬鹵嚕壚戮擄擼櫨櫓瀘淥漉潞璐瘳祿籙簏臚艫蓼虜賂轤輅轆逯酪镥顱鱸鸕鷺麓",
	lua: "亂卵孌孿巒攣欒灤臠鑾",
	luan: "亂卵孌孿巒攣欒灤臠鑾鸞",
	lüe: "略掠撂鋝",
	lun: "論輪倫淪崙圇掄綸",
	luo: "落羅絡洛邏駱裸蘿鑼倮捋摞欏橐氌濼漯烙猓玀珞瘰硌籮腡犖蔂蜾螺蠃蠡袼躒酪鉻鏍雒騾㑩䲞",
	lv : "呂驢呂郘侶挔捛垏閭律侶梠祣哷旅慮捋稆鋁率綠僂縷屢嵂絽葎氯濾僂櫚屢褸閭鋁膂緑慺慮箻膢膟氀履瞜褸膐縷勴繂儢櫚濾藘櫖爈穭卛鷜鑢驢",
	m: "們麼沒無面民美明名門",
	ma: "馬嗎媽罵麻摩碼嘛瑪抹嘜嬤榪獁祃螞螞蟆貉靡麽",
	mai: "買賣麥埋脈邁勱狸蕒霢霾",
	man: "滿慢漫埋曼蠻瞞墁幔熳縵蔓蟎謾蹣鏝鞔顢饅鰻",
	mang: "忙茫盲芒氓漭瞢硭莽蟒邙鋩",
	mao: "毛冒貿貌矛貓帽茅茂卯峁懋旄昴泖牟犛瑁瞀耄茆蝥蟊袤鉚錨髦",
	me: "麼麼末麽們沒美門每妹",
	mei: "沒美每妹梅眉媒枚煤謎媚黴昧玫寐嵋楣浼湄猸瑁糜莓袂酶鎂镅靡魅鶥",
	men: "們門悶懣捫汶滿燜鍆鞔",
	meng: "夢蒙猛盟孟朦氓勐懵檬甍瞑瞢礞艋艨苧萌虻蜢蟊蟒蠓錳鹲黽",
	mi: "米密秘迷彌蜜謎覓佴咪嘧宓冪弭敉汨泌溟狝獼瞇瞇祢糜糸縻脒羋蘼謐醚靡麋",
	mia: "面免妙描苗廟棉綿眠勉",
	mian: "面免棉綿眠勉緬冕娩沔泯澠湎眄瞑靦黽",
	miao: "妙描苗廟秒渺喵杪淼眇瞄緲繆藐蜱邈鈔鶓",
	mie: "滅蔑乜咩篾羋蠛",
	min: "民敏岷憫愍抿汶泯澠玟珉緡苠閔閩鳘黽",
	ming: "明名命鳴盟銘冥暝溟皿瞑茗萌螟酩",
	miu: "謬繆",
	mo: "麼沒無萬模莫默摸麼末摩磨魔脈漠墨抹陌寞沫膜嫫摹歿瘼秣耱茉驀藐蘑蟆襪謨貉貊貘鏌霢靡饃麽",
	mou: "某謀侔厶哞毋牟眸瞀繆蛑蝥袤鍪",
	mu: "目母木模幕慕墓姆姥穆牧畝沐募仫坶拇暮毪牟牡睦繆苜鉬鶩",
	n: "你年那能女內難南呢拿",
	na: "那南拿哪納吶捺箬絮肭衲訥鈉镎",
	nai: "奶乃耐奈佴柰氖艿萘鼐",
	nan: "難南男喃囝囡楠罱腩蝻赧",
	nang: "囊囔攮曩馕",
	nao: "腦鬧惱呶垴孬撓橈淖猱瑙硇鐃䜧",
	ne: "呢哪吶疔訥那能內嫩",
	nei: "那內哪餒",
	nen: "嫩能",
	neng: "能",
	ng: "嗯",
	ni: "你呢尼泥擬逆倪妮膩伲匿坭嶷怩慝旎暱猊睨祢鈮霓鯢鹝鷁",
	nia: "年念娘鳥尿廿釀粘埝嬲",
	nian: "年念廿粘埝廾拈拈捻攆碾蔫輦輾鮎鯰黏",
	niang: "娘釀",
	niao: "鳥尿嬲氽溺脲蔦裊",
	nie: "捏乜囓囁孽捻涅聶臬蘗躡鑷鎳隉顳䯅",
	nin: "您寧凝佞嚀擰攘檸濘獰",
	ning: "寧凝佞嚀擰擰攘檸濘獰甯聹苧",
	niu: "牛扭紐妞忸拗狃蚴鈕",
	no: "農弄濃儂噥穠耨膿㶶",
	non: "農弄濃儂噥穠膿㶶",
	nong: "農弄濃儂噥穠膿㶶",
	nou: "耨",
	nü: "女忸恧狃絮肭衄釹乇瘧",
	nu: "怒努奴孥帑弩胬褥駑那",
	nua: "暖濡",
	nuan: "暖濡",
	nüe: "乇瘧虐謔",
	nuo: "那諾娜挪儺喏懦搦濡砹糯锘",
	o: "哦喔噢區歐偶嘔慪毆漚",
	ou: "區歐偶嘔嘔慪毆漚甌眍禺耦藕謳鷗",
	p: "便被平品派片怕般破批",
	pa: "派怕爬帕啪趴扒杷琶筢耙芭葩鈀",
	pai: "派排拍牌迫俳哌徘湃蒎",
	pan: "般判盤番胖盼叛拚潘畔攀弁扳拌樊泮爿皤磐蟠袢襻蹣鄱",
	pang: "旁胖膀龐乓龎彷滂磅磅耪蒡螃逄鰟",
	pao: "跑炮拋袍泡刨匏咆庖狍皰脬苞趵齙",
	pe: "朋配培碰陪彭佩賠鵬盆",
	pei: "配培陪佩賠呸妃帔徘旆沛淠碚胚艴茇蜚裴轡邳醅锫霈",
	pen: "盆噴汾湓朋碰彭鵬捧棚",
	peng: "朋碰彭鵬捧棚蓬砰篷膨嘭堋怦抨澎烹甏硼蟛迸",
	pi: "被批否皮罷壞屁匹疲披脾闢劈啤僻譬丕仳噼圮坯埤媲庀擗枇毗淠濞琵甓疋痞痦癖睥砒篦紕羆芘苤萆蕃薜蚌蚍蜱螵裨貔邳郫鄱鈹陂陴霹鮍鼙䴙",
	pia: "便片票篇偏飄騙漂樸扁",
	pian: "便片篇偏騙扁犏緶翩胼蝙褊諞蹁駢",
	piao: "票飄漂樸剽剽嘌嫖殍瓢瞟瞟縹膘莩螵驃髟",
	pie: "撇氕癟瞥苤",
	pin: "品貧聘頻拼拚姘嬪榀泵牝顰",
	ping: "平評憑萍瓶馮屏蘋乒坪俜娉枰秤鮃",
	po: "破婆迫頗坡泊樸潑魄叵攴濼溥珀皤笸粕膊跛鄱醱釙钷陂",
	pou: "剖掊涪瓿裒踣锫",
	pu: "普暴撲鋪譜僕堡浦樸菩葡蒲瀑匍噗圃埔攴曝氆溥濮璞脯苻莆蹼醭镤镨",
	q: "去起前其全情氣卻期親",
	qi: "起其氣期吃七器奇企齊妻汽旗棋棄啟騎豈枝欺戚契淒歧漆泣乞迄亓亟伎俟偈嘁圻屺岐崎憩挈杞柒棲榿槭欹汔淇琦琪甭畦畸磧祁祈祺稽綦綺緝耆臍芑芪薺萁萋葺蘄蟣蠐蜞訖齎趿蹊錡頎騏鯕鰭麒",
	qia: "卡恰掐洽疴葜髂前強錢",
	qian: "前錢千簽潛牽淺遷乾遣欠歉謙纖鉛譴嵌仟僉倩塹岍慳愆慊扦掮搴撖柑槧涔犍箝繾肷腱芊芡茜蕁虔褰蹇釬鈐鉗鋟阡騫鹐黔",
	qiang: "強槍牆搶腔丬嗆嬙戕戧檣熗爿瑲箐羌羥薔蜣襁蹌跫錆鏘鏹鸧",
	qiao: "瞧橋悄喬巧敲僑殼雀翹俏劁嶠峭愀憔撬樵橇毳磽硝竅繰舄茭蕎蕉誚譙跤蹺醮鍬鞒鞘",
	qie: "且切契竊怯伽唼妾愜慊挈沏沏渫砌篋脞茄蕺趄鍥",
	qin: "親侵琴秦勤欽擒吣嗪噙寢嶔廑撳槿檎沁滲溱矜禽芩芹蓁螓衾衿覃鋟駸",
	qing: "情親清請青輕慶傾晴卿倩圊擎檠氫氰磬箐綮罄檾蜻謦頃鯖鯨黥䞍",
	qio: "窮瓊穹筇芎檾煢蛩跫邛",
	qion: "窮瓊穹筇芎檾煢蛩跫邛",
	qiong: "窮瓊穹筇芎檾煢蛩跫邛銎鞠",
	qiu: "求球秋仇丘龜囚瞅俅巰楸氽泅湫犰糗艽虯蚯蝤裘賕逑遒邱酋钆馗鰍鶖鼽",
	qu: "去區取曲趣屈驅趨娶渠軀凵劬嶇朐枸氍璩癯瞿磲祛絮苣蕖蘧蛆蛐蠼衢覷詘遽闃鞠鞫鴝麴黢齲",
	qua: "全權卷拳圈勸泉券悛桊",
	quan: "全權卷拳圈勸泉券悛桊犬獾畎痊筌綣荃蜷詮輇醛銓顴鬈鳈",
	que: "卻確缺雀愨攉榷炔瘸舭芍觳郄闋闕鵲",
	qun: "群裙蝽逡遁麇",
	r: "兒人然日如入任讓認",
	ra: "然讓染繞擾燃饒嚷壤冉",
	ran: "然染燃冉苒蚺髯讓嚷壤",
	rang: "讓嚷壤攘瓤禳穰",
	rao: "繞擾饒嬈橈蕘蟯",
	re: "若熱惹喏人兒任認仍忍",
	ren: "人兒任認忍仁刃仞壬妊恁稔紉紝荏葚衽讱賃軔韌飪䌾",
	reng: "仍扔穰艿",
	ri: "日驲",
	ro: "容肉榮蓉柔融揉絨熔溶",
	ron: "容榮蓉融絨熔溶冗嶸戎",
	rong: "容榮蓉融絨熔溶冗嶸戎榕狨肜茸蠑鎔",
	rou: "肉柔揉糅蹂鞣",
	ru: "如入辱儒乳汝嚅孺洳溽濡縟茹蓐薷褥襦銣顬",
	rua: "軟朊濡蠕阮",
	ruan: "軟朊濡蠕阮",
	rui: "瑞銳枘睿芮蕊蕤蚋䌼",
	run: "潤閏",
	ruo: "若弱偌箬芮",
	s: "是上說時生事手十所三",
	sa: "薩撒灑仨卅檫脎趿钑颯",
	sai: "思賽塞噻腮蓑鰓",
	san: "三散傘叁毿糝馓喪桑嗓",
	sang: "喪桑嗓搡磉顙",
	sao: "掃嫂騷埽搔瘙繅繰臊鳋",
	se: "色塞瑟圾嗇槭澀穡薔銫",
	sen: "森僧",
	seng: "僧",
	sh: "是上說時生事手十身實",
	sha: "殺沙傻莎廈剎啥紗煞杉唼嗄歃痧砂裟賒鎩霎鯊",
	shai: "色曬篩釃",
	shan: "山單善閃衫禪扇珊陝杉刪剡埏姍嬗彡撣摻擅柵檀汕潸澹煽疝繕膳羶舢芟苫蟮詹訕贍跚鄯釤騸髟鱔鱣",
	shang: "上商傷尚賞湯晌垧墒殤熵裳觴",
	shao: "少燒紹稍哨邵劭勺捎杓梢溲潲笤筲艄芍苕蛸鞘韶",
	she: "社設折射舍涉蛇拾攝舌佘厙奢懾揲歙灄猞畬睫蛞賒赦鉈阇麝䞌",
	shei: "誰",
	shen: "身什神深甚參伸申審沈慎紳吲呻哂娠嬸抻椹渖滲瘆矧砷腎胂莘葚蜃詵諗鰺",
	sheng: "生聲省勝升聖乘盛剩牲繩嵊晟澠甥眚笙",
	shi: "是時事十實什使世市師士式識始史失似石視示勢室食詩試施適釋氏屍侍拾駛濕飾逝誓殖獅匙蝕仕嗜噬塒屎峙弒弛恃拭柿炻矢礻筮耆舐蒔蓍蝨螫諡豉豕貰軾郝釃鈰鉈饣鰣鲺鳀鳲䴓",
	sho: "手受收首授守熟售瘦壽",
	shou: "手受收首授守熟售瘦壽獸狩綬艏",
	shu: "書數術樹屬輸熟述束叔舒殊署鼠疏俞豎暑抒梳蔬淑樞倏塾墅姝孰庶恕戍攄曙杼樗殳毹沭涑漱澍疋秫紓腧荼菽薯蜀蜍贖黍",
	shua: "刷耍唰涮雙率摔爽衰帥",
	shuai: "率摔衰帥甩縗蟀",
	shuan: "拴揎栓汕涮踹閂雙爽霜",
	shuang: "雙爽霜孀瀧淙驦鸘",
	shui: "說水誰睡稅蛻",
	shun: "順瞬吮舜䞐",
	shuo: "說數碩爍妁搠朔杓槊溯濯芍蒴鑠",
	si: "四死司思似斯食絲私寺撕廁肆伺俟兕廝厶噝嗣嘶姒巳汜泗澌祀祠笥糸纟緦耜肄苡菥螄鍶雉颸飼飴駟鷥",
	so: "送松宋搜頌嵩艘聳誦訟",
	son: "送松宋頌嵩聳誦訟凇崧",
	song: "送松宋頌嵩聳誦訟凇崧忪慫悚淞竦菘鍶",
	sou: "搜艘叟嗖嗽嗾擻涑溲漱瞍藪螋鎪颼餿",
	su: "蘇訴速素俗宿肅穌塑僳嗉夙愫涑溯簌粟蓿蔌觫謖酥驌鷫",
	sua: "算酸狻蒜",
	suan: "算酸狻蒜",
	sui: "雖隨歲碎尿遂彗攵濉燧眭睢祟穗綏荽蓑誶邃隋隧髓䍁",
	sun: "孫損榫猻筍蓀跣隼飧",
	suo: "所索縮鎖莎嗦唆嗩嗍娑挲桫梭瑣睃羧蓑逡",
	t: "他她天頭同聽太體通提",
	ta: "他她它達踏塔拓塌嗒撻榻沓溻漯獺趿躂蹋遢鉈闥闒鰨",
	tai: "太台態抬泰胎汰炱肽苔薹跆邰酞鈦駘鮐",
	tan: "談彈探嘆坦壇貪攤灘譚潭毯炭嘽坍忐曇檀湛澹痰癱眈碳羶舔蕁蕈袒覃賧郯鉭錟鐔",
	tang: "堂唐倘躺湯糖趟塘燙膛淌儻帑惝搪棠樘溏瑭羰耥螗螳醣铴镋鏜",
	tao: "討套逃挑桃濤陶掏萄淘滔叨啕檮洮燾絛绹韜饕鞀",
	te: "特忑忒慝铽騰疼藤滕謄",
	ten: "騰疼藤滕謄",
	teng: "騰疼藤滕謄",
	ti: "體提題弟替踢梯倜剃剔啼嚏屜悌惕棣涕睇綈緹荑裼諦蹄逖醍銻鳀鵜䗖䴘",
	tia: "天條調田跳挑甜添填佃",
	tian: "天田甜添填佃嗔忝恬掭栝殄滇町畋腆舔苫蠶蚺覥鈿锘闐頲䩄",
	tiao: "條調跳挑佻眺祧稠窕笤糶苕蜩踔迢銚髫鰷齠",
	tie: "鐵貼帖帖揲萜鋨餮",
	tin: "聽停庭廳挺亭廷艇婷梃",
	ting: "聽停庭廳挺亭廷艇婷梃汀烴町耵莛葶蜓鋌霆頲",
	to: "頭同通統投痛童透偷銅",
	ton: "同通統痛童銅桐桶筒仝",
	tong: "同通統痛童銅桐桶筒仝佟侗侗僮嗵垌峒彤恫慟捅潼瞳砼硐艟茼酮鮦",
	tou: "頭投透偷鈄骰",
	tu: "突土圖徒途吐塗屠禿兔凸堍芏荼菟酴釷",
	tua: "團彖摶揣湍疃鶉",
	tuan: "團彖摶揣湍疃鶉",
	tui: "推退腿煺萑褪頹",
	tun: "吞屯囤暾氽沌燉窀肫臀褪豚飩魨",
	tuo: "他脫托拖妥拓陀魄乇佗佗唾坨庹捝摭柁柝棁橢橐沱沲砣籜綏託跎迤酏酡鉈隋飥馱駝鴕鼉䓕",
	w: "我為文無外問位五萬王",
	wa: "瓦娃挖哇佤凹媧窪膃蛙襪鮭",
	wai: "外歪呙夭㖞",
	wan: "萬完晚灣玩碗彎腕頑挽婉宛丸剜娩惋浣烷琬畹皖箢紈綰脘芄莞菀蔓蜿豌鞔鯇",
	wang: "王望往網忘亡汪妄旺枉芒尢惘罔輞魍㲿",
	we: "為文問位未委微聞衛韋",
	wei: "為位未委微衛韋圍威維味遺偉危謂唯慰尾違魏餵偽畏胃惟倭偎囗圩娓尉崴嵬巍幃帷桅溈洧潿渭濰煒煨熨猗猥猬瑋痿眭磑緯艉芟葦荽萎葳蔚薇諉軎逶闈阢隈隗隹韙鮪鳂鳚㧑䓕",
	wen: "文問聞溫穩吻紋刎慍揾歿汶煴玟珉璺瘟笏紊缊蕰蚊轀閿雯鰮",
	weng: "翁嗡甕蓊蕹鶲",
	wo: "我握窩臥沃倭喔夭幄撾斡渦瘟硪肟萵蝸齷",
	wou: "渥",
	wu: "無五務物武惡屋吳午舞誤污烏伍於悟霧吾嗚勿侮兀捂仵唔圬塢埡嫵婺寤巫廡忤憮悮戊晤杌梧毋浯渥焐牾痦瞀笏蕪芴蜈蝥誣迕鄔鋈鎢铻阢騖鹀鵡鼯",
	x: "下學小心想行見現些向",
	xi: "西系息喜希席細習吸戲洗惜析悉稀襲熙嘻夕犧錫膝撕溪昔臘媳晰粞隙熄僖兮唏嘶奚嬉屃屎屣嵇徙戯曦棲樨檄欷歙汐浠淅澌烯熹犀璽畦皙矽硒禊禧穸绤羲翕舄舾茜菥葸蓰蜥蜴螅蟋裼褶覡誒蹊郄郗釃醯鍚銑鬩餼鰓鳛鼷䜣",
	xia: "下夏嚇俠霞峽瞎廈狹暇蝦匣呷挾柙歃毳洽狎瑕瘕硤罅葭轄遐黠",
	xian: "見現先顯線險限縣鮮獻洗仙閒陷賢憲灑嫌掀纖羨弦銜伣冼咸嫻峴嶮彡挦摻暹氙涎濂燹狝獫痃癇癬鹼祆筅秈腺舷莧薟蘚蜆跣躚酰釤銑銛鍁錟霰餡鷴黹",
	xiang: "想向相像像香響項鄉降享箱詳祥襄湘巷廂翔鑲橡庠攘緗舡薌葙蟓饗餉驤鯗",
	xiao: "小笑校消效銷曉蕭肖削瀟孝嘯宵嘵哮囂姣梟枵梢淆爻狡硝筱簫綃芍茭蛸蠨逍酵霄驍魈鴞",
	xie: "些解寫血謝葉協鞋斜邪脅攜洩歇械屑諧卸瀉褻偕勰廨懈挾擷桔楔榍榭歙渫溉瀣燮獬眭紲纈苴薤蠍蟹跬躞迦邂隰頡骱鮭䙊",
	xin: "心新信尋辛欣芯薪馨囟忻昕歆莘釁鑫鋅鐔騂䜣",
	xing: "行性形興星省姓型幸醒刑腥杏悻惺擤猩硎荇邢鈃鉶陘餳",
	xio: "兄雄胸兇熊洶匈芎讻诇",
	xion: "兄雄胸兇熊洶匈芎讻诇",
	xiong: "兄雄胸兇熊洶匈芎讻诇",
	xiu: "修秀休袖宿臭羞繡朽咻嗅岫庥溴煦莠貅銹饈髹鵂",
	xu: "許需續須虛徐序緒蓄籲敘畜婿勗咻噓圩墟嶼卹戌旭旮旯栩洫溆煦盱砉糈絮肷胥芋蓿詡諝酗醑雩頊馘",
	xua: "選宣玄旋懸券喧儇塤揎",
	xuan: "選宣玄旋懸券喧儇塤揎擐暄楦泫洵涓渲漩炫煊璇癬眩碹絢萱諼軒鉉鏇饌",
	xue: "學血雪穴削薛噱澩炔謔踅靴鱈鸴",
	xun: "尋訊訓迅詢巡遜循旬勳塤孫峋巽徇恂挦曛梭殉汛洵潯浚熏狻獯窨荀葷蕁蕈薰逡遁郇醺鑫馴鱘",
	y: "一有也要以麼於用又已",
	ya: "亞壓呀牙雅押啞崖涯丫鴨訝鴉伢埡婭岈挜揠柙椏氬琊疋瘂睚砑碣芽蚜衙軋輅迓錏䅉",
	yai: "睚",
	yan: "眼言研廣嚴演驗煙燕延沿顏殷掩厭岩咽炎艷鹽宴嫣雁焰淹焉彥儼偃兗剡厴唁埏堰奄妍崦懨晏簷氤湮涎湮灩焱琰硯硎筵罨胭醃芫菸蔫蜒衍覃觃讠諺讞贗趼郾鄢釅閆閹閻閼阽饜魘鵪黶鼴",
	yang: "樣陽楊央養洋揚羊仰癢佯徉怏恙暘殃氧泱漾煬烊瑒瘍秧蛘鞅颺鴦",
	yao: "要么約藥搖腰遙咬耀邀姚喲妖窯謠吆夭堯嶢崤崾么徭徼曜杳洮淆瀹爻珧瑤瘧祆窈窕繇餚舀蕘軺鑰銚颻鰩鷂",
	ye: "也業夜爺葉野耶頁邪咽液冶噎拽掖揲揶曄曳椰湮燁琊腋荼謁鄴铘靨饁",
	yi: "一以已意義議衣易醫依異藝億疑益移遺儀亦憶譯伊宜尾蛇椅誼翼艾洩役抑姨毅逸夷裔倚溢矣乙疫仡佗佚佾刈劓勚囈咦咿嗌噫圯埸壹奕屹嶧嶷弈弋彝怡懌悒懿挹掖揖旖昱曳欹殪洫渫漪焱熠犄猗疙痍瘞癔眙硪禕紲繹縊羿翊翌翳肄胰腋臆艤芸苡荑薏蟻蛾蜴袂詒詣貽軼迤邑酏釔銥錡鎰鐿阝雉頤飴饻驛鹝鷁鷖黝黟",
	yin: "因音引印銀煙隱陰飲殷吟姻淫尹蔭吲喑圻垠堙夤寅廴氤沂湮湮狺癮窨纼胤芩茚茵蚓訚鄞銦霪駰鰥龂齦䜣",
	ying: "應英影營迎硬映盈贏鷹嬰穎嚶媵嬴攖楹櫻瀅瀠瀛瑛瓔癭绬纓罌膺塋滎熒瑩鶯螢縈鎣蠅逞郢锳潁颕鸚",
	yo: "育喲唷有用又由友遊右",
	yon: "用永擁勇湧庸泳傭俑詠",
	yong: "用永擁勇湧庸泳傭俑詠喁墉壅恿慵甬癰臃臾蕹蛹踴邕鏞雍顒饔鲬鳙",
	you: "有又由友遊右優油郵猶尤憂幽幼悠誘佑侑卣呦囿宥尢揄攸柚泅牖猷疣繇聱莜莠蕕蚰蚴蝣蝤酉釉鈾銪魷鲉黝鼬",
	yu: "於與語育餘遇雨玉預魚欲域譽予獄愈於宇禦鬱豫漁籲愚俞愉羽寓浴裕娛輿喻粥傴俁喁噢圄圉圩妤嫗尉嶼峪嵛庾揄昱梧榆歟毓毹汩淤渝澦煜煨熨燠狳瑜畬瘀瘐盂禹禺窬窳竽紆聿肀腧腴臾舁芋菸萸蕷蔚虞蜍蜮蝓衙衙覦諛諭迂逾鈺铻閾閼隅雩飫馀馭鬻鵒鷸齬",
	yua: "員原元遠院願園源圓袁",
	yuan: "員原元遠院願園源圓袁緣怨援冤宛淵圜垣塬媛掾櫞沅涓爰猿瑗畹眢箢芫苑菀螈贠轅阮隕鳶鴛鵷黿",
	yue: "月越樂約躍閱岳曰悅刖噦櫟樾瀹粵蠖鑰鉞龠",
	yun: "運雲允暈韻孕勻蘊惲慍昀榅殞氳煴熨狁瘟筠篔紜缊耘芸苑菀蕰贠贇鄆鄖醞隕韞",
	z: "在這中子自著之只作主",
	za: "雜扎砸咋匝咂唼拶糴鲝",
	zai: "在再載災仔宰哉崽栽甾",
	zan: "咱贊暫拶攢昝涔湔瓚簪糌臢趲酇鏨",
	zang: "藏臟葬奘臢臧贓駔",
	zao: "早造遭糟躁灶燥噪鑿唣棗槽澡皂窖繅繰藻蚤",
	ze: "則澤責擇咋仄嘖幘昃柞稷窄笮簀舴賾迮鰂",
	zei: "賊",
	zen: "怎譖曾增綜贈憎甑繒罾",
	zeng: "曾增綜贈憎甑繒罾鋥",
	zh: "這中著之只主長知種者",
	zha: "查扎炸詐眨咋乍吒吒哳喋喳揸札柞柵楂榨槎渣渫猹痄砟碴笮苴蚱軋鍘閘鮓齄",
	zhai: "摘齊擇側債宅寨齋疵瘵砦窄翟膪",
	zhan: "戰展站佔顫斬沾粘嶄搌旃棧氈湔湛澶盞瞻綻蘸袒覘詹譫躔輾醮颭骣鱣鸇黏",
	zhang: "長張掌章丈帳仗障漲杖脹賬彰仉嫜嶂幛樟漳獐璋瘴绱蟑鄣",
	zhao: "著找照招著朝趙召兆罩昭爪啁搔棹沼淖濯笊肇蚤詔釗鸼",
	zhe: "這著者著折哲浙遮乇懾摺柘磔耷聶蔗蟄蜇螫褚褶謫赭輒轍鍺陬鷓䗖",
	zhei: "這",
	zhen: "真陣鎮震針珍振圳診偵貞枕斟朕椹榛湞溱滇甄畛疹砧稹箴纼縝胗臻蓁賑軫鴆",
	zheng: "正政爭證整徵丁鄭掙睜怔症蒸崢幀徵拯楨猙町瞠禎箏諍鉦錚鋥鯖",
	zhi: "之只知制至直指治識志支職質致止值織紙置智執址氏遲枝植旨擲殖芷芝侄秩肢滯汁脂稚幟卮吱咫埴夂峙帙彘徵忮摯摭暱枳枳梔櫛桎氐炙痔痣砥祁祉祗窒縶耆胝膣蛭蟄蜘觶豸贄趵趾蹠躓躑軹輊郅酈酯銍鑕陟雉騭鴟鷙黹㛿",
	zho: "中種重週眾終州鐘洲忠",
	zhon: "中種重眾終鐘忠仲衷腫",
	zhong: "中種重眾終鐘忠仲衷腫塚忪潼盅舂舯蚣螽踵鍾",
	zhou: "週州洲舟皺宙驟粥軸晝咒啁啄妯帚碡籀繇紂縐肘冑舳荮謅譸賙輈酎騶鬻鮦鸼",
	zhu: "主住術注著助屬朱諸逐竹珠駐豬築祝柱燭囑煮株鑄蛛佇侏妯拄杼柚楮櫧櫫濘洙渚瀦澍炷疰瘃矚竺紵翥舳苧苧茁茱蚰蛀褚誅貯邾銖鬻麈㔉",
	zhua: "抓爪撾傳轉專裝狀莊撞",
	zhuai: "轉拽",
	zhuan: "傳轉專賺磚撰囀摶沌湍篆顓饌",
	zhuang: "裝狀莊撞壯妝樁幢僮奘戇艟",
	zhui: "追墜惴揣椎槌綴縋萑贅錐隧隹騅",
	zhun: "準盹窀肫胗諄隼",
	zhuo: "著著桌捉卓繳琢倬勺啄拙擢斫棁棹濁浞涿淖濯灼焯禚箸絀肫茁蕞諑趵踔躅酌鐲",
	zi: "子自字資齊紫姊姿仔滋諮茲吱呲孜孳嵫恣梓淄漬滓甾疵瘠眥秭笫籽粢糍緇耔茈觜訾諮貲趑輜錙鎡髭鯔鶿齜",
	zo: "走總宗縱奏踪綜傯揍樅",
	zon: "總宗縱踪綜傯樅棕瘲粽",
	zong: "總宗縱踪綜傯樅棕瘲粽腙骔鬃",
	zou: "走奏揍楱謅諏鄒鄹陬騶鯫",
	zu: "組足族祖阻租俎卒咀沮淬苴菹詛蹴鏃駔",
	zua: "鑽賺攥纂纘躦",
	zuan: "鑽賺攥纂纘躦",
	zui: "最罪嘴堆醉羧蕞觜",
	zun: "尊遵撙樽鱒",
	zuo: "作做坐左座昨佐琢乍鑿唑嘬怍撮柞砟祚笮胙迮酢阼"
}