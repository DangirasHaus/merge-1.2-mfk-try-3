from flask import Blueprint, render_template, request, Response
import json
import Common.LiteralsDWS as L
import Common.ServerConnection.updateSystem as updateSystem


bp = Blueprint(__name__, __name__, template_folder='templates')

@bp.route('/backUpPage', methods=['POST', 'GET'])
def show():
    # All Collection Backups
    acb = updateSystem.get_all_col_backups()
    # All Products Backups
    apb = updateSystem.get_all_prod_backups()
    return render_template('backUpPage.html', literals=L.getAllLits(), acb=acb, apb=apb)


@bp.route('/reinstalBackup', methods=['POST', 'GET'])
def reinstalBackup():
    path = request.args.get('path')
    prodId = request.args.get('prodId')
    isCollection = request.args.get('isCollection') == "true"
    res = updateSystem.reinstall_backup(path, isCollection, prodId)
    return Response(json.dumps(res), mimetype='application/json', status=res["code"])
