from flask import Blueprint, render_template, request, redirect, url_for
import os, time
import Common.LogsDWS as LOG
import Common.functions as app
import Common.LancerCommande as LancerCommande
import Common.LiteralsDWS as L

bp = Blueprint(__name__, __name__, template_folder='templates')

# Literals were already initialized in screen 1
literals = L.getAllLits()
# Current path of data folder
order_folder = app.getConfPath("order_folder")

@bp.route('/screen3', methods=['POST', 'GET'])
def show():
    LOG.info("INIT screen 3")
    debug_mode = app.getConf("debug_node")
    engravingWithOnlyRedPointer = app.show_option_for_red_rectangle()
    if request.method == 'POST':
        if request.form.get('product') and request.form.get('pathimg') and request.form.get('textToEngrave'):
            # Retrieving variables
            ts = request.form.get('ts')
            clientOrderId = request.form.get('clientOrderId')
            pathimg = request.form.get('pathimg')
            product = request.form.get('product')
            # Product volume to display in e-commerce
            if "/" in product:
                volume = product.split("/")[1]
            else:
                volume = product
            clientProdName = request.form.get('clientProdName')
            clientProductId = request.form.get('clientProductId')
            pictogram = request.form.get('pictogram')
            # TODO TODO this TEMP VARIABLE is only for Hennessy
            app.storeOrderTempVariables("pictogram", pictogram)
            color = request.form.get('color')
            textColor = request.form.get('textColor')
            loaded_keyboard = request.form.get('keyboard')
            loaded_keyboard_extra_conf = request.form.get('keyboardConf')
            # INIT fichier info data
            app.setCurrentProduct(product)
            productData = app.getProductData()
            fullpathimg = app.getConfPath("main_folder") + "/" + pathimg
            textToEngrave = request.form.get('textToEngrave')
            app.storeOrderTempVariables("textToEngrave", textToEngrave)
            LOG.info("Screen 3 product: " + str(product))
            LOG.info("Screen 3 textToEngrave: " + str(textToEngrave.encode('unicode-escape')))

            # We receive two images, one to display on the web and another to process and engrave
            base64img_display = str(request.form.get('base64img_display'))
            base64img_process = str(request.form.get('base64img_process')).replace("data:image/png;base64,","")

            # TODO only to store the image that we are going to engrave
            #app.genererImage(str(textToEngrave), fullpathimg, police, float(rapport), float(hauteurBas), False)

            loadModal = "false"

            inputs, intermedios, outputs = app.calculate_data(fullpathimg)
            canvas_px = inputs["dmm"]/inputs["hatch"]
            pinMode = request.form.get('pinMode')
            font = request.form.get('font')
            app.storeOrderTempVariables("fontToEngrave", font)
            # Getting media to show for positioning
            platformNumberPNG = app.get_positioning_media_name()
            #image remove cale
            cale_remove_img = app.getImageRemoveCale()
            # Getting name to show for positioning
            cale_name = app.get_cale_name()
            #get brand shop
            brand_shop = app.getConf("collection")
            collection = list(brand_shop.split("_"))
            # show positioning only on dior shop
            isDior = False
            if "Dior" in collection: 
                isDior=True
            # Loading question to show in modal
            qid = app.getCollectionConf("question_id")
            q = L.getQuestion(qid)
            # Using translations
            if literals.get("lang_code") != "" and literals.get("lang_code") in q["traductions"]:
                q["text"] = q["traductions"][literals["lang_code"]]
            app.storeOrderTempVariables("product", product)

            traineeID = request.form.get('traineeID')
            engravingTrainingDuration = request.form.get('engravingTrainingDuration')
            # Admin Mode Active
            ama = app.e_commerce_admin_active()
            e_commerce_active = app.e_commerce_active()
            return render_template('screen3.html', pathimg=pathimg, product=product, canvas_px=canvas_px,
                                   base64img_process=base64img_process, q=q, ama=ama, ts=ts, debug_mode=debug_mode,
                                   engravingTrainingDuration=engravingTrainingDuration, traineeID=traineeID,
                                   loaded_keyboard_extra_conf=loaded_keyboard_extra_conf, color=color,clientOrderId=clientOrderId,
                                   textColor=textColor, pictogram=pictogram, e_commerce_active=e_commerce_active,
                                   volume=volume, cale_name=cale_name, isDior=isDior, cale_remove_img=cale_remove_img,
                                   clientProdName=clientProdName, clientProductId=clientProductId,
                                   textToEngrave=textToEngrave, font=font, loaded_keyboard=loaded_keyboard,
                                   intermedios=intermedios, outputs=outputs, staticversion=app.getStaticVersion(),
                                   base64img=base64img_display, pinMode=pinMode, platformNumberPNG=platformNumberPNG,
                                   loadModal=loadModal, literals=literals,
                                   engravingWithOnlyRedPointer=engravingWithOnlyRedPointer)
        else:
            return redirect(url_for('web.views.screen1.show'))
    else:
        return redirect(url_for('web.views.screen1.show'))

