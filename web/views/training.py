# -*- coding: utf-8 -*-
import os, traceback, random
from flask import Blueprint, render_template, request
import Common.functions as app
from datetime import datetime
import Common.LogsDWS as LOG
import Tests.cnc_performance.test_cnc_performance as cnc_perf
import Common.training as training
import Common.LiteralsDWS as L
import Common.ServerConnection.updateSystem as updateSystem


bp = Blueprint(__name__, __name__, template_folder='templates')




@bp.route('/training', methods=['POST', 'GET'])
def show():
    literals = app.initLiterals()
    static_version = app.getStaticVersion()
    # Retrieving images to show for training screen
    n_imgs = int(app.getConf("number_training_images"))
    max_n_trainees = int(app.getConf("max_number_trainees"))
    langlist = app.getLangList()
    new_lang = request.args.get('new_lang')

    if new_lang is not None:
        literals = app.changeLiteralLang(new_lang)
        # usip = user_guide_imgs_path
    else:
        new_lang = app.getCollectionConf("app_lang")
        literals = L.getAllLits()
    LOG.info("USER GUIDE IMAGES training:")
    imgs, imgs_path = get_general_training_images(new_lang)
    LOG.info(imgs)
    for i in range(len(imgs)):
        if i >= n_imgs:
            imgs.pop(len(imgs)-1)
        else:
            imgs[i] = imgs[i].split(".")[0]
    return render_template('training.html', staticversion=static_version, imgs=imgs, max_n_trainees=max_n_trainees,
                           literals=literals, langlist=langlist, imgs_path=imgs_path, new_lang=new_lang)


def get_general_training_images(new_lang):
    machine_type = ""
    if app.getConf("machine_type").upper() == "XL":
        machine_type = "_DS2"
    user_guide_imgs_path = app.getConfPath("training_images_folder") + machine_type + "/" + new_lang
    imgs_path = "/static/images/training" + machine_type + "/" + new_lang + "/"
    imgs = cnc_perf.get_images_in_folder(user_guide_imgs_path, "*.jpg")
    return imgs, imgs_path


# Prepares the app for installation
@bp.route('/prepareForInstallation', methods=['POST', 'GET'])
def prepare_for_installation():
    literals = app.initLiterals()


    '''
    TODO Add language selector when we have the user guide in multiple languages
    new_lang = request.args.get('new_lang')
    if new_lang:
        literals = app.changeLiteralLang(new_lang)
    '''



    res = {}
    brand_id = app.getCollectionConf("id_brand")
    shop_id = app.getCollectionConf("id_shop")
    col = app.getConf("collection")
    lhfb = app.getConf("laser_height_from_base")
    liv = app.getConf("laser_interlock").split(":")[1]
    f_d = app.getConf("focal_distance")
    rot_c = app.getConf("rotation_correction")
    prepare_installation = request.form.get('prepare_installation')
    if prepare_installation:
        res = {"result": "OK", "errors": []}
        LOG.info("prepare_installation TRUE")
        debug_mode = request.form.get('debug_mode')
        ratings_file = request.form.get('ratings_file')
        failed_requests = request.form.get('failed_requests')
        trainees_file = request.form.get('trainees_file')
        log_file = request.form.get('log_file')
        laser_conf_file = request.form.get('laser_conf_file')
        html_static = request.form.get('html_static')
        first_backup = request.form.get('first_backup')
        force_training_mode = request.form.get('force_training_mode')
        laser_rotation = request.form.get('laser_rotation')
        laserInterlock = request.form.get('laserInterlock')
        laser_height_from_base = request.form.get('laser_height_from_base')
        focal_distance = request.form.get('focal_distance')



        new_conf_values = {}
        if debug_mode:
            new_conf_values["debug_node"] = "False"
        if ratings_file:
            rf_res = empty_ratings_file()
            if rf_res != "":
                res["errors"].append(rf_res)
        if failed_requests:
            fr_res = remove_failed_request_file()
            if fr_res != "":
                res["errors"].append(fr_res)
        if trainees_file:
            tf_res = remove_trainees_file()
            if tf_res != "":
                res["errors"].append(tf_res)
        if log_file:
            lf_res = reset_log_file()
            if lf_res != "":
                res["errors"].append(lf_res)
        if laser_conf_file:
            rlc_res = remove_lasr_conf_file()
            if rlc_res != "":
                res["errors"].append(rlc_res)
        if html_static:
            v = app.getConf("code_version")
            new_conf_values["html_static_version"] = v + "." + str(random.randint(0, 10000))
        if first_backup:
            cb_res = create_first_backup()
            if cb_res != "":
                res["errors"].append(cb_res)
        if not force_training_mode:
            traineeDTO = create_DWS_dummy_trainee()
            training.add_new_trainee(traineeDTO)
        if laser_rotation:
            laser_rotation_value = request.form.get('laser_rotation_value')
            new_conf_values["rotation_correction"] = str(laser_rotation_value)
        if laserInterlock:
            laser_rotation_value = request.form.get('laserInterlock_value')
            new_conf_values["laserInterlock"] = "0:"+str(laser_rotation_value)
        if laser_height_from_base:
            laser_height_from_base_value = request.form.get('laser_height_from_base_value')
            new_conf_values["laser_height_from_base"] = str(laser_height_from_base_value)
        if focal_distance:
            focal_distance_value = request.form.get('focal_distance_value')
            new_conf_values["focal_distance"] = str(focal_distance_value)



        # Changing all the properties in conf.properties at the same time
        if len(new_conf_values.keys()) > 0:
            cv_res = app.change_conf_prop(new_conf_values)
            if cv_res != "":
                res["errors"].append(cv_res)








    # Form to create collection. Display depends on the value of variable "res"
    return render_template('prepareForInstallation.html', result=res, literals=literals, brand_id=brand_id,
                           shop_id=shop_id, col=col, lhfb=lhfb, f_d=f_d, rot_c=rot_c, liv=liv)


def create_DWS_dummy_trainee():
    trainee_id = "3141592"
    first_name = "DWS"
    last_name = "DWS"
    date_app = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    date_engraving = "None"
    general_training_duration = "1"
    engraving_training_duration = "1"
    first_engraving = "None"
    general_training = True
    engraving_training = True
    stored_in_server = False
    return training.trainee(trainee_id, first_name, last_name, date_app, date_engraving,
         general_training_duration, engraving_training_duration, first_engraving,
         general_training, engraving_training, stored_in_server)

def empty_ratings_file():
    path = app.getConfPath("ratings_path")
    LOG.info("Removing ratings file: " + str(path))
    return remove_file(path, "Error removing ratings file")


def remove_failed_request_file():
    path = app.getConfPath("failed_requests_path")
    LOG.info("Removing failed requests file: " + str(path))
    return remove_file(path, "Error removing failed requests file")


def remove_trainees_file():
    path = app.getConfPath("trainees_list_path")
    LOG.info("Removing trainees file: " + str(path))
    return remove_file(path, "Error removing trainees requests file")

def remove_lasr_conf_file():
    path = app.getConfPath("laserConfFilePath")
    LOG.info("Removing laser conf file: " + str(path))
    return remove_file(path, "Error removing laser conf file in install app")


# We cant remove the file since it is being used. We delete rotations and add a log
def reset_log_file():
    path = app.getConfPath("app_log_path")
    LOG.info("Reseting logs file: " + str(path))
    res = ""
    for i in range(5):
        # We remove the log files
        this_path = path + "." + str(i)
        LOG.info("Removing logs file: " + str(this_path))
        res = remove_file(this_path, "Error removing log file " + str(i))
    LOG.info("*************************************************************************")
    LOG.info("*                                                                       *")
    LOG.info("*                    RESETTING LOGS FOR INSTALLATION                    *")
    LOG.info("*                                                                       *")
    LOG.info("*************************************************************************")
    return res


# Creates the first folder backup for collection and products
def create_first_backup():
    LOG.info("Creating first backup")
    res = ""
    try:
        # STEP 1: Create collection backup
        col_name = app.getConf("collection")
        updateSystem.collection_back_up(col_name)
        # STEP 2: Create backup for all products
        col_conf_json = app.getAllCollectionConf()
        LOG.info("Backup for collection done")
        products = app.get_products_from_products_structure(col_conf_json["products_structure"])
        # Iterating all products
        for product in products:
            app.setCurrentProduct(product)
            prod_data = app.getProductData()
            prod_cat = None
            if prod_data.get("product_category_id"):
                prod_cat = prod_data.get("product_category_id")
            prod_path = app.getConfPath("products_id_folder") + "/" + product
            LOG.info("Prod path to overwrite: " + str(prod_path))
            updateSystem.productBackUp(prod_path, prod_cat)
        LOG.info("Backup for products done")
    except Exception as e:
        res = "Error creating backup"
        LOG.error("Error creating backup: " + str(e))
    return res


# Common method to remove files before installation
def remove_file(path, msg):
    res = ""
    try:
        if os.path.exists(path):
            os.remove(path)
    except Exception:
        LOG.error(msg)
        res = msg
        LOG.error(str(traceback.format_exc()))
    return res



@bp.route('/traineeExists', methods=['POST', 'GET'])
def trainee_xists():
    trainee_id = request.args.get('trainee_id')
    return str(training.get_trainee(trainee_id))


@bp.route('/testgettrainingconfig', methods=['POST', 'GET'])
def testgettrainingconfig():
    item = request.args.get('item')
    lang = request.args.get('lang')
    lit = app.changeLiteralLang(lang)
    config = {
        "/screen1": [{
            "name" 		: "tour_1_0",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "B",
            "text"		: lit["tour_1_0"],
            "onLoad"  	: "showHideAdminPopup(false)",
            "time" 		: 5000
        },{
            "name" 		: "tour_1_1",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "B",
            "text"		: lit["tour_1_1"],
            "onLoad"  	: "showHideAdminPopup(false)",
            "time" 		: 5000
        },{
            "name" 		: "tour_1_2",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_1_2"],
            "position"	: "TL",
            "onLoad"  	: "showHideAdminPopup(false)",
            "time" 		: 5000
        },{
            "name" 		: "tour_1_3",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_1_3"],
            "onLoad"  	: "showHideAdminPopup(true)",
            "position"	: "B",
            "time" 		: 5000
        },{
            "name" 		: "tour_1_4",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_1_4"],
            "position"	: "T",
            "time" 		: 5000
        },{
            "name" 		: "tour_1_6",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_1_6"],
            "position"	: "T",
            "time" 		: 5000
        },{
            "name" 		: "tour_1_7",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_1_7"],
            "position"	: "T",
            "time" 		: 5000
        },{
            "name" 		: "tour_1_8",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_1_8_1"],#"",
            "position"	: "B",
            "onLoad"  	: "showHideAdminPopup(true)",
            "time" 		: 5000
        },{
            "name" 		: "tour_1_8",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_1_8_2"],#"",
            "position"	: "B",
            "onLoad"  	: "showHideAdminPopup(false)",
            "time" 		: 5000
        }],
        "/screen2": [{
            "name" 		: "tour_2_1",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "T",
            "text"		: lit["tour_2_1"],#"",
            "onLoad"	: "showHideKeyboard(false)",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_2",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_2"],#"",
            "position"	: "R",
            "onLoad"	: "showHideKeyboard(false)",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_3",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_3_1"],#"",
            "position"	: "B",
            "onLoad"	: "showHideKeyboard(true)",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_3",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_3_2"],#"",
            "position"	: "B",
            "onLoad"	: "showHideKeyboard(true)",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_3",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_3_3"],#"",
            "position"	: "B",
            "onLoad"	: "showHideKeyboard(true)",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_4",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_4"],#"",
            "position"	: "L",
            "onLoad"	: "showHideKeyboard(false)",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_5",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_5"],#"",
            "position"	: "TL",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_6",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_6"],#"",
            "position"	: "R",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_7",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_7"],#"",
            "position"	: "BL",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_8",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_8_1"],#"",
            "position"	: "BR",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_8",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_8_2"],#"",
            "position"	: "BR",
            "time" 		: 5000
        },{
            "name" 		: "tour_2_9",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_2_9"],#"",
            "position"	: "TL",
            "time" 		: 5000
        }],
        "/screen3": [{
            "name" 		: "tour_3_2",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_1"],#"",
            "position"	: "L",
            "time" 		: 5000
        },{
            "name" 		: "tour_3_2",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_2"],#"",
            "position"	: "L",
            "time" 		: 5000
        },{
            "name" 		: "tour_3_3",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_3_1"],#"",
            "position"	: "R",
            "time" 		: 5000
        },{
            "name" 		: "tour_3_3",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_3_2"],#"",
            "position"	: "R",
            "time" 		: 5000
        },{
            "name" 		: "tour_3_4",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_4"],#"",
            "position"	: "R",
            "time" 		: 5000
        },{
            "name" 		: "tour_3_5",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_5"],#"",
            "position"	: "R",
            "time" 		: 5000
        },{
            "name" 		: "tour_3_5",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_5_2"],#"",
            "position"	: "R",
            "time" 		: 5000
        },{
            "name" 		: "tour_3_6",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_6"],#"",
            "position"	: "BL",
            "time" 		: 5000
        },{
            "name" 		: "tour_3_7",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_7_1"],#"",
            "position"	: "BR",
            "time" 		: 5000
        },{
            "name" 		: "tour_3_7",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_7_2"],#"",
            "position"	: "BR",
            "time" 		: 5000
        },{
            "name" 		: "tour_3_8",
            "bgcolor"	: "white",
            "color"		: "black",
            "text"		: lit["tour_3_8"],#"",
            "position"	: "R",
            "time" 		: 5000
        }],
        "engravingModal": [{
            "name" 		: "tour_4_1",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "T",
            "text"		: lit["tour_4_1_1"],#"",
            "time" 		: 5000
        },{
            "name" 		: "tour_4_1",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "T",
            "text"		: lit["tour_4_1_2"],#"",
            "time" 		: 5000
        },{
            "name" 		: "tour_4_1",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "T",
            "text"		: lit["tour_4_1_3"],#"",
            "time" 		: 5000
        },{
            "name" 		: "tour_4_2",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "T",
            "text"		: lit["tour_4_2_1"],#"",
            "time" 		: 5000
        },{
            "name" 		: "tour_4_2",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "T",
            "text"		: lit["tour_4_2_2"],#"",
            "time" 		: 5000
        },{
            "name" 		: "tour_4_3",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "T",
            "text"		: lit["tour_4_3_1"],#"",
            "time" 		: 5000
        },{
            "name" 		: "tour_4_3",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "T",
            "text"		: lit["tour_4_3_2"],#"",
            "time" 		: 5000
        },{
            "name" 		: "tour_4_4",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "B",
            "text"		: lit["tour_4_4"],#"",
            "time" 		: 5000
        },{
            "name" 		: "tour_4_5",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "R",
            "text"		: lit["tour_4_5"],#"",
            "time" 		: 5000
        },{
            "name" 		: "tour_4_7",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "R",
            "text"		: lit["tour_4_7_1"],#"",
            "time" 		: 5000
        },{
            "name" 		: "tour_4_7",
            "bgcolor"	: "white",
            "color"		: "black",
            "position"	: "R",
            "text"		: lit["tour_4_7_2"],#",
            "time" 		: 5000
        }]
    }
    # Formatting string response to parse to JSON in JavaScript
    return str(config.get(item)).replace("'", "\"")

