from datetime import datetime


from flask import Blueprint, render_template, request, redirect, url_for, Response
import math, time, os, json
import Common.LogsDWS as LOG
import Common.functions as app
import Common.LiteralsDWS as L
import Common.ServerConnection.Connection as server
import Common.training as training

# Check if collection exists

if not os.path.isdir(app.getConfPath("data_folder")):
    print("ERROR - INCORRECT COLLECTION NAME")
    LOG.info("ERROR - INCORRECT COLLECTION NAME")

# init the literals
literals = app.initLiterals()
langlist = app.getLangList()

debug_mode = app.getConf("debug_node")
static_version = app.getStaticVersion()

bp = Blueprint(__name__, __name__, template_folder='templates')

ts = 1640012706354898


@bp.route('/ecommerce1', methods=['POST', 'GET'])
def show():
    # Setting admin mode to false
    app.change_e_commerce_admin_state(False)
    # Checking if lang change
    new_lang = request.args.get('new_lang')
    if new_lang is not None:
        app.changeLiteralLang(new_lang)
    # Checking if switching mode

    switchMode = request.args.get('switchMode') == "True"
    if switchMode:
        app.switch_e_commerce_active_state()
    # Checking if there was any error
    errorcode = request.args.get('errorcode')
    errorIntrs = None
    # Allow switch modes between e-commerce and in-store
    awm = app.allow_switch_modes()
    if not app.e_commerce_active():
        return redirect(url_for('web.views.screen1.show'))

    VersionAndIp = app.getConf("code_version") + app.getIpAdress() + ":" + app.getTeamviewerID()
    screen_saver_time = app.getCollectionConf("screen_saver_time")
    LOG.info("INIT e-commerce 1")

    # List of Pending E-commerce Engravings
    pee = server.get_pending_e_engravings()

    # If pee is none, it means there is no internet
    

    if pee is None:
        errorcode = "e00"
        pee = []
    if errorcode:
        LOG.info("errorcode:" + str(errorcode))
        errorIntrs = L.getErrorInstr(errorcode)

    global_e = []

    idShop = app.getCollectionConf("id_shop")
    # Check if idShop is "event-03-2020"
    if idShop == "event-03-2020":
        for engraving in pee:
            if engraving["idStore"] is None:
                engraving["idStore"] = "SFCCINT"
            if engraving["idStore"] == "SFCCINT" or engraving["idStore"] is None:
                global_e.append(engraving)
    

    data_to_send = pee
    if idShop == "event-03-2020":
        data_to_send = global_e
        
    # Format date
    for engraving in data_to_send:
        engraving["date"] = engraving["date"].replace("T", " ")[:-7]

    # DISPLAY 0: E-commerce screen
    return render_template('ecommerce1.html', screen_saver_time=screen_saver_time, literals=literals,
                           langlist=langlist, staticversion=static_version, awm=awm, pee=data_to_send, debug_mode=debug_mode,
                           VersionAndIp=VersionAndIp, errorcode=errorcode, errorIntrs=errorIntrs, idShop=idShop)


@bp.route('/getEEngraving2', methods=['POST', 'GET'])
def get_e_engraving():
    # engraved = True
    engravings, status = server.get_e_engraving_all()
    print("engravings::" + str(engravings))
    return engravings
    # return Response(json.dumps(engravings), mimetype='application/json', status=status)


# Closes the app from the tablet without the need of a keyboard
@bp.route('/activateAdminMode', methods=['POST', 'GET'])
def close_app():
    LOG.info("Checking password to activate admin mode")
    res = {
        "result": False
    }
    status = 401
    rec_pwd = request.form.get('pass')
    if app.check_admin_password(rec_pwd):
        res["result"] = True
        status = 200
    app.change_e_commerce_admin_state(res["result"])
    return Response(json.dumps(res), mimetype='application/json', status=status)


# Closes the app from the tablet without the need of a keyboard
@bp.route('/checkAdminPassword', methods=['POST', 'GET'])
def checkAdminPassword():
    LOG.info("Checking admin password")
    res = {
        "result": False
    }
    status = 401
    rec_pwd = request.form.get('pass')
    if app.check_admin_password(rec_pwd):
        res["result"] = True
        status = 200
    return Response(json.dumps(res), mimetype='application/json', status=status)


# show engraved products
@bp.route('/ecommerce2', methods=['POST', 'GET'])
def show_engraved():
    # Setting admin mode to false
    app.change_e_commerce_admin_state(False)
    # Checking if lang change
    new_lang = request.args.get('new_lang')
    if new_lang is not None:
        app.changeLiteralLang(new_lang)
    # Checking if switching mode
    switchMode = request.args.get('switchMode') == "True"
    if switchMode:
        app.switch_e_commerce_active_state()
    # Checking if there was any error
    errorcode = request.args.get('errorcode')
    errorIntrs = None
    # Allow switch modes between e-commerce and in-store
    awm = app.allow_switch_modes()
    if not app.e_commerce_active():
        return redirect(url_for('web.views.screen1.show'))

    VersionAndIp = app.getConf("code_version") + app.getIpAdress() + ":" + app.getTeamviewerID()
    screen_saver_time = app.getCollectionConf("screen_saver_time")
    LOG.info("INIT e-commerce 2")

    # List of Pending E-commerce Engravings
    pee = server.get_engraved()

    if pee is None:
        errorcode = "e00"
        pee = []
    if errorcode:
        LOG.info("errorcode:" + str(errorcode))
        errorIntrs = L.getErrorInstr(errorcode)
    # DISPLAY 0: E-commerce screen
    return render_template('ecommerce2.html', screen_saver_time=screen_saver_time, literals=literals,
                           langlist=langlist, staticversion=static_version, awm=awm, pee=pee, debug_mode=debug_mode,
                           VersionAndIp=VersionAndIp, errorcode=errorcode, errorIntrs=errorIntrs)
