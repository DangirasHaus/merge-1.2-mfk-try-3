from flask import Blueprint, render_template, request, redirect, url_for, Response
import Common.LogsDWS as LOG
import Common.functions as app
import Common.Gcode as Gcode
import Common.LiteralsDWS as L
import Common.LancerCommande as laser
import Common.ServerConnection.Connection as server
import Tests.cnc_performance.test_cnc_performance as cnc_perf
import Common.ServerConnection.updateSystem as updateSystem
import Common.training as training
import base64, io, time, os, json
import socket, traceback
from datetime import datetime
from telnetlib import Telnet
from threading import Thread
from PIL import Image
from sys import platform
import requests

bp = Blueprint(__name__, __name__, template_folder='templates')


# Stores an engraving in a CSV and sends the engraving to the server
@bp.route('/dataManagement', methods=['POST', 'GET'])
def show():
    answer = request.form.get('stars')
    product = request.form.get('product')
    text = request.form.get('text')
    font = request.form.get('font')
    ts = request.form.get('ts')
    engravingTime = request.form.get('engravingTime')
    idQuestion = request.form.get('idQuestion')
    questionType = request.form.get('questionType')
    errorMessage = request.form.get('errorMessage')
    stopped = request.form.get('stopped')
    relaunch = request.form.get('relaunched')
    print("engravingTime = " + str(engravingTime))
    ratings_file = app.getConfPath("ratings_path")
    current_day = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    # SENDING engraving information to server
    # (idBrand, idTask, idProduct, idShop, Text, Time, Date, Satisfaction, Version)
    idTask = server.addEngraving(product, text, engravingTime, current_day, answer, font, idQuestion, questionType, errorMessage, stopped, relaunch)




    # If we receive the traineeID we need to store that they finished the engraving training
    if request.form.get('traineeID'):
        traineeID = request.form.get('traineeID')
        engravingTrainingDuration = request.form.get('engravingTrainingDuration')
        training.finish_engraving_training(traineeID, engravingTrainingDuration, idTask)

    # If we receive ts, we update e-engraving status in server
    if ts:
        server.update_engraving_order(ts)


    # Writing True or False for stopped. In the server, if it is false, we dont store anything
    stopped = stopped == "true"
    # Open the csv file with mode "a" to append a new entry
    # add try exception if rating file not found
    try:
        ratings_folder = app.getConfPath("ratings_folder")
        os.makedirs(ratings_folder)
    except FileExistsError:
        # folder already exists
        pass
    csv_ratings = open(ratings_file, "a")
    # The new entry is the number of stars received and the current date
    new_line = "\n" + str(answer) + "," + str(current_day) + "," + str(product) + "," + str(text.encode('unicode-escape')) + "," + \
               str(idQuestion) + "," + str(questionType) + "," + str(errorMessage) + "," + str(stopped)
    csv_ratings.write(new_line)
    csv_ratings.close()
    # Editing file last_engraving_date_path to know the last time we did an engraving and know if we need to warm up
    app.update_last_engraving_date()
    LOG.info("New rating stored. Answer [" + str(answer) + "]. Product [" + str(product) + "]. Text [" + str(text.encode('unicode-escape')) +
             "]. Date [" + str(current_day) + "]" + "]. idQuestion [" + str(idQuestion) + "]" + "]. errorMessage [" +
             str(errorMessage) + "]" + "]. stopped [" + str(stopped) + "]")
    return str(new_line)

# Checks if the laser is engraving
@bp.route('/checkLaserStatus', methods=['POST', 'GET'])
def laser_status():
    #LOG.info("Checking laser status...")
    try:
        is_laser_on = laser.is_laser_on()
        #LOG.info("Is laser on? " + str(is_laser_on))
    except socket.timeout:
        is_laser_on = "Error"
        LOG.error("Socket timeout!")
    return str(is_laser_on)


# Checks if the laser is ready to engrave, Ready External
@bp.route('/checkLaserIsReady', methods=['POST', 'GET'])
def laser_is_ready():
    LOG.debug("Checking if laser is ready to engrave...")
    try:
        is_laser_ready = laser.is_laser_ready()
        LOG.debug("Is laser on? " + str(is_laser_ready))
    except socket.timeout:
        is_laser_ready = "Error"
        LOG.error("Socket timeout!")
    return str(is_laser_ready)


# Checks if the laser is turned on
@bp.route('/checkLaserIsOn', methods=['POST', 'GET'])
def Check_laser_Is_On():
    IpAdress = app.getLaserIP()
    LOG.debug("Checking laser is on or not.. IP address is" + str(IpAdress))
    # and then check the response...
    try:
        telnetTimeout = int(app.getConf("laser_Telnet_timeout"))
        tn = Telnet(IpAdress, timeout=telnetTimeout)
        bool = True
        tn.close()
        LOG.debug("laser on")
    except Exception:
        bool = False
        LOG.error("laser Off")
    return str(bool)


# Sends a telnet message to stop the laser engraving
@bp.route('/stopLaser', methods=['POST', 'GET'])
def turn_off_laser():
    destination = app.getLaserIP()  #Ip du USCserver (statique)
    telnetTimeout = int(app.getConf("laser_Telnet_timeout"))
    tn = Telnet(destination, timeout=telnetTimeout)
    tn.read_until(b"INET> ", telnetTimeout)
    tn.write(b"M 0\r\n")
    # We need to wait a bit for the response from the laser.
    time.sleep(0.2)
    laser_status = tn.read_eager()
    # If laser_status contains 0:1, the laser is on
    laser_stopped = "0:" in str(laser_status)
    unf_file_path = app.getConfPath("unfReturnLaserToOrigin")
    if not (os.path.exists(unf_file_path)):
        cnc_file_path = app.getConfPath("cncReturnLaserToOrigin")
        LOG.info("unf not found, ->fetch cnc..."+cnc_file_path)
        if not(os.path.exists(cnc_file_path)):
            LOG.info("cnc not found, ->fetch cnc..."+cnc_file_path)
            app.createCNCFileToReturnRedPointerToOrigin()
        cnc_file_path = cnc_file_path.replace("/returnLaserToOrigin.cnc", "")
        cnc_file_path = cnc_file_path.replace("Common/../", "")
        Gcode.createUnfFile(cnc_file_path, "returnLaserToOrigin")
    laser.sendUnfToLaser(unf_file_path)
    LOG.info("unf file have been sent to stop laser...")
    return str(laser_stopped)

# Check file and get it via FTP
@bp.route('/testConfFile', methods=['POST', 'GET'])
def testConfFile():
    laser_conf_file = "laserConfFile.properties"
    return str(app.checkLaserConfFile(laser_conf_file))

# Send file and get it via FTP
@bp.route('/sendLaserConfToLaser', methods=['POST', 'GET'])
def sendLaserConfToLaser():
    laserConfFilePath = app.getConfPath("laserConfFilePath")
    return str(laser.sendLaserConfToLaser(laserConfFilePath))

# Closes the app from the tablet without the need of a keyboard
@bp.route('/closeApp', methods=['POST', 'GET'])
def close_app():
    LOG.info("Checking password to close app")
    def_pwd = app.getConf("close_app_pass")
    brand_id = app.getCollectionConf("id_brand")
    shop_id = app.getCollectionConf("id_shop")

    pwd = def_pwd + "-" + brand_id + "-" + shop_id
    rec_pwd = request.form.get('pass')
    # This way we have a default admin password for all of our lasers and we give a different password to each shop
    DWS_admin_pwd = def_pwd + "-1618"
    if pwd == rec_pwd or rec_pwd == DWS_admin_pwd:
        LOG.info("Received password matches, closing browser")
        if platform == "linux" or platform == "linux2":
            LOG.info("We are in linux")
            os.system('pkill chromium')
        elif platform == "win32":
            LOG.info("We are in windows")
            os.system("taskkill /f /im chrome.exe")
        res = "true"
    else:
        LOG.info("INCORRECT password, doing nothing")
        res = "false"
    return res



@bp.route('/drawLadder', methods=['POST', 'GET'])
def draw_ladder():
    pathDossierCalibration = app.getConfPath("Calibration_files_folder")
    FileName = "calibration_ladder"
    if not os.path.exists(pathDossierCalibration+"/"+FileName+".unf"):
        LOG.info("unf file not found try to find cnc file... ")
        if not os.path.exists(pathDossierCalibration+"/"+FileName+".cnc"):
            LOG.info("cnc file not found, try to create cnc file..")
            Gcode.drawLadderToFindFocalDistance()
            LOG.info("cnc file created at the path :"+pathDossierCalibration+FileName+".cnc")
        Gcode.createUnfFile(pathDossierCalibration, FileName)
        LOG.info("unf file created")
    result = laser.sendUnfToLaser(pathDossierCalibration + "/" + FileName + ".unf")
    LOG.info("unf file have been sent, laser should be engraving now the ladder...")
    return str(result)

@bp.route('/drawHorizontalLine', methods=['POST', 'GET'])
def drawHorizontalLine():
    pathDossierCalibration = app.getConfPath("Calibration_files_folder")
    FileName = "calibration_angle"
    if not os.path.exists(pathDossierCalibration + "/" + FileName + ".unf"):
        LOG.info("unf file not found try to find cnc file... ")
        if not os.path.exists(pathDossierCalibration + "/" + FileName + ".cnc"):
            LOG.info("cnc file not found, try to create cnc file..")
            Gcode.drawHLineToDeterminateAngle()
            LOG.info("cnc file created at the path :" + pathDossierCalibration + FileName + ".cnc")
        Gcode.createUnfFile(pathDossierCalibration, FileName)
        LOG.info("unf file created")
    result = laser.sendUnfToLaser(pathDossierCalibration + "/" + FileName + ".unf")
    LOG.info("unf file have been sent, laser should be engraving now the horizontal line...")
    return str(result)

@bp.route('/launchPylint', methods=['POST' , 'GET'])
def launchPylint():
    result_dict = {}
    module_path = app.getConfPath("Common_folder")
    os.chdir(module_path)
    LOG.info("Testing all the files in this folder "+str(module_path))
    result_dict = app.getPylintFolderResult(module_path, result_dict)
    LOG.info("files tested are "+str(result_dict))
    return render_template("launchpylint.html", result_dict=result_dict)


# Checks if the laser is engraving
@bp.route('/cncPerformance', methods=['POST', 'GET'])
def cncPerformance():
    nIt = 10
    times = cnc_perf.test_cnc_performance(nIt)
    avg = sum(times)/len(times)
    return render_template('performance.html', times=times, avg=avg)


#This endpoint takes the message of the client from the javascript function sendFeedBackMessage() and send it to the AWS server via the informError DTO
@bp.route('/sendFeedBackMessage', methods=['POST', 'GET'])
def SendFeedBackMessage():
    FeedBackMessage = request.form.get("feedBack_message")
    LOG.info("Client sent a feedBack, the message is :" + FeedBackMessage)
    errorReceived = "FeedBack"
    print("the feed back message of the client is ", FeedBackMessage)
    response = server.informError(errorReceived, FeedBackMessage, True)
    StatusCode = str(response.get("code"))
    LOG.info("The Status code is "+ StatusCode)
    return Response(json.dumps(response), mimetype='application/json', status=StatusCode)


# Starts the process of updating the installad collection
# Returns the result of the update to show in the pop up
@bp.route('/updateSystemCollection', methods=['POST', 'GET'])
def updateSystemCollection():
    print("updateSystemCollection")
    '''
        If we edit the logic of these steps, pay attention to function "download_new_collection_from_server"
        since it has a very similar logic
    '''
    # STEP 1: Download server collection data and current collection data
    step = 1
    LOG.info("Starting step: " + str(step))
    col_data = server.getServerCollectionData()
    cur_col_data = app.getAllCollectionConf()
    col_conf_json = {}
    is_there_an_update_products = False
    is_there_an_update_collection = False
    try:
        # Checking current timestamp and received timestamp
        # If we have "timestamp" in response, this means we have an available version for our code
        #  - col_data is from server
        #  - cur_col_data is local
        if app.check_timestamps(col_data, cur_col_data, "products_structure"):
            LOG.info("Starting collection update")
            step += 1
            LOG.info("Starting step: " + str(step))  # 2
            # STEP 2: Generate collection_conf.json - like file from received collection
            LOG.info("Generating collection conf json file from server data")
            col_conf_json = updateSystem.server_collection_to_conf_json(col_data, cur_col_data)
            col_conf_json["id_brand"] = cur_col_data.get("id_brand")
            col_conf_json["id_shop"] = cur_col_data.get("id_shop")
            step += 1
            LOG.info("Starting step: " + str(step))  # 3
            # STEP 3: Create backup of current collection folder
            LOG.info("Creating backup for collection")
            col_name = app.getConf("collection")
            updateSystem.collection_back_up(col_name)
            step += 1
            LOG.info("Starting step: " + str(step))  # 4
            # STEP 4: Store downloaded collection
            LOG.info("Overwriting collection with server data")
            css = col_data.get("customCSS")
            js = col_data.get("customJS")
            updateSystem.overwrite_collection_conf(col_name, col_conf_json, js, css)
            step += 1
            LOG.info("Starting step: " + str(step))  # 5
            # STEP 4.1: Summary of collection update
            is_there_an_update_collection = True
        step = 5
        # Now we check updates for products
        if "products_structure" in col_conf_json:
            # products_structure comes from the server
            LOG.info("products_structure comes from the server")
            products_structure = col_conf_json["products_structure"]
        else:
            # products_structure comes from local
            LOG.info("products_structure comes from local")
            products_structure = app.getCollectionConf("products_structure", True)
        # Checking also products even if there is no update for collection
        # STEP 5: Get all products from server for new collection data, if needed, backup and update them
        # The function get_products_from_products_structure downloads the product, creates a back up and saves the downloaded conf
        LOG.info("Updating products in collection: " + str(products_structure))
        products = app.get_products_from_products_structure(products_structure)
        LOG.info("Checking products to update: " + str(products))
        step += 1
        LOG.info("Starting step: " + str(step))  # 6
        fonts, is_there_an_update_products = updateSystem.update_products_from_server(products, cur_col_data.get("id_brand"))
        step += 1
        LOG.info("Starting step: " + str(step))  # 7
        # STEP 6: Check if all fonts are available and download missing fonts
        # Removing repeated fonts by creating a set
        fonts = list(set(fonts))
        LOG.info("Downloading missing fonts for products")
        updateSystem.download_missing_fonts_from_server(fonts)
        # CHECKING RESULT OF BOTH UPDATES, COLLECTION AND PRODUCTS
        if is_there_an_update_products or is_there_an_update_collection:
            LOG.info("Collection OR Products from server have an update available")
            res = {
                "status": "OK",
                "code": "001"
            }
            # STEP 7: Return summary of update
            app.change_static_version()
            LOG.info("The result of the update is: " + str(res))
        else:
            LOG.info("Collection from server does not have an update available")
            res = {
                "status": "OK",
                "code": "000"
            }
    except Exception as e:
        LOG.error("There was some error updating the collection in step [" + str(step) + "]: " + str(e))
        res = {
            "status": "KO",
            "code": "00" + str(step)
        }
    literal_id = "system_update_" + res["code"] + "_message"
    res["message"] = L.getLit(literal_id)
    return Response(json.dumps(res), mimetype='application/json')


def process_image_thread(base64img, pinMode=1, color=app.getConf("default_text_color"), colorAndPENConf=None):
    order_folder = app.getConfPath("order_folder")
    filename = order_folder + '/default.png'
    start_generate_image = time.time()
    LOG.info("img received!!!!!")
    imgdata = base64.b64decode(base64img)
    # Storing received image in /Order folder
    with io.open(filename, 'wb') as f:
        f.write(imgdata)
    end_generate_image = time.time()
    generate_image_time = end_generate_image - start_generate_image

    # Generation of the CNC file from the received image.
    start_cnc_time = time.time()
    if colorAndPENConf is None:
        colorAndPENConf = {}
        if "," not in color:
            color = app.getConf("default_text_color")
        colorAndPENConf[color] = pinMode.split(",")
    else:
        colorAndPENConf = json.loads(colorAndPENConf)
    print("colorAndPENConf:", colorAndPENConf)
    cnc_sub_times = Gcode.new_methods_image_to_cnc(filename, colorAndPENConf)

    end_cnc_time = time.time()
    cnc_time = end_cnc_time - start_cnc_time

    # Generation of the UNF file from the CNC file.
    start_unf_time = time.time()
    Gcode.createUnfFile(order_folder, "default")
    end_unf_time = time.time()
    unf_time = end_unf_time - start_unf_time
    LOG.info("TIME - generate image time:" + str(generate_image_time))
    LOG.info("TIME - CNC time:" + str(cnc_time))
    LOG.info("TIME - UNF time:" + str(unf_time))

    # In debug mode, we store the CNC and UNF times to measure performance
    if app.getConf("debug_node") == "True":
        app.storeTimesInCSV(cnc_time, unf_time, pinMode, cnc_sub_times)


# From an image, generates the CNC and UNF files
@bp.route('/image', methods=['POST', 'GET'])
def process_image():
    pinMode = request.form.get('pinMode')
    color = request.form.get('color')
    base64img = str(request.form.get('base64img_process')).replace("data:image/png;base64,", "")
    # Check if we have received the image
    try:
        if request.form.get('base64img_process') and request.form.get('pinMode'):
            # We remove old files in the order folder
            app.remove_order_files()
            # We thread so the browser doesnt get stuck with one request that takes 20 sec
            Thread(target=process_image_thread, args=(base64img, pinMode, color,)).start()
            # UNF file should be generated correctly
            return str(L.returnResponse(None))
        else:
            return str(L.returnResponse("003"))
    except Exception:
        LOG.error("ERROR processing image to CNC and to UNF:")
        LOG.error(str(traceback.format_exc()))
        return str(L.returnResponse("003"))


# Draws a predefined shape to find the focal distance of the laser and the rotation angle
@bp.route('/calibrateLaser', methods=['POST', 'GET'])
def calibrateLaser():
    # Check if we calibrate with red pointer or with CO2 laser
    if request.form.get('onlyRedPointer'):
        onlyRedPointer = request.form.get('onlyRedPointer') == "True"
    elif request.args.get('onlyRedPointer'):
        onlyRedPointer = request.args.get('onlyRedPointer') == "True"
    else:
        return str(L.returnResponse("009"))

    if onlyRedPointer:
        fileName = app.getConf("calibration_red_filename")
    else:
        fileName = app.getConf("calibration_CO2_filename")

    # Check if the unf files already exist
    calibrationFolder = app.getConfPath("Calibration_files_folder")

    # Check if the folder for calibration files exists
    if not os.path.isdir(calibrationFolder):
        os.makedirs(calibrationFolder)

    unfFile = calibrationFolder + "/" + fileName + ".unf"
    if os.path.exists(unfFile):
        # If the file exists, we send it to engrave
        print("FILE EXISTS")
        result = laser.sendUnfToLaser(unfFile)
    else:
        # If the file doesnt exist, we generate it and send it to engrave
        print("FILE DOESNT EXISTS")
        resultGcode = Gcode.drawCrossToCalibrateLaser(onlyRedPointer)

        if resultGcode["result"] == "OK":
            result = laser.sendUnfToLaser(unfFile)
        else:
            LOG.error(resultGcode["msg"])
            return str(resultGcode)
    return str(result)


# TODO improve this function
# Transform the PNG inside folder Order into CNC and then into UNF to send it to the laser
@bp.route('/defaulToUNF', methods=['POST', 'GET'])
def defaulToUNF():
    collection = "DWS/flyer"
    app.setCurrentProduct(collection)
    order_folder = app.getConfPath("order_folder")
    filename = order_folder + '/default.png'
    cnc_filename = order_folder + '/default.cnc'
    pinMode = 1
    # Uncomment this line if you want to engrave an image and not a CNC
    #Gcode.imageToCncFile(filename, cnc_filename, pinMode)
    Gcode.createUnfFile(order_folder, "default")
    result = laser.init()
    return str(result)


# Tries old failed requests to the server
@bp.route('/sendFailedRequests', methods=['POST', 'GET'])
def sendFailedRequests():
    server.tryOldFailedRequests()
    return "OK"


# Sends the UNF file of the rectangle containing the engraving.
@bp.route('/showRedRectangle', methods=['POST', 'GET'])
def showRedRectangle():
    turn_off_laser()
    unfRectangleFile = str(app.getConf("red_rectangle"))
    unfRectangleFilePath = str(app.getConfPath("order_folder")) + "/" + str(app.getConf("red_rectangle")) + ".unf"
    cncRectangleFilePath = str(app.getConfPath("order_folder")) + "/" + str(app.getConf("red_rectangle")) + ".cnc"
    print("cncRectangleFilePath:" + str(cncRectangleFilePath))

    if not os.path.exists(unfRectangleFilePath):
        LOG.info("UNF file doesnt exist, we will create it")
        count = 0
        while not os.path.exists(cncRectangleFilePath) and count < 90:
            LOG.info("UNF red rectangle file not found after " + str(count) + " seconds.")
            print("UNF red rectangle file not found after " + str(count) + " seconds.")
            waiting_time = 0.5
            count = count + waiting_time
            time.sleep(waiting_time)
        if not os.path.exists(cncRectangleFilePath):
            print("##unittest.case 1")
            return Response(str(L.returnResponse("006")), mimetype='application/json', status=500)
        else:
            unf_creation_result = Gcode.createUnfFile(str(app.getConfPath("order_folder")), unfRectangleFile)
            print(unf_creation_result)
            if unf_creation_result["result"] == "OK":
                # Since UNF generation went well, we delete the CNC file
                LOG.info("Removing unfFileCheck")
                os.remove(cncRectangleFilePath)
                result = laser.sendUnfToLaser(unfRectangleFilePath)
            else:
                LOG.error(unf_creation_result["msg"])
                return Response(str(unf_creation_result), mimetype='application/json', status=500)
    else:
        LOG.info("UNF file already existed")
        result = laser.sendUnfToLaser(unfRectangleFilePath)
        LOG.info(result["msg"])

    if result["result"] == "OK":
        status = 200
    else:
        status = 500
    print("##unittest.case 3")
    return Response(str(result), mimetype='application/json', status=status)

#Check mydws intenet connection
@bp.route('/ChecMydwsConnection', methods=['POST', 'GET'])
def ChecMydwsConnection():
    #url = app.getConf("healthCheck_url")
    url = app.getApiUrl("healthCheck_url", app.getConf("debug_node") != "True")
    print("url is ", url)
    result = {}
    try:
        # TODO move this GET request to Connection.py
        response = requests.get(url)
        status_code = response.status_code
        url_is_up = status_code == 200
        result["status_code"] = status_code
        result["response"] = response.text
        if(url_is_up):
            LOG.info("Mydws connected")
            print("status_code:", status_code)
            return Response(json.dumps(result), mimetype='application/json', status=status_code)
        else:
            LOG.info("Connection to Mydws failed")
            return Response(json.dumps(result), mimetype='application/json', status=status_code)
    except Exception as error:
        LOG.error("There is an error on ChecMydwsConnection function: "+str(error))
        return Response(json.dumps(result), mimetype='application/json', status=500)

#Check mydws teamviewer connection
@bp.route('/CheckTeamviewerConnection', methods=['POST', 'GET'])
def CheckTeamviewerConnection():
    url = app.getConf("teamviewer_ping_url")
    print("url is ", url)
    result = {}
    try:
        # TODO move this GET request to Connection.py
        response = requests.get(url)
        status_code = response.status_code
        url_is_up = status_code == 200
        result["status_code"] = status_code
        result["response"] = response.text
        if(url_is_up):
            LOG.info("TeamViewer connected")
            return Response(json.dumps(result), mimetype='application/json', status=status_code)
        else:
            LOG.info("Connection to TeamViewer failed")
            return Response(json.dumps(result), mimetype='application/json', status=status_code)
    except Exception as error:
        LOG.error("There is an error on CheckTeamviewerConnection function: " + str(error))
        return Response(json.dumps(result), mimetype='application/json', status=500)


# Screen that allows the user to see missing literals and edit them
@bp.route('/FixLiterals', methods=['POST', 'GET'])
def FixLiterals():
    originalFile = r"Literals.txt"#may be we need to put it in confproperty to be able vhanging it from there
    path = app.getConfPath("literals_path")
    LOG.info("pathe of ltterlas is "+path)
    main_dict = app.prepareGlobalData(path+"\\"+originalFile,path)
    if request.method == 'POST':
        ff = request.form
        ff = ff.to_dict(flat=False)
        logmessage="form recived from Fixliterlas web page is"+str(ff)
        LOG.info(logmessage.encode("utf-8"))
        M_dict = app.getMissingLiterals(ff)
        LOG.info("Missing Literlas are: \n ",M_dict)
        for key in M_dict:
            app.addMissingLiteralsToFile(M_dict.get(key),path+"//"+key)
            LOG.info("Missing Literals have been sent")
        return  redirect(url_for('web.views.dataManagement.FixLiterals'))
    return render_template("FixLiterals.html", main_dict=main_dict, originalFile=originalFile)




# Looks for the UNF file and when found, sends it to the laser
@bp.route('/engraveOrder', methods=['POST', 'GET'])
def engrave_order():
    turn_off_laser()
    order_folder = app.getConfPath("order_folder")
    unfFileCheck = order_folder + "/default.unfCHECK"
    LOG.info("Checking UNF file...")
    count = 0
    while not os.path.exists(unfFileCheck) and count < 90:
        waiting_time = 0.5
        count = count + waiting_time
        time.sleep(waiting_time)

    if not os.path.exists(unfFileCheck):
        LOG.error("UNF file not found after waiting MAX time " + str(count) + " seconds.")
        return str(L.returnResponse("003"))

    LOG.info("UNF file found after " + str(count) + " seconds.")
    LOG.info("UNF exists, laser.init()")
    result = laser.init()
    LOG.info("engrave order result:" + str(result))


    #result["result"] = "OK"


    if result["result"] == "OK":
        result["engravingTime"] = app.getOrderTempVariables("engravingTime")
        result["engravingTimeFormat"] = app.getOrderTempVariables("engravingTimeFormat")
    return str(result)



# Looks for the UNF file and when found, sends it to the laser
@bp.route('/healthCheckLaser', methods=['POST', 'GET'])
def health_check_laser():
    print("health_check_laser")
    wifi_is_ok = True
    laser_connection_is_ok = False
    laser_files_are_ok = False

    # TODO Check wifi
    if not wifi_is_ok:
        return str(L.returnResponse("010"))

    # Check laser connection
    IpAdress = app.getLaserIP()
    try:
        telnetTimeout = int(app.getConf("laser_Telnet_timeout"))
        tn = Telnet(IpAdress, timeout=telnetTimeout)
        laser_connection_is_ok = True
        tn.close()
        LOG.info("Tablet is connected to the wifi and laser is on")
    except Exception:
        laser_connection_is_ok = False
        LOG.error("Tablet is NOT connected to the wifi OR the laser is OFF")
    if not laser_connection_is_ok:
        return str(L.returnResponse("002"))

    # Check CNc and UNF files
    order_folder = app.getConfPath("order_folder")
    png_file = order_folder + "/default.png"
    cnc_file = order_folder + "/default.cnc"
    unf_file = order_folder + "/default.unf"
    unf_file_check = order_folder + "/default.unfCHECK"
    LOG.info("Checking files generation...")
    if not os.path.exists(png_file):
        LOG.error("PNG was not generated")
        return str(L.returnResponse("0031"))
    if not os.path.exists(cnc_file):
        LOG.error("CNC was not generated")
        return str(L.returnResponse("0032"))
    if not os.path.exists(unf_file):
        LOG.error("UNF was not generated")
        return str(L.returnResponse("0033"))
    if not os.path.exists(unf_file_check):
        LOG.error("UNF CHECK was not generated")
        return str(L.returnResponse("0033"))
    LOG.info("All files were found (PNG, CNC and UNF)")
    return str(L.returnResponse(None))
