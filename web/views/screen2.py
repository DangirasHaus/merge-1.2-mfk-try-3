from flask import Blueprint, render_template, request, redirect, url_for, session
import os
import Common.LogsDWS as LOG
import Common.functions as app
import Common.LiteralsDWS as L

bp = Blueprint(__name__, __name__, template_folder='templates')

@bp.route('/screen2', methods=['POST', 'GET'])
def show():
    LOG.info("INIT screen 2")
    debug_mode = app.getConf("debug_node")
    if request.form.get('product') or request.args.get('product'):
        product = request.form.get('product') or request.args.get('product')
        # Product volume to display in e-commerce
        if "/" in product:
            volume = product.split("/")[1]
        else:
            volume = product
        app.storeOrderTempVariables("product_name", product)
        LOG.info("Screen 2 product: " + str(product))
        textToEngrave = request.form.get('textToEngrave')
        ts = request.form.get('ts')
        clientOrderId = request.form.get('clientOrderId')
        print("clientOrderId:",clientOrderId)
        fontToEngrave = request.form.get('font')
        color = request.form.get('color')
        pictogram = request.form.get('pictogram')
        clientProdName = request.form.get('clientProdName')
        clientProductId = request.form.get('clientProductId')
        position = request.form.get('position')
        loaded_keyboard = request.form.get('keyboard')
        loaded_keyboard_extra_conf = request.form.get('keyboardConf')
        # INIT fichier info data
        if app.setCurrentProduct(product):
            productData = app.getProductData()
            if textToEngrave is None:
                textToEngrave = ""
            if fontToEngrave is None:
                fontToEngrave = ""
            product_imageName = productData.get("ImageName")
            LOG.info("product_imageName is "+product_imageName)
            pathimg = app.get_products_folder_conf() + '/' + product + "/Pictures/" + product_imageName
            textSize = 2
            font_list = [item for item in productData.get("fonts") if not item.get("hide") == "True"]
            ''' DATA NEEDED'''
            fullpathimg = app.get_products_folder_path() + '/' + product + "/Pictures/" + product_imageName
            inputs, intermedios, outputs = app.calculate_data(fullpathimg)
            keyboardList = app.selectKeyboardList()
            if loaded_keyboard is None:
                loaded_keyboard = keyboardList[0]["value"]
            if loaded_keyboard_extra_conf is None:
                loaded_keyboard_extra_conf = ""
            defaultFont = app.get_default_font_for_keyboard(font_list)["police"]
            canvas_px = inputs["dmm"]/inputs["hatch"]

            # Font line spacing path
            flsp = app.get_line_spacing_fonts()
            # Fonts list configuration
            flc = app.get_fonts_configuration()
            # Training variables
            traineeID = request.form.get('traineeID')
            # If screen 2 is reloaded, we might get a None value, which will be transformed into string in the HTML
            if not traineeID:
                traineeID = ""
            engravingTrainingDuration = request.form.get('engravingTrainingDuration')

            # If the color of the text is white, we add this color to the pictograms URL
            picto_apend = ""
            if "white" == inputs["text_color"]:
                picto_apend = "_white"
            # Setting classes for fonts
            for font in font_list:
                font["keyboard_list"] = font_keyboard_list_to_class(font.get("police"), flc)
            # Checking if e-commerce mode is active
            # Admin Mode Active
            ama = app.e_commerce_admin_active()
            # add logic in html and js
            allow_edition = not app.e_commerce_active() or ama
            e_commerce_active = app.e_commerce_active()
            return render_template('screen2.html', pathimg=pathimg, product=product, textToEngrave=textToEngrave,
                                   inputs=inputs, canvas_px=canvas_px, textSize=textSize, flsp=flsp, traineeID=traineeID,
                                   fontToEngrave=fontToEngrave, loaded_keyboard=loaded_keyboard, ts=ts,clientOrderId=clientOrderId,
                                   picto_apend=picto_apend, e_commerce_active=e_commerce_active,
                                   loaded_keyboard_extra_conf=loaded_keyboard_extra_conf,debug_mode=debug_mode, ama=ama,
                                   color=color, position=position, clientProductId=clientProductId,
                                   clientProdName=clientProdName, volume=volume,
                                   engravingTrainingDuration=engravingTrainingDuration, allow_edition=allow_edition,
                                   staticversion=app.getStaticVersion(), defaultFont=defaultFont, keyboardList=keyboardList,
                                   intermedios=intermedios, literals=L.getAllLits(), font_list=font_list)
        # Product not found
        url = request.referrer + "?errorcode=e01"
        return redirect(url)
    else:
        return redirect(url_for('web.views.screen1.show'))


def font_keyboard_list_to_class(font, font_list_conf):
    font_class = ""
    if not font_list_conf.get(font) or not font_list_conf.get(font).get("keyboard_list"):
        font_class = "all-keyboards"
    else:
        for keyboard in font_list_conf.get(font).get("keyboard_list"):
            font_class = font_class + " " + keyboard
    return font_class
