from flask import Blueprint, jsonify, request
import Common.LogsDWS as LOG
import Common.functions as app
import os.path
from flask import send_file
bp = Blueprint(__name__, __name__, template_folder='templates')


@bp.route("/download/<path:filename>")
def download(filename):
    full = app.getConfPath("main_folder") + filename
    if os.path.isfile(full):
        LOG.info("Serving file: " + str(full))
        return send_file(full)
    else:
        #LOG.error("Serving file 404: " + str(full))
        message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
        }
        resp = jsonify(message)
        resp.status_code = 404
        return resp


@bp.route("/customStatic/<path:filename>")
def customStatic(filename):
    full = app.getConfPath("collection_static_folder") + "/" + filename
    common_file = app.getConfPath("static_folder") + "/" + filename
    if os.path.isfile(full):
        LOG.info("Serving custom file: " + str(full))
        return send_file(full)
    elif os.path.isfile(common_file):
        LOG.info("Serving common file: " + str(common_file))
        return send_file(common_file)
    else:
        LOG.error("Serving custom file 404: " + str(full))
        message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
        }
        resp = jsonify(message)
        resp.status_code = 404
        return resp
