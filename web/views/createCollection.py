from flask import Blueprint, render_template, request, redirect, url_for
import math, time, os, json
import Common.LogsDWS as LOG
import Common.functions as app
import Common.LiteralsDWS as L
import Common.ServerConnection.DTOS as dto
import Common.ServerConnection.Connection as server
from shutil import copyfile


######literals system (c'est un systeme qui permet pour n'apporte quelle utilisateur de
###modifier le contenue de la l'interface comme "select your product" et ceci a partir d'un fichier txt Lirerals.txt

############################################################

bp = Blueprint(__name__, __name__, template_folder='templates')

col_fol_needed = ["Order", "Ratings", "Static/images"]
col_fil_needed = [app.getConf("collection_conf_filename"), "Static/custom.js", "Static/custom.css"]




col_params = app.get_collection_creation_needed_parameters()

def createCollectionFoldersAndFiles(data):
    response = {"result": "OK", "msg": "", "code": "", "errorName": "", "errorInstr": ""}
    col_folder_name = data.get("id_brand") + "_" + data.get("id_shop")
    col_folder_path = app.getConfPath("collections_folder") + "/" + col_folder_name
    if not os.path.exists(col_folder_path):
        # CREATING FOLDERS AND EMPTY FILES
        os.makedirs(col_folder_path)
        for fol in col_fol_needed:
            os.makedirs(col_folder_path + "/" + fol)
        for file in col_fil_needed:
            with open(col_folder_path + "/" + file, "w+"):
                LOG.info("creating file:" + str(file))

        # FILLING DATA IN COLLECTION CONF FILE
        with open(col_folder_path + "/" + col_fil_needed[0], "w+") as f:
            f.seek(0)
            json.dump(data, f, indent=2)
            f.truncate()
            '''for param in col_params:
                str_write = param["name"] + "=" + data.get(param["name"]) + "\n"
                f.write(str_write)'''

    else:
        response["result"] = "KO"
        response["msg"] = "Collection folder already exists"
    return response



@bp.route('/createCollection', methods=['POST', 'GET'])
def show():
    debug_mode = app.getConf("debug_node")
    static_version = app.getStaticVersion()
    res = {}
    # Creating collection
    if request.form.get('id_brand') and request.form.get('id_shop'):
        # We check if the brand exists on the server. If it doesnt, we dont create the collection
        res_brand = server.checkIfBrandAndShopExist(request.form.get('id_brand'), request.form.get('id_shop'))
        print("Result checkIfBrandAndShopExist: " + str(res_brand))
        if res_brand["result"] is not "OK":
            res = res_brand
        else:
            # We transform the form into the JSON format
            data = {}
            for elem in request.form:
                if request.form.get(elem) != "":
                    if elem == "products_structure":
                        data[elem] = json.loads(request.form.get(elem))
                    else:
                        data[elem] = request.form.get(elem)
            # We create the folders and files. We also create the products on the server
            res_folders = createCollectionFoldersAndFiles(data)
            print("Result createCollectionFoldersAndFiles: " + str(res_folders))
            if res_folders["result"] is not "OK":
                res = res_folders
            else:
                res = {"result": "OK", "msg": "Everything was created correctly", "code": "", "errorName": "", "errorInstr": ""}
                res["errorInstr"] = "Things to do by hand: 1) Create proper images for products. " \
                   "2) Upload the images to the angular page. 3) Fill FONT and LASER parameters in fichier info JSON. "


    # Form to create collection
    return render_template('createCollection.html', literals=app.initLiterals(), col_params=col_params, result=res)


# Page to send the products of a collection to be stored in the server
@bp.route('/sendProductsToProd', methods=['POST', 'GET'])
def sendToProd():
    debug_mode = app.getConf("debug_node")
    static_version = app.getStaticVersion()
    # This part of the code is to execute only the first time
    PROD = True if request.form.get('prod') else False
    LOG.info("Send products to AWS")
    LOG.info("Send to PROD? " + str(PROD))
    # If we receive the path to mydws folder in computer, we copy the image of te product into the MYDWS products folder
    path_to_mydws = request.form.get('path_to_mydws')
    if request.form.get('do_all_products'):
        LOG.info("do_all_products")
        from pathlib import Path
        for path in Path(app.getConfPath("products_id_folder")).rglob(app.getConf("fichier_info_filename")):
            if "Pictures" in os.listdir(os.path.dirname(path)) and "PENDING" not in str(path):
                prod_id = str(path).split("Products")[1].replace("\\fichierinfo.json", "")
                LOG.info("Storing prod id:" + str(prod_id))
                res = server.storeProdInDB(prod_id, PROD, path_to_mydws)
                LOG.info("Result:" + str(res))
                time.sleep(0.5)
        res_final = res
        return str(res_final)
    elif request.form.get('prod_folder'):
        res = server.storeProdInDB(request.form.get('prod_folder'), PROD, path_to_mydws)
        return str(res)


    # Form to create collection
    return render_template('sendProductsToProd.html', literals=app.initLiterals(), send_prod_params=send_prod_params)


send_prod_params = [{
    "class": "required",
    "name": "prod_folder",
    "td_text": "Product ID",
    "placeholder": "ID of the product to send",
    "default_value": ""
}]

