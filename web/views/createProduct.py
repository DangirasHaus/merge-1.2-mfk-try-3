from flask import Blueprint, render_template, request, redirect, url_for
import math, time, os, json
import Common.LogsDWS as LOG
import Common.functions as app
import Common.LiteralsDWS as L
import Common.ServerConnection.DTOS as dto
import Common.ServerConnection.Connection as server
from shutil import copyfile
from PIL import Image

######literals system (c'est un systeme qui permet pour n'apporte quelle utilisateur de
###modifier le contenue de la l'interface comme "select your product" et ceci a partir d'un fichier txt Lirerals.txt
literals = app.initLiterals()
############################################################
debug_mode = app.getConf("debug_node")
static_version = app.getStaticVersion()

bp = Blueprint(__name__, __name__, template_folder='templates')

prod_fol_needed = ["Pictures", "Other pictures"]
prod_fil_needed = ["fichierInfo.json"]


prod_params = app.get_product_creation_needed_parameters() + app.get_product_creation_needed_laser_parameters()

def createProductFiles(prod_data):
    #products_folder = app.getConfPath("products_folder")
    # TODO pending tests
    products_folder = app.get_products_folder_path()
    res = {"result": "KO", "msg": "", "code": "", "errorName": "", "errorInstr": ""}

    if prod_data.get("product_category_id"):
        # Product is inside category
        prod_id = prod_data.get("product_category_id") + "/" + prod_data.get("product_id")
    else:
        # Single product
        prod_id = prod_data.get("product_id")
    id_brand = prod_data.get("id_brand")
    prod_folder = products_folder + "/" + prod_id
    if not os.path.exists(prod_folder):
        LOG.info("Creating product folder: " + str(prod_folder))
        os.makedirs(prod_folder)
        if prod_data.get("product_category_id"):
            fichier_info_category_path = products_folder + "/" + prod_data.get("product_category_id") + "/" + prod_fil_needed[0]
            app.createCategoryFichierInfo(fichier_info_category_path, prod_data.get("product_category_name"), id_brand)
        for folder in prod_fol_needed:
            os.makedirs(prod_folder + "/" + folder)

            LOG.info("Copying fichier info: " + str(prod_folder))
        fichier_info_path = prod_folder + "/" + prod_fil_needed[0]
        # Creating fichier info JSON for product
        app.createProductFichierInfo(fichier_info_path, prod_data)
        pictures_path = prod_folder + "/" + prod_fol_needed[0]
        prod_width, prod_height = prod_data.get("dimensions").split(",")
        # Creating image for product
        app.createImageForProduct(pictures_path, prod_width, prod_height)




        # Creation of product in database
        res_prod_database = server.storeProdInDB(prod_id)



        if res_prod_database["result"] is not "OK":
            res["errorInstr"] = "Product could not be stored in the server"
        else:
            res["errorInstr"] = "Product stored in server correctly"

        res["result"] = "OK"
        res["msg"] = "Product created correctly. Now you have to parametrize the laser and set the image of the product"

    else:
        print("Product already exists")
        res["msg"] = "Product already exists"
        res["errorInstr"] = "The product ID and category ID already exists, check that the IDs are correct."
    return res












@bp.route('/createProduct', methods=['POST', 'GET'])
def show():
    # Form to create collection
    show_names = request.form.get('show_names') if request.form.get('show_names') else True
    res = {}
    if request.form.get('product_id'):
        try:
            res = createProductFiles(request.form)
        except:
            res = {"result": "KO", "msg": "There was an error creating product", "code": "", "errorName": "",
                   "errorInstr": "Try again, if the error persists, contact and administrator"}
    return render_template('createProduct.html', literals=literals, prod_params=prod_params, show_names=show_names,
                           result=res)


