from datetime import date, datetime
from flask import Blueprint, render_template, request, redirect, url_for
import math, time, os
import Common.LogsDWS as LOG
import Common.functions as app
import Common.LiteralsDWS as L
import Common.ServerConnection.Connection as server
import Common.LancerCommande as laser
import Common.training as training

######literals system (c'est un systeme qui permet pour n'apporte quelle utilisateur de
###modifier le contenue de la l'interface comme "select your product" et ceci a partir d'un fichier txt Lirerals.txt


# init the literals


############################################################

bp = Blueprint(__name__, __name__, template_folder='templates')


@bp.route('/screen1', methods=['POST', 'GET'])
def show():
    # Setting admin mode to false
    app.change_e_commerce_admin_state(False)
    ##### INIT values
    debug_mode = app.getConf("debug_node")
    static_version = app.getStaticVersion()
    try:
        langlist = app.getLangList()
    except Exception as e:
        LOG.error("Error loading collection")
        return "NOM de COLLECTION INCORRECTE"
    #dict_products = app.obtain_products_structure()
    # filter products basing on expiration date of display
    current_date = date.today()
    temp_dict_products = app.obtain_products_structure()
    dict_products = []
    print("current_date %s" % current_date)
    print("temp_dict_products %s" % temp_dict_products)
    for it_product in temp_dict_products:
        can_add_products = True
        if "expiration_display" in it_product:
            try:
                expiration_display_converted = datetime.strptime(it_product["expiration_display"], "%Y-%m-%d").date()
                if current_date >= expiration_display_converted:
                    can_add_products = False
            except Exception as exc:
                pass
        if can_add_products == True:
            dict_products.append(it_product)

    show_categories = len(dict_products) != 1
    ##### END of INIT values
    do_general_training = app.do_general_training()
    trainees = []
    new_lang = request.args.get('new_lang')
    if new_lang is not None:
        app.changeLiteralLang(new_lang)
    # For later engraving trainings we send this parameter
    do_engraving_training = request.args.get('do_engraving_training') == "true" or training.do_engraving_training()
    allowCloseModal = request.args.get('allowCloseModal') == "true"

    VersionAndIp = app.getConf("code_version") + app.getIpAdress() + ":" + app.getTeamviewerID()
    screen_saver_time = app.getCollectionConf("screen_saver_time")
    LOG.info("INIT screen 1")
    app.resetProductData()
    order_folder = app.getConfPath("order_folder")
    unfFileCheck = order_folder + "/default.unfCHECK"
    if os.path.exists(unfFileCheck):
        LOG.info("Removing unfFileCheck")
        os.remove(unfFileCheck)
    errorcode = request.args.get('errorcode')
    if not app.getConf("id_machine"):
        if not app.checkLaserConfFile():
            if laser.is_laser_on():
                errorcode = "203"
            else:
                errorcode = "202"

    option = request.args.get('option')
    n_trainees = request.form.get('n_trainees')

    # If there is no file trainees.json and we are not just receiving them, we redirect to /training
    if do_general_training and n_trainees is None:
        return redirect(url_for('web.views.training.show'))

    # We just got new trainees to store
    if n_trainees is not None:
        do_engraving_training = True
        # TODO add response to this function to check if everything went alright
        training.add_recevied_trainees(request.form)

    # DISPLAY 0: Restart or Shutdown
    if option is not None:
        if str(option) == "shutdown":
            LOG.info("Retrying failed requests...")
            # Retrying failed requests to the server
            server.tryOldFailedRequests()
        return app.screen1TabletOption(option)

    switchMode = request.args.get('switchMode') == "True"
    if switchMode:
        app.switch_e_commerce_active_state()

    # DISPLAY 0.1: Redirect to e-commerce mode
    print("app.e_commerce_active():", str(app.e_commerce_active()))
    if app.e_commerce_active():
        return redirect(url_for('web.views.ecommerce1.show'))
    # Allow switch modes between e-commerce and in-store
    awm = app.allow_switch_modes()

    # DISPLAY 1: Error received display
    if errorcode is not None:
        print(" DISPLAY 1 ")
        try:
            LOG.info("errorcode:" + str(errorcode))
            errorIntrs = L.getErrorInstr(errorcode)
        except:
            LOG.info("errorcode contained non UTF characters")
            errorIntrs = ["Restart the tablet"]
        return render_template('screen1.html', staticversion=static_version, show_categories=show_categories,
                               dict_products=dict_products, screen_saver_time=screen_saver_time,
                               literals=L.getAllLits(), langlist=langlist, errorcode=errorcode, errorIntrs=errorIntrs,
                               debug_mode=debug_mode, VersionAndIp=VersionAndIp, awm=awm, current_date=current_date)
    # DISPLAY 2: Do engraving training
    # We will display the popup to select trainee and then the onboarding help for them
    elif do_engraving_training:
        print(" DISPLAY 2 ")
        trainees = training.get_all_trainees()
        return render_template('screen1.html', dict_products=dict_products, show_categories=show_categories,
                               screen_saver_time=screen_saver_time, trainees=trainees, langlist=langlist,
                               do_engraving_training=do_engraving_training, allowCloseModal=allowCloseModal,
                               literals=L.getAllLits(), staticversion=static_version, debug_mode=debug_mode,
                               VersionAndIp=VersionAndIp, awm=awm, current_date=current_date)
    # DISPLAY 3: Regular display
    else:
        new_lang = request.args.get('new_lang')
        if new_lang is None:
            print(" DISPLAY 3.1 ")
            # DISPLAY 3.1: Regular display
            #    return redirect(url_for('web.views.screen2.show', product=dict_products[0]["folderName"]), code="307")
            return render_template('screen1.html', dict_products=dict_products,
                                   do_engraving_training=do_engraving_training,
                                   screen_saver_time=screen_saver_time, show_categories=show_categories, awm=awm,
                                   literals=L.getAllLits(), langlist=langlist, staticversion=static_version,
                                   debug_mode=debug_mode, VersionAndIp=VersionAndIp, current_date=current_date)
        else:
            print(" DISPLAY 3.2 ")
            # DISPLAY 3.2: Regular display changing language
            return render_template('screen1.html', dict_products=dict_products, show_categories=show_categories,
                                   new_lang=new_lang, screen_saver_time=screen_saver_time, awm=awm,
                                   do_engraving_training=do_engraving_training, langlist=langlist,
                                   literals=app.changeLiteralLang(new_lang), staticversion=static_version,
                                   debug_mode=debug_mode, VersionAndIp=VersionAndIp, current_date=current_date)
