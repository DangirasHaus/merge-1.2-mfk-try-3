from flask import Flask, redirect, url_for
from flask import jsonify, request
from web.views.dataManagement import bp as dataManagement_bp
from web.views.downloads import bp as donwload_bp
from SAFE.views.safe import bp as safe_bp
import Common.LogsDWS as LOG
import Common.functions as functions

app = Flask(__name__)

app.register_blueprint(dataManagement_bp)
app.register_blueprint(donwload_bp)
app.register_blueprint(safe_bp)


@app.errorhandler(404)
def page_not_found(e):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp


@app.errorhandler(500)
def error500(e):
    LOG.error("Error 500 detected: " + str(e))
    return redirect(url_for('web.views.screen1.show', errorcode="500"))

