function validateToken(){
	var token = $('input[name=token]').val();
	console.log("AJAX to check token validity")
	console.log(token)
    var ajax_data = {
	    "token" : token
    };
    $.ajax({
        type : 'POST',
        url : "./validateToken",
        data : ajax_data,
        beforeSend : function() {
        },
        success : function(data) {
            console.log(data);
            if(data["status"] == "OK"){
                console.log("Token is valid");
                $('.valid-token').show();
                $('.invalid-token').hide();
                var userInfo = "Welcome " + data["firstName"] + " " + data["lastName"]
                $('#user-info').html(userInfo)
            } else {
                console.log("Error validating token");
                $('.valid-token').hide();
                $('.invalid-token').show();
            }
        },
        error : function(request, status, error) {
        }
    });
}

function goToScreen1(){
	location.href = "/safe1"
}

function filterProducts(){
	var idBrand = $('input[name=filter-idBrand]').val();
	var idProduct = $('input[name=filter-idProduct]').val();
	var name = $('input[name=filter-name]').val();
	var finished = $('input[name="filter-finished"]:checked').val();
	$('.product-grid').hide();
	var jqueryCondition = "div"
	if(idBrand) jqueryCondition += '[class*="' + idBrand + '"i]';
	if(idProduct) jqueryCondition += '[class*="' + idProduct + '"i]';
	if(name) jqueryCondition += '[class*="' + name + '"i]';
	if(finished == "finished"){
		jqueryCondition += ':not(.not-finished)';
	} else if(finished == "not-finished"){
		jqueryCondition += '[class*="not-finished"i]';
	} else if(finished == "not-final-version"){
		jqueryCondition += ':not(.final-version)';
	} else if(finished == "final-version"){
		jqueryCondition += '[class*="final-version"i]';
	}
	$(jqueryCondition).show();
}

function removeFilters(){
	$('input[name=filter-idBrand]').val("");
	$('input[name=filter-idProduct]').val("");
	$('input[name=filter-name]').val("");
	$('.product-grid').show();
}

// Radio button filters
function radioButtonFilter(){
	filterProducts();
}

function collapseAll(){
	$('.in').removeClass("in");
}

function markLaserPinForFont($btn){
	var target = $btn.attr("data-target");
	target += " input[id^=laserPin]";
	var targetVal = $(target).val();
	markLaserPin(targetVal)
}

function markLaserPin(targetVal){
	$('button[id^=laser-btn-]').removeClass("btn-info");
	for(let PEN of targetVal.split(",")){
		var laserButton = "#laser-btn-" + PEN;
		$(laserButton).addClass("btn-info");
		if($(laserButton).length == 0) alert("No laser configuration for written pen " + PEN + "!")
	}
}

$(document).ready(function(){
	// Definitions for screen safe1 only
	if(location.href.indexOf("safe1") != -1) {
		// Filtering products when clicking enter
		$('input').each(function(){
			$(this).keydown(function(event) {
				if ( event.which == 13 ) {
					filterProducts();
				}
			});
		});
	} else if(location.href.indexOf("safe2") != -1){
		// Loading tool tips
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		})
		// Gets values to display canvas
		getIntermedios();
		// Setting font
		var selectedFont = $(".fonts-container .btn.btn-info").attr("data-target");
		var fontName = $(selectedFont).find("input[id*=policeName]").val();
		engravingFontName = "engravingFont" + fontName;
		// Checking input values
		$('input[type=number]').on("input",function() {
			var max = parseInt($(this).attr("max"));
			var min = parseInt($(this).attr("min"));
			var value = parseInt($(this).val());
			if(min <= value && value <= max){
				$(this).parent().removeClass("has-error");
			} else {
				$(this).parent().addClass("has-error");
			}
		});

		// Keeping one font or laser parameter selected
		$('.fonts-container button[data-toggle="collapse"]').on("click",function() {
			// Display
			$(this).parent().find(".btn-info").removeClass("btn-info");
			$(this).addClass("btn-info");
			markLaserPinForFont($(this));
			var selectedFont = $(".fonts-container .btn.btn-info").attr("data-target");
			// Setting color
			var fontColor = $(selectedFont).find("input[id*=color]").val();
			if(fontColor) {
				$('input[name=textColor]').val(fontColor);
			} else {
				$('input[name=textColor]').val($('input[id=text_color]').val());
			}
			// Setting font
			var fontName = $(selectedFont).find("input[id*=policeName]").val();
			engravingFontName = "engravingFont" + fontName;

	        setTimeout(function(){
				writeTextInCanvas($('#textToEngrave').val());
	        }, 100);
		});

		// If laser pin changes in font, we need to change laser parameter pin
		$('input[id^=laserPin]').on("change", function(){
			markLaserPin($(this).val())
		});

		// Selecting first button laser pin parameters
		markLaserPinForFont($('.fonts-container button.btn-info'));

		// Hides collapse elements to only allow one at a time
		jQuery('button').click( function(e) {
		    // Hides all collapse elements
		    //jQuery('.collapse').collapse('hide');
		    // Hides only siblings collapse elements
		    $(this).parent().find('.collapse').collapse('hide');
		});

		// Automatically generate written text
		$('#textToEngrave').on("keyup",function(){
			console.log("keyup value to:", $(this).val());
			writeTextInCanvas($(this).val());
			// When we update parameters, we hide result texts from last engraving:
			$('#result-engrave').hide();
		});
		// If input changes, we update the main product data object
		assignEventsToInputs();
	} else if(location.href.indexOf("safeGrid") != -1){
		// Gets values to display canvas
		getIntermedios();
		// If input changes, we update the main product data object
		assignEventsToInputs();
		updateLaserParameters();
	}

	// Filter fonts
	$('#filter-fonts').keyup(function(){
		filterDisplayedFonts($(this).val());
	});
});

function assignEventsToInputs(){
	// If input changes, we update the main product data object
	$('input:not(#textToEngrave,[type=checkbox],.no-get-intermedios), textarea, select:not(.no-get-intermedios)').on("change", function(){
		console.log("Changing:", $(this))
		console.log("With value:", $(this).val())
		console.log("Structure to edit:", $(this).attr("name"));
		var structure = $(this).attr("name");
		// If laser use new CNC we send boolean
		if(/use_new_cnc_method/i.test(structure) || /diagonal_engraving/i.test(structure)){
			_.set(product_data, structure, $(this).val() == "True");
		} else {
			_.set(product_data, structure, $(this).val());
		}
		// When we update parameters, we hide result texts from last engraving:
		$('#result-engrave').hide();
		// Every time we update data, we redraw the canvas
		getIntermedios();
	});

	// Events to update the laser parameters
	$('select.grid-events').on("change", function(){
		updateLaserParameters();
	});

	// events to update the canvas in grid
	$('input.grid-events').on("change", function(){
		showPatternGrid();
	});
}


function writeTextInCanvas(textVal){
	var selectedFont = $(".fonts-container .btn.btn-info").attr("data-target");
	var input_fontsize_min = $(selectedFont).find("input[id*=fontsize-]").val();
	var input_fontsize_max = $(selectedFont).find("input[id*=fontsize_max]").val();
	console.log("selectedFont:", selectedFont)
	console.log("input_fontsize_min:", input_fontsize_min)
	console.log("input_fontsize_max:", input_fontsize_max)
	// Font size max is optional, if not defined, it is equal to min
	if(!input_fontsize_max) input_fontsize_max = input_fontsize_min;
	if(parseFloat(input_fontsize_min) <= parseFloat(input_fontsize_max)){
		fontsize_min = obtainFontSizeGivenMM(vertical_rapport, input_fontsize_min);
		fontsize = fontsize_min;
		fontsize_max = fontsize_min
		// If font size max is the same as min, we dont need to recalculate font size
		if(input_fontsize_max != input_fontsize_min){
			fontsize_max = obtainFontSizeGivenMM(vertical_rapport, input_fontsize_max);
		}
		if(isTextInBoundaries(textVal)){
			console.log("GOOD!");
			SAFEtextToEngravePreview(textVal);
		}
	} else {
		alert("fontsize max cant be lower than fontsize min")
	}
}

var onlyStoreParams = false;

// Sends the current laser parameters defined on the page to create an engraving file
function sendEngrave(redRectangle = false){
	console.log("sendEngrave");
	var selectedFont = $(".fonts-container .btn.btn-info").attr("data-target");
	var pinMode = $(selectedFont).find("input[id*=laserPin]").val();
	var color = $('input[name=textColor]').val();
	var font = $(selectedFont).find("input[id*=police-]").val()
	var text = $('#textToEngrave').val();
	if(text != "" || onlyStoreParams){
		showEngraveResults(true, false, false)
		// Global variable product_data
	    generateBase64img("printCanvas", "base64img_process");
	    var ajax_data = {
		    "product_data" : JSON.stringify(product_data),
		    "pinMode": pinMode,
		    "color": color,
		    "img64": $('input[name="base64img_process"]').val(),
		    "redRectangle": redRectangle
		};
	    $.ajax({
	        type : 'POST',
	        url : "./engraveSAFEOrder",
	        data : ajax_data,
	        beforeSend : function() {
	        },
	        success : function(data) {
	            console.log(data);
				$('#open-comment-popup').click();
	            var ftpOk = data.resultLaser.result == "OK";
	            //var serverOk = data.resultServer.status == 200;
	            showEngraveResults(false, ftpOk, undefined);
	        },
	        error : function(request, status, error) {
	            console.log("engraveSAFEOrder error");
	        }
	    });
	} else {
		alert("Text must not be empty")
	}
}


function storeSAFEOrder(){
	console.log("sendEngrave");
	var selectedFont = $(".fonts-container .btn.btn-info").attr("data-target");
	var font = $(selectedFont).find("input[id*=police-]").val()
	var text = $('#textToEngrave').val();
	var storeIdProduct = $("input[id=store-id-product]").val();
	var engravingComment = $("input[id=engraving-comment]").val();
    var ajax_data = {
	    "product_data" : JSON.stringify(product_data),
	    "text": text,
	    "font": font
	};
	if(storeIdProduct) ajax_data["storeIdProduct"] = storeIdProduct;
	if(engravingComment) ajax_data["engravingComment"] = engravingComment;
    $.ajax({
        type : 'POST',
        url : "./storeSAFEOrder",
        data : ajax_data,
        beforeSend : function() {
        },
        success : function(data) {
            console.log(data);
            var serverOk = data.resultServer.status == 200;
            showEngraveResults(false, undefined, serverOk);
        },
        error : function(request, status, error) {
            console.log("storeSAFEOrder error");
        }
    });
}

// Gets intermedios
function getIntermedios(){
	// Global variable product_data
    var ajax_data = {
	    "product_data" : JSON.stringify(product_data)
	    };
    $.ajax({
        type : 'POST',
        url : "./getIntermedios",
        data : ajax_data,
        beforeSend : function() {
        },
        success : function(data) {
            console.log(data);
            $('#canvas-container').css("bottom", data["laserpoint"]);
            $('#canvas-container').css("height", data["squareheight"]);
            $('#canvas-container').css("width", $('#canvas-container').css("height"));
            $('#textCanvas').attr("height", $('#canvas-container').css("height"));
            $('#textCanvas').attr("width", $('#canvas-container').css("height"));
            setDefaultHeight(data["default_height"])
            vertical_engraving = product_data.parameters.engraving_angle;
            vertical_rapport = data.vertical_rapport / $('img.picture').height();
			maxHeightMM = product_data.parameters.maxHeightMM;
			maxWidthMM = product_data.parameters.maxWidthMM;
			$('input[name=textColor]').val(product_data.parameters.text_color)
			// Getting smallest hatch for product
			hatch = getSmallestHatchForProduct(product_data.parameters.laser, product_data.parameters.hatch)
			// Setting print canvas dimensions
			var printCanvasDim = 60/parseFloat(hatch)
			$('#printCanvas').attr("width", printCanvasDim);
			$('#printCanvas').attr("height", printCanvasDim);
			canvasFactor = parseFloat($('#printCanvas').attr("width"))/parseFloat($('#textCanvas').attr("width"));
			if(location.href.indexOf("safe2") != -1){
				writeTextInCanvas($('#textToEngrave').val())
			}
        },
        error : function(request, status, error) {
            console.log("engraveSAFEOrder error");
            alert("Error getting parameters for product. Check if there is a correct picture for the product")
        }
    });
}


function getSmallestHatchForProduct(lasers, hatch){
	var minHatch = hatch;
	for(let laser of lasers){
		if("hatch" in laser && parseFloat(laser["hatch"]) < minHatch){
			minHatch = parseFloat(laser["hatch"]);
		}
	}
	return minHatch;
}

function setDefaultHeight(init_default_height){
    default_height = init_default_height * $('img.picture').height();
    var canvas_element = document.getElementById("textCanvas")
    var rect = canvas_element.getBoundingClientRect();
    var canvas_orig = [rect.left, rect.top];

    // Origin of coordinates of the image
    img_element = document.getElementsByClassName("picture")[0]
    var rect = img_element.getBoundingClientRect();
    var img_orig = [rect.left, rect.bottom];

    // Change of coordinates origin
    origin_move = [img_orig[0] - canvas_orig[0], img_orig[1] - canvas_orig[1]]
    default_height = -default_height + origin_move[1];
}




function SAFEtextToEngravePreview(value, logoUrl){
    // Prints text in image
    var previousValue = $("#textToEngrave").attr("previous-val");
    createImageForDisplay(value,fontsize, undefined, default_height, "printCanvas");
    removeElementsWhenWriting(previousValue);
    $("#textToEngrave").attr("previous-val", value);
}


function storeProductInServer(){
	var promptText = "You are about to send this configuration to the server and it will be stored there.\n\n" +
	"It will overwrite the product parameters for all shops contaning this product!!\n\n" +
	"Are you sure?"
	var sendProduct = confirm(promptText);
	if(sendProduct){
		console.log("storeProductInServer YES")
		sendProdToStore()
	} else {
		console.log("storeProductInServer NO")
	}
}


function sendProdToStore(){
    var ajax_data = {
	    "prod_id" : product_data["idProduct"],
	    "parametrizationFinished": $('input[name=parametrizationFinished]').prop('checked'),
	    "newUpdateAvailable": $('input[name=newUpdateAvailable]').prop('checked')
	};
    $.ajax({
        type : 'POST',
        url : "./safeSendProdToProd",
        data : ajax_data,
        beforeSend : function() {
        },
        success : function(data) {
            console.log(data);
            if(data["result"] != "OK"){
                alert("There was an error, talk to an administrator!");
                var msg = data.code + " | " + data.msg + " | " + data.errorName + " | " + data.errorInstr
                $("#Result").html(msg)
            } else {
                $("#Result").html("Product updated correctly")
                alert("Product updated correctly. The page will reload")
                document.location.reload(true)
            }
            $("#Result").show();
        },
        error : function(request, status, error) {
            console.log("engraveSAFEOrder error");
        }
    });
}


function showEngraveResults(loadingGif, ftpOk, serverOk){
	$('.result-engrave-all').hide();
	if(loadingGif) {
		$('#sending-engraving-loadingGif').show();
	} else {
		$('#sending-engraving-loadingGif').hide();
		if(typeof(ftpOk) !== "undefined"){
			if(ftpOk){
				$('#ok-sending-engraving').show();
			} else {
				$('#error-sending-engraving').show();
			}
		}
		if(typeof(serverOk) !== "undefined"){
			if(serverOk){
				$('#ok-storing-engraving').show();
			} else {
				$('#error-storing-engraving').show();
			}
		}
	}
	$('#result-engrave').show();
}

// Adds a new font
function addNewFont(){
	var fontConf = createJsonForNewFont();
	if(fontConf){
		console.log("adding new font:", fontConf);
		// Updating product conf in memory
		product_data.parameters.fonts.push(fontConf);
		// Storing conf property in local disk
		storeProdConf('.new-font-result');
		// Asking user to store changes in server to persist new font
	} else {
		alert("You must fill all mandatory items");
	}
}

// Stores the product configuration without engraving
function storeProdConf(resultSelector){
    var ajax_data = {
	    "product_data" : JSON.stringify(product_data)
	    };
    $.ajax({
        type : 'POST',
        url : "./storeProdConf",
        data : ajax_data,
        beforeSend : function() {
        },
        success : function(data) {
            console.log(data);
            $(resultSelector).removeClass("hidden");
        },
        error : function(request, status, error) {
            console.log("storeProdConf error");
        }
    });
}

// Filters the displated fonts on the pop up
function filterDisplayedFonts(filterText){
	$(".font-iter").addClass("hidden");
	$(".font-iter").each(function(){
		var text = $(this).find("a").html().toLowerCase();
		if(text.includes(filterText.toLowerCase())){
			$(this).removeClass("hidden");
		}
	});
}

// This functions opens the 2nd step to add a new font
function newFontSelected(fontFileName){
	$('.new-font-step-1').addClass("hidden");
	$('.new-font-step-2').removeClass("hidden");
    $('.new-font-result').addClass("hidden");
	$('.new-font-step-2 table input').each(function(){
		$(this).val("");
	});
	$('#new-font-file-name').val(fontFileName);

}

function backToSelectFont(){
	$('.new-font-step-2').addClass("hidden");
	$('.new-font-step-1').removeClass("hidden");
    $('.new-font-result').addClass("hidden");
}


function createJsonForNewFont(){
	var fontConf = {
        "fontsize": $("#new-font-fontsize").val(),
        "laserPin": $("#new-font-pin").val(),
        "police": $("#new-font-file-name").val(),
        "policeName": $("#new-font-name").val()
	}
	if($("#new-font-fontsize-max").val()) fontConf["fontsize_max"] = $("#new-font-fontsize-max").val();
	$('.new-font-step-2 table .required input').each(function(){
		if(!$(this).val()) {
			console.log("MANDATORY INPUT IS EMPTY")
			fontConf = false;
		}
	});
	return fontConf;
}

// Removes a font from the product
function removeFont(index){
	console.log("removing font in position:", index)
	var selector = ".fonts-" + index;
	product_data.parameters.fonts.splice(index,1);
	storeProdConf('.new-font-result');
	$(selector).remove();
	alert("Font removed. To keep changes store product in server")
}

// Removes a pint from the product
function removePin(index){
	console.log("removing pin in position:", index)
	var selector = ".pins-" + index;
	product_data.parameters.laser.splice(index,1);
	storeProdConf('.new-pin-result');
	$(selector).remove();
	alert("Pin removed. To keep changes store product in server")
}

// List of predefined set of laser parameters
var predefinedPinParameters = {
	9: {
        "description": "This is the warm up pin",
        "doublehatch": "1",
        "frequence": "80",
        "mark_delay": "50",
        "n_jump_lines": "1",
        "nbpassage": "25",
        "puissance": "70",
        "use_new_cnc_method": true,
        "vitesse": "3500",
        "pinLoad": "1"
	},
	8: {
        "description": "This is the protection mode pin",
        "doublehatch": "0",
        "frequence": "80",
        "mark_delay": "50",
        "n_jump_lines": "1",
        "nbpassage": "25",
        "puissance": "70",
        "use_new_cnc_method": false,
        "vitesse": "3500",
        "pinLoad": "1"
	},
}

// Fills the new pin parameters with a predefined set of parameters
function addPredefPin(pinId){
	var pinConf = predefinedPinParameters[pinId];
	console.log("pinConf:", pinConf);
	for(item in pinConf){
		var selector = "#new-pin-" + item;
		console.log(selector)
		$(selector).val(pinConf[item].toString());
	}
}

// Adds the new pin to the product data
function addNewPin(){
	var pinConf = createJsonForNewPin();
	if(pinConf){
		console.log("adding new pin:", pinConf);
		// Updating product conf in memory
		product_data.parameters.laser.push(pinConf);
		// Storing conf property in local disk
		storeProdConf('.new-pin-result')
		// Asking user to store changes in server to persist new font
	} else {
		alert("You must fill all mandatory items");
	}
}

// Creates the JSON for a new pin
function createJsonForNewPin(){
	var pinConf = {
        "description": $("#new-pin-description").val(),
        "use_new_cnc_method": $("#new-pin-use_new_cnc_method").val() == "true",
        "pinLoad": $("#new-pin-pinLoad").val()
	}
	if($("#new-pin-vitesse").val()) pinConf["vitesse"] = $("#new-pin-vitesse").val();
	if($("#new-pin-doublehatch").val()) pinConf["doublehatch"] = $("#new-pin-doublehatch").val();
	if($("#new-pin-frequence").val()) pinConf["frequence"] = $("#new-pin-frequence").val();
	if($("#new-pin-mark_delay").val()) pinConf["mark_delay"] = $("#new-pin-mark_delay").val();
	if($("#new-pin-n_jump_lines").val()) pinConf["n_jump_lines"] = $("#new-pin-n_jump_lines").val();
	if($("#new-pin-nbpassage").val()) pinConf["nbpassage"] = $("#new-pin-nbpassage").val();
	if($("#new-pin-puissance").val()) pinConf["puissance"] = $("#new-pin-puissance").val();
	if($("#new-pin-hatch").val()) pinConf["hatch"] = $("#new-pin-hatch").val();
	if($("#new-pin-hatch_angle").val()) pinConf["hatch_angle"] = $("#new-pin-hatch_angle").val();
	if($("#new-pin-protection_mode_pen").val()) pinConf["protection_mode_pen"] = $("#new-pin-protection_mode_pen").val();
	if($("#new-pin-wait_time").val()) pinConf["hatch"] = $("#new-pin-wait_time").val();
	if($("#new-pin-engraving_mode").val()) pinConf["engraving_mode"] = $("#new-pin-engraving_mode").val();
	$('.new-pin-step-1 table .required input').each(function(){
		if(!$(this).val()) {
			console.log("MANDATORY INPUT IS EMPTY")
			pinConf = false;
		}
	});
	return pinConf;
}

function closeModalComment(){
	$('#close-comment-modal').click();
	storeSAFEOrder();
}

function showRedRectangleSafe(){
	console.log("showRedRectangleSafe INIT");
	// redRectangle: true
	sendEngrave(true);
}

function updateColorFont(val){
	if(val.indexOf(",") != -1 && val.split(",").length == 4){
		$('input[name=textColor]').val(val)
	} else {
		alert("You must put an RGBA color separated by commas")
	}
}


// Creates the JSON for a new pin
function loadPenToNewPen(pinLoad){
console.log("loadPenToNewPen pinLoad:", pinLoad)
	for(pen of product_data.parameters.laser){
		if(pen["pinLoad"] == pinLoad){
			// First we empty the new pen configuration
			$("#new-pin-description").val("")
			$("#new-pin-pinLoad").val("")
			$("#new-pin-vitesse").val("");
			$("#new-pin-frequence").val("");
			$("#new-pin-mark_delay").val("");
			$("#new-pin-n_jump_lines").val("");
			$("#new-pin-nbpassage").val("");
			$("#new-pin-puissance").val("");
			$("#new-pin-hatch").val("");
			$("#new-pin-hatch_angle").val("");
			$("#new-pin-protection_mode_pen").val("");
			$("#new-pin-wait_time").val("");
			$("#new-pin-doublehatch").val(0);
			$("#new-pin-use_new_cnc_method").val("true")
			$("#new-pin-engraving_mode").val("hatching");
			// Now we overwrite the empty values with the selected PEN
			for(key in pen){
				var selector = "#new-pin-" + key;
				console.log("\tselector:", selector)
				console.log("\tpen[key]:", pen[key])
				$(selector).val(pen[key].toString())
			}
		}
	}
}


/**************************** GRID INIT ****************************/
// Displays the pattern on the canvas
function showPatternGrid(){
	// Generating values for GRID and filling table
	var grid = getGridValue();
	// Setting table height
	$('#grid-table').height($('#div-side-table').height());
	// Display grid on canvas
	writeTextGridInCanvas(grid);
}

// Returns a matrix of the values for the grid
function getGridValue(){
	console.log("getGridValue()")
	var id = 0;
	var color_list = []
	var nH = Number($('input[name="nH-grid"]').val());
	var nV = Number($('input[name="nV-grid"]').val());
	var vMin = Number($('input[name="h-min-val"]').val());
	var vMax = Number($('input[name="h-max-val"]').val());
	var hMin = Number($('input[name="v-min-val"]').val());
	var hMax = Number($('input[name="v-max-val"]').val());
	var hInterval = parseInt((hMax - hMin)/(nH - 1));
	var vInterval = parseInt((vMax - vMin)/(nV - 1));
	var grid = [];
	// Filling table with correct number of columns and rows
	var tableHtml = ""
	var hIntVal = hMin;
	for(var i = 0; i < nH; i++){
		var line = [];
		tableHtml += "<tr>";
		var vIntVal = vMin;
		for(var j = 0; j < nV; j++){
			var color = getColorFromIndex(id);
			color_list.push(color)
			line.push([vIntVal, hIntVal, color])
			// If we put a lower number the filter messes up
			id = id+1000;
			// Filling each grid with its value
			tableHtml += "<td>" + vIntVal + ":" + hIntVal + "</td>";
			vIntVal += vInterval;
		}
		tableHtml += "</tr>"
		grid.push(line);
		hIntVal += hInterval;
	}
	$('#grid-table').html(tableHtml)
	$('input[name="color_list"]').val(color_list);
	return grid;
}

// Returns a HEX color incremental with an index
function getColorFromIndex(id){
	var hex = id.toString(16);
	while(hex.length < 6){
		hex = "0" + hex;
	}
	hex = "#" + hex;
	return hex;
}

// Transforms a HEX color to RGBA
function hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return [(c>>16)&255, (c>>8)&255, c&255].join(',')+',255';
    }
    throw new Error('Bad Hex');
}

function writeTextGridInCanvas(grid){
	var text = $('#textForGrid').val();
	// Removing previous canvas textCanvas
    var canvas = document.getElementById("textCanvas");
    if(typeof canvas != "undefined" && canvas != null){
        var  c = canvas.getContext("2d");
        c.clearRect(0, 0, canvas.width, canvas.height);
    }
	// Removing previous canvas printCanvas
    var canvas = document.getElementById("printCanvas");
    if(typeof canvas != "undefined" && canvas != null){
        var  c = canvas.getContext("2d");
        c.clearRect(0, 0, canvas.width, canvas.height);
    }
    // Displaying all texts in grid
	var input_fontsize = $("input[id*=fontsize-0]").val();
	fontsize = obtainFontSizeGivenMM(vertical_rapport, input_fontsize);
	for(i in grid){
		for(j in grid[i]){
			// Calculate coordinates of each grid element
			pos = addPositionToGrid(j, i);
			//console.log("grid[i][j][2]:", grid[i][j][2]);
			writeTextInCanvas_forGrid(text, grid[i][j][2], pos[0], pos[1], fontsize);
			//writeTextInCanvas_forGrid(text, grid[i][j][2], j*10, i*10, fontsize);
		}
	}
}


function writeTextInCanvas_forGrid(textVal, color, xPos, yPos, fontsize){
	// Translating mm to pixel
	var xPosMmText = mmToPixelInCanvas(xPos, "textCanvas");
	var yPosMmText = mmToPixelInCanvas(yPos, "textCanvas");
	var xPosMmPrint = mmToPixelInCanvas(xPos, "printCanvas");
	var yPosMmPrint = mmToPixelInCanvas(yPos, "printCanvas");
	var font = $('#fixed-font').val();
    createImageForDisplay_grid("textCanvas", textVal, fontsize, xPosMmText, yPosMmText, color, font);
    var canvasFactor = document.getElementById("printCanvas").width / document.getElementById("textCanvas").width;
    createImageForDisplay_grid("printCanvas", textVal, fontsize*canvasFactor, xPosMmPrint, yPosMmPrint, color, font);
}


// Iterates the grid to add the right position in the canvas
function addPositionToGrid(i, j){
	// Getting center position of the grid and spacings
	var dV = Number($('input[name="dV-grid"]').val());
	var dH = Number($('input[name="dH-grid"]').val());
	var vCenter = Number($('input[name="vCenter"]').val());
	var hCenter = Number($('input[name="hCenter"]').val());
	var nH = Number($('input[name="nH-grid"]').val());
	var nV = Number($('input[name="nV-grid"]').val());
	// Height and width of the grid
	var h = (nH - 1)*dH;
	var v = (nV - 1)*dV;
	// Position of the i,j element
	var x = hCenter - v/2 + i*dV;
	var y = vCenter - h/2 + j*dH;
	return [x, y];
}



function createImageForDisplay_grid(canvasID ,text, myfontSize, xCenter, yCenter, color, engravingFontName=""){
    var canvas = document.getElementById(canvasID);
    if(typeof canvas != "undefined" && canvas != null){
        var  c = canvas.getContext("2d");
        if(engravingFontName){
            var desiredFont = myfontSize + 'px CustomEmojisFont, ' + engravingFontName + ', EmojisFont'
        } else {
	        var desiredFont = myfontSize + 'px CustomEmojisFont, EmojisFont'
        }
        c.font = desiredFont;
        c.textAlign = "center";
        if(typeof xCenter == "undefined" || Number.isNaN(xCenter)) xCenter = canvas.width/2 + canvas.width*recenterText();
        if(typeof yCenter == "undefined" || Number.isNaN(yCenter)) yCenter = canvas.height/2;
        var resText = text.split("\n");
        // Rotation angle of the text
        var angle = getRotationAngle(canvasID, text);
        var xDeviation = ((resText.length - 1) * myfontSize * getLineSeparationValue()) * Math.sin(angle);
        var yDeviation = ((resText.length - 1) * myfontSize * getLineSeparationValue()) * Math.cos(angle);
        var newXCenter = xCenter - xDeviation/2;
        var newYCenter = getNewYCenterValue(yCenter, yDeviation, canvasID);
        c.fillStyle = color;
        c.textBaseline = getTextBaseline();
        c.translate( newXCenter, newYCenter );
        // Rotation of the canvas according to the rotation of the text
        c.rotate( -angle);
        for (var i=0;i<resText.length;i++){
            c.fillText(resText[i], 0, 0 + i * myfontSize * getLineSeparationValue());
        }
        c.rotate( angle);
        c.translate( -newXCenter, -newYCenter );

        var img = canvas.toDataURL("image/png");
    }
}


function mmToPixelInCanvas(measure_received, canvasId){
	//var canvasSelector = "#" + canvasId;
	//var canvasWidth = $(canvasSelector).width();
	var canvas = document.getElementById(canvasId);
	var canvasWidth = canvas.width;

	var engravableArea = 60;
	var measure = measure_received * canvasWidth / engravableArea;
    return measure;
}

// Updates the table and dropdowns to display the correct laser parameters in each part
function updateLaserParameters(){
	var typeH = $('#typeH-grid').val();
	var typeV = $('#typeV-grid').val();
	// Marking readonly the fixed parameters on the table
	var hideTypeH = "#fixed-" + typeH;
	var hideTypeV = "#fixed-" + typeV;
	$('table.fixed-parameters input').prop("readonly", false);
	$(hideTypeH).prop("readonly", true);
	$(hideTypeV).prop("readonly", true);
	// Hiding/showing options in selects
	$('#typeH-grid option').show();
	$('#typeV-grid option').show();
	$('#typeH-grid option[value="' + typeV + '"]').hide();
	$('#typeV-grid option[value="' + typeH + '"]').hide();
}

// Transforms the grid used for display to the expected object to send to CNC generation
function prepareColorsToSendToCNC(){
	var counter = 1;
	var grid = getGridValue();
	// Object to send to generate CNC
	var colorAndPENConf = {};
	// Object to store temporary PEN configurations
	var tempPENConfig = []
	// Getting grid parameter types
	var gridParam1 = $("#typeH-grid").val();
	var gridParam2 = $("#typeV-grid").val();
	for(let row of grid){
		for(let el of row){
			// Adding color to object colorAndPENConf
			colorAndPENConf[hexToRgbA(el[2])] = [counter.toString()];
			// Adding PEN config to store later
			var obj = getNotGridLaserParams();
			obj[gridParam1] = el[0].toString();
			obj[gridParam2] = el[1].toString();
			obj["pinLoad"] = counter.toString();
			tempPENConfig.push(obj);
			counter++;
		}
	}
	console.log("tempPENConfig:", tempPENConfig)
	console.log("colorAndPENConf:", colorAndPENConf)
	var responseObj = {
		"colorAndPENConf": colorAndPENConf,
		"tempPENConfig": tempPENConfig
	}
	return responseObj;
}

// Returns the laser parameters to use that are not on the grid
function getNotGridLaserParams(){
	var inputs = $("#non-grid-parameters input:not([readonly])");
	var params = {};
	for(let input of inputs){
		params[input.id.replace("fixed-", "")] = input.value
	}
	return params;
}

// Sends the current laser parameters defined on the page to create an engraving file
function sendGridEngrave(){
	console.log("sendGridEngrave");
	showEngraveResults(true, false, false)
	var selectedFont = $(".fonts-container .btn.btn-info").attr("data-target");
	var font = $(selectedFont).find("input[id*=police-]").val()
	var text = $('#textToEngrave').val();
	var storeIdProduct = $("input[id=store-id-product]").val();
	var engravingComment = $("input[id=engraving-comment]").val();
	var objectWithCNCConf = prepareColorsToSendToCNC();
	// Now we overwrite the laser parameters of the product with the new GRID PENs
	product_data.parameters.laser = objectWithCNCConf["tempPENConfig"];
	if(text != "" || onlyStoreParams){
		// Global variable product_data
	    generateBase64img("printCanvas", "base64img_process");
	    var ajax_data = {
		    "product_data" : JSON.stringify(product_data),
		    "text": text,
		    "font": font,
			"colorAndPENConf": JSON.stringify(objectWithCNCConf["colorAndPENConf"]),
		    "img64": $('input[name="base64img_process"]').val()
		};
		if(storeIdProduct) ajax_data["storeIdProduct"] = storeIdProduct;
		if(engravingComment) ajax_data["engravingComment"] = engravingComment;
	    $.ajax({
	        type : 'POST',
	        url : "./engraveGridSafeOrder",
	        data : ajax_data,
	        beforeSend : function() {
	        },
	        success : function(data) {
	            console.log(data);
	            var ftpOk = data.resultLaser.result == "OK";
	            //var serverOk = data.resultServer.code == 200;
	            showEngraveResults(false, ftpOk, undefined);
	        },
	        error : function(request, status, error) {
	            console.log("engraveGridSafeOrder error");
	        }
	    });
	} else {
		alert("Text must not be empty")
	}
}

// Loads the select set of parameters in the modal as the engraving parameters
function confirmPreDefParamsSelected(){
	var selectedMaterial = $("input[name=selected_material]:checked").val();
	if(selectedMaterial){
		var grid_params_counter = 0;
		for(let param in predef_params_material[selectedMaterial]){
			if(predef_params_material[selectedMaterial][param]["activeGrid"]){
				if(grid_params_counter == 0){
					// Changing the selected horizontal option
					var selector = "#typeH-grid option[value=" + param + "]";
					$(selector).prop("selected", true);
					$("#typeH-grid").change();
					// Changing the min value
					$("#h-min-val").val(predef_params_material[selectedMaterial][param]["min"]);
					// Changing the max value
					$("#h-max-val").val(predef_params_material[selectedMaterial][param]["max"]);
				} else if(grid_params_counter == 1){
					// Changing the selected vertical option
					var selector = "#typeV-grid option[value=" + param + "]";
					$(selector).prop("selected", true);
					$("#typeV-grid").change();
					// Changing the min value
					$("#v-min-val").val(predef_params_material[selectedMaterial][param]["min"]);
					// Changing the max value
					$("#v-max-val").val(predef_params_material[selectedMaterial][param]["max"]);
				} else{
					alert("THE PREDEFINED PARAMETERS FOR THIS MATERIAL ARE INCORRECT, CHECK WITH ADMINISTRATOR")
				}
				grid_params_counter++;
			} else {
				var selector = "input[name=fixed-" + param + "]"
				$(selector).val(predef_params_material[selectedMaterial][param]["def"]);
			}
		}
		$('#load-modalPreDefParams').click();
	}
}
/**************************** GRID END ****************************/