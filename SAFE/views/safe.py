from flask import Blueprint, render_template, request, redirect, url_for, Response
import Common.SAFE.functions as safeApp
import Common.functions as app
import Common.ServerConnection.updateSystem as updateSystem
import Common.ServerConnection.Connection as server
import Common.LancerCommande as laser
import Common.LogsDWS as LOG
import web.views.dataManagement as DM
import json
from datetime import datetime


bp = Blueprint(__name__, __name__, template_folder='templates')


@bp.route('/safe0', methods=['POST', 'GET'])
def show0():
    return render_template('safe0.html')


@bp.route('/safe1', methods=['POST', 'GET'])
def show1():
    uc = safeApp.getUserInfo()
    if uc.get("user_is_configured"):
        products = safeApp.getAvailableProducts(uc.get("token"))
        if isinstance(products, list):
            return render_template('safe1.html', products=products)
        else:
            return "Your token is not valid. Maybe it has expired"
    else:
        return redirect(url_for('SAFE.views.safe.show0'))


@bp.route('/safe2', methods=['POST', 'GET'])
def show2():
    idProduct = request.args.get("idProduct")
    idBrand = request.args.get("idBrand")
    uc = safeApp.getUserInfo()
    if uc.get("user_is_configured"):
        # We dont need the second parameter "is_there_an_update" for SAFE
        product_data, is_there_an_update = updateSystem.getProductDataFromServer(idBrand, idProduct, uc.get("token"))
        prod_params = safeApp.getDisplayParametersProduct()
        prod_is_finished = True
        if "finished" in product_data:
            prod_is_finished = product_data["finished"]
        if "dimensions_orig" not in product_data["parameters"]:
            product_data["parameters"]["dimensions_orig"] = product_data["parameters"]["dimensions"]
        # Font line spacing path
        flsp = app.get_line_spacing_fonts()
        # List of available fonts
        af = safeApp.getAvailableFonts()
        if "parameters" not in product_data:
            return "This product is not correctly defined on the server. Or your token has expired"
        return render_template('safe2.html', idBrand=idBrand, idProduct=idProduct, product_data=product_data,
                               help_content=safeApp.return_help(),
                               prod_params=prod_params, flsp=flsp, prod_is_finished=prod_is_finished, af=af)
    else:
        return redirect(url_for('SAFE.views.safe.show0'))


@bp.route('/safeGrid', methods=['POST', 'GET'])
def safeGrid():
    idProduct = request.args.get("idProduct")
    idBrand = request.args.get("idBrand")
    uc = safeApp.getUserInfo()
    if uc.get("user_is_configured"):
        # We dont need the second parameter "is_there_an_update" for SAFE
        product_data, is_there_an_update = updateSystem.getProductDataFromServer(idBrand, idProduct, uc.get("token"))
        prod_params = safeApp.getDisplayParametersProduct()
        prod_is_finished = True
        if "finished" in product_data:
            prod_is_finished = product_data["finished"]
        if "dimensions_orig" not in product_data["parameters"]:
            product_data["parameters"]["dimensions_orig"] = product_data["parameters"]["dimensions"]
        # Font line spacing path
        flsp = app.get_line_spacing_fonts()
        # List of available fonts
        af = safeApp.getAvailableFonts()
        if "parameters" not in product_data:
            return "This product is not correctly defined on the server. Or your token has expired"
        return render_template('safeGrid.html', idBrand=idBrand, idProduct=idProduct, product_data=product_data,
                               pdlpg=safeApp.predefind_laser_parameters_grid(),
                               prod_params=prod_params, flsp=flsp, prod_is_finished=prod_is_finished, af=af)
    else:
        return redirect(url_for('SAFE.views.safe.show0'))

@bp.route('/validateToken', methods=['POST', 'GET'])
def validateToken():
    token = request.form.get('token')
    res = safeApp.validateToken(token)
    return Response(json.dumps(res), mimetype='application/json')



@bp.route('/storeProdConf', methods=['POST', 'GET'])
def storeProdConf():
    product_data = json.loads(request.form.get('product_data'))
    LOG.info("Saving product:" + str(product_data))
    updateSystem.storeDownloadedProduct(product_data)
    return "OK"


@bp.route('/engraveSAFEOrder', methods=['POST', 'GET'])
def engraveSAFEOrder():
    # Input parameters
    product_data = json.loads(request.form.get('product_data'))
    pinMode = request.form.get('pinMode')
    color = request.form.get('color')
    redRectangle = request.form.get('redRectangle')
    base64img = str(request.form.get('img64')).replace("data:image/png;base64,", "")
    # First: we store the product configuration in local
    LOG.info("Saving product:" + str(product_data))
    updateSystem.storeDownloadedProduct(product_data)
    # Second: we set this new modified project as current project for "ALYCE"
    temp_prod = app.getConf("use_temp_prod_safe") == "True"
    if temp_prod:
        folder_name = app.getConf("temp_prod_id")
    else:
        folder_name = product_data["idProduct"]
    app.setCurrentProduct(folder_name)
    # Third: we process the image to transform it to UNF
    # We remove old files in the order folder
    app.remove_order_files()
    DM.process_image_thread(base64img, pinMode, color)
    # Update last time there was an engraving
    app.update_last_engraving_date()
    # Fourth: we send the UNF to engrave
    if redRectangle == "true":
        print("................ redRectangle")
        return DM.showRedRectangle()
    else:
        resultLaser = laser.init()
        full_res = {
            "resultLaser": resultLaser
        }
        LOG.info("SAFE full_res: " + str(full_res))
        return Response(json.dumps(full_res), mimetype='application/json')


@bp.route('/storeSAFEOrder', methods=['POST', 'GET'])
def storeSAFEOrder():
    # Input parameters
    product_data = json.loads(request.form.get('product_data'))
    text = request.form.get('text')
    font = request.form.get('font')
    engravingComment = request.form.get('engravingComment')
    storeIdProduct = request.form.get('storeIdProduct')
    current_day = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    engravingTime = app.getOrderTempVariables("engravingTime")
    uc = safeApp.getUserInfo()
    username = uc.get("uniqueName")
    resultServer = server.addSAFEEngraving(product_data, username, text, engravingTime, current_day, font,
                                           engravingComment, storeIdProduct)
    LOG.info("SAFE resultServer: " + str(resultServer))
    status_code = resultServer.status_code
    full_res = {}
    try:
        full_res = {
            "resultServer": {
                "status": status_code
            }
        }
        LOG.info("SAFE full_res: " + str(full_res))
    except Exception:
        import traceback
        errorMsg = "SAFE resultServer.status_code not existing"
        LOG.error(errorMsg)
        LOG.error(str(traceback.format_exc()))
    return Response(json.dumps(full_res), mimetype='application/json')



@bp.route('/safeSendProdToProd', methods=['POST', 'GET'])
def safeSendProdToProd():
    LOG.info("safeSendProdToProd init")
    prod_id = request.form.get('prod_id')
    parametrizationFinished = request.form.get('parametrizationFinished') == "true"
    newUpdateAvailable = request.form.get('newUpdateAvailable') == "true"
    print("## parametrizationFinished:", parametrizationFinished)
    print("## newUpdateAvailable:", newUpdateAvailable)
    LOG.info("prod_id: " + str(prod_id))
    res = server.storeProdInDB(prod_id, True, "", parametrizationFinished, newUpdateAvailable)
    LOG.info("safeSendProdToProd result: " + str(res))
    return Response(json.dumps(res), mimetype='application/json')

@bp.route('/getIntermedios', methods=['POST', 'GET'])
def getIntermedios():
    product_data = json.loads(request.form.get('product_data'))
    temp_prod = app.getConf("use_temp_prod_safe") == "True"
    if temp_prod:
        folder_name = app.getConf("temp_prod_id")
    else:
        folder_name = product_data["idProduct"]
    fullpathimg = app.get_products_folder_path() + '/' + folder_name + "/Pictures/default.png"
    inputs, intermedios, outputs = app.calculate_data(fullpathimg, product_data, True)
    return Response(json.dumps(intermedios), mimetype='application/json')


# ---------------------------------------- INIT GRID ---------------------------------------- #
# grid process the image once New Engraving Methods is finished -->
# https://drive.google.com/drive/folders/1l-6lrsCvJszpdsUSyebG58c_rXrP4qYC
@bp.route('/engraveGridSafeOrder', methods=['POST', 'GET'])
def engraveGridSafeOrder():
    # Input parameters
    product_data = json.loads(request.form.get('product_data'))
    redRectangle = request.form.get('redRectangle')
    colorAndPENConf = request.form.get('colorAndPENConf')
    base64img = str(request.form.get('img64')).replace("data:image/png;base64,", "")
    # First: we store the product configuration in local
    LOG.info("Saving product:" + str(product_data))
    updateSystem.storeDownloadedProduct(product_data)
    # Second: we set this new modified project as current project for "ALYCE"
    temp_prod = app.getConf("use_temp_prod_safe") == "True"
    if temp_prod:
        folder_name = app.getConf("temp_prod_id")
    else:
        folder_name = product_data["idProduct"]
    app.setCurrentProduct(folder_name)
    # Third: we process the image to transform it to UNF
    # We remove old files in the order folder
    app.remove_order_files()
    DM.process_image_thread(base64img, colorAndPENConf=colorAndPENConf)
    # Update last time there was an engraving
    app.update_last_engraving_date()
    # Fourth: we send the UNF to engrave
    if redRectangle == "true":
        print("................ redRectangle")
        return DM.showRedRectangle()
    else:
        resultLaser = laser.init()
        full_res = {
            "resultLaser": resultLaser
        }
        LOG.info("SAFE full_res: " + str(full_res))
        return Response(json.dumps(full_res), mimetype='application/json')

# ---------------------------------------- END GRID ---------------------------------------- #
